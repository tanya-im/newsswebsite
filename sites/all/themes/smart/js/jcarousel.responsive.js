(function($) {
    $(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var width = jcarousel.innerWidth();
				//alert(width);
                if (width >= 855) {
                    width = width / 4;
                } 
				else if (width >= 600) {
                    width = width / 3;
                }
				else if (width >= 480) {
                    width = width / 2;
                }
				else if (width >= 320) {
                    width = width / 1;
                }

                //jcarousel.jcarousel('items').css('width', width + 'px');
				jcarousel.jcarousel('items').css('width', width + 'px');    
				var element_width = $("#jc1 li").width();               
				$("#jc2 li").each(function(){                   
					$(this).css("width",element_width);                                 
				});
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);
