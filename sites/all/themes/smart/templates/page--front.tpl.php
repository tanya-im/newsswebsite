    <!--header section starts-->
    <header class="clearfix">
        <div class="col-lg-12 headertop">
            <div class="container_site clearfix">
                <div class="col-xs-6"> 
                  <?php if ($page['top_header_strip']): ?>
                      <?php print render($page['top_header_strip']); ?>
                  <?php endif; ?>
                </div>
                <div class="col-xs-6 text-right">
                    <ul class="toplinks_site"> 
                        <?php if ($page['login_popup']): ?>
                            <li><?php print render($page['login_popup']); ?></li>
                        <?php endif; ?>
                        <?php if($user->uid):?>
                         <li><?php print l("Dashboard","dashboard"); ?></li>
                         <li><?php print l("Logout","user/logout"); ?></li>
                        <?php endif;?>
                         
                        <?php if(!$user->uid):?>
                        <li><a href="#" title="Register">Register</a></li>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
        </div>
        <!--headertop-->
         
        <div class="col-lg-12">
            <div class="container_site padtop10 padbtm10 clearfix">
               
                <?php if ($logo): ?>
                  <div class="col-xs-3">
                    <a class="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                      <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                    </a>
                  </div>
                <?php endif; ?>  

                  <div class="col-xs-9 nogutter">
                      <?php print render($page['custom_menu']); ?> 
                  </div> <!-- /. nogutterright custom_menu --> 
            </div>
        </div>
        <?php print render($page['header']); ?> 
    </header>   
    <!--header section ends-->

    <?php if ($messages): ?>
      <div id="messages"><div class="section clearfix">
        <?php print $messages; ?>
      </div></div> <!-- /.section, /#messages -->
    <?php endif; ?> 
    
    <!--Employee/Employer section starts-->
    <section class="col-lg-12 category_site clearfix">
        <h1>Which option best describes you?</h1>
        <div class="container_site padtop50 clearfix">
            <div class="col-md-6">
                <button type="button" class="btn btn-transparent bounceIn animated">I'm an employee</button>
            </div>
            <div class="col-md-6">
                <button type="button" class="btn btn-transparent bounceIn animated">I'm an employer</button>
            </div>
        </div>
      <!--category_site--> 
    </section>
    <!--Employee/Employer section ends--> 

    <!--Banner section starts-->
    <section class="col-lg-12 banner_site">
          <div class="container_site clearfix">
              <div class="col-sm-8 car_lease">
                  <?php if ($page['middle_section_row_column1']): ?>
                          <?php print render($page['middle_section_row_column1']); ?>
                  <?php endif; ?>
              </div> 
              <div class="col-sm-4">
                  <div class="col-lg-12">
                      <h1>Lease Calculator</h1>
                      <span>Choose the car you want</span>
                      <select class="selectpicker">
                        <option>Car Make</option>
                        <option>Car Make1</option>
                        <option>Car Make2</option>
                      </select>
                      <select class="selectpicker">
                        <option>Car Model</option>
                        <option>Car Model1</option>
                        <option>Car Model2</option>
                      </select>
                      <select class="selectpicker">
                        <option>Year</option>
                        <option>Year1</option>
                        <option>Year2</option>
                      </select>
                      <select class="selectpicker">
                        <option>Body Type</option>
                        <option>Body Type1</option>
                        <option>Body Type2</option>
                      </select>
                      <select class="selectpicker">
                        <option>Variant</option>
                        <option>Variant1</option>
                        <option>Variant2</option>
                      </select>
                      <a href="http://101.2.170.203/smartleasing" class="btn btn-orange btn-lg btn-block margintop7">Get a free quote now</a>
                    </div>
              </div>
          </div>
    </section>
    <!--Banner section ends-->

    <!--Product package carousel starts-->
    <section class="col-lg-12 greybg clearfix">
      <h1 class="text-center">Did you know you can also package the following products</h1>
      <div class="container_site clearfix carousel_site"> 
        <div class="jcarousel-wrapper">
          <div class="jcarousel" id="jc1">
            <ul>
              <li class="col-lg-12">
                <div class="col-xs-12">
                  <h1 class="circle_heading">
                    <p class="icon_tax"></p>
                  </h1>
                  <h2>Save tax by eating out</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, mauris convallis.</p>
                  <button type="button" class="btn btn-lg btn-block btn-blue">Find out more</button>
                </div>
              </li>
              <li class="col-lg-12">
                <div class="col-xs-12">
                  <h1 class="circle_heading">
                    <p class="icon_cart"></p>
                  </h1>
                  <h2>Save tax on your groceries</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, mauris convallis.</p>
                  <button type="button" class="btn btn-lg btn-block btn-blue">Find out more</button>
                </div>
              </li>
              <li class="col-lg-12">
                <div class="col-xs-12">
                  <h1 class="circle_heading">
                    <p class="icon_fund"></p>
                  </h1>
                  <h2>Boost your super fund</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, mauris convallis.</p>
                  <button type="button" class="btn btn-lg btn-block btn-blue">Find out more</button>
                </div>
              </li>
              <li class="col-lg-12">
                <div class="col-xs-12">
                  <h1 class="circle_heading">
                    <p class="icon_savings"></p>
                  </h1>
                  <h2>Tax savings on investments</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, mauris convallis.</p>
                  <button type="button" class="btn btn-lg btn-block btn-blue">Find out more</button>
                </div>
              </li>
              <li class="col-lg-12">
                <div class="col-xs-12">
                  <h1 class="circle_heading">
                    <p class="icon_fund"></p>
                  </h1>
                  <h2>Boost your super fund</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, mauris convallis.</p>
                  <button type="button" class="btn btn-lg btn-block btn-blue">Find out more</button>
                </div>
              </li>
              <li class="col-lg-12">
                <div class="col-xs-12">
                  <h1 class="circle_heading">
                    <p class="icon_savings"></p>
                  </h1>
                  <h2>Tax savings on investments</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, mauris convallis.</p>
                  <button type="button" class="btn btn-lg btn-block btn-blue">Find out more</button>
                </div>
              </li>
            </ul>
          </div>
          <a href="#" class="jcarousel-control-prev fa fa-chevron-left"></a>
          <a href="#" class="jcarousel-control-next fa fa-chevron-right"></a>
          <p class="jcarousel-pagination"></p>
        </div>
      </div>
    </section>
    <!--Product package carousel ends-->  

    <!--video and connect section starts-->
    <section class="col-lg-12 video_block clearfix">
      <div class="container_site">
        <div class="col-md-8">
          <h5 class="text-center">Need some mor information about salary packaging?</h5>
          <div class="col-lg-12 nogutter clearfix">
            <!--videos section desktop view-->
            <div class="video_nocarousel">
                <div class="col-sm-6 nogutterleft">
                  <div class="col-lg-12 nogutter">
                    <div class="video_site"><img src="<?php echo base_path().path_to_theme();?>/images/video_placeholder.jpg" /></div>
                    <h1>How does salary packaging work?</h1>
                    <p>This short video makes it easy</p>
                    <button class="btn btn_block_md btn-blue" type="button">Find out more</button>
                  </div>
                </div>
                <div class="col-sm-6 nogutterleft">
                  <div class="col-lg-12 nogutter">
                    <div class="video_site"><img src="<?php echo base_path().path_to_theme();?>/images/video_placeholder.jpg" /></div>
                    <h1>Salary packaging explained</h1>
                    <p>For hospitals, charities and research institutes</p>
                    <button class="btn btn_block_md btn-blue" type="button">Find out more</button>
                  </div>
                </div>
            </div>
            
            <!--videos section mobile view-->
            <div class="jcarousel-wrapper">
                <div class="jcarousel" id="jc2">
                    <ul>
                        <li class="col-lg-12">
                        <div class="col-xs-12 nogutter">
                        <div class="video_site"><img src="<?php echo base_path().path_to_theme();?>/images/video_placeholder.jpg" /></div>
                        <h1>How does salary packaging work?</h1>
                        <p>This short video makes it easy</p>
                        <button class="btn btn_block_md btn-blue" type="button">Find out more</button>
                        </div>
                        </li>
                        <li class="col-lg-12">
                        <div class="col-xs-12 nogutter">
                        <div class="video_site"><img src="<?php echo base_path().path_to_theme();?>/images/video_placeholder.jpg" /></div>
                        <h1>Salary packaging explained</h1>
                        <p>For hospitals, charities and research institutes</p>
                        <button class="btn btn_block_md btn-blue" type="button">Find out more</button>
                        </div>
                        </li>
                    </ul>
                </div>
                <a href="#" class="jcarousel-control-prev fa fa-chevron-left"></a>
                <a href="#" class="jcarousel-control-next fa fa-chevron-right"></a>
            </div>            
          </div>
        </div>
        <div class="col-md-4 nogutterleft block_fb">
          <h5 class="text-center">Connect with Smartsalary</h5>
          <div class="col-xs-12 nogutter"><img src="<?php echo base_path().path_to_theme();?>/images/image_fb.jpg" alt="Connect with Smartsalary" /></div>
        </div>
      </div>
    </section>
    <!--video and connect section ends-->   

    <!--smartsalary mobile view image section starts-->
    <section class="col-lg-12 greybg1 clearfix">
        <div class="container_site ss_mobile clearfix">
            <div class="col-sm-6">
                <?php if ($page['last_section_row_column1']): ?>
                          <?php print render($page['last_section_row_column1']); ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-6 ss_mobile_content">
                <h2>Salary Packaging made easy with Smartsalary</h2>
                <p class="padtop15">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ut ullamcorper ante, a vehicula massa. In porttitor sodales neque, nec iaculis dolor vestibulum eu. Morbi id mi metus. Aliquam tincidunt ornare fringilla. </p>
                <button type="button" class="btn btn_block_sm btn-blue margintop40">Find out more</button>
            </div>
        </div>
    </section>
    <!--smartsalary mobile view image section ends--> 
    <!--footer section starts-->
    <footer class="col-lg-12">
      <div class="container_site">
        <div class="col-lg-12"> 
          <!--footer first row starts-->
          <div class="footer_row1 clearfix"> 
            <!--desktop view-->
            <div class="footerlinks_desktop">
              <div class="col-sm-10 clearfix">
                <ul class="footerlinks_site clearfix">
                  <li>
                    <h6>Novated car leasing</h6>
                    <ul>
                      <li><a href="#" title="Save thousands on your next car">Save thousands on your next car</a></li>
                      <li><a href="#" title="Tax free running costs">Tax free running costs</a></li>
                      <li><a href="#" title="Save more with our buying power">Save more with our buying power</a></li>
                      <li><a href="#" title="Get a free quote now">Get a free quote now</a></li>
                    </ul>
                  </li>
                  <li>
                    <h6>Employee benefits</h6>
                    <ul>
                      <li><a href="#" title="Save up to 46.5% on laptops">Save up to 46.5% on laptops</a></li>
                      <li><a href="#" title="Meal entertainment card">Meal entertainment card</a></li>
                      <li><a href="#" title="Living expenses card">Living expenses card</a></li>
                      <li><a href="#" title="Investment loans">Investment loans</a></li>
                      <li><a href="#" title="Queensland Government">Queensland Government</a></li>
                    </ul>
                  </li>
                  <li>
                    <h6>Employer Solutions</h6>
                    <ul>
                      <li><a href="#" title="Tailored solutions">Tailored solutions</a></li>
                      <li><a href="#" title="No admin hassles">No admin hassles</a></li>
                      <li><a href="#" title="Fleet Management services">Fleet Management services</a></li>
                    </ul>
                  </li>
                  <li>
                    <h6>Have any questions?</h6>
                    <ul>
                      <li><a href="#" title="Am I eligible?">Am I eligible?</a></li>
                      <li><a href="#" title="How does salary packaging work?">How does salary packaging work?</a></li>
                      <li><a href="#" title="What can I package?">What can I package?</a></li>
                      <li><a href="#" title="Getting started">Getting started</a></li>
                    </ul>
                  </li>
                  <li>
                    <h6>What we are about?</h6>
                    <ul>
                      <li><a href="#" title="CEO's Blog">CEO's Blog</a></li>
                      <li><a href="#" title="About Smartsalary">About Smartsalary</a></li>
                      <li><a href="#" title="Executive team">Executive team</a></li>
                      <li><a href="#" title="Carbon offset program">Carbon offset program</a></li>
                      <li><a href="#" title="Contact us">Contact us</a></li>
                      <li><a href="#" title="Customer Service Charter">Customer Service Charter</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="col-sm-2 nogutter">
                <div class="img_award"><img src="<?php echo base_path().path_to_theme();?>/images/image_award.png" /></div>
              </div>
            </div>
            <!--tablet/mobile view-->
            <div class="footerlinks_tab">
              <div class="col-sm-6 nogutter panel-group" id="accordion">
                <ul>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <h6 class="panel-title">Novated car leasing</h6>
                      </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseOne" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="Save thousands on your next car">Save thousands on your next car</a></li>
                          <li><a href="#" title="Tax free running costs">Tax free running costs</a></li>
                          <li><a href="#" title="Save more with our buying power">Save more with our buying power</a></li>
                          <li><a href="#" title="Get a free quote now">Get a free quote now</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                          <h6 class="panel-title">Employee benefits</h6>
                      </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseTwo" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="Tailored solutions">Tailored solutions</a></li>
                          <li><a href="#" title="No admin hassles">No admin hassles</a></li>
                          <li><a href="#" title="Fleet Management services">Fleet Management services</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <h6 class="panel-title"> Employer Solutions</h6>
                      </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseThree" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="Tailored solutions">Tailored solutions</a></li>
                          <li><a href="#" title="No admin hassles">No admin hassles</a></li>
                          <li><a href="#" title="Fleet Management services">Fleet Management services</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                          <h6 class="panel-title">Have any questions?</h6>
                        </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseFour" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="Am I eligible?">Am I eligible?</a></li>
                          <li><a href="#" title="How does salary packaging work?">How does salary packaging work?</a></li>
                          <li><a href="#" title="What can I package?">What can I package?</a></li>
                          <li><a href="#" title="Getting started">Getting started</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                        <h6 class="panel-title">What we are about?</h6>
                      </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseFive" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="CEO's Blog">CEO's Blog</a></li>
                          <li><a href="#" title="About Smartsalary">About Smartsalary</a></li>
                          <li><a href="#" title="Executive team">Executive team</a></li>
                          <li><a href="#" title="Carbon offset program">Carbon offset program</a></li>
                          <li><a href="#" title="Contact us">Contact us</a></li>
                          <li><a href="#" title="Customer Service Charter">Customer Service Charter</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                </ul>
              </div>
              <div class=" col-sm-6 nogutterright block_fb"><img src="<?php echo base_path().path_to_theme();?>/images/image_fb.jpg" alt="Connect with Smartsalary" /></div>
            </div>
          </div>
          <!--footer first row ends--> 
          
          <!--footer second row starts-->
          <div class="footer_row2 clearfix">
            <div class="col-sm-9 padtop15 nogutter">
              <div class="copyright_site nogutterright">&#169; 2014 Smartsalary</div>
              <div class="footerlinks2_site nogutter">
                <ul>
                  <li><a href="#" title="Privacy Policy">Privacy Policy</a></li>
                  <li><a href="#" title="Website Terms of Use">Website Terms of Use</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3 nogutter clearfix"> <a href="#" class="logo_footer"></a> </div>
          </div>
          <!--footer second row ends--> 
        </div>
      </div>
    </footer>
    <!--footer section ends--> 
     
    <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
      <div id="footer-columns" class="clearfix">
        <?php print render($page['footer_firstcolumn']); ?>
        <?php print render($page['footer_secondcolumn']); ?>
        <?php print render($page['footer_thirdcolumn']); ?>
        <?php print render($page['footer_fourthcolumn']); ?>
      </div> <!-- /#footer-columns -->
    <?php endif; ?>

    <?php if ($page['footer']): ?>
      <div id="footer" class="clearfix">
        <?php print render($page['footer']); ?>
      </div> <!-- /#footer -->
    <?php endif; ?>
    <!--footer section ends-->

    <!--script for selectbox-->
     <script type="text/javascript"> 
       $(document).ready(function(){
          $('.selectpicker').selectpicker();
          var ath = addToHomescreen({
              skipFirstVisit: false,  // show at first access
              startDelay: 3,          // display the message right away
              maxDisplayCount: 0,      // do not obey the max display count
              displayPace: 0,         // do not obey the display pace
              maxDisplayCount: 0      // do not obey the max display count
          }); 
       }); 

      $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
      });

      // ADD SLIDEUP ANIMATION TO DROPDOWN //
      $('.dropdown').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
      });
      
      var carousel = $('#jc1');

      carousel.swipe({
          swipeLeft: function(event, direction, distance, duration, fingerCount) {   
              carousel.jcarousel('scroll', '+=1');
          },
          swipeRight: function(event, direction, distance, duration, fingerCount) {
              carousel.jcarousel('scroll', '-=1');
          }
      });
      var carousel2 = $('#jc2');

      carousel2.swipe({
          swipeLeft: function(event, direction, distance, duration, fingerCount) {   
              carousel2.jcarousel('scroll', '+=1');
          },
          swipeRight: function(event, direction, distance, duration, fingerCount) {
              carousel2.jcarousel('scroll', '-=1');
          }
      });
      
     </script>
 
