<?php 

$form['name']['#title']='';
$form['name']['#required']=0;
$form['name']['#attributes']['placeholder']='Enter your email address';
$form['pass']['#title']='';
$form['pass']['#required']=0;
$form['pass']['#attributes']['placeholder']='Enter your password';
$form['actions']['submit']['#attributes']=array('class'=>array('btn btn-orange btn-block'));
$form['actions']['submit']['#value']='Log In';
?>
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">Sign In</button>
<div class="collapse navbar-collapse nogutter col-xs-5" id="bs-example-navbar-collapse-2">
    <div class="triangle_wrap clearfix"><div class="triangle"></div></div>
    <div class="signin_content">
      	<?php print drupal_render($form['name']);	?>
        <?php print drupal_render($form['pass']);	?>
         
        <?php print drupal_render($form['remember_me']); ?>
         
        
        <!--input type="submit" value="Submit" class="btn btn-orange btn-block"-->
        <?php print drupal_render($form['form_build_id']);
	    print drupal_render($form['form_id']);
	     
	    print drupal_render($form['actions']);
	    ?>
    </div>
</div> 