<?php

/**
 * @file
 * Bartik's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<!--header section starts-->
    <header class="clearfix">
        <div class="col-lg-12 headertop">
            <div class="container_site clearfix">
                <div class="col-xs-6">
                  <h6>Get in touch <span class="call">1300 476 278</span></h6>
              </div>
                <div class="col-xs-6 text-right">
                    <ul class="toplinks_site">
                        <li>
                            <?php if ($page['login_popup']): ?>
                                <?php print render($page['login_popup']); ?>
                            <?php endif; ?>
                            <?php if($user->uid):?>
                             <?php print l("Logout","user/logout"); ?>
                            <?php endif;?>
                        </li>
                        <?php if(!$user->uid):?>
                        <li><a href="#" title="Register">Register</a></li>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
        </div>
         
        <div class="col-lg-12">
            <div class="container_site padtop10 padbtm10 clearfix">
               
                <?php if ($logo): ?>
                  <div class="col-xs-3">
                    <a class="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
                      <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                    </a>
                  </div>
                <?php endif; ?>  

                  <div class="col-xs-9 nogutter">
                      <?php print render($page['custom_menu']); ?> 
                  </div> <!-- /. nogutterright custom_menu --> 
            </div>
        </div>
        <?php print render($page['header']); ?> 
    </header>   
    <!--header section ends-->

    <?php if ($messages): ?>
      <div id="messages"><div class="section clearfix">
        <?php print $messages; ?>
      </div></div> <!-- /.section, /#messages -->
    <?php endif; ?>

    <?php if ($breadcrumb): ?>
      <!--div id="breadcrumb"><?php print $breadcrumb; ?></div-->
    <?php endif; ?> 
    
    <!--smartsalary page content section starts-->
    <div class="dashboard_banner"><img src="<?php echo base_path().path_to_theme();?>/images/banner_inner.jpg" /></div>
    <section class="col-lg-12 contentinner_site clearfix">
        <div class="container_site">
            <div class="dashboard_site">
				        <?php if ($title): ?>
                  <h1 class="title" id="page-title">
                    <?php print $title; ?>
                  </h1>
                <?php endif; ?>
                <?php 
			             $sessionUser=$_SESSION['user_packman'];			         
			          ?>
                <div class="clearfix">
                	<h4 class="col-sm-2">Name</h4>
                    <div class="col-sm-10"><?php echo $sessionUser['Title']." ".$sessionUser['FirstName']." ".$sessionUser['Surname'];?></div>
                </div>
                <div class="clearfix">
                	<h4 class="col-sm-2">Email</h4>
                    <div class="col-sm-10"><?php echo $sessionUser['Email'];?></div>
                </div>
                <div class="clearfix">
                	<h4 class="col-sm-2">Employer Name</h4>
                    <div class="col-sm-10"><?php echo $sessionUser['SelectedPackage']['EmployerName'];?></div>
                </div>
            </div> 
        </div>
    </section>
    <!--smartsalary page content section ends-->

    <!--footer section starts-->
    <footer class="col-lg-12">
      <div class="container_site">
        <div class="col-lg-12"> 
          <!--footer first row starts-->
          <div class="footer_row1 clearfix"> 
            <!--desktop view-->
            <div class="footerlinks_desktop">
              <div class="col-sm-10 clearfix">
                <ul class="footerlinks_site clearfix">
                  <li>
                    <h6>Novated car leasing</h6>
                    <ul>
                      <li><a href="#" title="Save thousands on your next car">Save thousands on your next car</a></li>
                      <li><a href="#" title="Tax free running costs">Tax free running costs</a></li>
                      <li><a href="#" title="Save more with our buying power">Save more with our buying power</a></li>
                      <li><a href="#" title="Get a free quote now">Get a free quote now</a></li>
                    </ul>
                  </li>
                  <li>
                    <h6>Employee benefits</h6>
                    <ul>
                      <li><a href="#" title="Save up to 46.5% on laptops">Save up to 46.5% on laptops</a></li>
                      <li><a href="#" title="Meal entertainment card">Meal entertainment card</a></li>
                      <li><a href="#" title="Living expenses card">Living expenses card</a></li>
                      <li><a href="#" title="Investment loans">Investment loans</a></li>
                      <li><a href="#" title="Queensland Government">Queensland Government</a></li>
                    </ul>
                  </li>
                  <li>
                    <h6>Employer Solutions</h6>
                    <ul>
                      <li><a href="#" title="Tailored solutions">Tailored solutions</a></li>
                      <li><a href="#" title="No admin hassles">No admin hassles</a></li>
                      <li><a href="#" title="Fleet Management services">Fleet Management services</a></li>
                    </ul>
                  </li>
                  <li>
                    <h6>Have any questions?</h6>
                    <ul>
                      <li><a href="#" title="Am I eligible?">Am I eligible?</a></li>
                      <li><a href="#" title="How does salary packaging work?">How does salary packaging work?</a></li>
                      <li><a href="#" title="What can I package?">What can I package?</a></li>
                      <li><a href="#" title="Getting started">Getting started</a></li>
                    </ul>
                  </li>
                  <li>
                    <h6>What we are about?</h6>
                    <ul>
                      <li><a href="#" title="CEO's Blog">CEO's Blog</a></li>
                      <li><a href="#" title="About Smartsalary">About Smartsalary</a></li>
                      <li><a href="#" title="Executive team">Executive team</a></li>
                      <li><a href="#" title="Carbon offset program">Carbon offset program</a></li>
                      <li><a href="#" title="Contact us">Contact us</a></li>
                      <li><a href="#" title="Customer Service Charter">Customer Service Charter</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="col-sm-2 nogutter">
                <div class="img_award"><img src="<?php echo base_path().path_to_theme();?>/images/image_award.png" /></div>
              </div>
            </div>
            <!--tablet/mobile view-->
            <div class="footerlinks_tab">
              <div class="col-sm-6 nogutter panel-group" id="accordion">
                <ul>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <h6 class="panel-title">Novated car leasing</h6>
                      </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseOne" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="Save thousands on your next car">Save thousands on your next car</a></li>
                          <li><a href="#" title="Tax free running costs">Tax free running costs</a></li>
                          <li><a href="#" title="Save more with our buying power">Save more with our buying power</a></li>
                          <li><a href="#" title="Get a free quote now">Get a free quote now</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                          <h6 class="panel-title">Employee benefits</h6>
                      </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseTwo" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="Tailored solutions">Tailored solutions</a></li>
                          <li><a href="#" title="No admin hassles">No admin hassles</a></li>
                          <li><a href="#" title="Fleet Management services">Fleet Management services</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <h6 class="panel-title"> Employer Solutions</h6>
                      </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseThree" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="Tailored solutions">Tailored solutions</a></li>
                          <li><a href="#" title="No admin hassles">No admin hassles</a></li>
                          <li><a href="#" title="Fleet Management services">Fleet Management services</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                          <h6 class="panel-title">Have any questions?</h6>
                        </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseFour" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="Am I eligible?">Am I eligible?</a></li>
                          <li><a href="#" title="How does salary packaging work?">How does salary packaging work?</a></li>
                          <li><a href="#" title="What can I package?">What can I package?</a></li>
                          <li><a href="#" title="Getting started">Getting started</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                  <li class="panel panel-default">
                    <div class="panel-heading">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                        <h6 class="panel-title">What we are about?</h6>
                      </a>
                    </div>
                    <!--panel-heading-->
                    <div id="collapseFive" class="panel-collapse collapse">
                      <div class="panel-body">
                        <ul>
                          <li><a href="#" title="CEO's Blog">CEO's Blog</a></li>
                          <li><a href="#" title="About Smartsalary">About Smartsalary</a></li>
                          <li><a href="#" title="Executive team">Executive team</a></li>
                          <li><a href="#" title="Carbon offset program">Carbon offset program</a></li>
                          <li><a href="#" title="Contact us">Contact us</a></li>
                          <li><a href="#" title="Customer Service Charter">Customer Service Charter</a></li>
                        </ul>
                      </div>
                      <!--panel-body--> 
                    </div>
                    <!--panel-collapse--> 
                  </li>
                </ul>
              </div>
              <div class=" col-sm-6 nogutterright block_fb"><img src="<?php echo base_path().path_to_theme();?>/images/image_fb.jpg" alt="Connect with Smartsalary" /></div>
            </div>
          </div>
          <!--footer first row ends--> 
          
          <!--footer second row starts-->
          <div class="footer_row2 clearfix">
            <div class="col-sm-9 padtop15 nogutter">
              <div class="copyright_site nogutterright">&#169; 2014 Smartsalary</div>
              <div class="footerlinks2_site nogutter">
                <ul>
                  <li><a href="#" title="Privacy Policy">Privacy Policy</a></li>
                  <li><a href="#" title="Website Terms of Use">Website Terms of Use</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3 nogutter clearfix"> <a href="#" class="logo_footer"></a> </div>
          </div>
          <!--footer second row ends--> 
        </div>
      </div>
    </footer>
    <!--footer section ends--> 
    <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
      <div id="footer-columns" class="clearfix">
        <?php print render($page['footer_firstcolumn']); ?>
        <?php print render($page['footer_secondcolumn']); ?>
        <?php print render($page['footer_thirdcolumn']); ?>
        <?php print render($page['footer_fourthcolumn']); ?>
      </div> <!-- /#footer-columns -->
    <?php endif; ?>

    <?php if ($page['footer']): ?>
      <div id="footer" class="clearfix">
        <?php print render($page['footer']); ?>
      </div> <!-- /#footer -->
    <?php endif; ?>
    <!--footer section ends-->
