﻿/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

//<debug>
Ext.Loader.setPath({
    'Ext': 'touch/src'
}).setConfig({disableCaching: false});
//</debug>

Ext.application({
    name: 'SmartSalary',
    /*viewport : {
        autoMaximize : true
    },*/
    requires: [
    	'SmartSalary.config.config',
		'SmartSalary.config.Messages',
		'SmartSalary.config.MenuConfig',
		'Ext.device.Connection',
		'Ext.device.Communicator',
		'Ext.device.connection.Sencha',
		'Ext.MessageBox',
		'Ext.Img',
        'Ext.Label',
        'Ext.field.Password',
		'Ext.form.FieldSet',
		'Ext.field.Radio',
		'Ext.field.Hidden',
		'Ext.field.Number',
		'Ext.field.Select',
		'Ext.field.Email',
		'Ext.plugin.ListPaging',
		'Ext.ProgressIndicator' ,
		'SmartSalary.lib.Logger',
		'SmartSalary.lib.Shared',
        'SmartSalary.lib.ClaimForm',
		'SmartSalary.Calendar.TouchCalendarSimpleEvents.TouchCalendarView',
		'SmartSalary.Calendar.TouchCalendarSimpleEvents.TouchCalendarSimpleEvents',
        'SmartSalary.store.SlideList_S',
		'Ext.NavigationView',
		'Ext.draw.gradient.Linear'
    ],
	views: ['Calendar.customCalendar','CapturePicture.CapturePicture'],
    controllers:[
    	//'Login_C',
        //'Dashboard_C',
	    'ContentDetail_C',
		'HeaderFooter_C',
        'ClaimList_C',
	    'EmployerList_C',
	    'SwipeContainerView_C',
	    'SearchList_C',
		'VehicleRegistrationList_C',
		'VehicleRunningExpenseClaim_C',
        'CappedClaim_C',
        'MealClaim_C',
        'VehicleLeasing_C',
		'PersonalDetail_C',
		'Superannuation_C'],
	stores:[
        'ClaimListFull_S',
		'User_S',
		'EmployerList_S',        
		'SlideList_S',
		'SearchList_S',
        'Claims_S',
		'VehicleRegistrationList_S',
		'Localdata_S',
		'Vehicle_S',
		'Dashboard_S',
		'DashboardTransaction_S',
		'SuperannuationSuperFund_S'],
	
	models:[
		'EmployerList_M',
        'ClaimList_M',
		'SlideList_M',
		'SearchList_M',
		'VehicleRegistrationList_M',
		'Localdata_M',
		'VehicleTransaction_M',
		'DashboardTransaction_M',
		'SuperannuationSuperFund_M'],
	
	profiles:[
		'Phone',
		'Tablet',
		'Desktop'	
	],
	isIconPrecomposed: true,
    launch: function() {
		if(Ext.os.is.Android){
			Ext.Viewport.on('painted',function(){
				Ext.Viewport.setHeight(window.innerHeight);
			});
			Ext.Viewport.on('autoMaximize',function(){
				Ext.Viewport.setHeight(window.innerHeight);
			});
		}
		if (Ext.browser.is.ChromeMobile || Ext.browser.is.Chrome) {			 
			window.onbeforeunload = function () {
				return "You will lose the unsaved changes you have made. Are you sure you want to navigate away from this page?";
			}
		} else {
			window.onpagehide = function () {
				alert("You are about to navigate away from this page. Unfortunately you will lose your unsaved data.");
			}
		};
		Ext.Msg.defaultAllowedConfig.showAnimation = false;
	}
});