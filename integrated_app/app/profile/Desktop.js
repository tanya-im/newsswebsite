Ext.define('SmartSalary.profile.Desktop', {
    extend: 'Ext.app.Profile',
    config:{
        views:[
        'AppBodyView_V',
        'SwipeContainerView_V',
        'HeaderFooter_V',
        'Login_V',
        'Dashboard_V',
        'ContentDetail_V',
        'EmployerList_V',
        'AccordionList',
        'AccordionListItem',
        'SearchList_V',
        'MenuDropdown_V',
        /********** Dashboard Transactions views ************/
        'DashboardTransactions_V',
        /********** track my claims views ************/
        'TrackMyClaims.ClaimList_V',
        'TrackMyClaims.ClaimDetail_V',
        'TrackMyClaims.ClaimReasonsList_V',
        'TrackMyClaims.ClaimData_V',
        /********** vehicle claim views ************/
        'VehicleClaim.VehicleRegistrationList_V',
        'VehicleClaim.VehicleRunningExpenseClaimStep1_V',
        'VehicleClaim.VehicleRunningExpenseClaimStep2_V',
        'VehicleClaim.VehicleRunningExpenseClaimStep3_V',
        'VehicleClaim.VehicleRunningExpenseClaimStep4_V',
        'VehicleClaim.VehicleRunningExpenseClaimStep5_V',
        /********** capped claim views ************/
        'CappedClaim.ExpenseTypeSelectionPage_V',
        'CappedClaim.ClaimFormPage_V',
        'CappedClaim.ClaimsListingPage_V',
        'CappedClaim.ConfirmationPage_V',
        'CappedClaim.SubmittedSummaryPage_V',
        /********** meal claim views ************/
        'MealClaim.ExpenseTypeSelectionPage_V',
        'MealClaim.ClaimFormPage_V',
        'MealClaim.ClaimsListingPage_V',
        'MealClaim.ConfirmationPage_V',
		'MealClaim.SubmittedSummaryPage_V',
		/********** vehicle leasing views ************/
        'VehicleLeasing.VehicleDetails_V',
		'VehicleLeasing.LeaseDetails_V',
		'VehicleLeasing.FBTDetails_V',
		'VehicleLeasing.ODOReading_V',
		'VehicleLeasing.ManageFuelCard_V',
		'VehicleLeasing.MyProducts_V',
		'VehicleLeasing.LeasingHistory_V',
		'VehicleLeasing.VehiclesExpenditureOverview_V',
		'VehicleLeasing.VehiclesExpenditureTransactions_V',
		'VehicleLeasing.VehiclesTransactionsDetail_V',
		'VehicleLeasing.ManageMyBudget_V',
		'VehicleLeasing.BudgetManagement_V',
		'VehicleLeasing.BudgetBreakdown_V',
		
		'VehicleLeasing.VehicleLeasingAvailableProducts_V',
		'VehicleLeasing.VehicleLeasingAvailableProductsDetail_V',
		'VehicleLeasing.VehicleLeasingAvailableProductsThankyou_V',
		'VehicleLeasing.VehicleLeasingAvailableProductsUserLogedInDetail_V',
		
		'VehicleLeasing.VehicleLeasingDaysUnavailable_V',
		'VehicleLeasing.VehicleLeasingDaysUnavailableAdd_V',
		'VehicleLeasing.VehicleLeasingDaysUnavailableConfirmScreen_V',
		
		'VehicleLeasing.VehicleLeasingLostOrStolenCards_V',
		'VehicleLeasing.VehicleLeasingLostOrStolenCardsWhatHappenOptions_V',
		'VehicleLeasing.VehicleLeasingLostOrStolenCardsAcceptedRequest_V',
		
		'VehicleLeasing.VehicleLeasingOrderNewCard_V',
		'VehicleLeasing.VehicleLeasingOrderNewCardThankYou_V',
		
		/*****Personal Detail View*****/
		'PersonalDetail_V'],
		
        controllers:[ 'Dashboard_C']
	},
    isActive:function(){
        return Ext.os.is.Desktop;
    },
    launch:function(){

        SmartSalary.app.getApplication().getController('desktop.Dashboard_C').initializeAppSetup();
        var objSlideView=Ext.create('SmartSalary.view.desktop.AppBodyView_V');
        Shared.objMainContainer=Ext.getCmp('homeContainer');
		Shared.objMainContainer.push([{xtype:'dashboardView_D'}]);
                
		// Initialize the Slide view
        Ext.Viewport.add(objSlideView); 
         
    }
});
