Ext.define('SmartSalary.store.ClaimList_S', {
	extend: 'Ext.data.Store',
        config:{
            storeId: "ClaimList",
            model: "SmartSalary.model.ClaimList_M",
            //autoLoad:true, 
            proxy:
            {
                type: 'ajax',
                url : '',
                reader: {
                    type: 'json', 
                    rootProperty:'result.ClaimOnlineList.ClaimOnline'
                }
            }    
        }
});