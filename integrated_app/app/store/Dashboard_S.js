Ext.define('SmartSalary.store.Dashboard_S', {
	extend: 'Ext.data.Store',
	config:{
      storeId: "DashboardStore",
      fields: [{
            name: 'account', type: 'auto' 
      },{
            name: 'selectedPackage', type: 'auto' 
      },{
            name: 'widgets', type: 'auto' 
      }], 
      proxy:
      {
        type: 'ajax',
        url :'',
        reader: {
          type: 'json' 
        }
      }
	}
});