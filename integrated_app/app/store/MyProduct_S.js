Ext.define('SmartSalary.store.MyProduct_S', {
	extend: 'Ext.data.Store',
	config:{
      storeId: "myProductsStore",
      fields: [{ 
              "vehicleProductId": 'string',
              "name": 'string',
              "description": 'string',
              "provider": 'string',
              "policyNumber": 'string',
              "startDate": 'string',
              "endDate": 'string',
              "status": 'string'
          }], 
      proxy:
      {
        type: 'ajax',
        url :'',
        reader: {
          type: 'json' 
        }
      }
	}
});