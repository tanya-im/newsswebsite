Ext.define('SmartSalary.store.EmployerList_S', {
	extend: 'Ext.data.Store',
	config:{
    storeId: "EmployerList",
	//autoLoad: false,
	model: "SmartSalary.model.EmployerList_M",
	
	proxy: {
           type: 'ajax',
           url : '',
		   reader: {
              type: 'json', 
              rootProperty:'Packages'
           }
	}
	}
});