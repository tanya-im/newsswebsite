Ext.define('SmartSalary.store.VehicleRegistrationList_S', {
	extend: 'Ext.data.Store',
	require:[
	'SmartSalary.model.VehicleRegistrationList_M'
	],
	config:{
    	storeId: "VehicleRegistrationList",
		autoLoad: false,
		model: "SmartSalary.model.VehicleRegistrationList_M",
	
		proxy: {
           type: 'ajax',
           //url: 'resources/services/vehicleExpressClaim.php',
		   reader: {
              type: 'json', 
              rootProperty:'result.Vehicle'
           }
		}
	}
});