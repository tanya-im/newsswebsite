Ext.define('SmartSalary.store.Product_S', {
	extend: 'Ext.data.Store',
	config:{
      storeId: "ProductsStore",
      fields: [{
            name: 'result', type: 'auto' 
      }], 
      proxy:
      {
        type: 'ajax',
        url :'',
        reader: {
          type: 'json' 
        }
      }
	}
});