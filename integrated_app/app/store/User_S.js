// User detail store.
Ext.define('SmartSalary.store.User_S', {
    extend: 'Ext.data.Store',
    config:{
        fields: [
            {name: 'AccountID',   type: 'int'},
            {name: 'FirstName', type: 'string'},
            {name: 'EmployerName', type: 'string'}, 
	        {name: 'EmployerCode', type: 'string'},
            {name: 'PackageId', type: 'int'},
            {name: 'SelectedPackage', type: 'auto'},
            {name: 'WorkPhone', type: 'string'},
            {name: 'Email', type: 'string'}
        ]
    }
});

