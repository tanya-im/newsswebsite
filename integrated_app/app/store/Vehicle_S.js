Ext.define('SmartSalary.store.Vehicle_S', {
	extend: 'Ext.data.Store',
	config:{
      storeId: "VehiclesStore",
      fields: [{
            name: 'result', type: 'auto' 
      }], 
      proxy:
      {
        type: 'ajax',
        url :'',
        reader: {
          type: 'json' 
        }
      }
	}
});