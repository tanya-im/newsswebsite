Ext.define('SmartSalary.store.ClaimListFull_S', {
	extend: 'Ext.data.Store',
	config:{
      storeId: "ClaimListFull",	 
      model: "SmartSalary.model.ClaimList_M",      	
    	proxy: 
      {
        type: 'ajax',
        url : '',
        reader: {
          type: 'json', 
          rootProperty:'result.ClaimOnlineList.ClaimOnline'
        }
      } 
	} 
});