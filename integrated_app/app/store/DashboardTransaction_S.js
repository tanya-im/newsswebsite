Ext.define('SmartSalary.store.DashboardTransaction_S', {
	extend: 'Ext.data.Store',
	require:['SmartSalary.model.DashboardTransaction_M'],
	config:{
    	storeId: "dashboardTransactionList",
		autoLoad: false,
		model: "SmartSalary.model.DashboardTransaction_M",
		data : [{amount: "1001",gst: "testoneGst",description:"dsc one",transactionDate:'03/09/14'},
        	{amount: "1202",gst: "testtwoGst",description:"dsc two",transactionDate:'05/09/14'},
        	{amount: "1303",gst: "testthreeGst",description:"dsc three",transactionDate:'06/09/14'},
        	{amount: "1404",gst: "testfourGst",description:"dsc four",transactionDate:'07/09/14'}]
	}
});