Ext.define('SmartSalary.store.SlideList_S', {
    extend: 'Ext.data.TreeStore',
    requires: [
        'SmartSalary.model.SlideList_M'
    ],
	config: {
    	storeId:'storeSlideList',
        defaultRootProperty: 'menus',
        model: 'SmartSalary.model.SlideList_M',
        autoLoad:false, 
        
    }

});
