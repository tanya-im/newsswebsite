Ext.define('SmartSalary.store.Localdata_S', {
	extend: 'Ext.data.Store',
	config:{
        storeId: "storeLocalData",
        model: "SmartSalary.model.Localdata_M"
	}
});