Ext.define('SmartSalary.store.SearchList_S', {
    extend: 'Ext.data.Store',
    requires: [
        'SmartSalary.model.SearchList_M'
    ],

    config: {
    	storeId:'storeSearchList',
        model: 'SmartSalary.model.SearchList_M',
		autoLoad:false,
        proxy: {
            type: 'ajax',
			url: '',
			reader: {
            	type: 'json',
            	rootProperty: 'searchresult'
			}
       }
    }

});
