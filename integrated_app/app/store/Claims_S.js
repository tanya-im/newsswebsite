Ext.define('SmartSalary.store.Claims_S', {
	extend: 'Ext.data.Store',
	config:{
      storeId: "ClaimsStore",
      fields: [{
            name: 'result', type: 'auto' 
      }], 
      proxy:
      {
        type: 'ajax',
        url :'',
        reader: {
          type: 'json' 
        }
      }
	}
});
