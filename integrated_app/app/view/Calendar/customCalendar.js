Ext.define('SmartSalary.view.Calendar.customCalendar', {	
	extend: 'Ext.Container',
	xtype: 'customCalendar',
	config:{
	items:[
	{
		xtype: 'container',
		cls:'customCalendarContainer',
		layout:'hbox',
	 	items:[
			{
				xtype:'label',
				flex:1,
				cls: 'calendar-icon-cust pad-5', 
			},
			{
				xtype: 'container',
				cls: 'pad-left-5',
				flex:7,
				items:[
				{
					xtype:'label',
					flex:2,
					itemId:'dayLabel', 
					cls: 'customCalendarDayColor font-14 pad-top-10',	
				},{
					xtype:'label',
					flex:2,
					itemId:'dateLabel',
					cls: 'font-13 f-bold font-helveticalight f-darkgrey text-capital'	
				}]
				 
			},{
				xtype: 'container',	 
				flex:1,
				cls: 'pad-5',
				items:[{
					xtype:'label', 
					cls: 'cal-arrow-right'
				}]
			}

		]
		
	},
	{
	    xtype:'touchcalenderview',
	    itemId:'calendarView',
	    height: "268px",
	    mode: 'month',
	    hidden:true,
	    value: new Date(),
	    plugins: [new SmartSalary.Calendar.TouchCalendarSimpleEvents.TouchCalendarSimpleEvents()]
	}]

},
setValues: function(date){
	this.getAt(1).setValue(date);
	this.getAt(1).refresh();
	
},
setMaxDate: function(date){
	this.getAt(1).maxDate=date;
	this.getAt(1).refresh();
	
}
});