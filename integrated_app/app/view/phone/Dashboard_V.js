// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.Dashboard_V', {
    extend: 'Ext.Container',
	requires: ['Ext.TitleBar'],
	alias: 'widget.dashboardView_P',
    config: {
		itemId:'dashboardView',
		id:'dashboardscreen',
		cls:'font-14 dashboardscreen',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
           	xtype: 'panel',
            title: 'Main Menu',
            layout: 'vbox',
			items: [{
				xtype: 'container',
				cls:'bggrey',
				layout:'hbox',
				id: 'toptool',
           		items:[{
					xtype:'container',
					id:'userdetail',
					itemId:'userdetail',
					items:[{
							xtype: 'label',
							itemId: 'wcuser',
							cls: 'wcuser font-white font-16'				
					},{
						xtype: 'label',
						itemId: 'acno',
						cls: 'font-828395'				
					},{
						xtype: 'label',
						itemId: 'employername',
						cls: 'font-828395'				
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-10 fontcolor_vl',
				items:[{
					xtype: 'container',
					itemId:'activitySummaryWidget',
					hidden:true,
					cls:'fa-vl bgwhite bordergrey_lt pad-10',
					items:[{
						xtype: 'label',						 
						itemId: 'activitySummary',
						cls:'activitySummary font-18 f-bold fontcolor_vl pad-top-10'	
					},{
						xtype: 'container',
						itemId:'activitySummaryFields',
						items:[]
					},{
						xtype: 'button',
						text: 'View Full Statement',
						itemId: 'viewFullStatementActivity',
						action: 'viewFullStatementActivity',
						cls:'button_grey_vl_2 claimStepButton font-16 pad-10 margin-top-10',	
					}]
				},{
					xtype: 'container',
					cls:'fa-vl margin-top-20 bgwhite bordergrey_lt pad-10',
					items:[{
						xtype: 'label',
						html: 'Notifications',
						cls: 'font-18 f-bold pad-2-0'	
					},{
						xtype: 'label',
						html: 'Due to the new superStream laws that were enforced, you will need to update your personal detail with youe TFN details. Please click here to update.',
						itemId: 'notificationsTxt',
						cls: '  pad-2-0'	
					}]
				},{
					xtype: 'container',
					itemId:'novatedleasingWidget',
					hidden:true,
					cls:'fa-vl margin-top-20 bgwhite bordergrey_lt pad-10',
					items:[{
						xtype: 'label',
						itemId:'NLTitle', 
						cls: 'font-18 f-bold'	
					},{
						xtype: 'container',
						cls :'bgwhite pad-bottom-10 pad-top-10',
						items:[{
							xtype: 'container',
							cls:'',
							items:[{
								xtype: 'fieldset',
								cls:'selectbox_vl_2 selectbox_vl',
								itemId:'selectVContainer' 
							}]
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'margin-top-10',
						items:[{
							xtype: 'label',
							flex:1,							 
							itemId: 'ntCurrentSpent', 
						},{
							xtype: 'label', 
							flex:1,
							itemId: 'ntBudget',
							cls: 'txt-right'	
						}]
					},{
						xtype: 'container',
						height:'40px',
						id:'novatedCarLeaseGraph',
						itemId:'novatedCarLeaseGraph',
						cls:'animationspac'
					},{
						xtype: 'label',
						itemId:'OdometerVL',
						cls: 'pad-top-10'	
					},{
						xtype: 'button',
						text: 'View Details',
						itemId: 'viewNovatedLeasing',
						action: 'viewNovatedLeasing',
						cls:'button_grey_vl_2 claimStepButton txt-left font-16 pad-10 margin-top-10',	
					}]
				},{
					xtype: 'container',
					cls:'txt-center fa-vl margin-top-20',
					items:[{
						xtype: 'container',
						cls:'border',
					},{
						xtype: 'container',
						html: '<h2>Your Packaged Products</h2>',
					}]
				},{
					xtype: 'container',
					itemId:'thresholdCapWidget',
					hidden:true,
					cls:'fa-vl bgwhite bordergrey_lt pad-10 margin-top-20',
					items:[{
						xtype: 'label',
						itemId:'titleTH',
						cls: 'font-18 f-bold'	
					},{
						xtype: 'label',
						itemId:'entitlementId',						
						cls: 'margin-top-10'	
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-top-10',
						items:[{
							xtype: 'label',
							flex:1, 
							itemId: 'thresholdCapSpent',
							cls: 'pad-2-0'	
						},{
							xtype: 'label',
							 
							flex:1,
							itemId: 'thresholdCapRemaining',
							cls: 'txt-right'	
						}]
					},{
						xtype: 'container',
						height:'40px',
						itemId:'thresholdGraph',
						cls:'animationspac'
					},{
						xtype: 'button',
						text: 'View Details',
						itemId: 'manageMyThresholdCapButton',
						action: 'manageMyThresholdCapButton',
						cls:'button_grey_vl_2 claimStepButton txt-left font-16 pad-10 margin-top-10',	
					}]
				},{
					xtype: 'container',
					itemId:'superannuationWidget',
					hidden:true,
					cls:'fa-vl bgwhite bordergrey_lt margin-top-20 pad-10',
					items:[{
						xtype: 'label',
						itemId: 'SuperannuationTitle',
						cls: 'font-18 f-bold'	
					},{
						xtype: 'container',
						layout:'hbox',
						itemId:'FinancialYearToDate',
						items:[{
							xtype: 'label',
							flex:2,
							html: '<span class="f-bold">Financial year to date :</span>',
							cls: 'margin-top-10'	
						},{
							xtype: 'label',
							flex:1,
							html: '$6773.00',
							cls: 'margin-top-10 txt-right'	
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						itemId:'nextContribution',
						items:[{
							xtype: 'label',
							flex:2,
							html: '<span class="f-bold">Next contribution :</span>',
						},{
							xtype: 'label',
							flex:1,
							html: '$6773.00',
							cls:'txt-right'
						}]
					},{
						xtype: 'button',
						text: 'View Details',
						itemId: 'superannuationButton',
						action: 'superannuationButton',
						cls:'button_grey_vl_2 claimStepButton txt-left margin-top-10 pad-10',	
					}]
				},{
					xtype: 'container',
					itemId:'livingExpenseWidget',
					hidden:true,
					cls:'fa-vl bgwhite bordergrey_lt margin-top-20 pad-10',
					items:[{
						xtype: 'label',
						itemId: 'titleLE',
						cls: 'font-18 f-bold'	
					},{
						xtype: 'container',
						layout:'hbox',
						items:[{
							xtype: 'label',
							itemId:'lblLE',
							flex:2,
							cls: 'margin-top-10'	
						},{
							xtype: 'label',
							flex:1,
							itemId:'valueLE',
							cls: 'margin-top-10 txt-right'	
						}]
					},{
						xtype: 'button',
						text: 'View Details',
						itemId: 'livingExpenseCardButton',
						action: 'livingExpenseCardButton',
						cls:'button_grey_vl_2 claimStepButton txt-left pad-10 margin-top-10',	
					}]
				},{
					xtype: 'container',
					itemId:'MEwidget',
					hidden:true,
					cls:'fa-vl margin-top-20 bgwhite bordergrey_lt pad-10',
					items:[{
						xtype: 'label',
						itemId:'titleME',
						cls: 'font-18 f-bold'	
					},{
						xtype: 'container',
						layout:'hbox',
						items:[{
							xtype: 'label',
							flex:2,
							itemId:'lblme', 
							cls: 'margin-top-10'	
						},{
							xtype: 'label',
							flex:1,
							itemId:'valme',
							cls: 'margin-top-10 txt-right'	
						}]
						},{
							xtype: 'button',
							text: 'View Details',
							itemId: 'mealsAndEntertainmentCardButton',
							action: 'mealsAndEntertainmentCardButton',
							cls:'button_grey_vl_2 claimStepButton txt-left pad-10 margin-top-10',	
						}]
					},
					{
						xtype: 'container',
						cls:'txt-center fa-vl margin-top-20',
						items:[{
							xtype: 'container',
							cls:'border',
						},{
							xtype: 'container',
							html: '<h2>Available Products</h2>',
						}]
					}, 
					{
						xtype: 'container',
						cls:'margin-top-20 fa-vl',
						items:[{
							//xtype: 'label',
							//html: 'Available Products',
							//cls: 'font-16'	
						},{
							xtype: 'button',
							text: 'Novated car Lease',
							itemId: 'novatedCarLease',
							action: 'novatedCarLease',
							cls:'button_white_vl claimStepButton fa fa-chevron-right',	
						},{
							xtype: 'button',
							text: 'Home Office Item',
							itemId: 'homeOfficeItem',
							action: 'homeOfficeItem',
							cls:'button_white_vl claimStepButton margin-top-10 fa fa-chevron-right',	
						},{
							xtype: 'button',
							text: 'Mortgage Repayment',
							itemId: 'mortgageRepayment',
							action: 'mortgageRepayment',
							cls:'button_white_vl claimStepButton margin-top-10 fa fa-chevron-right',	
						}]
					},{
						xtype: 'container',
						cls:'txt-center fa-vl margin-top-20',
						items:[{
							xtype: 'container',
							cls:'placeholder_car',
							html:'<img src="resources/images/placeholder_car.png">',
						}]
					}]
        	}]
		}]
    }
});