// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.TrackMyClaims.ClaimList_V', {
    extend: 'Ext.Container',
    alias : 'widget.claimList_P',    
    config: {
        id:"claimlistscreen", 
        itemId:"claimlistscreen",    
        scrollable : {
            direction : 'vertical',
            directionLock : true
        },         
        items: [{
			xtype: 'container',
			cls :'claimListheader',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Track My Claim',                        
					cls: 'font-white font-18 pad-5 txt-left'
				}]
			},{
				xtype:"button",
                itemId:"claimListBackBtn",
				id:'claimListBackBtn',
                flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			layout:'hbox',            
			cls :'claimListAdminMessage',
			hidden:true,
			items:[{
				xtype: 'panel',
				flex:2,
				items:[{
					xtype: 'label',
					id:'claimListMsgId',
					itemId:'claimListMsgId',
					cls: 'font-14 pad-5'
				}]
			}]
		},
		{
		xtype: 'panel', 
		width: '100%',
		cls:'claimListContainerFilters gray_box',                
		items: [{
			xtype: 'container',  
			layout:'hbox',
			items:[{
				xtype: 'label',
				html: 'Filter claims',
				cls:'fliterClaimLabel',
				flex:4                                 
			},{
				xtype: 'label',
				cls: 'arrow-up fliterClaimLabelToggleBtn',
				id:'toggleBtn',
				itemId:'toggleBtn',
				width:'12px',
				height:'12px',
				                      
			}],
			listeners : {
					element : 'element',
					tap : function (){
						SmartSalary.app.getApplication().getController('ClaimList_C'). onSlideInSlideOutRequest()
					}
				}
			},{
				xtype: 'container',
				type: 'card', 
				height: 250,                         
				id:'filterSection',
				itemId:'filterSection',
				items:[{
					xtype: 'container',  
					layout:'vbox',
					cls:'gray_box margin-top-16',
					docked : 'top', 
					items: [{
					xtype: 'selectfield',
						label: 'Type of claim:',
						cls:'fieldHeight pad-10',
						id:'selectClaimType',
						itemId:'selectClaimType',
						data: {"initdata":true},
						labelCls:'selectLabelCls',
						width:'100%',
						labelWidth:'40%' 
					},{
						xtype: 'selectfield',
						label: 'Status:',
						cls:'fieldHeight pad-10',
						labelCls:'selectLabelCls',
						data: {"initdata":true},
						id:'selectStatus',
						itemId:'selectStatus',
						width:'100%',
						labelWidth:'40%' 
					},{
						xtype: 'textfield',
						label: 'Date:',
						cls:'fieldHeight pad-10',
						labelCls:'selectLabelCls',
						data: {"initdata":true},
						placeHolder: 'DD/MM/YYYY',
						id:'selectDate',
						itemId:'selectDate',
						width:'100%',
						labelWidth:'40%' 
					}]
				},
				{
			xtype: 'panel',
			width: '100%',
			cls:'claimListSearchContainer',
			items: [{
				xtype: 'fieldset',
				title: 'Search by reference number',
				layout:'hbox',
				cls:'searchFieldset',
				items: [{
					xtype: 'textfield',                                
					//placeHolder :"Enter reference number", 
					name: 'query',
					flex:6,                                 
					id:'searchField',
					itemId:'searchField',
					cls:'fieldHeight',
					name:'searchField'                                  
				},{
					xtype: 'image',                                
					name: 'srchBtn',
					//src:'resources/images/search-icon.png',
					flex:1,
					cls:'searchBtn',
					itemId:'srchBtn',
					id:'srchBtn' 
			},{
					xtype: 'image',                                
					name: 'srchBtn',
					//src:'resources/images/icon_info@2x.png',
					flex:1,
					cls:'searchInfoBtn',
					itemId:'searchInfoBtn',
					id:'searchInfoBtn'
					
			}]
		}]
	}]
			}]
		},{
			xtype: 'container',                
			height:'400px',
			layout:'vbox',
			items:[{
				xtype:'list',
				flex:1,
				loadingText :null,
				id:'claimlisting_page',
				itemId:'claimlisting_page',                    
				cls:'claimListContainer pad-10',                    
				itemTpl:'<div class="listContainer_aro clearfix"><div class="listContainer listContainer_1"><div class="row1"><div class="claimId">{ClaimReferenceNumber}</div><div class="claimStatus ">{ExpenseStatus}</div><div class="clearboth"></div></div><div class="ClaimType">{ClaimType}</div><div class="date colorstyle1 font-12">Submitted: {DateSubmitted} </div><div class="listContainer_aro_right"></div></div></div>',                                       
				mode : 'SINGLE',
				listeners: {
					painted: function(comp,eOpts) {
						this.getScrollable().getScroller().on('scrollstart',function(){
							Ext.getCmp('claimlistscreen').setScrollable(false);
						});
						this.getScrollable().getScroller().on('scrollend',function(){
							Ext.getCmp('claimlistscreen').setScrollable(true);
						});
						this.getScrollable().getScroller().on('scroll',function(scroll,x,y,eOpts){
							if(y<0){
								Ext.getCmp('claimlistscreen').setScrollable(true);	
								Ext.getCmp('claimlistscreen').getScrollable().getScroller().scrollTo( 0, 0, true);
							}
						});
						
						
					}
				}   
			},{
				xtype:'list',
				flex:1,
				loadingText :null,
				hidden :true,
				store: 'ClaimListFull',
				id:'claimListingFilter',
				itemId:'claimListingFilter',
				emptyText: 'Claim(s) not found',
				cls:'claimListContainer pad-10',                    
				itemTpl:'<div class="listContainer"><div class="row1"><div class="claimId">{ClaimReferenceNumber}</div><div class="claimStatus ">{ExpenseStatus}</div><div class="clearboth"></div></div><div class="ClaimType">{ClaimType}</div><div class="date colorstyle1 font-12">Submitted: {DateSubmitted} </div><div class="listContainer_aro_right"></div></div>',		
				mode : 'SINGLE',
                listeners: {
					painted: function(comp,eOpts) {
						this.getScrollable().getScroller().on('scrollstart',function(){
							Ext.getCmp('claimlistscreen').setScrollable(false);
						});
						this.getScrollable().getScroller().on('scrollend',function(){
							Ext.getCmp('claimlistscreen').setScrollable(true);
						});
						this.getScrollable().getScroller().on('scroll',function(scroll,x,y,eOpts){
							if(y<0){
								Ext.getCmp('claimlistscreen').setScrollable(true);	
								Ext.getCmp('claimlistscreen').getScrollable().getScroller().scrollTo( 0, 0, true);
							}
						});
						
						
					}
				}   
			}]
		}] 
    }
 
});