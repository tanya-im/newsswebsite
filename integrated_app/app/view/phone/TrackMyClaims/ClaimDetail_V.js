// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.TrackMyClaims.ClaimDetail_V', {
    extend: 'Ext.Container',
	requires: ['Ext.TitleBar'],
	alias: 'widget.ClaimDetailView_P',
    config: {		 
		itemId:'ClaimDetailPageId',
		id :'ClaimDetailPageId',
		layout:'vbox',
		items: [{
                xtype: 'container',
                layout:'hbox',            
                cls :'claimListheader',
                items:[{
                    xtype: 'panel',
                    flex:4,
                    items:[{
                        xtype: 'toolbar',
						title: 'Track My Claim',                         
                        cls: 'font-white font-18 pad-5'
                    }]
                },{
                	xtype:"button",
                	itemId:"backBtn",
                	id:"backBtn",
                	flex:1,
                	html:'Back',
					cls: 'font-white searchlistbackbtn'
                }]
            },
            {
                xtype: 'container',
                layout:'hbox',            
                cls:'vehicleNumberHeader', 
                items:[
                {
                    xtype: 'panel',                     
                    items:[
                    {
                        xtype: 'label',
                        itemId:'VehicleNumber',                                                 
                        cls: 'font-16 pad-5'
                    }]
                }]
            },             
            {
                    xtype:'list',
                    flex:6,
		            id:'claimdetailList_page',
                    itemId:'claimdetailList_page',
                    emptyText:'Claims not found!',
                    cls:'claimListContainer pad-10',                    
                    itemTpl:new Ext.XTemplate('<div class="listContainer margin-10">',
                    '<div class="claimTypeAndRefNum">{claimTypeAndRefNum}</div>',
                    '<div class="rowDate">',
                      	'<div class="text">Date Paid: {ExpenseDate}</div>',
                      	'<div class="date">{ExpenseStatus}</div>',
                      	'<div class="clearboth"></div>',
                    '</div>',
                    '<div class="rowClaimed">',
                      	'<div class="text">Claimed:</div>',
                      	'<div class="value">${Amount}</div>',
                      	'<div class="clearboth"></div>',
                    '</div>',
                    '<div class="rowClaimed">',
	                  	'<div class="text">To be Paid:</div>',
	                  	'<div class="value">${AmountToBePaid}</div>',
	                  	'<div class="clearboth"></div>',
	                '</div>',
	                '<div class="rowClaimed blueText">',
                      	'<div class="text">Total Amount:</div>',
                      	'<div class="value">${AmountToBePaid}</div>',
                      	'<div class="clearboth"></div>',
                    '</div>',                    
                    '<div class="link"><span class="linkText">Detail</span><span class="w_arrow"></span><div class="clearboth"></div></div>',
                    '</div>'),

                    mode : 'SINGLE'  
            }] 
    }
});