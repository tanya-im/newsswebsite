// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.ODOReading_V', {
    extend: 'Ext.Container',
	alias: 'widget.oDOReading_P',
    config: {
		itemId :'oDOReadingPageId',
		id:'oDOReadingPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Odometer Reading',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToFBTDetails",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10',
			items:[{
				xtype: 'container',
				cls:'pad-right-10 margin-left-10',
				items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl_2 selectbox_vl',
							itemId:'selectVContainer',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType_1 vlabel',
								usePicker:false,
								id:'selectVehicleODOReading',
								itemId:'selectVehicleODOReading', 
								label: 'Vehicle :'  
							}]
						}]
			}]
		},
		{
			xtype: 'container',
			cls :'pad-10 font-14 color-2B2F3B',
			items:[
			{
				xtype:'container',
				itemId:'lastReadingDate',
				cls:'bgE7E7E7 pad-top-15 pad-left-10 pad-right-10 txt-center',
				html:'Last reading (on 20/10/2014)'
			},
			{
				xtype:'container',
				itemId:'lastReadingId',
				cls: 'txt-center f-bold font-16 bgE7E7E7 pad-top-20 pad-bottom-20',
				html:'22,258 Km'
			},
			{
				xtype: 'container',
				items:[{
					xtype: 'container',
					cls :'vborder',
					items:[{
						xtype: 'container',
						cls :'pad-10-0',
						items:[{
							xtype: 'label',
							itemId:'residualValue',
							html:"New odometer reading (Km)"
						}]	  
					},{
						xtype: 'container',
						items:[{
							xtype: 'numberfield',
							itemId: 'oDOReading',
							cls:'input_wrap_vl',
							tabIndex :1,
							name: 'oDOReading'
						}]
					}]
				}]
			}]
		},{
			xtype: 'container',
			cls :'margin-10 fa-vl',
			items:[
			{
				xtype: 'button',
				itemId: 'oDOReadingButton',
				ui: 'none',
				action: 'oDOReadingSubmitButtonPress',
				tabIndex :2,
				cls:'button_greyblue_vl claimStepButton txt-left font-16 pad-10',
				text: 'Submit'
			}]
		}]	
	}
});