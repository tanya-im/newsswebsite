// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.VehicleLeasingOrderNewCard_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingOrderNewCard_P',
    config: {
		itemId :'vehicleLeasingOrderNewCardPageId',
		id:'vehicleLeasingOrderNewCardPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Order a new card',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'vehicleLeasingOrderNewCardTopBackBtnPress',
				action:'vehicleLeasingOrderNewCardTopBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10',
			items:[{
				xtype: 'container',
				cls:'pad-right-10 margin-left-10',
				items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl_2 selectbox_vl',
							itemId:'selectVContainer',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType_1 vlabel',
								usePicker:false,
								id:'selectVehicleOrderNewCard',
								itemId:'selectVehicleOrderNewCard', 
								label: 'Vehicle :'  
							}]
						}]
			}]
		},
		{
			xtype: 'container',
			cls: 'pad-10 fa-vl',
			items: [{				
				xtype: 'container',
				cls :'font-normal font-14',
				items:[{
					xtype: 'label',
					html: '<p>Please confirm your personal details:</p>',
				},{
					xtype: 'container',
					items:[{
						xtype: 'container',
						layout:'hbox',
						cls:'m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Name:',
							flex:0.5
						},{
							xtype: 'label',
							itemId:'user_name',
							cls:'txt-left',
							flex:2
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Email:',
							flex:0.5
						},{
							xtype: 'label',
							itemId:'user_email',
							 
							cls:'txt-left',
							flex:2
						}]
					},
					{
						xtype: 'container',
						layout:'hbox',
						cls:'m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Phone:',
							flex:0.5
						},{
							xtype: 'label',
							itemId:'user_contact',
							 
							cls:'txt-left',
							flex:2
						}]
					}]	
				}]
			},{
				xtype: 'container',
				cls:'fa-vl pad-top-10',
				items: [{
					xtype: 'button',
					text: 'Submit',
					itemId: 'vehicleLeasingOrderNewCardSubmitBtnPress',
					cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				}]
			}]
		}]
	}
});