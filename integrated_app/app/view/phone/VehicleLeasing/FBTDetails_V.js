 // View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.FBTDetails_V', {
    extend: 'Ext.Container',
	alias: 'widget.fBTDetails_P',
    config: {
		itemId :'fBTDetailsPageId',
		id:'fBTDetailsPageId',
		cls :'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items:
		[{
			items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'FBT Details',
					cls: 'font-white font-16 pad-5'
				}]
			},{
				xtype:"button",
                action:"vehicleDetailBackBtnPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10',
			items:[{
				xtype: 'container',
				cls:'pad-right-10 margin-left-15',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl_2 selectbox_vl',
					itemId:'selectVContainer' 
				}]
			}]
		},{
			xtype: 'container',
			cls :'pad-15-20 bgE7E7E7 font-14 f-bold fontcolor_vl height100',
			items:[{
				xtype: 'container',
				cls :'vborder',
				items:[{
					xtype: 'container',
					cls :'pad-bottom-10',
					items:[{
						xtype: 'label',
						itemId:'kmStatusHeader',
						cls:'',
						
					}]	  
				},{
					xtype: 'container',
					itemId:'FBTCarAni',  			 
				},{
					xtype: 'container',
					cls :'bgwhite pad-6-10-7 margin-top-10 fa-vl',
					itemId:'residualValue',
					items:[{
						xtype: 'label', 
						cls:'fa fa-check f-bold f-0E9F80',
						html:"<div class='fontFFamilyHelvetica pad-left-36'>No FBT Liability</div>"		
					}]	
				},{
					xtype: 'container',
					cls :'pad-top-15',
					items:[{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-bottom-5 pad-bottom-7 font-14 fontcolor_vl',
						items:[{
							xtype: 'label',
							html: 'Travelled Kms:',
							flex:1
						},{
							xtype: 'label',
							itemId:'travelledKms',
							cls:'pad-right-5 txt-right f-bold',
							flex:1,
							html:"xxxxxx"
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-bottom-5 pad-bottom-7 font-14 fontcolor_vl',
						items:[{
							xtype: 'label',
							html: 'Remaining Kms:',
							flex:1
						},{
							xtype: 'label',
							itemId:'remainingKms',
							cls:'pad-right-5 txt-right f-bold',
							flex:1,
							html:"xxxxxx"
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-bottom-5 pad-bottom-7 font-14 fontcolor_vl',
						items:[{
							xtype: 'label',
							html: 'Average Kms Required:',
							flex:1
						},{
							xtype: 'label',
							itemId:'averageKmsRequired',
							cls:'pad-right-5 txt-right f-bold',
							flex:1,
							html:"xxxxxx"
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-bottom-5 pad-bottom-7 font-14 fontcolor_vl',
						items:[{
							xtype: 'label',
							html: 'Remaining Days:',
							flex:1
						},{
							xtype: 'label',
							itemId:'remainingDays',
							cls:'pad-right-5 txt-right f-bold',
							flex:1,
							html:"xxxxxx"
						}]
					}]	
				}]
			},{
				xtype: 'container',
				cls :'margin-top-10 fa-vl',
				items:[{
					xtype: 'button',
					itemId: 'myODOReadingButton',
					ui: 'none',
					action: 'myODOReadingButtonPress',
					cls:'button_white_vl fa fa-chevron-right claimStepButton txt-left font-16 pad-10 margin-top-7',
					text: 'My Odometer Reading - 41,059'
				}]
			},{
				xtype: 'container',
				cls :'margin-top-3 fa-vl',
				items:[{
					xtype: 'button',
					itemId: 'daysUnavailableButton',
					ui: 'none',
					action: 'daysUnavailableButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
					text: 'Days unavailable'
				}]
			}]
		}]
			}]
	}
});