// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.VehiclesExpenditureOverview_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehiclesExpenditureOverview_P',
    config: {
		itemId :'vehiclesExpenditureOverviewPageId',
		id:'vehiclesExpenditureOverviewPageId',
		cls:'container_vl font-14',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicles Expenditure Overview',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToDashboardPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10',
			items:[{
				xtype: 'container',
				cls:'pad-right-10 margin-left-10',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl_2 selectbox_vl',
					itemId:'selectVContainer',
					 
				}]
			}]
		},{
			xtype: 'container',
			cls :'pad-10 bgwhite fa-vl',
			items:[{
				xtype: 'button',
				itemId: 'viewFullBudgetBreakdownButton',
				ui: 'none',
				action: 'viewFullBudgetBreakdownButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10-10-0-0 margin-10-0',
				text: 'View full budget breakdown'
			}]
		},{
			xtype: 'container',
			cls :'pad-0-10-10-10 bgwhite',
			layout: 'hbox',
			items:[{
				xtype: 'container',
				flex:1,
				itemId:'currentFilterLabel',
				html: 'Last 12 Months',
				cls:'font-14 pad-top-20'
			},{
				xtype: 'container',
				flex:1,
				items:[{
					xtype: 'button',
					itemId: 'vEchangeDatesButton',
					ui: 'none',
					action: 'vEchangeDatesButtonPress',
					cls:'claimStepButton txt-left font-16 margin-top-7 changedates_dd_dn',
					text: 'Change Dates'
				}]
			}]
		},{
			xtype: 'container',
			cls :'bgwhite fa-vl',
			hidden:true,
			id:'vehicleExpenditureDateContainer',
			items:[{
				xtype: 'button',
				itemId: 'liveToDateButton',
				ui: 'none',
				action: 'vehicleExpenditureDateChangeButtonPress',
				cls:'button_ltgrey_vl txt-left font-16 pad-10 margin-top-7',
				text: 'Live To Date'
			},{
				xtype: 'button',
				itemId: 'last12monthsButton',
				ui: 'none',
				action: 'vehicleExpenditureDateChangeButtonPress',
				cls:'button_ltgrey_vl txt-left font-16 pad-10 margin-top-7',
				text: 'Last 12 Months'
			},{
				xtype: 'button',
				itemId: 'fBTYearButton',
				ui: 'none',
				action: 'vehicleExpenditureDateChangeButtonPress',
				cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
				text: 'FBT Year'
			},{
				xtype: 'button',
				itemId: 'vEcustomDateRangeButton',
				id:'vEcustomDateRangeButton',
				ui: 'none',
				action: 'vehicleExpenditureDateChangeButtonPress',
				cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
				text: 'Custom Date Range'
			}]
		},{
			xtype: 'container',
			cls :'pad-0-10-10-10 bgwhite fa-vl',
			id:'vehicleExpenditureDateRange',
			hidden:true,
			items:[{
				xtype: 'container',
				id:'vehiclesExpenditureFromCalendar',
				itemId:'vehiclesExpenditureFromCalendar'
			},{
				xtype: 'container',
				id:'vehiclesExpenditureToCalendar',
				itemId:'vehiclesExpenditureToCalendar'
			},{
				xtype: 'button',
				itemId: 'vehiclesExpenditureDatesButton',
				ui: 'none',
				action: 'vehiclesExpenditureDatesButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				text: 'Submit'
			}]
		},{
			xtype: 'container',
			itemId: 'expenses',
		}]
	}
});
 
