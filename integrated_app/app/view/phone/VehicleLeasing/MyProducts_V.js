// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.MyProducts_V', {
    extend: 'Ext.Container',
	alias: 'widget.myProducts_P',
    config: {
		itemId :'myProductsPageId',
		id:'myProductsPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'My Products',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToDashboardPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10',
			items:[{
				xtype: 'container',
				cls:'pad-right-10 margin-left-10',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl_2 selectbox_vl',
					itemId:'selectVContainer',
				}]
			}]
		},
		{
			xtype: 'container',
			itemId: 'myProductList',
			cls :'pad-10 bgE7E7E7 font-16 f-bold color-2B2F3B height100 vehicleDetail', 
		},{
			xtype: 'container',
			cls :'margin-top-10 bgwhite pad-10 fa-vl',
			items:[{
				xtype: 'button',
				itemId: 'backToManageMyBudgetsButton',
				ui: 'none',
				action: 'goToVehicleLeasingAvailableProducts',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				text: 'View Available Products'
			}]
		}]
	}
});