// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.ManageFuelCard_V', {
    extend: 'Ext.Container',
	alias: 'widget.manageFuelCard_P',
    config: {
		itemId :'manageFuelCardPageId',
		id:'manageFuelCardPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Manage Fuel Cards',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToDashboardPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			 
			 
			xtype: 'container',
			cls:'pad-10',
			items:[{
				xtype: 'fieldset',
				cls:'selectbox_vl',
				itemId:'selectVContainer',
				 
			}]
			 
		},{
			xtype: 'container',
			itemId:'manageFuelCardList',
			cls :'pad-10  bgE7E7E7 font-16 color-2B2F3B height100 vehicleDetail txt-center'
		},{
			xtype: 'container',
			cls :'margin-10 bgwhite fa-vl',
			items:[{
				xtype: 'button',
				itemId: 'reportLostStolenCardButton',
				ui: 'none',
				hidden:true,
				action: 'reportLostStolenCardButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-10-0',
				text: 'Report lost/stolen card'
			},{
				xtype: 'button',
				itemId: 'orderNewCardButton',
				ui: 'none', 
				action: 'orderNewCardButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-10',
				text: 'Order new card'
			}]
		}]
	}
});