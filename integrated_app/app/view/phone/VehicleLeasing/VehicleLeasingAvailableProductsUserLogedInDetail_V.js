// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.VehicleLeasingAvailableProductsUserLogedInDetail_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingAvailableProductsUserLogedInDetail_P',
    config: {
		itemId :'vehicleLeasingAvailableProductsUserLogedInDetailPageId',
		id:'vehicleLeasingAvailableProductsUserLogedInDetailPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'View Available Products',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'availableProductsUserDetailPageBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		
		{
			xtype: 'label',
			cls: 'f-bold font-14 pad-10',
			itemId:'cProductLable'
		},
		{	
			xtype: 'container',
			cls :'font-normal pad-0-10-10-10 font-14',
			items:[
			{
				xtype: 'label',
				html: '<p>Please confirm your personal details:</p>',
			},
			{
				xtype: 'container',
				items:[
				{		
					xtype: 'container',
					layout:'hbox',
					cls:'m-5-0 font-14 f-darkgrey',
					items:[{
						xtype: 'label',
						cls:'f-bold',
						html: 'Name:',
						flex:1.2
					},{
						xtype: 'label',
						itemId:'user_name',
						cls:'txt-left',
						html: 'Lorem Ipsum',
						flex:5
					}]
				},
				{
					xtype: 'container',
					layout:'hbox',
					cls:'m-5-0 font-14 f-darkgrey',
					items:[{
						xtype: 'label',
						cls:'f-bold',
						html: 'Email:',
						flex:1.2
					},{
						xtype: 'label',
						itemId:'user_email',
						html: 'Lorem Ipsum', 
						cls:'txt-left',
						flex:5
					}]
				},
				{
					xtype: 'container',
					layout:'hbox',
					cls:'m-5-0 font-14 f-darkgrey',
					items:[{
						xtype: 'label',
						cls:'f-bold',
						html: 'Phone:',
						flex:1.2
					},{
						xtype: 'label',
						itemId:'user_contact',
						html: 'Lorem Ipsum',
						cls:'txt-left',
						flex:5
					}]
				}]	
			}]
		
		},
		{
			xtype: 'container',
			flex: 1,
			cls:'fa-vl pad-10',
			items: [{
				xtype: 'button',
				itemId: 'availableProductsUserDetailPageSubmitBtnPress',
				cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				text: 'Submit',
			}]
		}]
	}
});