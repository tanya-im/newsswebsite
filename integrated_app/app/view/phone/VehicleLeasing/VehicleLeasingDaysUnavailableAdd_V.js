Ext.define('SmartSalary.view.phone.VehicleLeasing.VehicleLeasingDaysUnavailableAdd_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingDaysUnavailableAdd_P',
	config: {
		itemId :'vehicleLeasingDaysUnavailableAddPageId',
		id:'vehicleLeasingDaysUnavailableAddPageId',
		cls: 'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{	
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Days Unavailable',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'vehicleLeasingDaysUnavailableAddBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls: 'pad-10 font-14 color-2B2F3B',
			items: [{
				xtype: 'container',
				itemId:'vehicleDetailText'
			},{
				xtype: 'container',
				cls: 'pad-top-10 pad-bottom-10',
				items : [{
					xtype: 'container',
					items: [{
						xtype: 'container',
						id:'daysUnavailableDropOffCalendar',
						itemId:'daysUnavailableDropOffCalendar',
					}]
				},{
					xtype: 'container',
					items: [{
						xtype: 'container',
						id:'daysUnavailablePickUpCalendar',
						itemId:'daysUnavailablePickUpCalendar',
					}]
				}]
			},{
				xtype: 'container',
				items: [{
					xtype: 'label',
					html: 'Select a reason',
					cls:'margin-bottom-10',
				},{
					xtype: 'fieldset',
					cls:' selectbox_vl',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType',
						itemId:'reasonsDropDown',
						usePicker: false,
						cls:'selectbox_vl',
						width:'100%' 
					}]	
				}]
			},{
				xtype: 'container',
				cls:'bgd7 margin-top-10 phone-pad-bottom-1 noactive',
				items: [{
					xtype:'container',
					html:'Please provide substantiation (either take a photo of the document or scanned documents)',
					cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-10'
				},{							
					xtype:'container',
					cls:'document_container margin-10',
					items:[{									
						xtype:'container',
						id:'snap_image',
						itemId:'snap_image',
						cls:'snap_images',
					},{
					/* external library used for uploading and displaying the image */
						xtype:'capturepicture',
						name: 'capturefiles',
						cls:'capture_images captureImageVehicleLeasing',
						height:'100%',
						width:'100%'
					}]
				}]
			},{
				xtype: 'container',
				cls:'fa-vl',
				items: [{
					xtype: 'button',
					itemId: 'vehicleLeasingDaysUnavailableAddNextBtnPress',
					cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
					text: 'Next',
				}]
			}]
		}]
	}
});