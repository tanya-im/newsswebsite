// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.ManageMyBudget_V', {
    extend: 'Ext.Container',
	alias: 'widget.manageMyBudget_P',
    config: {
		itemId :'manageMyBudgetPageId',
		id:'manageMyBudgetPageId',
		cls:'container_vl',
		scrollable: {
		   	direction: 'vertical',
		   	directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Manage My Budget',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backManageMyBudget",
				flex:1,
                html:'Back',
				tabIndex:1,
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10',
			items:[{
				xtype: 'container',
				cls:'pad-right-10 margin-left-10',
				items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl_2 selectbox_vl',
							itemId:'selectVContainer',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType_1 vlabel',
								usePicker:false,
								id:'selectVehicleBudgetBreakdownId',
								itemId:'selectVehicleBudgetBreakdown', 
								label: 'Vehicle :'  
							}]
						}]
			}]
		}
		,{
			xtype: 'container',
			cls:'pad-15-10-0 fa-vl',
			items:[{
				xtype: 'label',
				cls:'txt-left pad-top-10 pad-bottom-10',
				html: 'Which budget would you like to manage?'
			},{
				xtype: 'container',
				itemId:'expenseList'
			}]
		}]
	}
});