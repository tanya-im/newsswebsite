// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.DaysUnavailable_V', {
    extend: 'Ext.Container',
	alias: 'widget.daysUnavailable_P',
    config: {
		itemId :'daysUnavailablePageId',
		id:'daysUnavailablePageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Days Unavailable',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToFBTDetails",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'bgwhite border-bottom',
			items:[{
				xtype: 'container',
				layout:'hbox',
				cls:'pad-10',
				items:[{
					xtype: 'label',
					cls:'pad-10 font-18',
					itemId:'vehilceNo',
					html: 'Vehicle:xxxx',
					flex:1
				},{
					xtype: 'fieldset',
					flex:1,
					cls:'selectbox_vl_2 selectbox_vl',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType',
						options: []
					}]
				}]
			}]
		},{
			xtype: 'container',
			cls :'bgE7E7E7 pad-10 font-14',
			items:[{
				xtype: 'container',
				items:[{
					xtype: 'container',
					cls :'vborder',
					items:[{
						xtype: 'container',
						cls :'pad-10-0',
						items:[{
							xtype: 'label',
							itemId:'residualValue',
							html:"Please enter the number of days unavailable"
						}]	  
					},{
						xtype: 'container',
						items:[{
							xtype: 'textfield',
							itemId: 'daysUnavailable',
							cls:'input_wrap_vl',
							name: 'daysUnavailable'
						}]
					}]
				}]
			}]
		},{
			xtype: 'container',
			cls :'margin-10',
			docked: 'bottom',
			items:[{
				xtype: 'button',
				itemId: 'daysUnavailableButton',
				ui: 'none',
				action: 'daysUnavailableButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-rightn txt-left font-16 pad-10',
				text: 'Submit'
			}]
		}]	
	}
});