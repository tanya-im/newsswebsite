// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleLeasing.VehicleLeasingDaysUnavailable_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingDaysUnavailable_P',
	cls:'container_vl',
	config: {
		itemId :'vehicleLeasingDaysUnavailablePageId',
		id:'vehicleLeasingDaysUnavailablePageId',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{	
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Days Unavailable',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'vehicleLeasingDaysUnavailableBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
			
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10',
			items:[{
				xtype: 'container',
				cls:'pad-right-10 margin-left-10',
				items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl_2 selectbox_vl',
							itemId:'selectVContainer',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType_1 vlabel',
								usePicker:false,
								id:'selectVehicleDaysUnavailable',
								itemId:'selectVehicleDaysUnavailable', 
								label: 'Vehicle :'  
							}]
						}]
			}]
		},
		{
			xtype: 'container',
			cls: 'pad-10',
			items: [
			{
				xtype: 'container',
				html: '<div class="tablestrc_wrap_vl tablestrc_wrap_1_vl"><div class="tablestrc_vl font-14"><span>From</span><span>To</span><span>Description</span><span>No. of doc</span></div></div>',
			},
			{
				xtype: 'dataview',
				itemId:'dataGridItemId',
				height: '143px', 
				cls:'font-14 tablestrc_wrap_vl', 
				itemTpl: '<div class="tablestrc_vl font-14"><span>{startDate}</span><span>{endDate}</span><span>{reason}</span><span>{numberOfDaysUnavailable}</span></div>',
				listeners: {
					painted: function(comp,eOpts) {
						this.getScrollable().getScroller().on('scrollstart',function(){
							Ext.getCmp('vehicleLeasingDaysUnavailablePageId').setScrollable(false);
						});
						this.getScrollable().getScroller().on('scrollend',function(){
							Ext.getCmp('vehicleLeasingDaysUnavailablePageId').setScrollable(true);
						});
					}
				}
			},
			{
				xtype: 'container',
				cls: 'text-align-justify pad-top-20 pad-bottom-10 font-14',
				html: '<p>A car is only unavailable for private use if it meets the criteria for one or more whole days. A whole day is defined  as midnight to midnight.<p></br><p>Note: Claiming days unavailable does not reduce the kilometres that you are required to travel. It only reduces your FBT liability if you do not meet your estimated kilometre bracket. </p></br><p>If you wish to claim days unavailable specify the drop off date and pick up date below, select the reason and click add. You may enter multiple periods during the FBT year (1 April to 31 March) if applicable.</p>'
			},{	
					xtype: 'container',
					cls:'fa-vl',
					items: [{
						xtype: 'button',
						itemId: 'vehicleLeasingDaysUnavailableAddBtnPress',
						cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
						text: 'Add',
					}]
			}]
		}]
	}
});