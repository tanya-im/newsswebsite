// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.VehicleClaim.VehicleRunningExpenseClaimStep1_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleRunningExpenseClaimStepOne_P',
    config: {
		itemId :'vehicleRunningExpenseClaimStepOnePageId',
		id:'vehicleRunningExpenseClaimStepOnePageId',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-left-10 green-bg-gredient',
			items:[
			{
				xtype: 'panel',
				items:[
				{
					xtype: 'toolbar',
					title: 'Vehicle Running Expense Claim',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},
		{
			xtype: 'container',
			cls :'bgE7E7E7 font-16 txt-center header_wrap_grey',
			layout: 'hbox',
			items:[
			{
				xtype: 'image',
				//src:'resources/images/SS04-Mobile-Claims-Vehicle-1.png',
				cls:'Claims-Vehicle-steps-img step1_img'
			}]
		},
		{
			xtype: 'container',
			cls :'pad-10 bgwhite border-bottom',
			items:[
			{
				xtype: 'label',
				id:'vehicleRegLabel',
				itemId:'vehicleRegLabel',
				cls: 'font-white font-16 pad-left-5 bluetxtwrap'
			}]
		},{
			xtype: 'container',
			cls :'bgwhite claim-type-txt border-bottom',
			items:[
			{
				xtype: 'label',
				html: '<h3>Type of reimbursement claim</h3>',
				cls: 'font-16 pad-left-5'
			},
			{
				xtype: 'label',
				html: '<p>Select an expense description below.</p>',
				cls: 'font-12 pad-left-5 colorstyle1'
			}]
		},
		{
			xtype: 'container',
			id:'vehicleRunningExpenseClaimTypeWrapper',
			cls:'bgwhite noactive',
			itemId: 'vehicleRunningExpenseClaimTypeWrapper'
		},
		{
			xtype: 'container',
			cls :'bgE7E7E7 font-16',
			items:[{
					xtype: 'button',
					itemId: 'claimStepOneNextButton',
					id: 'claimStepOneNextButton',
					ui: 'none',
					hidden:true,
					action: 'claimStepOneNextButtonPress',
					cls:'claimStepRightArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Skip'
			},{
					xtype: 'button',
					itemId: 'claimStepOneCancelButton',
					ui: 'none',
					action: 'claimStepOneCancelButtonPress',
					cls:'claimStepLeftArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Cancel'
			}]
		}]
	}
});