Ext.define('SmartSalary.view.phone.Superannuation.SuperannuationAddNewFundDetail_V', {
    extend: 'Ext.Container',
	alias: 'widget.superannuationaddnewfunddetail_P',
    config: {
		itemId:'superannuationAddNewFundDetailPageId',
		id:'superannuationAddNewFundDetailPageId',
		cls:'font-14 container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Superannuation',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                id:'superfundDetailBackBtn',
                itemId:"superfundDetailBackBtn",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
           	xtype: 'container',
			cls:'fa-vl fontcolor_vl pad-10',
			items:[
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Name*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailNameField',
					name: 'superfundDetailNameField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Address*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailAddressField',
					name: 'superfundDetailAddressField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Contact number*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailContactNumberField',
					name: 'superfundDetailContactNumberField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Contact number*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailContactNumberField',
					name: 'superfundDetailContactNumberField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					itemId: 'superfundEsaUsi',
					html:''
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailUsiField',
					name: 'superfundDetailUsiField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'ABN*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailAbnField',
					name: 'superfundDetailAbnField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'BSB*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailBsbField',
					name: 'superfundDetailBsbField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Account Number*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailAccountNumberField',
					name: 'superfundDetailAccountNumberField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype: 'container',
				cls:'bgd7 margin-top-10 phone-pad-bottom-1 noactive',
				items: [{
					xtype:'container',
					html:'Please attach copy of super statement showing super fund name, account number and BSB as mentioned above.*',
					cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-10'
				},{							
					xtype:'container',
					cls:'document_container margin-10',
					items:[{									
						xtype:'container',
						id:'snap_image',
						itemId:'snap_image',
						cls:'snap_images',
					},{
					/* external library used for uploading and displaying the image */
						xtype:'capturepicture',
						name: 'capturefiles',
						cls:'capture_images captureImageVehicleLeasing',
						height:'100%',
						width:'100%'
					}]
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Reference number*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailRefrenceNumberField',
					name: 'superfundDetailRefrenceNumberField',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Start date*',
				},{
					xtype: 'fieldset',
					cls:'selectbox_vl_2 selectbox_vl',
					items: [{
						xtype: 'selectfield',
						itemId:'superfundDetailStartDate',
						usePicker:false,
						options: []
					}]
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'End date*',
				},{
					xtype: 'fieldset',
					cls:'selectbox_vl_2 selectbox_vl',
					items: [{
						xtype: 'selectfield',
						itemId:'superfundDetailEndDate',
						usePicker:false,
						options: []
					}]
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Per pay amount*',
				},{	
					xtype: 'textfield',
					itemId: 'superfundDetailPerPayAmount',
					name: 'superfundDetailPerPayAmount',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},{
				xtype:'container',
				items:[
				{
					xtype:'button',
					text:'Next',
					itemId:'superfundDetailNextBtn',
					action:'superfundDetailNextBtn',
					cls:'button_grey_vl_2 claimStepButton font-16 pad-10 margin-top-10 fa fa-chevron-right'	
				},
				{
					xtype:'button',
					text:'Back',
					itemId:'superfundDetailBackBtn',
					action:'superfundDetailBackBtn',
					cls:'button_grey_vl_2 claimStepButton font-16 pad-10 margin-top-10 fa fa-chevron-right'	
				}]
			}]
		}]
    }
});