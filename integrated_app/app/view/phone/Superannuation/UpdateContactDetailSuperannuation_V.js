// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.Superannuation.UpdateContactDetailSuperannuation_V', {
    extend: 'Ext.Container',
	alias: 'widget.updateContactDetailSuperannuation_P',
    config: {
		itemId:'updateContactDetailSuperannuation',
		id:'updateContactDetailSuperannuation',
		cls:'font-14 container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Superannuation',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToUpdateContributionScreenPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
           	xtype: 'container',
			cls:'pad-10 bgE7E7E7 fontcolor_vl',
			items:[{
				xtype: 'container',
				cls:'fa-vl bgwhite pad-10',
				items:[{
					xtype: 'label',
					cls:'font-18',						 
					html: 'Catholic Super Fund 234545'
				},{
                   xtype:'container',
             	    layout:'hbox',
             	    cls:'m-5-0',
                    items:[{
                        xtype:'label',
                        html:'USI: ',
                        flex:1
                	},{
                    	xtype:'label',
                    	cls:'f-bold txt-right',
                    	itemId:'USIDetail',
                    	flex:1
                	}]
				},{
					xtype:'container',
                    layout:'hbox',
                    cls:'m-5-0',
                    items:[{
                        xtype:'label',
                        html:'ABN:',
                        flex:1
                    },{
                        xtype:'label',
                        cls:'f-bold txt-right',
                        itemId:'ABNDetail',
                        flex:1
                    }]
				}]
			},{
				xtype: 'container',
				cls:'fa-vl margin-top-10 bgwhite pad-10',
				items:[{
					xtype: 'label',
					html: 'Your New Schedule',
					cls:'font-18'
				},{
					xtype: 'container',
					layout: 'hbox',
					cls:'m-5-0',
					items:[{
						xtype:'label',
						cls:'f-darkgrey',
						html:'Start date',
						flex:1
					},{
						xtype:'label',
						cls:'f-bold txt-right',
						itemId:'StartDateContactDetail',
						flex:1
					}]
				},{
					xtype: 'container',
					layout: 'hbox',
					cls:'m-5-0',
					items:[{
						xtype:'label',
						cls:'f-darkgrey',
						html:'End date',
						flex:'1'
					},{
						xtype:'label',
						cls:'f-bold txt-right',
						itemId:'endDateincontactDetail',
						html:'20/11/2014',
						flex:1
					}]
				},{
					xtype: 'container',
					layout: 'hbox',
					cls:'m-5-0',
					items:[{
						xtype:'label',
						cls:'f-darkgrey',
						html:'Amount',
						flex:1
					},{
						xtype:'label',
						cls:'f-bold txt-right',
						itemId:'updateAmountincontactDetail',
						html:'$220.00',
						flex:'1'
					}]
				}]
			},{
				xtype: 'container',
				cls :'fa-vl margin-top-10 bgwhite pad-10',
				items:[{
					xtype: 'label',
					html: 'Your Contact Details',
					cls:'font-18'
				},{
					xtype:'container',
					cls:'m-5-0',
					layout:'hbox',
					items:[{
						xtype:'label',
						flex:1,
						cls:'f-darkgrey',
						html:'Email:'
					},{
						xtype:'label',
						cls:'f-bold txt-right',
						itemId:'contactEmail',
						flex:1
					}]
				},{
					xtype:'container',
					cls:'m-5-0',
					layout:'hbox',
					items:[{
						xtype:'label',
						cls:'f-darkgrey',
						flex:1,
						html:'Contact number',
                        cls:'pad-top-10'
					},{
						xtype:'textfield',
						cls:'input_wrap_vl',
						itemId:'contactNumber',
						flex:1
					}]
				}]
			},{
				xtype: 'container',
				cls :'fa-vl margin-top-20 bgwhite pad-10',
				items:[{
					xtype: 'container',
					cls: 'pad-top-10 pad-bottom-10',
					items:[{
						xtype:'label',
						html:'Smart Salary Terms and condiion',
						cls:'pad-top-10'
            	    }]
				},{
             		xtype : 'container',
                	cls : 'pad-top-10',
                	items:[{
               	    	xtype:'checkboxfield',
                    	name:'terms_and_condition',
                    	docked:'left'
            		},{
                		xtype:'container',
                		html:'I have read and agreed to the SmartSalary Terms and coditions.',
            		}]
        		},{
        	    	xtype:'button',
        	    	text:'Submit',
        	    	cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10'
        		}]
			}]
		}]
    }
});