Ext.define('SmartSalary.view.phone.Superannuation.SuperannuationAddNewContributionList_V', {
    extend: 'Ext.Container',
	alias: 'widget.superannuationaddnewcontributionlist_P',
    config: {
		itemId:'superannuationAddNewContributionListPageId',
		id:'superannuationAddNewContributionListPageId',
		cls:'font-14 container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Superannuation',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToSuperannuationHome",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
           	xtype: 'container',
			cls:'fa-vl bgwhite fontcolor_vl pad-10',
			items:[
			{
				xtype:'container',
				html:'What type of fund would you like to add?'
			},
			{
				xtype:'container',
				items:[
				{
					xtype:'button',
					text:'Commercial Super Fund',
					itemId:'superannuationCommercialSuperFund',
					action:'superannuationCommercialSuperFundPress',
					cls:'button_grey_vl_2 claimStepButton font-16 pad-10 margin-top-10'	
				},
				{
					xtype:'button',
					text:'Self Managed Super Fund',
					itemId:'superannuationSelfManagedSuperFund',
					action:'superannuationSelfManagedSuperFundPress',
					cls:'button_grey_vl_2 claimStepButton font-16 pad-10 margin-top-10'	
				},
				{
					xtype:'button',
					text:'Cancel',
					itemId:'superannuationAddNewContributionCancel',
					action:'backToSuperannuationHome',
					cls:'button_grey_vl_2 claimStepButton font-16 pad-10 margin-top-10'	
				}]
			}]
		}]
    }
});