// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.Superannuation.GEBSuperannuation_V', {
    extend: 'Ext.Container',
	alias: 'widget.GEBSuperannuation_P',
    config: {
		itemId:'GEBSuperannuation',
		id:'GEBSuperannuation',
		cls:'font-14 container_vl bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Superannuation',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                                itemId:"backToSuperannuationHome",
                                action:"backToSuperannuationHomePress",
				flex:1,
                                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
           	xtype: 'container',
			cls:'pad-10 fontcolor_vl bgE7E7E7 height100' ,
                        items:[{
				xtype: 'container',
				cls:'fa-vl bgwhite bordergrey_lt pad-10',
				items:[{
					xtype: 'label',						 
					html: 'Please provide the following details',
					cls:'pad-top-10'
				},{
					xtype: 'label',						 
					html: 'Tax File Number (TFN)*',
					cls:'pad-top-10'
				},{	
					xtype: 'textfield',
					itemId: 'TFNTextBox',
					name: 'TFNTextBox',
					tabIndex:2,
					placeHolder:'TFN', 
					cls:'font-12 pad-left-5 input_wrap_vl'
				}]
			},{
				xtype: 'container',
				cls:'fa-vl bgwhite bordergrey_lt pad-10 margin-top-10',
				items:[{
					xtype: 'label',						 
					html: 'Creditor Details Requiring Update',
					cls:'pad-top-10'
				},{
					xtype: 'container',
					itemId:'fundIncomleteButtonsCnt',
					items:[]
				}]
			},{
				xtype: 'container',
				cls:'fa-vl margin-top-20 bgwhite',
				items:[{
					xtype: 'button',
					text: 'Submit',
					itemId: 'fundSubmitButton',
					action: 'fundSubmitButtonPress',
					cls:'button_greyblue_vl claimStepButton txt-left font-16 pad-10 margin-top-10',
				}]
			}]
		}]
    }
});