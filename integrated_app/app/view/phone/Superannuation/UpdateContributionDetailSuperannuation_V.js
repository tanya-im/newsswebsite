// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.Superannuation.UpdateContributionDetailSuperannuation_V', {
    extend: 'Ext.Container',
    alias: 'widget.updateContributionDetailSuperannuation_P',
    config: {
		itemId:'updateContributionDetailSuperannuation',
		id:'updateContributionDetailSuperannuation',
		cls:'font-14 container_vl bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Superannuation',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                                action:"backToSuperannuationScreenPress",
				flex:1,
                                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
           	xtype: 'container',
			cls:'pad-10 fontcolor_vl',
			items:[{
				xtype: 'container',
				cls:'fa-vl bgwhite bordergrey_lt pad-10',
				items:[{
					xtype: 'container',
					itemId:'superannuationFundContributionDetailBox',
                                        cls:'bgwhite',
					items:[{
                                                xtype:'label',
                                                itemId:'fundContributionName'
                                            },{
                                                xtype:'container',
                                                layout:'hbox',
                                                cls:'pad-top-15',
                                                items:[{
                                                        xtype:'label',
                                                        html:'USI:',
                                                        flex:1
                                                },{
                                                    xtype:'label',
                                                    itemId:'fundContributionUSILabel',
                                                    cls:'f-bold'
                                                }]
                                          },{
                                              xtype:'container',
                                              layout:'hbox',
                                              cls:'pad-top-15',
                                              items:[{
                                                      xtype:'label',
                                                      html:'ABN:',
                                                      flex:1
                                              },{
                                                  xtype:'label',
                                                  itemId:'fundContributionABNLabel',
                                                   cls:'f-bold'
                                              }]
                                          },{
                                              xtype:'container',
                                              layout:'hbox',
                                              cls:'pad-top-15',
                                              items:[{
                                                      xtype:'label',
                                                      html:'Pay frequency:',
                                                      flex:1
                                              },{
                                                  xtype:'label',
                                                  itemId:'fundContibutionPayFrequencyLabel',
                                                   cls:'f-bold'
                                              }]
                                          },{
                                              xtype:'container',
                                              layout:'hbox',
                                              cls:'pad-top-15',
                                              items:[{
                                                      xtype:'label',
                                                      html:'Total contribution:',
                                                      flex:1
                                              },{
                                                  xtype:'label',
                                                  itemId:'fundContibutionTotalContributionLabel',
                                                   cls:'f-bold'
                                              }]
                                          }]
				}]
			},{
				xtype: 'container',
				cls:'fa-vl margin-top-20 bgwhite bordergrey_lt pad-10',
				items:[{
					xtype: 'label',
					html: 'Schedule',
					cls: 'pad-2-0'	
				},{
					xtype: 'container',
					layout:'hbox',
					cls:'pad-top-15',
					items:[{
						xtype: 'label',
						flex:1,
						html: 'Start Date:'
					},{
						xtype: 'label',
						flex:1,
						html: '30/9/2014',
                                                cls:'f-bold txt-right'
					}]
				},{
					xtype: 'container',
					layout:'hbox',
					cls:'pad-top-10',
					items:[{
						xtype: 'label',
						flex:1,
						cls:'pad-top-15',
						html: 'End Date:'
					},{
						xtype: 'fieldset',
						flex:1,
						cls:'selectbox_vl_2 selectbox_vl',
						itemId:'selectVContainerSuperannuation',
						items: [{
							xtype:'selectfield',
							cls:'expenditureType_1 vlabel',
							usePicker:false,
							itemId:'fundEndDate', 
							options:[{text: '20/11/2014',  value: '20/11/2014'},
								{text: '21/11/2014', value: '21/11/2014'},
								{text: '22/11/2014',  value: '22/11/2014'}
							]
						}]
					}]
				},{
					xtype: 'container',
					layout:'hbox',
					cls:'pad-top-10',
					items:[{
						xtype: 'label',
						flex:1,
						cls:'pad-top-15',
						html: 'Amount:',
					},{
						xtype: 'textfield',
						itemId:'contributionAmount',
						cls:'font-12 input_wrap_vl',
						flex:1,
						value: '$520',
					}]
				}]
			},{
				xtype: 'container',
				cls :'fa-vl margin-top-20',
				items:[{
					xtype: 'button',
					text: 'Update My Contribution',
					itemId: 'updateMyContributionButton',
					action: 'updateMyContributionButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
				},{
					xtype: 'button',
					text: 'Stop My Contribution',
					itemId: 'stopMyContributionButton',
					action: 'stopMyContributionButtonPress',
                                        cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
				}]
			}]
		}]
    }
});