Ext.define('SmartSalary.view.phone.Superannuation.SuperannuationAddCommercialSuperFund_V', {
    extend: 'Ext.Container',
	alias: 'widget.superannuationaddcommercialsuperfund_P',
    config: {
		itemId:'superannuationAddCommercialSuperFundPageId',
		id:'superannuationAddCommercialSuperFundPageId',
		cls:'font-14 container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Superannuation',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToAddSuperannuationContributionList",
                itemId:"backToAddSuperannuationContributionList",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
           	xtype: 'container',
			cls:'fa-vl fontcolor_vl pad-10',
			items:[
			{
				xtype:'container',
				cls:'bgE7E7E7 pad-10',
				items:[{
					xtype:'label',
					cls:'font-18 f-bold pad-2-0',
					html:'Find creditor',
				},
				{
					xtype:'label',
					cls:'pad-2-0',
					html:'Select fund*'
				},{
					xtype: 'fieldset',
					cls:'selectbox_vl_2 selectbox_vl',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType',
						itemId:'commercialSuperFundList',
						usePicker:false,
						options: []
					}]
				},{
					xtype: 'button',
					text: "Can't find my super fund",
					itemId:'cantFindMySuperFundButton',
					action:'cantFindMySuperFundButtonPress',
					cls:'button_greyblue_vl claimStepButton font-16 pad-10 margin-top-10'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Policy number*',
				},{	
					xtype: 'textfield',
					itemId: 'commercialSuperFundPolicyNumber',
					name: 'commercialSuperFundPolicyNumber',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Start date*',
				},{
					xtype: 'fieldset',
					cls:'selectbox_vl_2 selectbox_vl',
					items: [{
						xtype: 'selectfield',
						itemId:'commercialSuperFundStartDate',
						usePicker:false,
						options: []
					}]
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'End date*',
				},{
					xtype: 'fieldset',
					cls:'selectbox_vl_2 selectbox_vl',
					items: [{
						xtype: 'selectfield',
						itemId:'commercialSuperFundEndDate',
						usePicker:false,
						options: []
					}]
				}]
			},
			{
				xtype:'container',
				cls:'pad-top-10',
				items:[
				{
					xtype:'label',
					html:'Per pay amount*',
				},{	
					xtype: 'textfield',
					itemId: 'commercialSuperFundPerPayAmount',
					name: 'commercialSuperFundPerPayAmount',
					cls:'font-12 pad-left-5 input_wrap_vl margin-top-7'
				}]
			},{
				xtype:'container',
				items:[
				{
					xtype:'button',
					text:'Next',
					itemId:'addCommercialSuperFundNextBtn',
					action:'addCommercialSuperFundNextBtn',
					cls:'button_grey_vl_2 claimStepButton font-16 pad-10 margin-top-10 fa fa-chevron-right'	
				},
				{
					xtype:'button',
					text:'Back',
					itemId:'addCommercialSuperFundBackBtn',
					action:'addCommercialSuperFundBackBtn',
					cls:'button_grey_vl_2 claimStepButton font-16 pad-10 margin-top-10 fa fa-chevron-right'	
				}]
			}]
		}]
    }
});