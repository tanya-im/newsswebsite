// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.Superannuation.HomeSuperannuation_V', {
    extend: 'Ext.Container',
    alias: 'widget.homeSuperannuation_P',
    config: {
		itemId:'homeSuperannuation',
		id:'homeSuperannuation',
		cls:'font-14 container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Superannuation',
					cls: 'font-white font-18 pad-5'
				}]
			},{
                xtype:"button",
                action:"backToDashboardPress",
                flex:1,
                html:'Back',
                cls: 'font-white searchlistbackbtn'
			}]
		},{
            xtype: 'container',
            cls:'pad-10 fontcolor_vl bgE7E7E7',
            items:[{
			xtype: 'container',
            cls:'fa-vl bgwhite bordergrey_lt pad-0-10-10-10',
			items:[{
				xtype: 'container',
				itemId:'superannuationFundContributionDetailHome'
			}]
		},{
            xtype: 'container',
            cls:'fa-vl margin-top-20 bgwhite bordergrey_lt pad-10',
            items:[{
				xtype: 'label',
				html: 'With the introduction of Super Stream, SmartSalary needs additional information about your chosen superannuation funds. Please tap here to update your mandatory details.',
				itemId: 'notificationsTxt',
				cls: '  pad-2-0'	
             },{
				xtype: 'button',
				text: 'Click To Update',
				itemId: 'clickToUpdateFundDetail',
				action: 'clickToUpdateFundDetailPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
			}]
		},{
            xtype: 'container',
            cls:'fa-vl margin-top-20 bgwhite bordergrey_lt pad-10',
            items:[{
				xtype: 'label',
				html: 'Your current contributions',
				cls: 'pad-2-0'	
        	},{
				xtype: 'container',
				itemId:'fundContributionButtonsHome'
			},{
				xtype: 'button',
				text: 'Add a New Contribution',
				itemId: 'addANewContributionButton',
				action: 'addANewContributionButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',	
            }]
		},{
            xtype: 'container',
            cls :'fa-vl margin-top-20 bgwhite bordergrey_lt pad-10',
            items:[{
				xtype: 'fieldset',
				cls:'selectbox_vl',
				itemId:'selectVContainerSuperannuation',
				items: [{
					xtype:'selectfield',
					cls:'expenditureType_1 vlabel',
					usePicker:false,
					itemId:'selectVehicle', 
					options:[]
                }]	
			},{
                xtype: 'container',
                cls: 'pad-top-10 pad-bottom-10',
                items : [{
					xtype: 'container',
					items: [{
                        xtype: 'container',
                        id:'superannuationFromCalendar',
                        itemId:'superannuationFromCalendar',
					}]
				},{
                    xtype: 'container',
                    items: [{
						xtype: 'container',
						id:'superannuationToCalendar',
						itemId:'superannuationToCalendar'
                    }]
				}]
			},
				{	 
					/* PriceDetails To show payment details along with funds listiing and pricing */
	                xtype: 'container',
	                cls :'bgwhite margin-top-10 expenselist_vl_wrap',
	                items:[{
	                xtype:'list',
	                height:'200px',
	                store: "dashboardTransactionList", // to fetch data from store.
	                loadingText :null,
	                mode : 'SINGLE',
	                cls :'expenseList expenselist_vl fa-vl', // CSS to display arrows to show indicator.

	                itemTpl:'<div class="vehicleRegistrationList"><div class="expenseDate">{transactionDate} </div><div class="expdetail clearfix"><div class="expenseName">{description} </div><div class="expenseAmount">{amount}</div></div><div class="fa fa-chevron-right" style="display:block;"></div></div>',
	                listeners: { 	 // applied listner to open the content and other element will get closed automatically.
						painted: function(comp,eOpts) {
						this.getScrollable().getScroller().on('scrollstart',function(){
							Ext.getCmp('homeSuperannuation').setScrollable(false);
	                    });
	                    this.getScrollable().getScroller().on('scrollend',function(){
	                        Ext.getCmp('homeSuperannuation').setScrollable(true);
	                    });
					}
	                }
	                                    }]
				} // closing of PriceDetails
			]
			 
				
			}]
		}]
    }
});