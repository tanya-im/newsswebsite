Ext.define('SmartSalary.view.phone.HeaderFooter_V', {
	extend: 'Ext.Container',
	xtype: 'homeview_P',
	config: {
		id:'viewHome',
		itemId:'viewHome',
		items : [{
			xtype:'container',
			id:'transparentContainer',
			itemId:'transparentContainer',
			left:'0px',	
			top:'0px',
			height:'100%',
			width:'100%',
			cls:'veiwHomeTransparentContainer',
			hidden:true
		},{
		/**Header view*/			
           	xtype: 'toolbar',
			cls:'bgwhite noborder',
			layout:'hbox',
			docked:'top',
           	items:[{
				xtype: "toolbar",
				cls:'bgwhite pad-5 border-bottom-0',
				itemId:'headerToolBar',
				flex:3,
				items: [{
					docked: 'left',
					xtype: 'image', 
					id: 'smartSalaryLogo',
					itemId:'smartSalaryLogo',
					cls:'logoimg' 
				}]
			},{
				xtype: "toolbar",
				flex:1,
				cls:'bgwhite margin-toolbar',
				items:[{
					docked: 'right',
					xtype: 'image',
					//src: 'resources/images/menu_toggle.jpg',
					cls: 'topicon menuToggle_n',
					itemId:'menuToggle'
				}]	
			}]			
		},{
		/**Main Container for holding all the views****/
			xtype:'navigationview',
			navigationBar:{hidden:true},			 
			layout:{
				type: 'card',
				animation: {duration:500,type:'slide'}
			},
			id:'homeContainer',
			itemId:'homeContainer',
			height:'100%',
			width:'100%'	
		}]
	}
});