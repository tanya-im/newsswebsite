// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.CappedClaim.SubmittedSummaryPage_V', {
    extend: 'Ext.form.Panel',
	alias: 'widget.CappedClaimStep3_P',
    config: {
		itemId :'cappedClaimStepThreePageId',
		id:'cappedClaimStepThreePageId',
		cls:'bgdarkblue bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',			 
			cls :'pad-left-10 green-bg-gredient',
			items:[{
				xtype: 'panel',
				items:[{
					xtype: 'toolbar',
					title: 'Reimbursement claim for tax-free cap expenses',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},{
			xtype: 'container',
			cls :'bgE7E7E7 font-16 txt-center header_wrap_grey',
			items:[{
				xtype: 'image',
				src:'resources/images/SS04-Mobile-Claims-Vehicle-5.png',
				cls:'Claims-Vehicle-steps-img step5_img'
			}]
		},{
			xtype: 'container',
			cls :'pad-10 bgwhite',
			id:'cappedClaimSubTxt',
			items:[{
				xtype: 'label',
				html: '<h3 class="bluetxt success">Congratulations, your claim submission is now complete.</h3>',
				cls: 'font-white font-16 pad-left-5 fontFFamilyHelvetica'
			}]
		},{
			xtype: 'container',
			cls :'margin-15-10-0-10 pad-10 font-16 bgwhite color-2B2F3B',
			 
			items:[{
				xtype: 'container',
				cls :'font-14 color-2B2F3B pad-bottom-5',
				layout:'hbox',
				items:[{
					xtype: 'label',
					flex: 1,
					itemId:'cappedClaimRefNo',
					id:'cappedClaimRefNo',
					html: '',
				}]
			},{
				xtype: 'label',
				flex: 1,
				cls:'font-14 f-bold',
				html: ' Your claim form will be reviewed and processed',
			}]
		},{		
			xtype: 'container',
			cls :'margin-0-10 bgwhite font-16 color-2B2F3B clearfix',			 
			id:'vehicleRunningExpenseClaimConainerId',
			items:[{
				xtype: 'button',
				ui: 'none',
				action: 'cappedTrackTheProgressOfYourClaimPress',
				cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 color-5A5B77',
				text: 'Track the progress of your claim'
			}]
		},{		
			xtype: 'container',
			cls :'font-16 color-2B2F3B margin-10 fontFFamilyHelvetica',
			items:[{
				xtype: 'label',
				html: ' Claim Summary',
				cls:'pad-left-20'
			}]
		},{
			xtype: 'container',
			cls :'pad-10 bgwhite font-14 color-2B2F3B margin-10',
			 
			items: [{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
								xtype: 'label',
								html: 'Claim lodgement date:',
								flex:3
							},{
								xtype: 'label',
								html: Ext.Date.format(new Date(),'d M Y'),
								cls:'pad-right-5 txt-right f-bold',
								flex:2
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
								xtype: 'label',
								html: 'Total Reimbursement Amount:',
								flex:3
							},{
								xtype: 'label',
								itemId:'totalAmountThirdPage',
								id:'cappedClaimTotalAmountSubmittedPage',
								html: '',
								cls:'pad-right-5 txt-right f-bold',
								flex:2
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
								xtype: 'label',
								html: 'Employee Name:',
								flex:3
							},{
								xtype: 'label',
								id: 'cappedClaimFirstName',
								html: '',
								cls:'pad-right-5 txt-right f-bold',
								flex:2
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
								xtype: 'label',
								html: 'Payroll Number:',
								flex:3
							},{
								xtype: 'label',
								id: 'cappedClaimPayrollNumber',
								html: '12182',
								cls:'pad-right-5 txt-right f-bold',
								flex:2
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
								xtype: 'label',
								html: 'Employer:',
								flex:3
							},{
								xtype: 'label',
								id:'cappedClaimEmployerName',
								html: '',
								cls:'pad-right-5 txt-right f-bold',
								flex:2
						}]
					}]
			},{	
				xtype: 'container',
				cls :'bgwhite font-16 color-2B2F3B m-20-10 mar-btm-0',
				id:'cappedClaimDetailedSummaryWrapper',
				itemId:'cappedClaimDetailedSummaryWrapper',
			},{
			xtype: 'container',
			cls :'pad-10 bgwhite font-16 txt-center margin-top-20',
			id:'cappedClaimReturnBtn',
			items:[{
				xtype: 'button',
				itemId: 'returnToYourClaimsButton',
				ui: 'none',
				action: 'returnToYourClaimsPress',
				 
				cls:'claimStepOneNextButton claimStepBtn txt-left font-16 pad-10 bgpurpule',
				text: 'Return to your claims'
			}]
		}]
	}
});