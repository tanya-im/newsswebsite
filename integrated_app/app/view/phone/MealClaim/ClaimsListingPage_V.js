// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.phone.MealClaim.ClaimsListingPage_V', {
    extend: 'Ext.form.Panel',
	alias: 'widget.MealClaimListingPageWidget_P',
    config: {
		itemId :'MealClaimListingPageId',
		id:'MealClaimListingPageId',
		cls:'bgdarkblue bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
                xtype: 'container',
                layout:'hbox',
                cls :'claimListheader',
                items:[{
                    xtype: 'panel',
                    flex:4,
                    items:[{
                        xtype: 'toolbar',
                        title: 'Meal Entertainment Expenses Claim',                         
                        cls: 'font-white font-18 pad-5'
                    }]
                },{
					xtype:"button",
					action:"mealClaimListingPageBackBtnUpper",
					flex:1,
					html:'Back',
					cls: 'font-white searchlistbackbtn'
				}]
		},{
			xtype: 'container',
			cls :'bgE7E7E7 font-16 txt-center header_wrap_grey',
			items:[
			{
				xtype: 'image',
				//src:'resources/images/SS04-Mobile-Claims-Vehicle-3.png',
				cls:'Claims-Vehicle-steps-img step2_img'
			}]
		},
		{
			xtype: 'spacer',
			cls :'pad-10 bgwhite',
		},
		{
			xtype: 'container',
			id:'mealClaimItemsWrapper',
			itemId:'mealClaimItemsWrapper',
			cls :'bgE7E7E7 font-normal',			 
		},{
			xtype: 'spacer',
			cls :'pad-10 bgwhite'
		}
		,{
			xtype: 'container',
			itemId:'leftTotalClaimDetailWrapper',
			cls :'pad-10 bgE7E7E7 font-white vclaimtotalwap font-normal',
			items:[
			{
				xtype: 'container',
				cls :'pad-10 bgpurpule ',
				items:[{
					xtype: 'label',
					id:'totalClaimAmountListingPage',
					itemId:'totalClaimAmountListingPage',
					cls:'font-16 pad-bottom-5',
					html: '',
				},{
					xtype: 'label',
					id:'totalClaimAmountValueFormListing',
					itemId:'totalClaimAmountValueFormListing',
					cls:'font-26 ',
				}]
			}]
		},{
                xtype: 'container',
                cls :'pad-top-10 pad-bottom-10 bgE7E7E7',
                layout: 'vbox',  
                items:[
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
	                items:[{
	                    xtype: 'button',
	                    ui: 'none',
	                    flex:1,
	                    action:'MealClaimStepTwoSaveAndAnotherButton',
	                    cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10 bgpurple1',
	                    text: 'Save & Add Another Expense'
	                }]
	            },
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
                    items:[{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'mealClaimListingPageBackBtn',
                        cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Back'
                    },
                    {
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'mealClaimListingPageNextButton',
                        cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Next'
                    }]
                },
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
                    items:[{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'mealClaimCancelButton',
                        cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Cancel'
                    },
                    {
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        cls:'claimStepOneSaveAsDraftButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Save as draft'
                    }]
                }]
            }]
	}
});