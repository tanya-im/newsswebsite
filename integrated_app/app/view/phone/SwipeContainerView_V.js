Ext.define('SmartSalary.view.phone.SwipeContainerView_V', {
	extend: 'Ext.Container',
	xtype: 'app_viewport',
	requires: ['Ext.TitleBar'],
	config: {
		fullscreen: true,
		layout: 'hbox',
		items : [
		{
			xtype: 'accordionlist_P',
			store: Ext.create('SmartSalary.store.SlideList_S'),
			cls:'nav-list',
			width:250,
			id:'basic',
			itemId: 'basic',
			listeners: {
				initialize: function() {
					this.load();
				}
			}
		},
		{
			xtype : 'homeview_P',
			cls: 'slide',
			width: '100%'												
		}],
		listeners: {
            resize: function(e){
            	if(Ext.os.is('Android')) {
                    this.androidResize();
                }
            }
        }
	},
    androidResize: function() {
	    var wHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        if (Ext.Viewport.getHeight() != wHeight) {
            Ext.Viewport.setHeight(wHeight);
        }
    }
});