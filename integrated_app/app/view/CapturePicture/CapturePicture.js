Ext.define('SmartSalary.view.CapturePicture.CapturePicture', {
    extend: 'Ext.Component',
    xtype: 'capturepicture',
    alternateClassName: ['capturePicture'],
	config: {
		captured: false,
        width: 140,
        height: 100,
        data_index : 0,
		files : new Array(),
		cls: 'capturePictureContainer',
		html: [
            '<div class="image_browse_wrapper clearfix">'
				+'<input class="upload_imgs" type="file" accept="image/*" />'
			+'</div>' //Step 1
			
        ].join('')
    },
	
	
    initialize: function() {
        this.callParent(arguments);

        this.file = this.element.down('input[type=file]');
        this.img = this.element.down('img');
		this.file.on('change', this.do_upload, this); //Step 2
		data_index = 0;
        //FIX for webkit
        window.URL = window.URL || window.webkitURL; //Step 3
    },

   do_upload: function (event) {
		this.files = event.target.files;
		Logger.debug(this.files)
		this.readURL(this.files,this);
		data_index=Shared.fileData.length;
		Ext.each(this.files, function(value,key){
			Shared.fileData[data_index] = value;
		});
		Logger.debug(Shared.fileData);
	},
	
	readURL: function(input,_this) {
		var reader = new FileReader();
		reader.onload = function (e) {
			//var div = document.getElementByClass('images');
			var img = Ext.create('Ext.Container', {
				itemId: 'snapcontainer',
				cls: 'snap_shot',
				items:[{
					xtype: 'container',
					html:'<img height="109px" width="109px" src="'+e.target.result+'" />',
					cls:"floatLeft"
				},
				{
					xtype: 'image', 
					data: {"snap_id":data_index}, 
					cls: 'remove_img floatLeft',
					listeners: {
						tap : function(b, e) {
							this.up('#snapcontainer').destroy();
							delete Shared.fileData[this.getData().snap_id]
							Shared.fileData = Shared.cleanArray(Shared.fileData);
						}
					}
				}]
			}); 
			var objSnapImageContainer=Ext.getCmp('snap_image');
			
			objSnapImageContainer.add(img);
			data_index++;
		}
		Logger.debug(this.files[0].type);
		var fileExt = this.files[0].type.split("/");
		var extension = fileExt[1].toUpperCase();
		if (extension!="PNG" && extension!="JPG" && extension!="GIF" && extension!="JPEG"){
            Logger.alert(Messages.AlertMsg,Messages.AlertMsgInvalidImageFormatMsg);
			return false;
        }
		reader.readAsDataURL(this.files[0]);
	},
	reset: function() {
    }
});