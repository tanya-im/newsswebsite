// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.SearchList_V', {
    extend: 'Ext.Container',
	requires: ['Ext.TitleBar'],
	alias: 'widget.searchListView_D',
    config: {
		 cls: 'bgwhite',
		 itemId:'searchListView',
		 id :'searchListView',
		items: [
		{
           	xtype: 'container',
			cls:'bgdarkblue pad-10 salaryfeepackagingtop',
			height:'20%',
			html:'Search result'
		},
		{
			xtype: 'list',
			cls:'searchList',
			itemId: 'listSearch',
			height:'90%',
			store:'storeSearchList',
			loadingText :null,
			onItemDisclosure: true,
			itemTpl:'<div style="font-weight:bold;">{title}</div> {shortdescription}'
		}]
    }
});