Ext.define('SmartSalary.view.desktop.HeaderFooter_V', {
	extend: 'Ext.Container',
	xtype: 'homeview_D',
	config: {
		id:'viewHome',
		itemId:'viewHome',
		layout:'vbox',
		items : [{
			/**Header view*/			
			xtype: 'toolbar',
			cls:'bgwhite pad-0',
			layout:'hbox',
			docked:'top',			
			items:[{
				xtype: "toolbar",
				cls:'bgwhite pad-10-0 border-bottom-0',
				itemId:'headerToolBar_d',
				flex:3,
				items: [{
					docked: 'left',
					xtype: 'image', 
					id: 'smartSalaryLogo',
					cls:'logoimg' 
				}]
			},{
				xtype:'container',
				cls:'search_wrap',
				flex:1.5,
				items:[{
					xtype:'textfield',
					cls:'txtSearchBar txtSearchField_p',
					placeHolder:'Enter search item',
					id:'txtSearchField',
					itemId:'txtSearchField'
				},{
					xtype:'button',
					cls:'btnSearch',
					id:'btnSearchField_notWroking',
					itemId:'btnSearchField_notWroking'
				}]
			},{
				xtype:'container', 
				itemId:'userdetail',
				flex:1.5,
				layout:'hbox',
				cls:'bggrey fa-vl',
				items:[{
					xtype:'container', 
					flex:3,
					items:[{
						xtype: 'label',
						itemId: 'wcuser',
						cls: 'wcuser font-white font-16 font-normal'				
					},{
						xtype: 'label',
						itemId: 'acno',
						cls: 'font-828395 font-14 pad-2-0'				
					},{
						xtype: 'label',
						itemId: 'employername',
						cls: 'font-828395 font-14 pad-2-0'				
					}]
				},{
					xtype:'container',
					flex:1,
					cls:'newlogoutbtn',
					items:[/*{
						xtype: 'label',
						cls: 'fa fa-cog',
						flex:1
					},*/
					{
						xtype: 'button',
						ui:'none',
						itemId:'NewLogoutBtn',
						cls:' fa fa-sign-out',
						//text:'Logout'
					}]
				}]
			}]			
		},{
			xtype: 'menudropdown',
			itemId:'menuControl'
		},
		/**Main Container for holding all the views****/
		{
			xtype: 'container',
			layout:'hbox', 
			flex: 9,
			items:[{
				xtype:'navigationview',
				navigationBar:{
					hidden:true
				},
				layout:{
					type: 'card',
					animation: {duration:1000,type: 'fade'}
				},
				id:'homeContainer',
				itemId:'homeContainer',
				//height:'100%',
				flex:4	
			},{
				xtype: 'container',
				cls:'pad-10 desktop_sidebar font-14',
				scrollable: {
					direction: 'vertical',
					directionLock: true,
					indicators: {
							y: {
								autoHide: false
							}
					}
				},
				flex:1,
				items:[{
                	xtype: 'container',
                    cls:'  margin-top-30 margin-left-10 margin-right-10',
                    flex:1,
                    items:[{
                        xtype: 'label',
                        html: 'Notifications',
                        cls: 'font-18 f-bold pad-bottom-10'   
                    },{
                        xtype: 'label',
                        html: 'Due to the new Super Stream laws that were enforced, you will need to update your personal detail with your TFN details. Please click here to update.',
                        itemId: 'notificationsTxt',
                        cls: 'bgwhite pad-10 bordergrey_lt' 
                    }]
                },{
                    xtype: 'container',
                    cls:'fa-vl margin-top-20 margin-left-10 margin-right-10',
                    items:[{
                        xtype: 'container',
                        cls:'border',
                    },{
                    	xtype: 'container',
                        html: 'Available Products',
						cls: 'font-18 f-bold pad-bottom-10'
                    },{
                    	xtype: 'button',
                        text: 'Novated car Lease',
                        itemId: 'novatedCarLease',
                        action: 'novatedCarLease',
                        cls:'button_white_vl claimStepButton fa fa-chevron-right',    
                    },{
                        xtype: 'button',
                        text: 'Home Office Item',
                        itemId: 'homeOfficeItem',
                        action: 'homeOfficeItem',
                        cls:'button_white_vl claimStepButton fa fa-chevron-right margin-top-10',    
                    },{
                        xtype: 'button',
                        text: 'Mortgage Repayment',
                        itemId: 'mortgageRepayment',
                        action: 'mortgageRepayment',
                        cls:'button_white_vl claimStepButton fa fa-chevron-right margin-top-10',    
                	},{
                    	xtype: 'container',
                    	cls:'fa-vl txt-center margin-top-20 margin-left-20',
                    	items:[{
                			xtype: 'container',
                       		cls:'placeholder_car',
                        	html:'<img src="resources/images/placeholder_car.png">',
                    	}]
                	}]
            	}/*{
					xtype: 'image',
					src: 'resources/images/sidebar_img.png',
					height:'834px',
					width:'270px',
					style:'margin:30px auto 0;',
				}*//*,{
					xtype: 'container',
					cls:'desktop_sidebar_img',
				}*/]	
			}]
		},{	
			xtype: 'container',
			cls: 'color-5A5B77',
			flex:0.7,
			items:[{
				xtype: 'container',
				cls: 'pad-10 font-white font-14',
				height:'100%',
				layout:'hbox',
				items :[{
					xtype: 'container',
					flex: 1,
					html: '<p>&copy; 2014 Smartsalary All Rights Reserved</p>',
					left: 0
				},{
					xtype: 'container',
					flex: 1,
					right: 0,
					items: [{
						xtype:'label',
						cls: 'floatLeft margin-right-10',
						html : 'Terms & Conditions'
					},{
						xtype:'label',
						cls: 'floatLeft margin-right-10',
						html : 'Privacy Policy'
					},{
						xtype:'label',
						cls: 'floatLeft margin-right-10',
						html : 'Site Map'
					}]	
				}]
			}]
		}]
	}
});