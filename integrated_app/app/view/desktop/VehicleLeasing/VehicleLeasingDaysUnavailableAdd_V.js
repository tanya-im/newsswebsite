Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingDaysUnavailableAdd_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingDaysUnavailableAdd_D',
	config: {
		itemId :'vehicleLeasingDaysUnavailableAddPageId',
		id:'vehicleLeasingDaysUnavailableAddPageId',
		cls: 'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'panel',
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Days Unavailable',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls: 'pad-15-20 font-14 color-2B2F3B bgwhite',
					items: [{
						xtype: 'container',
						itemId:'vehicleDetailText'
					},{
						xtype: 'container',
						cls: 'pad-top-10 pad-bottom-10',
						items : [{
							xtype: 'container',
							items: [{
								xtype: 'container',
								id:'daysUnavailableDropOffCalendar',
								itemId:'daysUnavailableDropOffCalendar'
							}]
						},{
							xtype: 'container',
							items: [{
								xtype: 'container',
								id:'daysUnavailablePickUpCalendar',
								itemId:'daysUnavailablePickUpCalendar'
							}]
						}]
					},{
						xtype: 'container',
						items: [{
							xtype: 'label',
							html: 'Select a reason',
							cls:'margin-bottom-10'
						},{
							xtype: 'fieldset',
							cls:' selectbox_vl',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType',
								itemId:'reasonsDropDown',
								usePicker: false,
								cls:'selectbox_vl',
								width:'100%'
							}]	
						}]
					},{
						xtype: 'container',
						cls:'bgd7 margin-top-10 phone-pad-bottom-1 noactive pad-bottom-1',
						items: [{
							xtype:'container',
							html:'Please provide substantiation (either take a photo of the document or scanned documents)',
							cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-10'
						},{							
							xtype:'container',
							cls:'document_container margin-10',
							items:[{									
								xtype:'container',
								id:'snap_image',
								itemId:'snap_image',
								cls:'snap_images'
							},{
								xtype:'capturepicture',
								name: 'capturefiles',
								cls:'capture_images captureImageVehicleLeasing',
								height:'100%',
								width:'100%'
							}]
						}]
					},{
						xtype: 'container',
						cls:'fa-vl',
						items: [{
							xtype: 'button',
							itemId: 'vehicleLeasingDaysUnavailableAddNextBtnPress',
							cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
							text: 'Next'
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'vehicleDetails',
					action: 'vehicleDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Vehicle Details'	
				},{
					xtype: 'button',
					itemId: 'leaseDetails',
					action: 'leaseDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Lease Details'
				},{
					xtype: 'button',
					itemId: 'FBTDetails',
					action: 'FBTDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'FBT Details'
				}]
			}]
		}]
	}
});