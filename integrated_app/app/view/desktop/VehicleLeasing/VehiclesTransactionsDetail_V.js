// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehiclesTransactionsDetail_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehiclesTransactionsDetail_D',
    config: {
		itemId :'vehiclesTransactionsDetailPageId',
		id:'vehiclesTransactionsDetailPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Vehicle Transaction Details',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls :'bgwhite shadow_vl font-16 f-bold color-2B2F3B vehicleDetail',
					items:[{
						xtype: 'container',
						cls :' pad-10 font-normal ',
						items:[{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14',
							items:[{
								xtype: 'label',
								html: 'Date:',
								flex:3
							},{
								xtype: 'label',
								itemId:'transactionDate',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xx/xx/xx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14',
							items:[{
								xtype: 'label',
								html: 'Description:',
								flex:2
							},{
								xtype: 'label',
								itemId:'transactionDescription',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"Credit"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14',
							items:[{
								xtype: 'label',
								html: 'Pre tax:',
								flex:2
							},{
								xtype: 'label',
								itemId:'preTax',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"$70126"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14',
							items:[{
								xtype: 'label',
								html: 'Post tax:',
								flex:2
							},{
								xtype: 'label',
								itemId:'postTax',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"+84.56"
							}]
						}]	  
					}]
				},{
					xtype: 'container',
					cls :'margin-top-30',
					layout:'hbox',
					items:[{
						xtype: 'button',
						itemId: 'previousTransactionButton',
						flex:1,
						ui: 'none',
						action: 'previousTransactionButtonPress',
						cls:'button_greyblue_vl claimStepButton fa fa-chevron-left fa-chevron-left_1 txt-right font-16 pad-10 prev_btn_vl margin-right-10',
						text: 'Previous'
					},{
						xtype: 'button',
						itemId: 'nextTransactionButton',
						ui: 'none',
						flex:1,
						action: 'nextTransactionButtonPress',
						cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-left-10',
						text: 'Next'
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'leasingOverviewButton',
					ui: 'none',
					action: 'leasingOverviewButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Expenditure Overview'	
				},{
					xtype: 'button',
					itemId: 'fullTransactionsButton',
					ui: 'none',
					action: 'fullTransactionsButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Full Transactions'
				},{
					xtype: 'button',
					itemId: 'manageMyBudgetButton',
					ui: 'none',
					flex:1,
					action: 'manageMyBudgetButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage My Budget'
				},{
					xtype: 'button',
					itemId: 'viewFullBudgetBreakdownButton',
					ui: 'none',
					action: 'viewFullBudgetBreakdownButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View full budget breakdown'	
				}]
			}]
		}]
	}
});