// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingAvailableProductsThankyou_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingAvailableProductsThankyou_D',
    config: {
		itemId :'vehicleLeasingAvailableProductsThankyouPageId',
		id:'vehicleLeasingAvailableProductsThankyouPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'View Available Products',
				cls: 'font-white font-18 pad-5'
			}]
		},{	
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					items:[{
						xtype: 'label',
						itemId:'productsNameLabel',
						cls: 'f-bold font-18 margin-bottom-10'
					},{	
						xtype: 'container',
						cls :'font-normal font-14 border-10-white',
						items:[{
							xtype: 'container',
							cls:'bgE7E7E7 pad-33 txt-center',
							html: '<p>Thank you for your request.We will call you back as ASAP.</p>'
						}]
						
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'backToManageMyBudgetsButton',
					ui: 'none',
					action: 'goToVehicleLeasingAvailableProducts',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Available Products'
				},{
					xtype: 'button',
					itemId: 'MyProductsButton',
					action: 'MyProductsButtonPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'My Products'
				}]
			}]
		}]
	}
});