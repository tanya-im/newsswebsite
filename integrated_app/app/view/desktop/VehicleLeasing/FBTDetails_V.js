// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.FBTDetails_V', {
    extend: 'Ext.Container',
	alias: 'widget.fBTDetails_D',
    config: {
		itemId :'fBTDetailsPageId',
		id:'fBTDetailsPageId',
		cls :'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items:[{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'FBT Details',
				cls: 'font-white font-16 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls :'',
					items:[{
						xtype: 'container',
						cls :'pad-15-20 font-14 f-bold fontcolor_vl height100 bgwhite pad-t10-b10',
						items:[{
							xtype: 'container',
							cls :'vborder',
							items:[{
								xtype: 'container',
								layout:'hbox',
								cls :'pad-10-0',
								items:[{
									xtype: 'label',
									html: 'Kms Travelled (FBT Year)',
									flex:1
								},{
									xtype: 'label',
									itemId:'kmStatusHeader',
									cls:'txt-right f-bold',
									flex:1, 
								}]
							},{
								xtype: 'container',
								cls: 'bgE7E7E7 pad-10 pad-l-20 pad-r-20',
								items: [{
									xtype: 'container',
									itemId:'FBTCarAni',  			 
								},{
									xtype: 'container',
									cls :'bgwhite pad-6-10-7 fa-vl',
									itemId:'residualValue',
									items:[{
										xtype: 'label', 
										cls:'fa fa-check f-bold f-0E9F80',
										html:"<div class='fontFFamilyHelvetica pad-left-36'>No FBT Liability</div>"					}]	
								}]
							},{
								xtype: 'container',
								cls :'pad-top-15',
								items:[{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-bottom-5 pad-bottom-7 font-14 fontcolor_vl',
									items:[{
										xtype: 'label',
										html: 'Travelled Kms:',
										flex:1
									},{
										xtype: 'label',
										itemId:'travelledKms',
										cls:'pad-right-5 txt-right f-bold',
										flex:1
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-bottom-5 pad-bottom-7 font-14 fontcolor_vl',
									items:[{
										xtype: 'label',
										html: 'Remaining Kms:',
										flex:1
									},{
										xtype: 'label',
										itemId:'remainingKms',
										cls:'pad-right-5 txt-right f-bold',
										flex:1
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-bottom-5 pad-bottom-7 font-14 fontcolor_vl',
									items:[{
										xtype: 'label',
										html: 'Average Kms Required:',
										flex:1
									},{
										xtype: 'label',
										itemId:'averageKmsRequired',
										cls:'pad-right-5 txt-right f-bold',
										flex:1
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-bottom-5 pad-bottom-7 font-14 fontcolor_vl',
									items:[{
										xtype: 'label',
										html: 'Remaining Days:',
										flex:1
									},{
										xtype: 'label',
										itemId:'remainingDays',
										cls:'pad-right-5 txt-right f-bold',
										flex:1
									}]
								}]	
							}]
						},{
							xtype: 'container',
							cls :'margin-top-10 fa-vl',
							items:[{
								xtype: 'button',
								itemId: 'myODOReadingButton',
								ui: 'none',
								action: 'myODOReadingButtonPress',
								cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-7',
								text: 'My Odometer Reading - 41,059'
							}]
						},{
							xtype: 'container',
							cls :'margin-top-3 fa-vl margin-bottom-15',
							items:[{
								xtype: 'button',
								itemId: 'daysUnavailableButton',
								ui: 'none',
								action: 'daysUnavailableButtonPress',
								cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
								text: 'Days unavailable'
							}]
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'container',
					cls :'border-bottom margin-bottom-15',
					items:[{
						xtype: 'container',
						items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl',
							itemId:'selectVContainer' 
						}]
					}]
				},{
					xtype: 'button',
					itemId: 'vehicleDetails',
					action: 'vehicleDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Vehicle Details'	
				},{
					xtype: 'button',
					itemId: 'leaseDetails',
					action: 'leaseDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Lease Details'
				},{
					xtype: 'button',
					itemId: 'FBTDetails',
					action: 'FBTDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'FBT Details'
				}]
			}]
		}]
	}
});