// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingAvailableProductsUserLogedInDetail_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingAvailableProductsUserLogedInDetail_D',
    config: {
		itemId :'vehicleLeasingAvailableProductsUserLogedInDetailPageId',
		id:'vehicleLeasingAvailableProductsUserLogedInDetailPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'panel',
				items:[{
					xtype: 'toolbar',
					title: 'View Available Products',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},{	
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					items:[{
						xtype: 'label',
						cls: 'f-bold font-18 margin-bottom-15',
						itemId:'cProductLable'
					},{	
						xtype: 'container',
						cls :'font-normal font-14 margin-bottom-15',
						items:[{
							xtype: 'label',
							html: '<p>Please confirm your personal details:</p>',
						},{
							xtype: 'container',
							items:[{		
								xtype: 'container',
								layout:'hbox',
								cls:'m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Name:',
									flex:1
								},{
									xtype: 'label',
									itemId:'user_name',
									cls:'txt-left',
									flex:5
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Email:',
									flex:1
								},{
									xtype: 'label',
									itemId:'user_email',
									cls:'txt-left',
									flex:5
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Phone:',
									flex:1
								},{
									xtype: 'label',
									itemId:'user_contact',
									cls:'txt-left',
									flex:5
								}]
							}]	
						}]
					},{
						xtype: 'container',
						flex: 1,
						cls:'fa-vl',
						items: [{
							xtype: 'button',
							itemId: 'availableProductsUserDetailPageSubmitBtnPress',
							cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',					text: 'Submit'
						}]		
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'backToManageMyBudgetsButton',
					ui: 'none',
					action: 'goToVehicleLeasingAvailableProducts',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Available Products'
				},{
					xtype: 'button',
					itemId: 'MyProductsButton',
					action: 'MyProductsButtonPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'My Products'
				}]
			}]
		}]
	}
});