// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.BudgetBreakdown_V', {
    extend: 'Ext.Container',
	alias: 'widget.budgetBreakdown_D',
	requires: ['Ext.chart.PolarChart', 'Ext.chart.series.Pie3D', 'Ext.chart.interactions.RotatePie3D'],
    config: {
		itemId :'budgetBreakdownPageId',
		id:'budgetBreakdownPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Budget Breakdown',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					layout: 'hbox',
					cls:'margin-bottom-10',
					items:[{
						xtype: 'container',
						flex:1,				 
						itemId:'currentFilterLabel',
						html: 'Last 12 Months',
						cls:'pad-12-0-5-0 font-14'
					},{
						xtype: 'container',
						flex:1,
						items:[{
							xtype: 'button',
							itemId: 'changeDatesBudgetBreakdownButton',
							ui: 'none',
							action: 'changeDatesBudgetBreakdownButtonPress',
							cls:'claimStepButton changedates_dd_dn txt-left font-16',
							text: 'Change Dates'
						}]
					}]
				},{
					xtype: 'container',
					cls:'bgwhite margin-bottom-10',
					items:[{
						xtype: 'container',
						cls :' fa-vl pad-10',
						hidden:true,
						id:'budgetBreakdownDateContainer',
						itemId:'budgetBreakdownDateContainer',
						items:[{
							xtype: 'button',
							itemId: 'liveToDateButton',
							ui: 'none',
							action: 'budgetBreakdownDateChangeButtonPress',
							cls:'button_ltgrey_vl txt-left font-16 pad-10',
							text: 'Live To Date'
						},{
							xtype: 'button',
							itemId: 'last12monthsButton',
							id:'last12monthsButton',
							ui: 'none',
							action: 'budgetBreakdownDateChangeButtonPress',
							cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
							text: 'Last 12 Months'
						},{
							xtype: 'button',
							itemId: 'fBTYearButton',
							ui: 'none',
							action: 'budgetBreakdownDateChangeButtonPress',
							cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
							text: 'FBT Year'
						},{
							xtype: 'button',
							itemId: 'customDateRangeButton',
							ui: 'none',
							action: 'budgetBreakdownDateChangeButtonPress',
							cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
							text: 'Custom Date Range'
						}]
					},{
						xtype: 'container',
						cls :'fa-vl margin-bottom-15 pad-0-10',
						id:'budgetBreakdowncustomDateRange',
						itemId:'budgetBreakdowncustomDateRange',
						hidden:true,
						items:[{
							xtype: 'container',
							id:'breakdownFromCalendar',
							itemId:'breakdownFromCalendar'
						},{
							xtype: 'container',
							id:'breakdownToCalendar',
							itemId:'breakdownToCalendar'
						},{
							xtype: 'button',
							itemId: 'breakdownDatesButton',
							ui: 'none',
							action: 'breakdownDatesButtonPress',
							cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
							text: 'Submit'
						}]
					}]
				},{
					xtype: 'container',
					cls:'bgwhite',
					items:[{
						xtype: 'container',
						//hidden:true, 
						layout:'fit', 
						height:'200px',
						items:[{ 
							id:'pieChartIn3D', 
							itemId:'pieChartInBudget',
							xtype: 'polar',
							width: '100%',  
							background: 'white',
							interactions: 'rotatePie3d',
							store: {
								fields: ['name', 'budget' ],
								data: [
									{'name':'Jan', 'budget':10 },
									{'name':'Feb', 'budget':7},
									{'name':'March','budget':5},
									{'name':'Apr', 'budget':2},
									{'name':'May', 'budget':10 },
									{'name':'June', 'budget':6},
									{'name':'July', 'budget':8 },                               
								]
							},
							colors: [
								'#4F2C7E',
								'#EB2939',
								'#0070BB',
								'#E88200',
								'#ABDDE4', 
								'#B0B3B1',
								'#BCD400'
							],
							animate: {
								duration: 1000,
								easing: 'easeInOut'
							},
							series: [
								{
									type: 'pie3d',
									field: 'budget',
									donut: 50,
									distortion: 0.4,
									style: {
										stroke: "white",
										strokeStyle: 'white',
										opacity: 0.90
									}
								}
							]
						}]
					},{
						xtype: 'container',
						//hidden:true,
						cls:'bgLightGreyVehicale fa-vl pad-10',
						id:'budgetTypeList',
						itemId:'budgetTypeList'
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'container',
					cls :'height50 margin-bottom-5-for-vehicle',
					items:[{
						xtype: 'container',
						items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl',
							itemId:'selectVContainer',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType_1 vlabel',
								usePicker:false,
								itemId:'selectVehicle', 
								label: 'Vehicle :'  
							}]
						}]
					}]
				},{
					xtype: 'button',
					itemId: 'leasingOverviewButton',
					ui: 'none',
					action: 'leasingOverviewButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Expenditure Overview'	
				},{
					xtype: 'button',
					itemId: 'fullTransactionsButton',
					ui: 'none',
					action: 'fullTransactionsButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Full Transactions'
				},{
					xtype: 'button',
					itemId: 'manageMyBudgetButton',
					ui: 'none',
					flex:1,
					action: 'manageMyBudgetButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage My Budget'
				},{
					xtype: 'button',
					itemId: 'viewFullBudgetBreakdownButton',
					ui: 'none',
					action: 'viewFullBudgetBreakdownButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
					text: 'View full budget breakdown'	
				}]
			}]
		}]
	}
});