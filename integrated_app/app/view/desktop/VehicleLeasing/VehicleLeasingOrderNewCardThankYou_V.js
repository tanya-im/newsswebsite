// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingOrderNewCardThankYou_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingOrderNewCardThankYou_D',
    config: {
		itemId :'vehicleLeasingOrderNewCardThankYouPageId',
		id:'vehicleLeasingOrderNewCardThankYouPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Order a new card',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls: 'fa-vl border-10-white',
					items: [{				
						xtype: 'container',
						cls :'font-normal font-14 bgE7E7E7 pad-33 txt-center',
						html : 'Thank you for your request.We will call you back as ASAP.'
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'reportLostStolenCardButton',
					ui: 'none',
					hidden:true,
					action: 'reportLostStolenCardButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Report lost/stolen card'
				},{
					xtype: 'button',
					itemId: 'orderNewCardButton',
					ui: 'none',
					action: 'orderNewCardButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Order new card'
				},{
					xtype: 'button',
					itemId: 'manageFuelCardsButtonPressThankYouPage',
					action: 'manageFuelCardsButtonPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage Fuel Cards'
				}]
			}]
		}]
	}
});