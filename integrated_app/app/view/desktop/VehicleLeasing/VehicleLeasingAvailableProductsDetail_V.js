// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingAvailableProductsDetail_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingAvailableProductsDetail_D',
    config: {
		itemId :'vehicleLeasingAvailableProductsDetailPageId',
		id:'vehicleLeasingAvailableProductsDetailPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				items:[{
					xtype: 'toolbar',
					title: 'View Available Products',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},{	
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					items:[{
						xtype: 'container',
						itemId:'productNameLabel',
						cls: 'f-bold font-18 margin-bottom-10'
					},{
						xtype: 'container',
						cls: 'font-14 margin-bottom-15',
						html: '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.And more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>'
					},{
						xtype: 'container',
						cls:'fa-vl',
						items: [{
							xtype: 'button',
							itemId: 'availableProductsDetailPageSendEnquiryBtnPress',
							cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',					text: 'Send enquiry',
						}]	
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'backToManageMyBudgetsButton',
					ui: 'none',
					action: 'goToVehicleLeasingAvailableProducts',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Available Products'
				},{
					xtype: 'button',
					itemId: 'MyProductsButton',
					action: 'MyProductsButtonPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'My Products'
				}]
			}]
		}]
	}
});