Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingAvailableProducts_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingAvailableProducts_D',
    config: {
		itemId :'vehicleLeasingAvailableProductsPageId',
		id:'vehicleLeasingAvailableProductsPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'panel',
				items:[{
					xtype: 'toolbar',
					title: 'Available Products',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls: 'fa-vl',
					items: [{
						xtype: 'container',
						itemId: 'availableProductsListing'
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'backToManageMyBudgetsButton',
					ui: 'none',
					action: 'goToVehicleLeasingAvailableProducts',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Available Products'
				},{
					xtype: 'button',
					itemId: 'MyProductsButton',
					action: 'MyProductsButtonPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'My Products'
				}]
			}]
		}]
	}
});