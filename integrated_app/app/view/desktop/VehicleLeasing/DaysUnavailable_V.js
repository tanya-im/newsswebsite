// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.DaysUnavailable_V', {
    extend: 'Ext.Container',
	alias: 'widget.daysUnavailable_D',
    config: {
		itemId :'daysUnavailablePageId',
		id:'daysUnavailablePageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items:[{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Days Unavailable',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			cls :'pad-10',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls :'container_vl_wrap',
					items:[{
						xtype: 'container',
						cls :'bgwhite border-bottom',
						items:[{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-10',
							items:[{
								xtype: 'label',
								cls:'pad-10 font-18',
								itemId:'vehilceNo',
								html: 'Vehicle:xxxx',
								flex:1
							},{
								xtype: 'fieldset',
								flex:1,
								cls:'selectbox_vl_2 selectbox_vl',
								items: [{
									xtype: 'selectfield',
									cls:'expenditureType',
									options: [{text: 'Vehicle One',  value: 'Vehicle One'},
										{text: 'Vehicle Two', value: 'Vehicle Two'},
										{text: 'Vehicle Three',  value: 'Vehicle Three'}]
								}]
							}]
						}]
					},{
						xtype: 'container',
						cls :'bgE7E7E7 pad-10 font-14',
						items:[{
							xtype: 'container',
							items:[{
								xtype: 'container',
								cls :'vborder',
								items:[{
									xtype: 'container',
									cls :'pad-10-0',
									items:[{
										xtype: 'label',
										itemId:'residualValue',
										html:"Please enter the number of days unavailable"
									}]	  
								},{
									xtype: 'container',
									items:[{
										xtype: 'textfield',
										itemId: 'daysUnavailable',
										cls:'input_wrap_vl',
										name: 'daysUnavailable'
									}]
								}]
							}]
						}]
					},{
						xtype: 'container',
						cls :'margin-10',
						docked: 'bottom',
						items:[{
							xtype: 'button',
							itemId: 'daysUnavailableButton',
							ui: 'none',
							action: 'daysUnavailableButtonPress',
							cls:'button_greyblue_vl claimStepButton fa fa-chevron-rightn txt-left font-16 pad-10',
							text: 'Submit'
						}]
					}]	
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'vehicleDetails',
					action: 'vehicleDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
					text: 'Vehicle Details'	
				},{
					xtype: 'button',
					itemId: 'leaseDetails',
					action: 'leaseDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-10-0',
					text: 'Lease Details'
				},{
					xtype: 'button',
					itemId: 'FBTDetails',
					action: 'FBTDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-10-0',
					text: 'FBT Details'
				}]
			}]
		}]
	}
});