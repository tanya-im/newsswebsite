Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingDaysUnavailableConfirmScreen_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingDaysUnavailableConfirmScreen_D',
	config: {
		itemId :'vehicleLeasingDaysUnavailableConfirmScreenPageId',
		id:'vehicleLeasingDaysUnavailableConfirmScreenPageId',
		cls: 'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Vehicle Days Unavailable',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls: 'pad-15-20 bgwhite',
					items:[{
						xtype: 'container',
						cls: 'font-14 color-2B2F3B',
						items: [{
							xtype: 'container',
							html: 'Please Confirm that:'
						},{
							xtype: 'container',
							itemId:'confirmText',
							cls:'margin-top-10'
						}]
					},{		
						xtype: 'container',
						cls: 'fa-vl margin-top-20',
						items: [{
							xtype: 'button',
							itemId: 'daysUnavailableConfirmBtnPress',
							action:'daysUnavailableConfirmBtnPress',
							cls: 'button_greyblue_vl claimStepButton txt-left font-16 pad-10 fa fa-chevron-right',
							text: 'Confirm'
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'vehicleDetails',
					action: 'vehicleDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Vehicle Details'	
				},{
					xtype: 'button',
					itemId: 'leaseDetails',
					action: 'leaseDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Lease Details'
				},{
					xtype: 'button',
					itemId: 'FBTDetails',
					action: 'FBTDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'FBT Details'
				}]
			}]
		}]
	}
});