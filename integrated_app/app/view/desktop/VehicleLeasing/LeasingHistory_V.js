// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.LeasingHistory_V', {
    extend: 'Ext.Container',
	alias: 'widget.leasingHistory_D',
    config: {
		itemId :'leasingHistoryPageId',
		id:'leasingHistoryPageId',
		cls:'container_vl bgE7E7E7',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Leasing History',
				cls: 'font-white font-18 pad-5'
			}]
		},{	
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					itemId:'leasinghistory', 
					cls :'font-16 f-bold color-2B2F3B vehicleDetail'
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'leasingHistory',
					action: 'leasingHistoryPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text:'Leasing History'
				}]
			}]
		}]
	}
});