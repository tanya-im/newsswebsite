// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingLostOrStolenCardsWhatHappenOptions_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingLostOrStolenCardsWhatHappenOptions_D',
    config: {
		itemId :'vehicleLeasingLostOrStolenCardsWhatHappenOptionsPageId',
		id:'vehicleLeasingLostOrStolenCardsWhatHappenOptionsPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Report a lost or stolen card',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					items: [{
						xtype: 'container',
						cls:'fa-vl bgE7E7E7 border-10-white margin-bottom-15 pad-19',
						itemId:'pressdeCradNo',
						id:'pressdeCradNo'
					},{
						xtype: 'container',
						cls:'fa-vl',
						items: [{
							xtype: 'button',
							text: 'Report the card lost',
							itemId: 'vehicleLeasingReportCardLostBtnPress',
							action: 'ReportCardLostBtnPress',
							cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 margin-bottom-15'
						},{
							xtype: 'button',
							text: 'Report the card Stolen',
							itemId: 'vehicleLeasingReportCardStolenBtnPress',
							action: 'ReportCardStolenBtnPress',
							cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 margin-bottom-15',
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'reportLostStolenCardButton',
					ui: 'none',
					hidden:true,
					action: 'reportLostStolenCardButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Report lost/stolen card'
				},{
					xtype: 'button',
					itemId: 'orderNewCardButton',
					ui: 'none',
					action: 'orderNewCardButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Order new card'
				},{
					xtype: 'button',
					itemId: 'manageFuelCardsButtonPressLostOrStolenCard',
					action: 'manageFuelCardsButtonPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage Fuel Cards'
				}]
			}]
		}]
	}
});