// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingOrderNewCard_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingOrderNewCard_D',
    config: {
		itemId :'vehicleLeasingOrderNewCardPageId',
		id:'vehicleLeasingOrderNewCardPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Order a new card',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls: 'fa-vl',
					items: [{				
						xtype: 'container',
						cls :'font-normal font-14',
						items:[{
							xtype: 'label',
							html: '<p>Please confirm your personal details:</p>',
						},{
							xtype: 'container',
							items:[{
								xtype: 'container',
								layout:'hbox',
								cls:'m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Name:',
									flex:1
								},{
									xtype: 'label',
									itemId:'user_name',
									cls:'txt-left',
									flex:6
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Email:',
									flex:1
								},{
									xtype: 'label',
									itemId:'user_email',
									cls:'txt-left',
									flex:6
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Phone:',
									flex:1
								},{
									xtype: 'label',
									itemId:'user_contact',
									cls:'txt-left',
									flex:6
								}]
							}]	
						}]
					},{
						xtype: 'container',
						cls:'fa-vl margin-10-0',
						items: [{
							xtype: 'button',
							text: 'Submit',
							itemId: 'vehicleLeasingOrderNewCardSubmitBtnPress',
							cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[
				{	
					xtype: 'container',
					cls :'height50 margin-bottom-5-for-vehicle',
					items:[{
						xtype: 'container',
						items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl',
							itemId:'selectVContainer',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType_1 vlabel',
								usePicker:false,
								itemId:'selectVehicleOrderNewCard', 
								label: 'Vehicle :'  
							}]
						}]
					}]
				},
				{
					xtype: 'button',
					itemId: 'reportLostStolenCardButton',
					ui: 'none',
					hidden:true,
					action: 'reportLostStolenCardButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Report lost/stolen card'
				},{
					xtype: 'button',
					itemId: 'orderNewCardButton',
					ui: 'none',
					action: 'orderNewCardButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Order new card'
				},{
					xtype: 'button',
					itemId: 'manageFuelCardsButtonPress',
					action: 'manageFuelCardsButtonPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage Fuel Cards'
				}]
			}]
		}]
	}
});