// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehicleLeasingLostOrStolenCards_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingLostOrStolenCards_D',
    config: {
		itemId :'vehicleLeasingLostOrStolenCardsPageId',
		id:'vehicleLeasingLostOrStolenCardsPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Report a lost or stolen card',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					items:[{
						xtype: 'label',
						itemId:'vehicleNameHeader',					
						cls: 'f-bold font-16'
					},{
						xtype: 'container',
						cls:'fa-vl pad-top-10',
						html:'Please note that we will process this request during business hours.'
					},{
						xtype: 'container',
						cls :'margin-top-20 expenselist_vl_wrap border-10-bottom-0',
						items:[{
							xtype:'list',
							cls :'expenseList expenselist_vl fa-vl',
							height:'210px',
							itemId: 'vehicleCardsListingLSPage',
							itemTpl: '<div class="fa_1 fa-chevron-right"><span class="font-helveticalight">{cardNumber}</span></div>',						 
							mode : 'SINGLE',
							listeners:{	
							painted: function(comp,eOpts) {
								this.getScrollable().getScroller().on('scrollstart',function(){
								Ext.getCmp('vehicleLeasingLostOrStolenCardsPageId').setScrollable(false);
								});
								this.getScrollable().getScroller().on('scrollend',function(){
								Ext.getCmp('vehicleLeasingLostOrStolenCardsPageId').setScrollable(true);
								});
							}}
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'reportLostStolenCardButton',
					ui: 'none',
					hidden:true,
					action: 'reportLostStolenCardButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Report lost/stolen card'
				},{
					xtype: 'button',
					itemId: 'orderNewCardButton',
					ui: 'none',
					action: 'orderNewCardButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Order new card'
				},{
					xtype: 'button',
					itemId: 'manageFuelCardsButtonPressLostOrStolenCard',
					action: 'manageFuelCardsButtonPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage Fuel Cards'
				}]
			}]
		}]
	}
});