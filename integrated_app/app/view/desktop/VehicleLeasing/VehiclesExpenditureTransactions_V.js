// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehiclesExpenditureTransactions_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehiclesExpenditureTransactions_D',
    config: {
		itemId :'vehiclesExpenditureTransactionsPageId',
		id:'vehiclesExpenditureTransactionsPageId',
		cls :'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Your Transactions',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls :'pad-bottom-10',
					items:[{
						xtype: 'container',
						items:[{
							xtype: 'label',
							cls:'txt-left pad-bottom-10',
							html: 'Transactions For'
						},{
							xtype: 'fieldset',
							cls:' selectbox_vl',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType',
								itemId: 'vehicleExpenditureTypes',
								usePicker:false, 
								options: []
							}]
						}]
					},{
						xtype: 'container',
						cls :'pad-10-0',
						items:[{
							xtype: 'container',
							items:[{
								xtype: 'container',
								id:'transactionsFromCalendar',
								itemId:'transactionsFromCalendar'
							},{
								xtype: 'container',
								id:'transactionsToCalendar',
								itemId:'transactionsToCalendar'
							}]
						}]
					},{
						xtype: 'container',
						cls :'fa-vl',
						items:[{
							xtype: 'button',
							itemId: 'vehicleTransactionsDatesButton',
							ui: 'none',
							action: 'vehicleTransactionsDatesButtonPress',
							cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
							text: 'Submit'
						}]
					},{
						xtype: 'container',
						cls :'bgwhite margin-top-20 expenselist_vl_wrap border-10-bottom-0',
						items:[{
							xtype:'list',
							height:'301px',
							itemId: 'vehicleExpenditureTransactionList',
							loadingText :null,
							mode : 'SINGLE',
							cls :'expenseList expenselist_vl expenselistTooltip',
							itemTpl:'<div class="normalTip" title="<p>Date: {transactionDate}</br>Desc: {description}</br>Amount: {amount}</br></p>">'
								+'<div class="vehicleRegistrationList clearfix">'
									+'<div>'
										+'<div class="expenseDesc floatLeft">'
											+'<div class="expenseDate">{transactionDate} </div>'
											+'<div class="clearfix"></div><div class="expdetail">{description}</div>'
										+'</div>'
										+'<div class="expenseAmountTransaction pad-top-10">{amount}</div>'
									+'</div>'	
								+'</div>'
							+'</div>'	,
							listeners: {
								painted: function(comp,eOpts) {
									this.getScrollable().getScroller().on('scrollstart',function(){Ext.getCmp('vehiclesExpenditureTransactionsPageId').setScrollable(false);});
									this.getScrollable().getScroller().on('scrollend',function(){Ext.getCmp('vehiclesExpenditureTransactionsPageId').setScrollable(true);});
									
								},
								refresh: function(){
									applyTooltip();
								}
						   },
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[
				{	
					xtype: 'container',
					cls :'height50 margin-bottom-5-for-vehicle',
					items:[{
						xtype: 'container',
						items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl',
							itemId:'selectVContainer',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType_1 vlabel',
								usePicker:false,
								itemId:'selectVehicleExpenditureTransaction', 
								label: 'Vehicle :'  
							}]
						}]
					}]
				},	
				{
					xtype: 'button',
					itemId: 'leasingOverviewButton',
					ui: 'none',
					action: 'leasingOverviewButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Expenditure Overview'	
				},{
					xtype: 'button',
					itemId: 'fullTransactionsButton',
					ui: 'none',
					action: 'fullTransactionsButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Full Transactions'
				},{
					xtype: 'button',
					itemId: 'manageMyBudgetButton',
					ui: 'none',
					flex:1,
					action: 'manageMyBudgetButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage My Budget'
				},{
					xtype: 'button',
					itemId: 'viewFullBudgetBreakdownButton',
					ui: 'none',
					action: 'viewFullBudgetBreakdownButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View full budget breakdown'	
				}]
			}]
		}]	
	}
});