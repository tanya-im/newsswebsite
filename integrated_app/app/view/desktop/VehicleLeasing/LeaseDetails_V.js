// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.LeaseDetails_V', {
    extend: 'Ext.Container',
	alias: 'widget.leaseDetails_D',
    config: {
		itemId :'leaseDetailsPageId',
		id:'leaseDetailsPageId',
		cls :'container_vl bgE7E7E7',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
    	},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
					xtype: 'toolbar',
					title: 'Lease Details',
					cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls :'bgE7E7E7 font-16 f-bold color-2B2F3B height100 vehicleDetail',
					items:[{
						xtype: 'container',
						cls :'bgwhite shadow_vl pad-10 font-normal',
						items:[{
							xtype: 'container',
							cls :'vborder',
							items:[{
								xtype: 'container',
								cls :'bgwhite',
								items:[{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-bottom-7 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'Lease Reference Number:',
										flex:1
									},{
										xtype: 'label',
										itemId:'leaseReferenceNumber',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-bottom-7 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'Financier:',
										flex:1
									},{
										xtype: 'label',
										itemId:'financier',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-bottom-7 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'Lease Start:',
										flex:1
									},{
										xtype: 'label',
										itemId:'leaseStart',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-bottom-7 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'Lease End:',
										flex:1
									},{
										xtype: 'label',
										itemId:'leaseEnd',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 margin-top-7 pad-bottom-7 font-14 f-2c2f3b greyborder_vl',
									items:[{
										xtype: 'label',
										html: 'Refinance Date:',
										flex:1
									},{
										xtype: 'label',
										itemId:'refinanceDate',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								}]	  
							},{
								xtype: 'container',
								cls :'bgwhite',
								items:[{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-top-10 pad-bottom-7 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'Residual Value:',
										flex:1
									},{
										xtype: 'label',
										itemId:'residualValue',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-bottom-7 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'Net Amount:',
										flex:1
									},{
										xtype: 'label',
										itemId:'netAmount',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-bottom-7 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'GST Amount:',
										flex:1
									},{
										xtype: 'label',
										itemId:'gstAmount',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-bottom-7 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'Total:',
										flex:1
									},{
										xtype: 'label',
										itemId:'total',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 pad-bottom-7 margin-bottom-10 font-14 f-2c2f3b',
									items:[{
										xtype: 'label',
										html: 'Frequency:',
										flex:1
									},{
										xtype: 'label',
										itemId:'frequency',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:"xxxxxx"
									}]
								}]	
							}]
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'container',
					cls:'margin-bottom-15',
					items:[{
						xtype: 'fieldset',
						flex:1,
						cls:'selectbox_vl',
						items: [{
							xtype: 'selectfield',
							cls:'expenditureType_1 vlabel',
							usePicker:false,
							label: 'Vehicle :',
							itemId:'selectVehicle', 
							options: [{text: 'Vehicle One',  value: 'Vehicle One'},
								{text: 'Vehicle Two', value: 'Vehicle Two'},
								{text: 'Vehicle Three',  value: 'Vehicle Three'}]
						}]
					}]
				},{
					xtype: 'button',
					itemId: 'vehicleDetails',
					action: 'vehicleDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Vehicle Details'	
				},{
					xtype: 'button',
					itemId: 'leaseDetails',
					action: 'leaseDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Lease Details'
				},{
					xtype: 'button',
					itemId: 'FBTDetails',
					action: 'FBTDetailsPress',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'FBT Details'
				}]
			}]
		}]
	}
});