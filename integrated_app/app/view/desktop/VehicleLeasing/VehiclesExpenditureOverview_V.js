// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.VehiclesExpenditureOverview_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehiclesExpenditureOverview_D',
    config: {
		itemId :'vehiclesExpenditureOverviewPageId',
		id:'vehiclesExpenditureOverviewPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
    	},
		items:[{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Vehicles Expenditure Overview',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					layout: 'hbox',
					cls:'margin-bottom-10',
					items:[{
						xtype: 'container',
						flex:1,
						itemId:'currentFilterLabel',
						html: 'Last 12 Months',
						cls:'pad-12-0-5-0 font-14'
					},{
						xtype: 'container',
						flex:1,
						items:[{
							xtype: 'button',
							itemId: 'vEchangeDatesButton',
							ui: 'none',
							action: 'vEchangeDatesButtonPress',
							cls:'claimStepButton changedates_dd_dn txt-left font-16',
							text: 'Change Dates'
						}]
					}]
				},{
					xtype: 'container',
					cls:'bgwhite margin-bottom-10',
					items:[{
						xtype: 'container',
						cls :'fa-vl pad-10',
						hidden:true,
						id:'vehicleExpenditureDateContainer',
						items:[{
							xtype: 'button',
							itemId: 'liveToDateButton',
							ui: 'none',
							action: 'vehicleExpenditureDateChangeButtonPress',
							cls:'button_ltgrey_vl txt-left font-16 pad-10',
							text: 'Live To Date'
						},{
							xtype: 'button',
							itemId: 'last12monthsButton',
							ui: 'none',
							action: 'vehicleExpenditureDateChangeButtonPress',
							cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
							text: 'Last 12 Months'
						},{
							xtype: 'button',
							itemId: 'fBTYearButton',
							ui: 'none',
							action: 'vehicleExpenditureDateChangeButtonPress',
							cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
							text: 'FBT Year'
						},{
							xtype: 'button',
							itemId: 'vEcustomDateRangeButton',
							id:'vEcustomDateRangeButton',
							ui: 'none',
							action: 'vehicleExpenditureDateChangeButtonPress',
							cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
							text: 'Custom Date Range'
						}]
					},{
						xtype: 'container',
						cls :'fa-vl pad-10',
						id:'vehicleExpenditureDateRange',
						hidden:true,
						items:[{
							xtype: 'container',
							id:'vehiclesExpenditureFromCalendar',
							itemId:'vehiclesExpenditureFromCalendar'
						},{
							xtype: 'container',
							id:'vehiclesExpenditureToCalendar',
							itemId:'vehiclesExpenditureToCalendar'
						},{
							xtype: 'button',
							itemId: 'vehiclesExpenditureDatesButton',
							ui: 'none',
							action: 'vehiclesExpenditureDatesButtonPress',
							cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',	
							text: 'Submit'
						}]
					}]
				},{
					xtype: 'container',
					itemId: 'expenses',
					cls:'bgwhite pad-0-10 font-14'
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'container',
					cls :'height50 margin-bottom-5-for-vehicle',
					items:[{
						xtype: 'container',
						items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl',
							itemId:'selectVContainer',
							items: [{
								xtype: 'selectfield',
								cls:'expenditureType_1 vlabel',
								usePicker:false,
								itemId:'selectVehicle', 
								label: 'Vehicle :' 
							}]
						}]
					}]
				},{
					xtype: 'button',
					itemId: 'leasingOverviewButton',
					ui: 'none',
					action: 'leasingOverviewButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Expenditure Overview'	
				},{
					xtype: 'button',
					itemId: 'fullTransactionsButton',
					ui: 'none',
					action: 'fullTransactionsButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Full Transactions'
				},{
					xtype: 'button',
					itemId: 'manageMyBudgetButton',
					ui: 'none',
					flex:1,
					action: 'manageMyBudgetButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage My Budget'
				},{
					xtype: 'button',
					itemId: 'viewFullBudgetBreakdownButton',
					ui: 'none',
					action: 'viewFullBudgetBreakdownButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View full budget breakdown'	
				}]
			}]
		}]
	}
});