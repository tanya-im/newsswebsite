// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.MyProducts_V', {
    extend: 'Ext.Container',
	alias: 'widget.myProducts_D',
    config: {
		itemId :'myProductsPageId',
		id:'myProductsPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'panel',
				items:[{
					xtype: 'toolbar',
					title: 'My Products',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},{	
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					itemId: 'myProductList',
					cls :'font-16 f-bold color-2B2F3B vehicleDetail' 
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[
				{
					xtype: 'container',
					cls :'height50 margin-bottom-5-for-vehicle',
					items:[{
						xtype: 'container',
						items:[{
							xtype: 'fieldset',
							cls:'selectbox_vl',
							itemId:'selectVContainer',
						}]
					}]
				},
				{
					xtype: 'button',
					itemId: 'backToManageMyBudgetsButton',
					ui: 'none',
					action: 'goToVehicleLeasingAvailableProducts',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Available Products'
				},{
					xtype: 'button',
					itemId: 'MyProductsButton',
					ui: 'none',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'My Products'
				}]
			}]
		}]
	}
});