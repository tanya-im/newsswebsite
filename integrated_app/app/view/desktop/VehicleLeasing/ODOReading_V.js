// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.ODOReading_V', {
    extend: 'Ext.Container',
	alias: 'widget.oDOReading_D',
    config: {
		itemId :'oDOReadingPageId',
		id:'oDOReadingPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Odometer Reading',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					items:[{
						xtype: 'container',
						cls :'font-14 color-2B2F3B',
						items:[{
							xtype: 'container',
							cls :'margin-left-20',
							items:[{
									xtype:'container',
									itemId:'lastReadingDate',
									html:'Last reading (on 20/10/2014)'
								},{
									xtype:'container',
									itemId:'lastReadingId',
									cls: 'txt-center margin-top-10 f-bold',
									html:'22,258 Km'
								},{
									xtype: 'container',
									cls :'pad-10-0',
									items:[{
										xtype: 'label',
										itemId:'residualValue',
										html:"New odometer reading (Km)"
									}]	  
								}]
							},{
								xtype: 'container',
								cls :'vborder',
								items:[{
									xtype: 'container',
									items:[{
										xtype: 'numberfield',
										itemId: 'oDOReading',
										cls:'input_wrap_vl',
										tabIndex :1,
										name: 'oDOReading'
									}]
								},{
									xtype: 'container',
									cls :'margin-top-20 fa-vl',
									items:[{
										xtype: 'button',
										itemId: 'oDOReadingButton',
										ui: 'none',
										action: 'oDOReadingSubmitButtonPress',
										tabIndex :2,
										cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
										text: 'Submit'
									}]
								}]
							}]
						}]
					}]
				},{
					xtype: 'container',
					cls:'pad-left-10 fa-vl margin-left-20',
					flex:2,
					items:[
					{
						xtype: 'container',
						cls :'height50 margin-bottom-5-for-vehicle',
						items:[{
							xtype: 'container',
							items:[{
								xtype: 'fieldset',
								cls:'selectbox_vl',
								itemId:'selectVContainer',
								items: [{
									xtype: 'selectfield',
									cls:'expenditureType_1 vlabel',
									usePicker:false,
									itemId:'selectVehicleODOReading', 
									label: 'Vehicle :'  
								}]
							}]
						}]
					},
					{
						xtype: 'button',
						itemId: 'vehicleDetails',
						action: 'vehicleDetailsPress',
						ui: 'none',
						cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
						text: 'Vehicle Details'	
					},{
						xtype: 'button',
						itemId: 'leaseDetails',
						action: 'leaseDetailsPress',
						ui: 'none',
						cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
						text: 'Lease Details'
					},{
						xtype: 'button',
						itemId: 'FBTDetails',
						action: 'FBTDetailsPress',
						ui: 'none',
						cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
						text: 'FBT Details'
				}]
			}]
		}]
	}
});