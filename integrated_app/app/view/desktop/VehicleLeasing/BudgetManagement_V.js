// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.VehicleLeasing.BudgetManagement_V', {
    extend: 'Ext.Container',
	alias: 'widget.BudgetManagement_D',
    config: {
		itemId :'BudgetManagementPageId',
		id:'BudgetManagementPageId',
		cls:'container_vl',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			items:[{
				xtype: 'toolbar',
				title: 'Fuel Budget Management',
				itemId:'budgetManagement',
				cls: 'font-white font-18 pad-5'
			}]
		},{
			xtype: 'container',
			cls :'pad-10',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls:'fa-vl',
					items:[{
						xtype: 'label',
						cls:'txt-left font-14 margin-bottom-15',
						html: 'Increase Annual Budget:'
					},{
						xtype: 'container',
						cls :'bgwhite font-16 f-bold margin-bottom-15',
						items:[{
							xtype: 'numberfield',
							itemId: 'budgetManagementTextBox',
							name: 'budgetManagementTextBox',
							tabIndex:2,
							placeHolder:'$', 
							cls:'font-12 pad-left-5 input_wrap_vl input_wrap_budgetm_vl'
						}]
					},{
						xtype: 'container',
						cls :'pad-10 margin-bottom-15 bgF9F9F9 font-14',
						layout: 'hbox',
						items:[{
							xtype: 'label',
							cls:'txt-left',
							flex:3,
							tabIndex:3,
							html: 'Life To Date Actual Spend:'	
						},{
							xtype: 'label',
							cls:'txt-right',
							itemId:'actualSpending',
							flex:1,
						}]
					},{
						xtype: 'container',
						cls :'pad-10 bgF9F9F9 font-14 margin-bottom-15',
						layout: 'hbox',
						items:[{
							xtype: 'label',
							cls:'txt-left',
							flex:3,
							html: 'Total Annual Budget:'	
						},{
							xtype: 'label',
							cls:'txt-right',
							flex:1,
							itemId:'totalBudget'
						}]
					},{
						xtype: 'container',
						cls:'fa-vl',
						layout: 'hbox',
						items:[{
							xtype: 'button',
							itemId:'budgetSubmitButtonPress', 
							ui: 'none',
							flex:1,
							action: 'budgetSubmitButtonPress',
							cls:'button_greyblue_vl claimStepButton fa_1 fa-chevron-right txt-left font-16 pad-10 next_btn_vl',
							tabIndex:4,
							data:{"expense":null},
							text: 'Update Budget'
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-left-10 fa-vl margin-left-20',
				flex:2,
				items:[{
					xtype: 'button',
					itemId: 'leasingOverviewButton',
					ui: 'none',
					action: 'leasingOverviewButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Expenditure Overview'	
				},{
					xtype: 'button',
					itemId: 'fullTransactionsButton',
					ui: 'none',
					action: 'fullTransactionsButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View Full Transactions'
				},{
					xtype: 'button',
					itemId: 'manageMyBudgetButton',
					ui: 'none',
					flex:1,
					action: 'manageMyBudgetButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'Manage My Budget'
				},{
					xtype: 'button',
					itemId: 'viewFullBudgetBreakdownButton',
					ui: 'none',
					action: 'viewFullBudgetBreakdownButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-15',
					text: 'View full budget breakdown'	
				}]
			}]
		}]
	}
});