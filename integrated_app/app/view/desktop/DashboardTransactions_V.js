// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.DashboardTransactions_V', {
    extend: 'Ext.Container',
	alias: 'widget.dashboardTransactions_D',
    config: {
		itemId :'dashboardTransactionsPageId',
		id:'dashboardTransactionsPageId',
		cls :'container_vl font-14',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			items:[{
				xtype: 'panel',
				items:[{
					xtype: 'toolbar',
					title: 'Activity Summary',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},{
			xtype: 'container',
			layout:'hbox',
			items:[{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					cls:'fa-vl',
					items:[{
						xtype: 'container',
						cls :'pad-10 bgwhite fa-vl',
						items:[{
							xtype: 'button',
							itemId: 'dashboardTransactionsDatesButton',
							ui: 'none',
							action: 'dashboardTransactionsDatesButtonPress',
							cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-10',
							text: 'Next pay periods'
						},{
							xtype: 'button',
							itemId: 'dashboardManageMyBenefitButton',
							ui: 'none',
							action: 'dashboardManageMyBenefitButtonPress',
							cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-10',
							text: 'Manage My Benefit'
						},{
							xtype: 'fieldset',
							cls:' selectbox_vl',
			            	items: [{
								xtype: 'selectfield',
								cls:'expenditureType',
								itemId:'dashboardExpenditureTypes',
								usePicker:false, 
								options: [ {text: 'First Option',  value: 'first'},
			                        {text: 'Second Option', value: 'second'},
			                        {text: 'Third Option',  value: 'third'}]
			            	}]
						}]
					},{
						xtype: 'container',
						cls :'bgwhite fa-vl pad-10',
						items:[{
							xtype: 'container',
							id:'dashboardTransactionsFromCalendar',
							itemId:'dashboardTransactionsFromCalendar'
						},{
							xtype: 'container',
							id:'dashBoardTransactionsToCalendar',
							itemId:'dashBoardTransactionsToCalendar'
						},{
							xtype: 'button',
							itemId: 'dashboardTransactionsDatesButton',
							ui: 'none',
							action: 'dashboardTransactionsDatesButtonPress',
							cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
							text: 'Submit'
						}]
					},{
						xtype: 'container',
						cls :'bgwhite margin-top-10 expenselist_vl_wrap pad-10',
						items:[{
							xtype:'list',
							height:'400px',
							store: "dashboardTransactionList",
							loadingText :null,
							mode : 'SINGLE',
							cls :'expenseList expenselist_vl fa-vl',
							itemTpl:'<div class="vehicleRegistrationList"><div class="expenseDate">{transactionDate} </div><div class="expdetail clearfix"><div class="expenseName">{description} </div><div class="expenseAmount">{amount}</div></div><div class="fa fa-chevron-right" style="display:block;"></div></div>',
							listeners: {
								painted: function(comp,eOpts) {
									this.getScrollable().getScroller().on('scrollstart',function(){
										Ext.getCmp('dashboardTransactionsPageId').setScrollable(false);
									});
									this.getScrollable().getScroller().on('scrollend',function(){
										Ext.getCmp('dashboardTransactionsPageId').setScrollable(true);
									});
								}
							}
						}]
					}]
				}]
			},{
				xtype: 'container',
				cls:'fa-vl margin-left-20 ',
				flex:2,
				items:[{
					xtype: 'container',
					itemId:'transectionAcDetail',
					cls:'pad-10 bgwhite margin-bottom-10',
					items:[{
					}]
				},{
					xtype: 'container',
                    height:'220px',
                    cls:'pad-10 bgwhite',
                    items:[ {
                        xtype: 'label',
                        html:'Last pay cycle expenditure', 
                        cls:'font-14'    
                    },{
                        xtype: 'container', 
                        layout:'fit', 
                        height:'180px',
                        items:[{ 
                            id:'pieChartIn3D_DashboardTransaction', 
                            itemId:'pieChartInBudgetDashboardTransaction',
                            xtype: 'polar',
                            width: '100%',  
                            interactions: 'rotatePie3d',
                            store: {fields: ['name', 'budget' ],
                                data: [{'name':'Jan', 'budget':10 },
                                {'name':'Feb', 'budget':7},
                                {'name':'March','budget':5},
                                {'name':'Apr', 'budget':2},
                                {'name':'May', 'budget':10 },
                                {'name':'June', 'budget':6},
                                {'name':'July', 'budget':8 }]
                            },
                            colors: ['#4F2C7E','#EB2939','#0070BB','#E88200','#ABDDE4','#B0B3B1','#BCD400'],
                            animate: {duration: 1000,easing: 'easeInOut'},
                            series: [{type: 'pie3d',field: 'budget',donut: 50,distortion: 0.4,
                            			style:{stroke: "white",strokeStyle: 'white',opacity: 0.90}
                            }]
                        }]
                    }]
				},{
					xtype: 'container',
					cls:'csvButton',
					items:[{
						xtype: 'button',
						itemId: 'dashboardTransactionsDatesButton',
						ui: 'none',
						action: 'dashboardTransactionsDatesButtonPress',
						cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
						text: 'Export To CSV'
					},{
						xtype: 'button',
						itemId: 'dashboardTransactionsDatesButton',
						ui: 'none',
						action: 'dashboardTransactionsDatesButtonPress',
						cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
						text: 'Print View'
					}]
				}]	
			}]
		}]			
	}
});