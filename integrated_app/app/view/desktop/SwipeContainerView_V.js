Ext.define('SmartSalary.view.desktop.SwipeContainerView_V', {
	extend: 'Ext.Container',
	xtype: 'app_viewport',
	requires: ['Ext.TitleBar'],
	config: {
		fullscreen: true,
		layout: 'hbox',
		items : [
		{
			xtype: 'accordionlist_D',
			store: Ext.create('SmartSalary.store.SlideList_S'),
			cls:'nav-list',
			width:250,
			id:'basic',
			itemId: 'basic',
			listeners: {
				initialize: function() {
					this.load();
				}
			}
		},
		{
			xtype : 'homeview_T',
			cls: 'slide',
			width: '100%'												
		}]
	}
});