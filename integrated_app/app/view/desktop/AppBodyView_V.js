Ext.define('SmartSalary.view.desktop.AppBodyView_V', {
	extend: 'Ext.Container',
	xtype: 'app_viewport',
	requires: ['Ext.TitleBar'],
	config: {
		fullscreen: true,
		layout: 'hbox',
		items : [
		{
			xtype : 'homeview_D',
			cls: 'slide',
			width: '100%'												
		}]
	}
});