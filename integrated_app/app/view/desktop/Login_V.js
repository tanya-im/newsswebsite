// View file for login page. This file create only html for the app no logic exist here.  
Ext.define('SmartSalary.view.desktop.Login_V', {
    extend: 'Ext.Container',
    alias: "widget.loginView_D",
    requires: ['Ext.util.DelayedTask'],
    config: {
        title: 'Login',
		id:'loginScreen',
		itemId:'loginScreen',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
        items:[
		{
           	xtype: 'panel',
			cls:'bgwhite',
           	items:[
			{
				xtype:'label',
				html:'Login',
				width:'100%',
				cls:'logintxt font-18'
			}]
        },
		{
			xtype:'container',
			cls:'pad-0-10-p bgwhite',
			items:[
			{
				xtype: 'label',
				html: 'Login failed. Please enter the correct credentials.',
				itemId: 'signInFailedLabel',
				hidden: true,
				hideAnimation: 'fadeOut',
				showAnimation: 'fadeIn',
				cls: 'loginemptymsg font-12'
			},
			{
				xtype:'label',
				itemId : 'loginform',
				html:'Enter your details below',
				cls:'pad-10 bgwhite font-14'
			}]
		},
		{
			xtype:'panel',
			cls:'pad-0-10-p bgEEEEEE',
			items:[
			{
				xtype:'label',
				html:'Username',
				cls:'bgtransparent pad-top-10 pad-left-20 font-12'
			},
			{
				xtype:'emailfield',
				itemId: 'userNameTextField',
                name: 'userNameTextField',
                required: true,
				cls:'margin-5-10 font-12 pad-left-5'
			},
			{
				xtype:'label',
				html:'Password',
				cls:'bgtransparent pad-top-10 pad-left-20 font-12'
			},
			{
				xtype:'passwordfield',
				itemId: 'passwordTextField',
                name: 'passwordTextField',
				cls:'margin-5-10 font-12 pad-left-5',
                required: true
			},
			{
				xtype: 'button',
				itemId: 'logInButton',
				ui: 'none',
				action: 'loginpress',
				cls:'loginbtn txt-left font-14 pad-10 m-20-10',
				text: 'Login'
			},
			{
				xtype: 'button',
				itemId: 'forGotButton',
				ui: 'none',
				cls:'forgobtn txt-left font-14 pad-10 margin-bottom-15 margin-10',
				text: 'Forgotten your password?'
			}]
		},
		{
           	xtype: 'panel',
			cls:'bgEEEEEE bdr-15-top pad-10 pad-bottom-20 pad-0-10-p',
			height:'150px',
           	items:[
			{
				xtype:'label',
				html:'New to Smartsalary?',
				cls:'pad-10 font-14'
			},
			{
				xtype: 'button',
				itemId: 'RegisterNowButton',
				ui: 'none',
				cls:'registerbtn txt-left font-14 pad-10 margin-0-10',
				text: 'Register now'
			}]
        }]
    }
});