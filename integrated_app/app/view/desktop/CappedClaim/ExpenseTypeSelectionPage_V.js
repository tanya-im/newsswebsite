// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.CappedClaim.ExpenseTypeSelectionPage_V', {
    extend: 'Ext.form.Panel', 
    alias : 'widget.CappedClaimStep1_D', 
    config: {
        id:"CappedClaimStepOnePageId",
        itemId:"CappedClaimStepOnePageId",    
        scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },         
        items: [{
                xtype: 'container',
                layout:'hbox',
                cls :'claimListheader',
                items:[
                {
                    xtype: 'panel',
                    flex:2,
                    items:[
                    {
                        xtype: 'toolbar',
                        title: 'Reimbursement claim for tax-free cap expenses',                         
                        cls: 'font-white font-18 pad-5'
                    }]
                }]
            },
            {
                xtype: 'container',
                cls :'pad-10 bgE7E7E7 font-16 txt-center header_wrap_grey',
                layout: 'hbox',
                items:[
                {
                    xtype: 'image',
                    //src:'resources/images/SS04-Mobile-Claims-Vehicle-1.png',
                    cls:'Claims-Vehicle-steps-img step1_img'
                }]
            }, 
            {
                xtype: 'container',                 
                cls :'pad-10 bgwhite',
                layout: 'hbox',
                items:[{
                        xtype: 'container',                        
                        cls :'pad-20 txt-center bg4e2683 font-white margin-right-5',
                        flex:5,
                        items:[
                            {
                                xtype: 'label',
                                html: 'Amount left for text-free cap expenses before end of FBT year (31 March)',
								cls:'font-16 pad-bottom-10'                                                
                            },

                            {
                                xtype: 'label',
                                itemId:'remainingClaimAmountLabel',
                                cls:' font-26'
                            }
                        ] 
                },{
                    xtype: 'container', 
                    width: '100%',
                    cls:'gray_box pad-20 margin-left-5',
                    flex:5,                
                    items: [
                        {
                            xtype: 'label',
                            html: '<b>Note:</b> If your claim, or a portion of your claim, exceeds the tax-free cap limit, that amount will be carried forward to the next FBT year'                                                
                        }
                    ]
                }] 
            },
            {
                xtype: 'container',
                cls :'pad-10 bgwhite border-bottom',
                items:[
                {
                    xtype: 'label',
                    html: 'Type of reimbursement claim',
                    cls: 'font-16 pad-5 font-bold'
                },
                {
                    xtype: 'label',
                    html: '<p>Select the item(s) you are claiming from the options:</p>',
                    cls: 'font-12 colorstyle1 pad-left-5'
                }]
            },{
                xtype: 'container',
                 
                cls:'bgwhite',
                itemId: 'cappedClaimExpTypeWrapper'
            }
            ,{
                xtype: 'container',
                cls :'pad-10 bgE7E7E7',
                layout: 'vbox', 
                items:[                
                {
                    xtype: 'container',
                    cls: 'font-16',                     
                    items:[
                    {
                        xtype: 'button',
                        itemId:'cappedClaimStepOneNextButton',
                        ui: 'none',
                        hidden:true,                        
                        action:'cappedClaimStepOneToStepThirdButtonPress',
                        cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-bottom-10',
                        text: 'Skip'
                    },{
                        xtype: 'button',
                        ui: 'none',                        
                        action:'cappedClaimStepOneCancelBtnPress',
                        cls:'claimStepLeftArrow claimStepButton txt-left font-16 pad-10',
                        text: 'Cancel'
                    }]
                }]
            }] 
    } 
 
});
