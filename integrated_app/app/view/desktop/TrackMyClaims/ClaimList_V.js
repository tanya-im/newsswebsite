// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.TrackMyClaims.ClaimList_V', {
    extend: 'Ext.form.Panel',
    alias : 'widget.claimList_D',    
    config: {
        id:"claimlistscreen",
        itemId:"claimlistscreen",      
        scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },         
        layout:{ type:'vbox' },
        items: [{
           	    xtype: 'container',
                layout:'hbox',
            	cls :'claimListheader',
                items:[{
                    xtype: 'panel',
                    flex:4,
                    items:[
                    {
                        xtype: 'toolbar',
                        title: 'Track My Claim',                         
                        cls: 'font-white font-18 pad-5'
                    }]
                },{
					xtype:"button",
					itemId:"claimListBackBtn",
					id:'claimListBackBtn',
					flex:1,
					html:'Back',
					cls: 'font-white searchlistbackbtn'
				}]
            },
            {
				xtype: 'panel', 
                width: '100%',
                cls:'gray_box claimListContainerFiltersT',                
                items: [
                    {
                        xtype: 'container',  
                        layout:'hbox',
                        items:[
                            {
                                xtype: 'label',
                                html: 'Filter claims',
                                cls:'fliterClaimLabel',
                                flex:4                                 
                            },
                            {
                                xtype: 'label',
                                cls: 'arrow-up fliterClaimLabelToggleBtn',
                                id:'toggleBtn',
                                itemId:'toggleBtn',
                                width:'12px',
                                height:'12px', 
                                
                            }
                        ],
                        listeners : {
                            element : 'element',
                            tap : function (){
                                SmartSalary.app.getApplication().getController('ClaimList_C'). onSlideInSlideOutRequest()
                            }
                        }
                    },                     
                    {
                        xtype: 'container',
                        type: 'card',          
                        id:'filterSection',
                        itemId:'filterSection',
                        items:[
                        {
                            xtype: 'container',  
                            layout:'hbox',
                            cls:'gray_box margin-top-10 pad-left-10 pad-right-10',
                            items: [
                                {
                                    xtype: 'selectfield',
                                    label: 'Type of claim:',
                                    cls:'fieldHeight pad-top-10',
									data: {"initdata":true},
                                    flex:2.8,
                                    id:'selectClaimType',
                                    labelCls:'selectLabelClsT txt-right',
                                    itemId:'selectClaimType',
                                    width:'100%',
                                    labelWidth:'auto' 
                                },
                                {
                                    xtype: 'selectfield',
                                    label: 'Status:',
                                    cls:'fieldHeight pad-top-10 pad-left-10',
									data: {"initdata":true},
                                    labelCls:'selectLabelClsT txt-right',
                                    id:'selectStatus',
                                    itemId:'selectStatus',
                                    flex:2.1,                                    
                                    width:'100%',
                                    labelWidth:'auto' 
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Date:',
                                    cls:'fieldHeight pad-top-10 pad-left-10',
									data: {"initdata":true},
                                    labelCls:'selectLabelClsT txt-right',
                                    id:'selectDate',
                                    placeHolder: 'DD/MM/YYYY',
									flex:2,
                                    itemId:'selectDate',
                                    width:'100%',
                                    labelWidth:'auto' 
                                }
                            ]
                        },{
							xtype: 'container',  
                            layout:'hbox',
                            cls:'gray_box margin-top-10 pad-left-10 pad-right-10',
							items:[{
								xtype: 'panel',
								flex:1,
								width: '100%',
								cls:'claimListSearchContainerT',
								items: [{
									xtype: 'fieldset',
									title: 'Search by reference number',
									layout:'hbox',
									cls:'searchFieldset',
									items:[{
										xtype: 'textfield',                                
										placeHolder :"Enter reference number", 
										name: 'query',
										flex:8,                                 
										id:'searchField',
                                        itemId:'searchField',
										cls:'fieldHeight',
										name:'searchField'                                  
									},{
										xtype: 'image',                                
										name: 'srchBtn',
										src:'resources/images/search-icon.png',
										flex:1,
										cls:'searchBtn',
										itemId:'srchBtn',
										id:'srchBtn' 
									}]
								}]
							},{
								xtype: 'container',
								cls :'claimListAdminMessage',
								flex:1,
								items:[{
										xtype: 'label',
										id:'claimListMsgId',
                                        itemId:'claimListMsgId',
										cls: 'pad-top-20 font-14 f-darkgrey pad-5'
								}]
							}]
						}]
                    }]
            }
            ,{
             	
				xtype:'list',
		    	flex:6,
		    	loadingText :null,
                id:'claimlisting_page',  
                itemId:'claimlisting_page',                    
                cls:'claimListContainer tabletSpList pad-10',                    
                itemTpl:'<div class="listContainer_aro clearfix"><div class="listContainer listContainer_1"><div class="row1"><div class="claimId">{ClaimReferenceNumber}</div><div class="claimStatus ">{ExpenseStatus}</div><div class="clearboth"></div></div><div class="ClaimType">{ClaimType}</div><div class="date colorstyle1 font-12">Submitted: {DateSubmitted} </div></div><div class="listContainer_aro_right"></div></div>',                
				mode : 'SINGLE',
                listeners: {
                	painted: function(comp,eOpts) {
                    	this.getScrollable().getScroller().on('scrollstart',function(){Ext.getCmp('claimlistscreen').setScrollable(false);});
                        this.getScrollable().getScroller().on('scrollend',function(){Ext.getCmp('claimlistscreen').setScrollable(true);});
                    }
               }
			      
            },{
				
				xtype:'list',
		    	flex:6,
		    	loadingText :null,
				hidden :true,
				store: 'ClaimListFull',
                id:'claimListingFilter',
                itemId:'claimListingFilter',
				emptyText: 'Claim(s) not found',                    
                cls:'claimListContainer tabletSpList pad-10',                    
                itemTpl:'<div class="listContainer_aro clearfix"><div class="listContainer listContainer_1"><div class="row1"><div class="claimId">{ClaimReferenceNumber}</div><div class="claimStatus ">{ExpenseStatus}</div><div class="clearboth"></div></div><div class="ClaimType">{ClaimType}</div><div class="date colorstyle1 font-12">Submitted: {DateSubmitted} </div></div><div class="listContainer_aro_right"></div></div>',
				mode : 'SINGLE',
                listeners: {
                	painted: function(comp,eOpts) {
                    	this.getScrollable().getScroller().on('scrollstart',function(){Ext.getCmp('claimlistscreen').setScrollable(false);});
                        this.getScrollable().getScroller().on('scrollend',function(){Ext.getCmp('claimlistscreen').setScrollable(true);});
                    }
               }
			      
            }]
    }
 
});