// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.TrackMyClaims.ClaimData_V', {
    extend: 'Ext.Container',
	requires: ['Ext.TitleBar'], 
	alias: 'widget.ClaimDataView_D',
    config: {		 
		itemId:'ClaimDataIdTab',
		id :'ClaimDataId',
		layout:'vbox',
		items: [{
                xtype: 'container',
                layout:'hbox',            
                cls :'claimListheader',
                items:[
                {
                    xtype: 'panel',
                    flex:4,
                    items:[
                    {
                        xtype: 'toolbar',
                        title: 'Track My Claim',                      
                        cls: 'font-white font-18 pad-5'
                    }]
                },{
                	xtype:"button",
                	itemId:"dataBackBtn",
                	id:"dataBackBtn",
                	flex:1,
                	html:'Back',
					cls: 'font-white searchlistbackbtn'
                }]
            },{
				xtype: 'container',
				layout:'hbox',            
				cls:'vehicleNumberHeader', 
				items:[{
					xtype: 'panel',
					items:[
					{
						xtype: 'label',
						itemId:'VehicleNumberData',                                                 
						cls: 'font-16 pad-5'
					}]
				}]
			},{
				xtype: 'container',
				flex:3,
				items:[{
					xtype: 'container',
					height:'100%',
					scrollable: {
						direction: 'vertical',
						directionLock: true,
						indicators: {
								y: {
									autoHide: false
								}
						}
					},
					items:[{
						xtype: 'container', 
						layout:'hbox',
						items:[
						{
							xtype:'label',
							itemId:'claimTypeAndRefNum',
							cls:'blueText refnumText font-16', 
							flex:4
						},{
							xtype: 'button',  
							itemId:'reasonBtn',
							text:'Check reasons',  
							ui: 'action',
							flex:1, 
							cls:"reasonBtn"
						}]
	
					},{
						xtype:'container',
						itemId:'claimDataContainer',
						cls:'pad-0-15 font-16'  
					}]
				}]
			}] 
    }
});