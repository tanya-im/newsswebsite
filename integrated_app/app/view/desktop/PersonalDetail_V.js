// View file for login page. This file create only html for the app no logic exist here.  
Ext.define('SmartSalary.view.desktop.PersonalDetail_V', {
    extend: 'Ext.Container',
    alias: "widget.personalDetailView_D",
    requires: ['Ext.util.DelayedTask'],
    config: {
		id:'personalDetailScreenId',
		itemId:'personalDetailScreenId',
        layout:'hbox',
        items:[
            /**Left side Container to hold the Personal detail accordian*/	
       	    {
		        xtype:'container',
		        flex:2,
		        scrollable: {
		            direction: 'vertical',
				    directionLock: true,
		            indicators: {
		                    y: {
		                        autoHide: false
		                    }
		            }
		        },
		        items:[
		        	{
						xtype:'label',
	       			 	html:'Change personal details',
	       			 	cls:''	
		   		 	},
				    
				    /**Accordian Buttons*/    
		 	        {
				        xtype:'container',
	       			 	cls:'',
				        items:[
				            /**Personal detail hide/unhide button*/
				        	{
					        	xtype:'button',
					        	text:'Personal details',
								cls:'',
								id:'btnAccordian1',
								itemId:'btnAccordian1',
								data:{
									buttonNumber:1
								}
				        	},	
				        	/**Personal Detail Hidden Part*/			        	
				        	{
					            xtype:'container',
								cls:'',
								hidden:false,
								itemId:'hideUnhideAccordianContainer1',
								items:[
								    {
										 xtype:'container',
										 cls:'',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'label',
											 html:'Name',
											 flex:1,
											 cls:''
										 },
										 {
											 xtype:'label',
											 itemId:'lblPersonalDetailName',
											 html:'Mark Bonarius',
											 flex:1,
											 cls:''
										 }
										 ]
									},
									{
											 xtype:'label',
											 html:'Title*',
											 cls:''
									},
									{
											xtype:'button',
											text:'Mr',
											cls:''
									},
									{
											 xtype:'label',
											 html:'Prefered name',
											 cls:''
									},
									{
									    	xtype:'textfield',
								    		value:'Mr',
								    		cls:''
									},
									
									{
											 xtype:'label',
											 html:'Gender*',
											 cls:''
									},
									{
											xtype:'button',
											text:'Male',
											cls:''
									},
									{
											 xtype:'label',
											 html:'Date of birth*',
											 cls:''
									},
									
									{
										 xtype:'container',
										 cls:'',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'textfield',
											 readOnly:true,
											 flex:4,
											 cls:''
										 },
										 {
											 xtype:'button',
											 itemId:'btnDateofBirth',
											 text:'Cal',
											 flex:1,
											 cls:''
										 }
										 ]
									},
									{
											 xtype:'label',
											 html:'Tax File Number*',
											 cls:''
									},
									{
									    	xtype:'textfield',
								    		value:'Mr',
								    		cls:''
									},
									{
										 xtype:'container',
										 layout:'hbox',
										 cls:'',
										 items:[
										 {
											 xtype:'button',
											 itemId:'btnPersonalDetailSave',
											 text:'Save',
											 flex:1,
											 cls:''
										 },
										 {
											 xtype:'button',
											 itemId:'btnPersonalDetailCancel',
											 text:'Cancel',
											 flex:1,
											 cls:''
										 }
										 ]
									}						
								]
				        	}
				        ]
			        },
			        /**Contact detail Container*/
			        {
				        xtype:'container',
				        items:[
					        {
					        	xtype:'button',
					        	text:'Contact details',
					        	cls:'',
								itemId:'btnAccordian2',
								data:{
									buttonNumber:2
								}
				        	},
				        	/**Hidden container of Contact Details*/				        	
							{
								xtype:'container',
								itemId:'hideUnhideAccordianContainer2',
								hidden:true,
								items:[
    								{
											 xtype:'label',
											 html:'Email (this will be used as login)*',
											 cls:''
									},
									{
									    	xtype:'textfield',
									    	itemId:'txtContactDetailEmail',
									    	cls:''
									},
									{
											 xtype:'label',
											 html:'Secondary email*',
											 cls:''
									},
									{
									    	xtype:'textfield',
									    	itemId:'txtContactDetailSecondaryEmail',
									    	cls:''
									},
									{
											 xtype:'label',
											 html:'Mobile phone*',
											 cls:''
									},
									{
									    	xtype:'textfield',
									    	itemId:'txtContactDetailMobilePhone',
									    	cls:''
									},
									{
											 xtype:'label',
											 html:'Work phone',
											 cls:''
									},
									{
									    	xtype:'textfield',
									    	itemId:'txtContactDetailWorkPhone',
									    	cls:''
									},
									{
											 xtype:'label',
											 html:'Home phone',
											 cls:''
									},
									{
									    	xtype:'textfield',
									    	itemId:'txtContactDetailHomePhone',
									    	cls:''
									},
									{
										 xtype:'container',
										 cls:'',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'button',
											 itemId:'btnContactDetailSave',
											 text:'Save',
											 flex:1,
											 cls:''
										 },
										 {
											 xtype:'button',
											 itemId:'btnContactDetailCancel',
											 text:'Cancel',
											 flex:1,
											 cls:''
										 }
										 ]
									}	
								]
							}
				        ]
			        },
			        {
				        xtype:'container',
				        cls:'',
				        items:[
					        {
					        	xtype:'button',
					        	text:'Address',
								itemId:'btnAccordian3',
								data:{
									buttonNumber:3
								}
				        	},
				        	
				        	/**Hidden container of Address part*/				        	
							{
								xtype:'container',
								hidden:true,
								itemId:'hideUnhideAccordianContainer3',
								items:[
									/**Home Address*/
									{
				     					 xtype:'label',
										 html:'Home street address*'
									},
									{
									    xtype:'textfield',
									    itemId:'txtAddressHomeStreet'
									},
									{
										xtype:'label',
										html:'Home suburb*'
									},
									{
									    xtype:'textfield',
									    itemId:'txtAddressHomeSuburb'
									},
									{
										xtype:'label',
										html:'Home postcode*'
									},
									{
									    xtype:'textfield',
									    itemId:'txtAddressHomePostcode'
									},
									{
										xtype:'label',
										html:'Home State*'
									},
									{
										xtype:'button',
										text:'NSW'
									},
									/**Postal Address*/
									{
				     					 xtype:'label',
										 html:'Postal street address*'
									},
									{
									    xtype:'textfield',
									    itemId:'txtAddressPostalStreet'
									},
									{
										xtype:'label',
										html:'Postal suburb*'
									},
									{
									    xtype:'textfield',
									    itemId:'txtAddressPostalSuburb'
									},
									{
										xtype:'label',
										html:'Postal postcode*'
									},
									{
									    xtype:'textfield',
									    itemId:'txtAddressPostalPostcode'
									},
									{
										xtype:'label',
										html:'Postal State*'
									},
									{
										xtype:'button',
										text:'NSW'
									},
									
									{
										 xtype:'container',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'button',
											 itemId:'btnAddressSave',
											 text:'Save',
											 flex:1
										 },
										 {
											 xtype:'button',
											 itemId:'btnAddressCancel',
											 text:'Cancel',
											 flex:1
										 }
										 ]
									}	

								]
							}				        								
				        ]
			        },
			        
			        /**Change my password container*/
			        {
				        xtype:'container',
				        items:[
					      		{
						        	xtype:'button',
						        	text:'Change my password',
									itemId:'btnAccordian4',
									data:{
										buttonNumber:4
									}
					        	},
					        	{
					        		xtype:'container',
					        		hidden:true,
					        		itemId:'hideUnhideAccordianContainer4',
							        items:[
									        {
						     					 xtype:'label',
												 html:'New password*'
											},
											{
											    xtype:'textfield',
											    itemId:'txtPasswordNewPassword'
											},
											{
												xtype:'label',
												html:'Password strength'
											},
											{
											    xtype:'container',
											    style:'border:2px solid gray;',
											    items:[{
												    xtype:'container',
												    height:'50px',
												    id:'passwordStrenghtIndicator',
												    style:'background-color:red;'}
											    ]
											    
											},
											{
												xtype:'label',
												html:'Confirm password*'
											},
											{
											    xtype:'textfield',
											    itemId:'txtPasswordConfirmPassword'
											},
											{
												 xtype:'container',
												 layout:'hbox',
												 items:[
												 {
													 xtype:'button',
													 itemId:'btnPasswordSave',
													 text:'Save',
													 flex:1
												 },
												 {
													 xtype:'button',
													 itemId:'btnPasswordCancel',
													 text:'Cancel',
													 flex:1
												 }
												 ]
											}	
							        ]
							    }
					        ]
			        },
			        {
				        xtype:'container',
				        items:[
				        	{
					        	xtype:'button',
					        	text:'Personal Bank details',
								itemId:'btnAccordian5',
								data:{
									buttonNumber:5
								}
				        	},
				        	/**Hidden part of Personal bank details*/
				        	{
					        		xtype:'container',
							        itemId:'hideUnhideAccordianContainer5',
							        hidden:true,
							        items:[
									        {
						     					 xtype:'label',
												 html:'Account name*'
											},
											{
											    xtype:'textfield',
											    itemId:'txtPersonalBankDetailAccountName'
											},
											{
												xtype:'label',
												html:'BSB*'
											},
											{
											    xtype:'textfield',
											    itemId:'txtPersonalBankDetailBSB'
											},
											{
												xtype:'label',
												html:'Account number*'
											},
											{
											    xtype:'textfield',
											    itemId:'txtPersonalBankDetailAccountNumber'
											},
											{
												 xtype:'container',
												 layout:'hbox',
												 items:[
												 {
													 xtype:'button',
													 itemId:'btnPersonalBankDetailSave',
													 text:'Save',
													 flex:1
												 },
												 {
													 xtype:'button',
													 itemId:'btnPersonalBankDetailCancel',
													 text:'Cancel',
													 flex:1
												 }
												 ]
											}	
							        ]
							    }

				        ]
			        },
			        {
				        xtype:'container',
				        items:[{
					        	xtype:'button',
					        	text:'Payroll details',
					        	itemId:'btnAccordian6',
								data:{
									buttonNumber:6
								}
				        	},
							{				        	
				        		xtype:'container',
					        	itemId:'hideUnhideAccordianContainer6',
					        	hidden:true,
						    	items:[
						    		{
										 xtype:'container',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'label',
											 html:'Employer',
											 flex:1
										 },
										 {
											 xtype:'label',
											 itemId:'lblPayrollDetailEmployer',
											 html:'Melbourrne Health',
											 flex:1
										 }
										 ]
									},{
										 xtype:'container',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'label',
											 html:'Employment Status',
											 flex:1
										 },
										 {
											 xtype:'label',
											 itemId:'lblPayrollDetailEmploymentStatus',
											 html:'Full time',
											 flex:1
										 }
										 ]
									},
									{
										 xtype:'container',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'label',
											 html:'Paygroup',
											 flex:1
										 },
										 {
											 xtype:'label',
											 itemId:'lblPayrollDetailPaygroup',
											 html:'Blah',
											 flex:1
										 }
										 ]
									},
									
									{
										 xtype:'container',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'label',
											 html:'Payroll ID',
											 flex:1
										 },
										 {
											 xtype:'label',
											 itemId:'lblPayrollDetailPayrollId',
											 html:'111995',
											 flex:1
										 }
										 ]
									},
									
									{
											 xtype:'label',
											 itemId:'lblPayrollDetailPaygroup',
											 html:'Gross annual salary*'
									},
									{
											 xtype:'textfield',
											 itemId:'txtPayrollDetailGrossAnnualSalary',
											 value:'$123,456.45'
									},
									{
										 xtype:'container',
										 layout:'hbox',
										 items:[
										 {
											 xtype:'button',
											 itemId:'btnPayrollDetailSave',
											 text:'Save',
											 flex:1
										 },
										 {
											 xtype:'button',
											 itemId:'btnPayrollDetailCancel',
											 text:'Cancel',
											 flex:1
										 }
										 ]
									}	
						    	]
					    	}
					     ]
			        }
		        ]
	        },
	        /**Right side container to hold the Static Data*/
	        {
		        xtype:'container',
		        flex:1,
		        
		        items:[
		        	{
			        	xtype:'container',
			        	style:'border:1px solid gray;padding:5px;',
			        	items:[
			        		{
				        		xtype:'label',
				        		html:'Current account balance'
			        		},
			        		{
				        		xtype:'label',
				        		html:'$6201.12',
				        		itemId:'lblCurrntAccountBalance'
			        		},
			        		
			        		{
				        		xtype:'label',
				        		html:'Last per cycle deduction (14/10/2014)'
			        		},
			        		{
				        		xtype:'label',
				        		html:'$600.00',
				        		itemId:'lblLastPerCycleDeduction'
			        		},
			        		
			        		{
				        		xtype:'label',
				        		html:'Next per cycle deduction (28/10/2014)'
			        		},
			        		{
				        		xtype:'label',
				        		html:'$650.00',
				        		itemId:'lblNextPerCycleDeduction'
			        		},
			        	]	
		        	},
		        	{
			        	xtype:'container',
			        	style:'border:1px solid gray;padding:5px;',
			        	items:[
			        		{
				        		xtype:'label',
				        		html:'Last updated on 02/10/2014',
				        		itemId:'lblLastUpdatedOn'
			        		},
			        		{
				        		xtype:'label',
				        		html:"To view your most recent transaction or to change your PIN number, please login to ANZ's online portal. Please click here for instructions. For all other card releated enquiries, please call 1300 476 278.",
				        		itemId:'lblRecnetTransaction'
			        		}
			          	]	
		        	}
		        ]
	        }
        ]
    }
});