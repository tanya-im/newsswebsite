Ext.define('SmartSalary.view.desktop.MenuDropdown_V', {
	extend: 'Ext.Container',
	xtype: 'menudropdown',	
	config: {
		itemId:'menuDropdown',	
		html:'',
		cls: 'smart_menu',
	},
	initialize:function (){
		this.element.on({
			tap: this.onMenuTap,
			scope: this,
			delegate: 'li.menuItem'
		});
		this.on({
            painted: this.syncHeight,             
		    scope: this
        });
		this.callParent();
	},
	onMenuTap:function(e, el){
		el = Ext.fly(el);
		this.fireEvent('itemTap', this, e);
	},
	syncHeight:function  (e, el){
		el = Ext.fly(el);
		this.fireEvent('menuPainted', this, el);
	},
});
