// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.EmployerList_V', {
    extend: 'Ext.Container',
	requires: ['Ext.TitleBar'],
	alias: 'widget.employerListView_D',
    config: {
		itemId :'myEmployerListPageId',
		id:'myEmployerListPageId',
		layout: 'vbox',
		cls:'bgdarkblue',
		scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        },
		items: [
		{
				xtype: 'container',
				layout:'hbox',
				docked: 'top',
				cls :'pad-10',
				flex:1,
				items:[
				{
					xtype: 'panel',
					flex:2,
					items:[
					{
						xtype: 'label',
						html: 'My Employer List',
						itemId: 'MyPackagingItem',
						cls: 'font-white font-16 pad-5'
					}]
				}]
			},
			{
				xtype: 'panel',
				docked: 'top',
				flex:2,
				height:'100%',
				id:'employerListWrapper',
				items:[
				{
					xtype:'list',
					store:'EmployerList',
					itemId: 'employerList',
					height:'100%',
					id:'employerList',
					cls :'employerList',
					itemTpl:'<div class="employerrow">{PackageID} {EmployerCode} {EmployerName} </div>',
					onItemDisclosure: true,
					listeners: {
                    	painted: function(comp,eOpts) {
                        	Ext.getCmp('employerList').getScrollable().getScroller().on('scrollstart',function(){Ext.getCmp('myEmployerListPageId').setScrollable(false);});
							Ext.getCmp('employerList').getScrollable().getScroller().on('scrollend',function(){Ext.getCmp('myEmployerListPageId').setScrollable(true);});
                           
                                }
                            }
				}]
			}]
		
	}
});