// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.desktop.MealClaim.ExpenseTypeSelectionPage_V', {
    extend: 'Ext.form.Panel', 
    alias : 'widget.MealClaimExpenseSelectionWidget_D', 
    config: {
        id:"MealClaimExpenseSelectionId",
        itemId:"MealClaimExpenseSelectionId",   
        scrollable: {
            direction: 'vertical',
		    directionLock: true,
            indicators: {
                    y: {
                        autoHide: false
                    }
            }
        }     ,         
        items: [{
                xtype: 'container',
                cls :'claimListheader',
                items:[{
                    xtype: 'panel',
                    items:[{
                        xtype: 'toolbar',
                        title: 'Meal Entertainment Expenses Claim',                           
                        cls: 'font-white font-18 pad-5'
                    }]
                }]
            },
            {
                xtype: 'container',
                cls :'pad-10 bgE7E7E7 font-16 txt-center header_wrap_grey',
                layout: 'hbox',
                items:[
                {
                    xtype: 'image',
                    //src:'resources/images/SS04-Mobile-Claims-Vehicle-1.png',
                    cls:'Claims-Vehicle-steps-img step1_img'
                }]
            },             
            {
                xtype: 'container',
                cls :'pad-10 bgwhite border-bottom',
                items:[
                {
                    xtype: 'label',
                    html: 'Type of meal entertainment claim',
                    cls: 'font-16 pad-5 font-bold'
                },
                {
                    xtype: 'label',
                    html: '<p>Select the item(s) you are claiming from the options:</p>',
                    cls: 'font-12 colorstyle1 pad-left-5'
                }]
            },{
                xtype: 'container',
                id:'mealClaimExpTypeWrapper',
                cls:'bgwhite', 
                itemId: 'mealClaimExpTypeWrapper', 
            }
            ,{
                xtype: 'container',
                cls :'pad-10 bgE7E7E7',
                layout: 'vbox', 
                items:[                
                {
                    xtype: 'container',
                    cls: 'font-16',                     
                    items:[
                    {
                        xtype: 'button',
                        id:'mealClaimSelectionPageSkipButton',
                        itemId:'mealClaimSelectionPageSkipButton',
                        ui: 'none',
                        hidden:true,                        
                        action:'mealClaimSelectionToListingPageSkipButton',
                        cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-bottom-10',
                        text: 'Skip'
                    },{
                        xtype: 'button',
                        ui: 'none',                        
                        action:'mealClaimSelectionPageCancelBtnPress',
                        cls:'claimStepLeftArrow claimStepButton txt-left font-16 pad-10',
                        text: 'Cancel'
                    }]
                }]
            }] 
    } 
 
});
