// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.CappedClaim.ClaimFormPage_V', {
    extend: 'Ext.form.Panel', 
    alias : 'widget.CappedClaimFormStep_T', 
    config: {
        id:"CappedClaimFormStepPageId",
        itemId:"CappedClaimFormStepPageId",    
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },         
        items: [{
                xtype: 'container',
                layout:'hbox',
                cls :'claimListheader',
                items:[{
                    xtype: 'panel',
                    flex:4,
                    items:[{
                        xtype: 'toolbar',
                        title: 'Reimbursement claim for tax-free cap expenses',                         
                        cls: 'font-white font-18 pad-5'
                    }]
                },{
					xtype:"button",
					action:"cappedClaimFormPageBackBtn",
					itemId:"cappedClaimFormPageBackBtn",
					flex:1,
					html:'Back',
					cls: 'font-white searchlistbackbtn'
				}]
            },
            {
                xtype: 'container',
                cls :'bgE7E7E7 font-16 txt-center header_wrap_grey',
                layout: 'hbox',
                items:[
                {
                    xtype: 'image',
                    //src:'resources/images/SS04-Mobile-Claims-Vehicle-2.png',
                    cls:'Claims-Vehicle-steps-img step2_img'
                }]
            }, 
            {
                xtype: 'container',                 
                cls :'pad-10 bgwhite',
                layout: 'hbox',
                items:[{
                        xtype: 'container',                        
                        cls :'pad-20 txt-center bg4e2683 font-white margin-right-5',
                        flex:5,
                        items:[
                            {
                                xtype: 'label',
                                html: 'Amount left for text-free cap expenses before end of FBT year (31 March)',
                                cls:'font-16 pad-bottom-10'                                                
                            },

                            {
                                xtype: 'label',
                                itemId:'remainingClaimAmountLabelOnFormPage',
                                cls:' font-26'
                            }
                        ] 
                },{
                    xtype: 'container', 
                    width: '100%',
                    cls:'gray_box pad-20 margin-left-5',
                    flex:5,                
                    items: [
                        {
                            xtype: 'label',
                            html: '<b>Note:</b> If your claim, or a portion of your claim, exceeds the tax-free cap limit, that amount will be carried forward to the next FBT year'                                                
                        }
                    ]
                }] 
            },
            {
                xtype: 'container', 
                cls:'bgwhite pad-top-10 pad-bottom-10', 
                itemId: 'cappedClaimFormFieldsWrapper' 
            }
            ,{
                xtype: 'container',
                cls :'pad-top-10 pad-bottom-10 bgE7E7E7',
                layout: 'vbox', 
                itemId:'cappedClaimStepFormButtonWrapper',
                items:[
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
                    items:[{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'cappedClaimStepFormBackButtonAction',
                        cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Back'
                    },
                    {
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'cappedClaimStepOneNextButtonPress',
                        cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Next'
                    }]
                },
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
                    items:[{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'cappedClaimStepFormCancelButtonPress',
                        cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Cancel'
                    },
                    {
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        cls:'claimStepOneSaveAsDraftButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Save as draft'
                    }]
                }]
            }] 
    } 
 
});
