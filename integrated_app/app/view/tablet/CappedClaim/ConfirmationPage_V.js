// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.CappedClaim.ConfirmationPage_V', {
    extend: 'Ext.form.Panel',
	alias: 'widget.CappedClaimConfirm_T',
    config: {
		itemId :'CappedClaimConfirmPageId',
		id:'CappedClaimConfirmPageId',
		cls:'bgdarkblue bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container', 
			cls :'pad-left-10 green-bg-gredient',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Reimbursement claim for tax-free cap expenses', 
					cls: 'font-white font-18 pad-5'
				}]
			},{
					xtype:"button",
					action:"cappedClaimConfirmationPageBackBtn",
					flex:1,
					html:'Back',
					cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'bgE7E7E7 font-16 txt-center header_wrap_grey',
			items:[
			{
				xtype: 'image',
				//src:'resources/images/SS04-Mobile-Claims-Vehicle-4.png',
				cls:'Claims-Vehicle-steps-img step4_img'
			}]
		},{
			xtype: 'spacer',
			cls :'pad-10 bgwhite'
		},{
			
			xtype: 'container',
			cls :'pad-10 bgE7E7E7 font-16 f-bold color-2B2F3B',
			items:[
			{
				xtype: 'label',
				flex: 1,
				cls:'pad-l-15',
				html: 'Your Details',
			},
			{
				xtype: 'container',
				cls :'bgwhite margin-10-5 font-normal',
				items:[{
					xtype: 'container',
					cls :'pad-5-10 bgwhite',
					items:[{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Name on account:',
							flex:3
						},{
							xtype: 'label',
							itemId:'cappedClaimNameOnAc', 
							cls:'pad-right-5 txt-right f-bold',
							flex:1
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'BSB:',
							flex:3
						},{
							xtype: 'label',
							itemId:'cappedClaimBSB', 
							cls:'pad-right-5 txt-right f-bold',
							flex:1
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Account number:',
							flex:3
						},{
							xtype: 'label',
							itemId:'cappedClaimAcNo', 
							cls:'pad-right-5 txt-right f-bold',
							flex:2
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Email:',
							flex:3
						},{
							xtype: 'label',
							itemId:'user_email',
							 
							cls:'pad-right-5 txt-right f-bold',
							flex:2
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Contact number:',
							flex:3
						},{
							xtype: 'label',
							itemId:'user_contact',
							 
							cls:'pad-right-5 txt-right f-bold',
							flex:2
						}]
					}]	
				},{
					xtype: 'container',
					cls :'bgwhite font-14 color-2B2F3B clearfix',
					items:[{
						xtype: 'button',
						ui: 'none',
						cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 color-5A5B77',
						text: 'Change account'
					}]
				}]
			}]
		},{
			xtype: 'container',
			cls :'pad-10 bgwhite',
			items:[
			{
				xtype: 'label',
				html: 'Declaration',
				cls: 'font-16 f-bold pad-15'
			},
			{
				xtype: 'container',
				cls:'pad-left-5 pad-bottom-10 m-5-0 font-16 f-darkgrey declarationWp',
				items:[{
					xtype: 'checkboxfield',
					name : 'declarationbox',
					id:'cappedDeclarationCheckbox',
					itemId:'cappedDeclarationCheckbox',
					docked:'left'
				},{
					xtype: 'label',
					itemId:'declarationtxt',
					cls:'fontDeclaration pad-left-5',
					id:'cappedClaimdeclarationtxt',
					html: "The information provided in this claim is to my knowledge true and correct.I have read and understand the Salary Packaging Guide.I have read and accept the Smartsalary Terms & Conditions.I authorise Smartsalary to alter my deductions in accordance to the requirements of my salary package.I acknowledge that payments/or reimbursements can only occur after sufficient salary sacrifice has occurred for that benefit item.I acknowledge that upon cessation of my salary package, any amounts owed by me in relation to my salary packaging that have not been collected from my pay must be paid directly by me. I request Smartsalary to direct debit the bank account held by Smartsalary as my Personal Bank Account for these amounts owing.I have read and understood the Direct Debit Service Agreement available on the Smartsalary website."
				}]
			}]
		},
		{
			
			xtype: 'container',
			cls :'pad-10 bgE7E7E7',
			layout: 'vbox',
			itemId:'cappedClaimStepsButtonWrapper',
			items:[
			{ 
				xtype: 'container',
				cls: 'font-16',
				layout: 'hbox',
				items:[{
					xtype: 'button',
					itemId: 'cappedClaimStepConfirmBackButton',
					ui: 'none',
					action: 'cappedClaimStepConfirmBackButton',
					flex:1,
					cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Back'
				},
				{
					xtype: 'button',
					itemId: 'cappedClaimStepTwoSubmitButton',
					ui: 'none',
					action: 'cappedClaimStepTwoSubmitButtonPress',
					flex:1,
					cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Submit Claim'
				}]
			},
            {
                xtype: 'container',
                cls: 'font-16',
                layout: 'hbox',
                items:[{
                    xtype: 'button',
                    ui: 'none',
                    flex:1,
                    action:'cappedClaimStepTwoCancelButtonPress',
                    cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                    text: 'Cancel'
                },
                {
                    xtype: 'button',
                    ui: 'none',
                    flex:1,
                    cls:'claimStepOneSaveAsDraftButton claimStepButton txt-left font-16 pad-10 margin-10',
                    text: 'Save as draft'
                }]
            }]
		
		}]
	}
});