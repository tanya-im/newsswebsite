// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.CappedClaim.ClaimsListingPage_V', {
    extend: 'Ext.form.Panel',
	alias: 'widget.CappedClaimStep2_T',
    config: {
		itemId :'CappedClaimStepTwoPageId',
		id:'CappedClaimStepTwoPageId',
		cls:'bgdarkblue bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container', 
			cls :'pad-left-10 green-bg-gredient',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Reimbursement claim for tax-free cap expenses', 
					cls: 'font-white font-18 pad-5'
				}]
			},{
					xtype:"button",
					action:"cappedClaimListingPageBackBtn",
					flex:1,
					html:'Back',
					cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'pad-10 bgE7E7E7 font-16 txt-center header_wrap_grey',
			items:[
			{
				xtype: 'image',
				//src:'resources/images/SS04-Mobile-Claims-Vehicle-3.png',
				cls:'Claims-Vehicle-steps-img step3_img'
			}]
		},
		{
			xtype: 'spacer',
			cls :'pad-10 bgwhite'
		},
		{
			xtype: 'container',
			layout: 'hbox',
			items:[
			{
				xtype: 'container',
				cls :'pad-0-10-10-10 bgE7E7E7 font-20 f-bold color-2B2F3B', 
				flex: 1,
				items:[
				{
					xtype: 'container',
					id:'claimDetailWrapper',
					itemId:'claimDetailWrapper',
					items:[{
						xtype: 'container',
						id:'cappedClaimItemsWrapper',
						itemId:'cappedClaimItemsWrapper',
						cls :'bgE7E7E7 font-normal'			 
					}]
				},{
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
	                items:[{
	                    xtype: 'button',
	                    ui: 'none',
	                    flex:1,
	                    action:'claimStepTwoSaveAndAnotherButton',
	                    cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10 bgpurple1',
	                    text: 'Save & Add Another Expense'
	                }]
	            }]
			},
			{
				xtype: 'container',
				flex: 1,
				itemId:'leftTotalClaimDetailWrapper',
				cls :'pad-10-20-10-10 bgE7E7E7 font-white font-normal cappedClaimRightSideWrapper',
				items:[
				{
					xtype: 'container',
					cls :'pad-10 bgpurpule vclaimtotalwap',
					items:[{
						xtype: 'label',
						id:'totalClaimAmountStepTwo',
						itemId:'totalClaimAmountStepTwo',
						cls:'pad-left-5 font-16 pad-bottom-10'
					},{
						xtype: 'label',
						id:'totalClaimAmountValueStepTwo',
						itemId:'totalClaimAmountValueStepTwo',
						cls:'pad-left-5 font-26'
					}]
				},
				{
					xtype: 'container',
					itemId:'ammountLeftTotalClaimDetailWrapper',
					cls :'margin-top-10 pad-10 bgpurpule vclaimtotalwap',
					items:[{
							xtype: 'label',
							cls:'pad-left-5 font-16',
							html: 'Amount left for tax-free cap expenses',
						},{
							xtype: 'label',
							cls:'pad-left-5 font-16 pad-bottom-10',
							html: 'before end of FBT year (31 March)'
						},{
							xtype: 'label',
							id:'amountLeftCappedClaimValue',
							itemId:'amountLeftCappedClaimValue',
							cls:'pad-left-5 font-26',
							html: '$0.00'
					}]
				},{
					xtype: 'container',
					cls :'pad-top-10 font-14 f-darkgrey',
					id:'cappedClaimNote',
					html: 'Note: If your claim, or a portion of your claim, exceeds the tax-free cap limit, that amount will be carried forward to the next FBT year.'
				}]
			}]
		},
		{
                xtype: 'container',
                cls :'pad-top-10 bgwhite',
                layout: 'hbox'
		},
		{
                xtype: 'container',
                cls :'pad-10 bgE7E7E7',
                layout: 'vbox', 
                itemId:'cappedClaimStepOneButtonWrapper',
                items:[
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
                    items:[{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'cappedClaimStepTwoBackButtonPress',
                        cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Back'
                    },
                    {
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'cappedClaimStepTwoNextButtonPress',
                        cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Next'
                    }]
                },
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
                    items:[{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'cappedClaimStepTwoCancelButtonPress',
                        cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Cancel'
                    },
                    {
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        cls:'claimStepOneSaveAsDraftButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Save as draft'
                    }]
                }]
            }]
	}
});