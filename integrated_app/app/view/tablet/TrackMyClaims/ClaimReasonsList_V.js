// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.TrackMyClaims.ClaimReasonsList_V', {
    extend: 'Ext.Container', 
	alias: 'widget.ClaimReasonsView_T',
    config: {		 
		itemId:'ClaimReasonsPageId',
		id :'ClaimReasonsPageId',
        layout:'vbox',
		items: [{
                xtype: 'container',
                layout:'hbox',            
                cls :'claimListheader',
                items:[
                {
                    xtype: 'panel',
                    flex:4,
                    items:[
                    {
                        xtype: 'toolbar',
                        title: 'Track My Claim',                        
                        cls: 'font-white font-16 pad-5'
                    }]
                },{
                	xtype:"button",
                	itemId:"reasonsBackBtn",
                	id:"reasonsBackBtn",
                	flex:1,
                	html:'Back',
					cls: 'font-white searchlistbackbtn'
                }]
            },             
            {
                xtype: 'container',                
                flex:5,
				scrollable : {
					direction : 'vertical',
					directionLock : true
				},
				layout:'vbox',                   
                items:[
                {
                    xtype:'list',
                    flex:5,                    
                    id:'claimReasonsListId', 
                    itemId:'claimReasonsListId',
                    emptyText:'Reasons not found!',
                    cls:'claimListContainer pad-10',                    
                    itemTpl:'<div class="reasonContainer"><div class="row1 title">{Title}</div><div class="row1 description">{Description}</div></div>',
                    mode : 'SINGLE',
                    scrollable : {
                        direction : 'vertical',
                        directionLock : true
                    }       
                }]
            }] 
    }
});