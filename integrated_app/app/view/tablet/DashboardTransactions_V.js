// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.DashboardTransactions_V', {
    extend: 'Ext.Container',
	alias: 'widget.dashboardTransactions_T',
    config: {
		itemId :'dashboardTransactionsPageId',
		id:'dashboardTransactionsPageId',
		cls :'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Activity Summary',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backDasboardTransactionPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls:'pad-15-10-0 fa-vl container_vl_wrap',
			items:[{
				xtype: 'container',
				cls :'pad-top-10 bgwhite fa-vl',
				items:[{
					xtype: 'button',
					itemId: 'dashboardTransactionsDatesButton',
					ui: 'none',
					action: 'dashboardTransactionsDatesButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-10',
					text: 'Next pay periods'
				},{
					xtype: 'button',
					itemId: 'dashboardManageMyBenefitButton',
					ui: 'none',
					action: 'dashboardManageMyBenefitButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-10',
					text: 'Manage My Benefit'
				},{
					xtype: 'fieldset',
					cls:' selectbox_vl',
	            	items: [{
						xtype: 'selectfield',
						cls:'expenditureType',
						itemId:'dashboardExpenditureTypes',
						usePicker:false, 
						options: [ {text: 'First Option',  value: 'first'},
	                        {text: 'Second Option', value: 'second'},
	                        {text: 'Third Option',  value: 'third'}]
	            	}]
				}]
			},{
				xtype: 'container',
				cls :'bgwhite fa-vl',
				items:[{
					xtype: 'container',
					id:'dashboardTransactionsFromCalendar',
					itemId:'dashboardTransactionsFromCalendar'
				},{
					xtype: 'container',
					id:'dashBoardTransactionsToCalendar',
					itemId:'dashBoardTransactionsToCalendar'
				},{
					xtype: 'button',
					itemId: 'dashboardTransactionsDatesButton',
					ui: 'none',
					action: 'dashboardTransactionsDatesButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
					text: 'Submit'
				}]
			},{
				xtype: 'container',
				cls :'bgwhite margin-top-10 expenselist_vl_wrap',
				items:[{
					xtype:'list',
					height:'400px',
					store: "dashboardTransactionList",
					loadingText :null,
					mode : 'SINGLE',
					cls :'expenseList expenselist_vl fa-vl',
					itemTpl:'<div class="vehicleRegistrationList"><div class="expenseDate">{transactionDate} </div><div class="expdetail clearfix"><div class="expenseName">{description} </div><div class="expenseAmount">{amount}</div></div><div class="fa fa-chevron-right" style="display:block;"></div></div>',
					listeners: {
						painted: function(comp,eOpts) {
							this.getScrollable().getScroller().on('scrollstart',function(){
								Ext.getCmp('dashboardTransactionsPageId').setScrollable(false);
							});
							this.getScrollable().getScroller().on('scrollend',function(){
								Ext.getCmp('dashboardTransactionsPageId').setScrollable(true);
							});
						}
					}
				}]
			}]
		}]	
	}
});