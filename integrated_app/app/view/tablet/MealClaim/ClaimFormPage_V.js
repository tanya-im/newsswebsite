// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.MealClaim.ClaimFormPage_V', {
    extend: 'Ext.form.Panel', 
    alias : 'widget.MealClaimFormWidget_T', 
    config: {
        id:"MealClaimFormPageId",
        itemId:"MealClaimFormPageId",    
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },         
        items: [{
                xtype: 'container',
                cls :'claimListheader',
                items:[{
                    xtype: 'panel',
                    items:[{
                        xtype: 'toolbar',
                        title: 'Meal Entertainment Expenses Claim',                           
                        cls: 'font-white font-18 pad-5'
                    }]
                }]
            },
            {
                xtype: 'container',
                cls :'bgE7E7E7 font-16 txt-center header_wrap_grey',
                layout: 'hbox',
                items:[
                {
                    xtype: 'image',
                    //src:'resources/images/SS04-Mobile-Claims-Vehicle-2.png',
                    cls:'Claims-Vehicle-steps-img step1_img'
                }]
            }, 
             
            {
                xtype: 'container',
                id:'mealClaimFormFieldsWrapper',
                cls:'bgwhite pad-top-10 pad-bottom-10', 
                itemId: 'mealClaimFormFieldsWrapper' 
            }
            ,{
                xtype: 'container',
                cls :'pad-top-10 pad-bottom-10 bgE7E7E7',
                layout: 'vbox',
                id:'mealClaimFormButtonWrapper',
                itemId:'mealClaimFormButtonWrapper',
                items:[
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
                    items:[{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        itemId:'mealClaimFormPageBackButton',
                        id:'mealClaimFormPageBackButton',
                        action:'mealClaimFormPageBackButtonAction',
                        cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Back'
                    },{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        hidden:true,
                        action:'mealClaimFormPageSkipButtonPress',
                        cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Skip'
                    },
                    {
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'mealClaimFormPageNextButtonPress',
                        cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Next'
                    }]
                },
                {
                    xtype: 'container',
                    cls: 'font-16',
                    layout: 'hbox',
                    items:[{
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        action:'mealClaimFormCancelButtonPress',
                        cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Cancel'
                    },
                    {
                        xtype: 'button',
                        ui: 'none',
                        flex:1,
                        cls:'claimStepOneSaveAsDraftButton claimStepButton txt-left font-16 pad-10 margin-10',
                        text: 'Save as draft'
                    }]
                }]
            }] 
    } 
 
});
