// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.ContentDetail_V', {
    extend: 'Ext.Container',
	requires: ['Ext.TitleBar'],
	alias: 'widget.contentDetailView_T',
    config: {
		 cls: 'bgwhite',
		 itemId:'contentDetailPageId',
		 id :'contentDetailPageId',
		 scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
           	xtype: 'container',
			cls:'bgdarkblue pad-10 salaryfeepackagingtop',
			id: 'MyPackagingItem',
			itemId:'MyPackagingItem',
			height:'20%',
			items:[]
		},
		{
			xtype: 'container',
			cls:'bgwhite pad-10',
			id: 'dataWrapper',
			itemId: 'dataWrapper',
			height:'80%',
			items:[],
			listeners: {
			    tap: {
			        fn:function(event, node, options, eOpts){
			        	SmartSalary.app.getController('ContentDetail_C').dataWrraperContainerClicked(event, node, options, eOpts);
			        },
			        delegate: 'a',
			        element: 'element'/**If not delegate:'a' then whole html is clickable*/
			    }
				}
		}]
    }
});