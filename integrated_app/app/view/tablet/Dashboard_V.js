// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.Dashboard_V', {
	extend: 'Ext.Container',
	requires: ['Ext.TitleBar'],
	alias: 'widget.dashboardView_T',
	requires: ['Ext.chart.PolarChart', 'Ext.chart.series.Pie3D', 'Ext.chart.interactions.RotatePie3D'],
	config: {
		itemId:'dashboardView',
		id:'dashboardscreen',
		cls:'dashboard_t_wrapper',
		scrollable: {
			direction: 'vertical',
			directionLock: true
		},
		items: [{
			xtype: 'panel',
			title: 'Main Menu',
			layout: 'vbox',
			items: [{
				xtype: 'container',
				cls:'bggrey pad-10',
				layout:'hbox',
				id: 'toptool',
				items:[{
					xtype:'container',
					id:'userdetail',
					itemId:'userdetail',
					items:[{
						xtype: 'label',
						html: '',
						itemId: 'wcuser',
						cls: 'wcuser wcuser font-white font-16'				
					},{
						xtype: 'label',
						html: '',
						itemId: 'acno',
						cls: 'font-828395 font-14 pad-2-0'				
					},{
						xtype: 'label',
						html: '',
						itemId: 'employername',
						cls: 'font-828395 font-14 pad-2-0'				
					}]
				}]
			},{
				xtype: 'container',
				cls:'pad-10 fontcolor_vl',
				items:[{
					xtype: 'container',
					layout:'hbox',
					itemId:'activitySummaryWidget',
					hidden:true,
					items:[{
						xtype: 'container',
						cls:'pad-10 bgwhite bordergrey_lt',
						layout:'hbox',
						flex:2,
						items:[{
							xtype: 'container',
							flex:2,
							items:[{
								xtype: 'label', 
								itemId: 'activitySummary',
								cls:'activitySummary font-18 f-bold pad-top-10'	
							},{
								xtype: 'container',
								itemId:'activitySummaryFields',
								items:[]
							},
							{
								xtype: 'button',
								text: 'View Full Statement',
								itemId: 'viewFullStatementActivity',
								action: 'viewFullStatementActivity',
								cls:'button_grey_vl_2 claimStepButton txt-left font-16 pad-10',	
							}]
						},{		
							xtype: 'container',
							flex:1.5,
							items:[{
								xtype: 'container',
								cls:'margin-bottom-10 pad-left-15',
								height:'180px',
								items:[ {
									xtype: 'label',
									html:'Last pay cycle expenditure', 
									cls:'font-14 pad-top-10'	
								},{
									xtype: 'container', 
									layout:'fit', 
									height:'180px',
									items:[{ 
										id:'pieChartIn3D_Dashboard', 
										itemId:'pieChartInBudgetDashboard',
										xtype: 'polar',
										width: '100%',  
										background: 'white',
										interactions: 'rotatePie3d',
										store: {
											fields: ['name', 'budget' ],
											data: [{'name':'Jan', 'budget':10 },
											{'name':'Feb', 'budget':7},
											{'name':'March','budget':5},
											{'name':'Apr', 'budget':2},
											{'name':'May', 'budget':10 },
											{'name':'June', 'budget':6},
											{'name':'July', 'budget':8 }]
										},
										colors: [
										'#4F2C7E',
										'#EB2939',
										'#0070BB',
										'#E88200',
										'#ABDDE4', 
										'#B0B3B1',
										'#BCD400'
										],
										animate: {
											duration: 1000,
											easing: 'easeInOut'
										},
										series: [
										{
											type: 'pie3d',
											field: 'budget',
											donut: 50,
											distortion: 0.4,
											style: {
												stroke: "white",
												strokeStyle: 'white',
												opacity: 0.90
											}
										}
										]
									}]
								}]
							}]
						}]	
					},{
						xtype: 'container',
						cls:'margin-left-20 bgwhite bordergrey_lt pad-10',
						flex:1,
						items:[{
							xtype: 'label',
							html: 'Notifications',
							cls: 'font-18 f-bold'	
						},{
							xtype: 'label',
							html: 'Due to the new superStream laws that were enforced, you will need to update your personal detail with youe TFN details. Please click here to update.',
							itemId: 'notificationsTxt',
							cls: ''	
						}]
					}]
				},{
					xtype: 'container',
					cls:'margin-top-20',					
					items:[{
						xtype: 'container',	
						itemId:'novatedleasingWidget',
						hidden:true,					
						cls:'bgwhite bordergrey_lt  pad-10',
						items:[{
							xtype: 'label',							 
							itemId:'NLTitle', 
							cls: 'font-18 f-bold'	
						},{
							xtype: 'container',
							cls :'pad-bottom-10 pad-top-10',
							items:[{
								xtype: 'container',
								cls:'',
								items:[{
									xtype: 'fieldset',
									cls:'selectbox_vl_2 selectbox_vl',
									itemId:'selectVContainer', 
								}]
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							items:[{
								xtype: 'label',
								flex:1, 
								itemId: 'ntCurrentSpent',
								cls: 'pad-2-0'	
							},{
								xtype: 'label', 
								flex:1,
								itemId: 'ntBudget',
								cls: ' txt-right pad-2-0'	
							}]
						},{
							xtype: 'container',
							height:'40px',
							id:'novatedCarLeaseGraph',
							itemId:'novatedCarLeaseGraph',
							cls:'animationspac margin-bottom-10'
						},{	
							xtype:'container',
							layout:'hbox',
							items:[{
								xtype: 'label',
								flex:2,
								itemId:'OdometerVL', 
								cls: 'valignMiddle pad-2-0'	
							},{
								xtype: 'button',
								text: 'View Details',
								flex:1,
								itemId: 'viewNovatedLeasing',
								action: 'viewNovatedLeasing',
								cls:'button_grey_vl_2 claimStepButton txt-left font-16 pad-10',	
							}]
						}]
					},{
						xtype:'container',
						//layout:'hbox',
						cls:'txt-center fa-vl margin-top-20',
						items:[{
							xtype: 'container',
							cls:'border',
						},{
							xtype: 'container',
							html: '<h2>Your Packaged Products</h2>',
						}]
					},{
						xtype:'container',
						layout:'hbox',
						itemId:'thresholdCapWidget',
						hidden:true,
						items:[{
							xtype: 'container',
							cls:'bgwhite bordergrey_lt pad-10 margin-top-20 btn_bottom_wrap',
							flex:1,
							items:[{
								xtype: 'label',
								itemId: 'titleTH',
								cls: 'font-18 f-bold'	
							},{
								xtype: 'label',
								itemId:'entitlementId',
								html: 'Total entitlement $9009.96 (per FBT)',
								cls: 'margin-top-10'	
							},{
								xtype: 'container',
								layout:'hbox',
								cls: 'margin-top-10',
								items:[{
									xtype: 'label',
									flex:1,  
									itemId: 'thresholdCapSpent',
									cls: 'pad-2-0'	
								},{
									xtype: 'label',
									flex:1,
									itemId: 'thresholdCapRemaining',
									cls: ' pad-2-0 txt-right'	
								}]
							},{
								xtype: 'container',
								height:'40px',
								itemId:'thresholdGraph',
								cls:'animationspac'
							},{
								xtype: 'button',
								text: 'View Details',
								itemId: 'manageMyThresholdCapButton',
								action: 'manageMyThresholdCapButton',
								cls:'button_grey_vl_2 claimStepButton btn_bottom txt-left font-16 pad-10',	
							}]
						},{
							xtype: 'container',
							flex:1,
							itemId:'superannuationWidget',							 
							cls:'pad-10 bgwhite bordergrey_lt pad-1 margin-top-20 margin-left-20 btn_bottom_wrap',
							items:[{
								xtype: 'label',
								itemId: 'SuperannuationTitle',
								cls: 'font-18 f-bold'	
							},{
								xtype: 'label',
								itemId: 'FinancialYearToDate',
								cls: 'margin-top-10'	
							},{
								xtype: 'label',
								itemId: 'nextContribution',
								cls: 'margin-top-10'	
							},{
								xtype: 'button',
								text: 'View Details',
								itemId: 'superannuationButton',
								action: 'superannuationButton',
								cls:'button_grey_vl_2 claimStepButton btn_bottom txt-left font-16 pad-10 margin-top-10',	
							}]
						}]
					},{
						xtype:'container',
						layout:'hbox',
						items:[{
							xtype: 'container',
							cls:'bgwhite bordergrey_lt pad-10 margin-top-20 btn_bottom_wrap',
							itemId:'livingExpenseWidget', 
							hidden:true,
							flex:1,
							items:[{
								xtype: 'label',
								itemId: 'titleLE',
								cls: 'font-18 f-bold'	
							},{
								xtype: 'label',
								itemId: 'lblLE',
								cls: 'margin-top-10'	
							},{
								xtype: 'button',
								text: 'View Details',
								itemId: 'livingExpenseCardButton',
								action: 'livingExpenseCardButton',
								cls:'button_grey_vl_2 claimStepButton btn_bottom txt-left font-16 pad-10 margin-top-10',	
							}]
						},{
							xtype: 'container',
							flex:1,
							itemId:'MEwidget',
							hidden:true,
							cls:'bgwhite bordergrey_lt pad-10 margin-top-20 margin-left-20 btn_bottom_wrap',
							items:[{
								xtype: 'label',
								itemId: 'titleME',
								cls: 'font-18 f-bold'	
							},{
								xtype: 'label',
								itemId: 'lblme',
								cls: 'margin-top-10'	
							},{
								xtype: 'button',
								text: 'View Details',
								itemId: 'mealsAndEntertainmentCardButton',
								action: 'mealsAndEntertainmentCardButton',
								cls:'button_grey_vl_2 claimStepButton btn_bottom txt-left font-16 pad-10 margin-top-10',	
							}]
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						items:[{
							xtype: 'container',
							flex:1,
							cls:'fa-vl txt-center margin-top-20',
							items:[{
								//xtype:'container',
								//layout:'hbox',
								//cls:'',
								//items:[{
									xtype: 'container',
									cls:'border',
								},{
									xtype: 'container',
									html: '<h2>Available Products</h2>',
								//}]
							},						
							{
								xtype: 'button',
								text: 'Novated car Lease',
								itemId: 'novatedCarLease',
								action: 'novatedCarLease',
								cls:'button_white_vl claimStepButton fa fa-chevron-right margin-top-20',	
							},{
								xtype: 'button',
								text: 'Home Office Item',
								itemId: 'homeOfficeItem',
								action: 'homeOfficeItem',
								cls:'button_white_vl claimStepButton fa fa-chevron-right margin-top-10',	
							},{
								xtype: 'button',
								text: 'Mortgage Repayment',
								itemId: 'mortgageRepayment',
								action: 'mortgageRepayment',
								cls:'button_white_vl claimStepButton fa fa-chevron-right margin-top-10',	
							}]
						},{
							xtype: 'container',
							flex:1,
							cls:'fa-vl txt-center margin-top-20 margin-left-20',
							
							items:[{
								xtype: 'container',
								cls:'placeholder_car',
								html:'<img src="resources/images/placeholder_car.png">',
							}]
						}]
					}]
				}]
			}]
		}]
	}
});