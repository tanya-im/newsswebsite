// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.ManageFuelCard_V', {
    extend: 'Ext.Container',
	alias: 'widget.manageFuelCard_T',
    config: {
		itemId :'manageFuelCardPageId',
		id:'manageFuelCardPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Manage fuel Cards',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToDashboardPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls:'container_vl_wrap pad-top-5',
			items:[{
				xtype: 'fieldset',
				cls:'selectbox_vl_2 selectbox_vl',
				itemId:'selectVContainer',
				items: [{
					xtype: 'selectfield',
					cls:'expenditureType_1 vlabel',
					usePicker:false,
					itemId:'selectVehicle', 
					label: 'Vehicle :', 
					options: []
				}]
			}]
			 
		},{
			xtype: 'container',
			cls :'bgE7E7E7 margin-top-10 txt-center',
			items:[{
					
			xtype: 'container',
			itemId:'manageFuelCardList',
			cls :'pad-10-0 font-16 color-2B2F3B height100 vehicleDetail container_vl_wrap'			 
		
				  }]
		},{
			xtype: 'container',
			cls :'pad-10-0 bgwhite fa-vl container_vl_wrap',
			items:[{
				xtype: 'button',
				itemId: 'reportLostStolenCardButton',
				ui: 'none',
				hidden:true,
				action: 'reportLostStolenCardButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-10-0',
				text: 'Report lost/stolen card'
			},{
				xtype: 'button',
				itemId: 'orderNewCardButton',
				ui: 'none',
				action: 'orderNewCardButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-bottom-10',
				text: 'Order new card'
			}]
		}]
	}
});