// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.MyProducts_V', {
    extend: 'Ext.Container',
	alias: 'widget.myProducts_T',
    config: {
		itemId :'myProductsPageId',
		id:'myProductsPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'My Products',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToDashboardPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10 ',
			items:[{
				xtype: 'container',
				cls:'container_vl_wrap',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl',
					itemId:'selectVContainer',
				}]
			}]
		},
		{
			xtype: 'container',
			cls :'',
			items:[{
				xtype: 'container',
				itemId: 'myProductList',
				cls :'container_vl_wrap_grey font-16 f-bold color-2B2F3B height100 vehicleDetail pad-t10-b10', 
			},{
				xtype: 'container',
				cls :'bgwhite pad-top-20 fa-vl',
				items:[{
					xtype: 'container',
					cls :'container_vl_wrap',
					items:[{
						xtype: 'button',
						itemId: 'backToManageMyBudgetsButton',
						ui: 'none',
						action: 'goToVehicleLeasingAvailableProducts',
						cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
						text: 'View Available Products'
						}]
					}]
			}]
		}]
	}
});