// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleLeasingOrderNewCardThankYou_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingOrderNewCardThankYou_T',
    config: {
		itemId :'vehicleLeasingOrderNewCardThankYouPageId',
		id:'vehicleLeasingOrderNewCardThankYouPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Order a new card',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'vehicleLeasingOrderNewCardThankYouReturnBtn',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls: 'pad-10 fa-vl container_vl_wrap',
			items: [{				
				xtype: 'container',
				cls :'font-normal pad-10-0 font-14',
				html : '<p>Thank you for your request.<p><p>We will call you back as ASAP.<p>'
			}/*,{
				xtype: 'button',
				text: 'Return',
				itemId: 'vehicleLeasingOrderNewCardThankYouReturnBtn',
				cls: 'button_greyblue_vl claimStepButton txt-left font-16 pad-10',
			}*/]
		}]
	}
});