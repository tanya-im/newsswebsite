// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehiclesTransactionsDetail_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehiclesTransactionsDetail_T',
    config: {
		itemId :'vehiclesTransactionsDetailPageId',
		id:'vehiclesTransactionsDetailPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Transaction Details',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backTransactionsDetailPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'pad-10-0 bgE7E7E7 font-16 f-bold color-2B2F3B height100 vehicleDetail',
			items:[{
				xtype: 'container',
				cls :'bgwhite pad-10 font-normal margin-10-0 shadow_vl container_vl_wrap',
				items:[{
					xtype: 'container',
					layout:'hbox',
					cls:'pad-left-5 margin-top-7 font-14',
					items:[{
						xtype: 'label',
						html: 'Date:',
						flex:3
					},{
						xtype: 'label',
						itemId:'transactionDate',
						cls:'pad-right-5 txt-right f-bold',
						flex:1,
						html:"xx/xx/xx"
					}]
				},{
					xtype: 'container',
					layout:'hbox',
					cls:'pad-left-5 margin-top-7 font-14',
					items:[{
						xtype: 'label',
						html: 'Description:',
						flex:2
					},{
						xtype: 'label',
						itemId:'transactionDescription',
						cls:'pad-right-5 txt-right f-bold',
						flex:1,
						html:"Credit"
					}]
				},{
					xtype: 'container',
					layout:'hbox',
					cls:'pad-left-5 margin-top-7 font-14',
					items:[{
						xtype: 'label',
						html: 'Amount',
						flex:2
					},{
						xtype: 'label',
						itemId:'transactionAmount',
						cls:'pad-right-5 txt-right f-bold',
						flex:1,
						html:"$70126"
					}]
				},{
					xtype: 'container',
					hidden: true,
					layout:'hbox',
					cls:'pad-left-5 margin-top-7 font-14',
					items:[{
						xtype: 'label',
						html: 'Post tax:',
						flex:2
					},{
						xtype: 'label',
						itemId:'postTax',
						cls:'pad-right-5 txt-right f-bold',
						flex:1,
						html:"+84.56"
					}]
				}]	  
			}]
		},{
			xtype: 'container',
			cls :'margin-top-10 bgwhite pad-10-0 container_vl_wrap',
			layout:'hbox',
			items:[{
				xtype: 'button',
				itemId: 'previousTransactionButton',
				flex:1,
				ui: 'none',
				action: 'previousTransactionButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-left fa-chevron-left_1 txt-right font-16 pad-10 margin-10-10-10-0 prev_btn_vl',
				text: 'Previous'
			},{
				xtype: 'button',
				itemId: 'nextTransactionButton',
				ui: 'none',
				flex:1,
				action: 'nextTransactionButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-10-0-10-10',
				text: 'Next'
			}]
		}]
	}
});