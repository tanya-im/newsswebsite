// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleLeasingLostOrStolenCards_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingLostOrStolenCards_T',
    config: {
		itemId :'vehicleLeasingLostOrStolenCardsPageId',
		id:'vehicleLeasingLostOrStolenCardsPageId',
		cls:'container_vl',
		items: [
		{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Report a lost or stolen card',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'vlostOrStolenCardsBackBtnPress',
				action:'vlostOrStolenCardsBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls: 'pad-10 container_vl_wrap',
			items:[{
					xtype: 'label',
					itemId:'vehicleNameHeader',					
					cls: 'f-bold font-16 pad-10-0'
			},{
				xtype: 'container',
				cls:'fa-vl pad-top-10',
				html:'Please note that we will process this request during business hours.'
			},{
				xtype: 'container',
				cls :'margin-top-20 expenselist_vl_wrap',
				items:[{
					xtype:'list',
					cls :'expenseList expenselist_vl fa-vl',
					height:'600px',
					itemId: 'vehicleCardsListingLSPage',
					itemTpl: '<div class="fa_1 fa-chevron-right"><span class="font-helveticalight">{cardNumber}</span></div>',						 
					mode : 'SINGLE',
					/*listeners: {
						painted: function(comp,eOpts) {
								this.getScrollable().getScroller().on('scrollstart',function(){
									Ext.getCmp('vehicleLeasingLostOrStolenCardsPageId').setScrollable(false);
								});
								this.getScrollable().getScroller().on('scrollend',function(){
									Ext.getCmp('vehicleLeasingLostOrStolenCardsPageId').setScrollable(true);
								});
							}
						}*/
				}]
			}]
		}]
	}
});