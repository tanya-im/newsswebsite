// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehiclesExpenditureTransactions_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehiclesExpenditureTransactions_T',
    config: {
		itemId :'vehiclesExpenditureTransactionsPageId',
		id:'vehiclesExpenditureTransactionsPageId',
		cls :'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Your Transactions',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backExpenditureTransactionPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10 ',
			items:[{
				xtype: 'container',
				cls:'container_vl_wrap',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl',
					itemId:'selectVContainer',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType_1 vlabel',
						usePicker:false,
						itemId:'selectVehicleExpenditureTransaction', 
						label: 'Vehicle :', 
					}]
				}]
			}]
		},
		{
			xtype: 'container',
			cls :'',
			items:[{
			xtype: 'container',
			cls :'pad-10-0 bgwhite container_vl_wrap',
			items:[{
				xtype: 'label',
				cls:'txt-left pad-10-0',
    			html: 'Transactions For'
			},{
				xtype: 'fieldset',
				cls:' selectbox_vl',
            	items: [{
					xtype: 'selectfield',
					cls:'expenditureType',
					itemId: 'vehicleExpenditureTypes',
					usePicker:false, 
					options: []
            	}]
			}]
		},{
			xtype: 'container',
			cls :'pad-10-0',
			items:[{
				xtype: 'container',
				cls :'container_vl_wrap',
				items:[{
						
						xtype: 'container',
						id:'transactionsFromCalendar',
						itemId:'transactionsFromCalendar'
					},{
						xtype: 'container',
						id:'transactionsToCalendar',
						itemId:'transactionsToCalendar'
					}]
					}]
		},
		{
			xtype: 'container',
			cls :'container_vl_wrap margin-top-10 fa-vl',
			items:[{
				xtype: 'button',
				itemId: 'vehicleTransactionsDatesButton',
				ui: 'none',
				action: 'vehicleTransactionsDatesButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
				text: 'Submit'
				}]
		},
		{
			xtype: 'container',
			cls :'bgwhite margin-top-20 expenselist_vl_wrap container_vl_wrap',
			items:[{
				xtype:'list',
				height:'240px',
				loadingText :null,
				mode : 'SINGLE',
				id: 'vehicleExpenditureTransactionList',
				itemId: 'vehicleExpenditureTransactionList',
				cls :'expenseList expenselist_vl fa-vl',
				itemTpl:'<div class="vehicleRegistrationList"><div class="expenseDate">{transactionDate} </div><div class="expdetail clearfix"><div class="expenseName">{description} </div><div class="expenseAmount">{amount}</div></div><div class="fa fa-chevron-right" style="display:block;"></div></div>',
				listeners: {
					painted: function(comp,eOpts) {
						this.getScrollable().getScroller().on('scrollstart',function(){Ext.getCmp('vehiclesExpenditureTransactionsPageId').setScrollable(false);});
						this.getScrollable().getScroller().on('scrollend',function(){Ext.getCmp('vehiclesExpenditureTransactionsPageId').setScrollable(true);});
					}
				},
			}]
		}]
		}]
	}
});