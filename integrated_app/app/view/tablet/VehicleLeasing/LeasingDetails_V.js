// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.LeasingDetails_V', {
    extend: 'Ext.Container',
	alias: 'widget.leasingDetails_T',
    config: {
		itemId :'leasingDetailsPageId',
		id:'leasingDetailsPageId',
		cls :'container_vl',
		layout:'vbox',
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Leasing Details',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"vClaimRegListBackBtn",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls:'pad-15-10-0 fa-vl container_vl_wrap',
			items:[{
				xtype: 'button',
				itemId: 'vehicleDetails',
				action: 'vehicleDetailsPress',
				ui: 'none',
				cls:'claimStepButton button_grey_vl_2 fa fa-chevron-right',
				text: '<span class="font-helveticalight">Vehicle Details</span>'
			},{
				xtype: 'button',
				itemId: 'leaseDetails',
				action: 'leaseDetailsPress',
				ui: 'none',
				cls:'claimStepButton button_grey_vl_2 fa fa-chevron-right margin-top-10',
				text: '<span class="font-helveticalight">Lease Details</span>'
			},{
				xtype: 'button',
				itemId: 'FBTDetails',
				action: 'FBTDetailsPress',
				ui: 'none',
				cls:'claimStepButton button_grey_vl_2 fa fa-chevron-right margin-top-10',
				text: '<span class="font-helveticalight">FBT Details</span>'
			}]
		}]
	}
});