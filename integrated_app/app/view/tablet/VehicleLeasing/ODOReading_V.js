// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.ODOReading_V', {
    extend: 'Ext.Container',
	alias: 'widget.oDOReading_T',
    config: {
		itemId :'oDOReadingPageId',
		id:'oDOReadingPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Odometer Reading',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToFBTDetails",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10 ',
			items:[{
				xtype: 'container',
				cls:'container_vl_wrap',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl',
					itemId:'selectVContainer',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType_1 vlabel',
						usePicker:false,
						itemId:'selectVehicleODOReading', 
						label: 'Vehicle :', 
					}]
				}]
			}]
		},
		{
			xtype: 'container',
			cls :'container_vl_wrap',
			items:[{
			xtype: 'container',
			cls :'pad-10 font-14 color-2B2F3B',
			items:[
			{
				xtype:'container',
				itemId:'lastReadingDate',
				html:'Last reading (on 20/10/2014)'
			},
			{
				xtype:'container',
				itemId:'lastReadingId',
				cls: 'txt-center margin-top-10 f-bold',
				html:'22,258 Km'
			},
			{
				xtype: 'container',
				items:[{
					xtype: 'container',
					cls :'vborder',
					items:[{
						xtype: 'container',
						cls :'pad-10-0',
						items:[{
							xtype: 'label',
							itemId:'residualValue',
							html:"New odometer reading (Km)"
						}]	  
					},{
						xtype: 'container',
						items:[{
							xtype: 'numberfield',
							itemId: 'oDOReading',
							cls:'input_wrap_vl',
							tabIndex :1,
							name: 'oDOReading'
						}]
					}]
				}]
			}]
		},{
			xtype: 'container',
			cls :'margin-10 fa-vl',
			items:[
			{
				xtype: 'button',
				itemId: 'oDOReadingButton',
				ui: 'none',
				action: 'oDOReadingSubmitButtonPress',
				tabIndex :2,
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				text: 'Submit'
			}]
		}]
		}]
	}
});