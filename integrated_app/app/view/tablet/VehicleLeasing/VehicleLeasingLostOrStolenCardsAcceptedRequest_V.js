// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleLeasingLostOrStolenCardsAcceptedRequest_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingLostOrStolenCardsAcceptedRequest_T',
    config: {
		itemId :'vehicleLeasingLostOrStolenCardsAcceptedRequestPageId',
		id:'vehicleLeasingLostOrStolenCardsAcceptedRequestPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Report a lost or stolen card',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'vehicleLeasingLostOrStolenCardsAcceptedRequestBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls: 'pad-10 fa-vl container_vl_wrap',
			items: [
				{
					xtype: 'container',
					cls:'fa-vl margin-10',
					html:'We have accepted your request. Please note that we will process this request during business hours.'
				},
				{
					xtype: 'button',
					text: 'Back',
					itemId: 'vehicleLeasingReportCardStolenAcceptRequestBackBtnPress',
					cls: 'button_grey_vl_2 claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				}]
		}]
	}
});