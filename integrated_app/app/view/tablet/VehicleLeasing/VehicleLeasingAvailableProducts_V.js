Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleLeasingAvailableProducts_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingAvailableProducts_T',
    config: {
		itemId :'vehicleLeasingAvailableProductsPageId',
		id:'vehicleLeasingAvailableProductsPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'View Available Products',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'availableProductsListingBackBtn',
				id:'availableProductsListingBackBtn',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls: 'pad-10 fa-vl container_vl_wrap',
			items: [{
				xtype: 'container',
				itemId: 'availableProductsListing',
			}]
		}]
	}
});