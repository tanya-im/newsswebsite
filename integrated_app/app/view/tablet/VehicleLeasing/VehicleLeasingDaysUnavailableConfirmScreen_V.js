Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleLeasingDaysUnavailableConfirmScreen_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingDaysUnavailableConfirmScreen_T',
	config: {
		itemId :'vehicleLeasingDaysUnavailableConfirmScreenPageId',
		id:'vehicleLeasingDaysUnavailableConfirmScreenPageId',
		cls: 'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{	
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Days Unavailable',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'daysUnavailableConfirmBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
			
		},
		{
			xtype: 'container',
			cls :'container_vl_wrap',
			items:[{
				xtype: 'container',
				cls: 'pad-10 font-14 color-2B2F3B',
				items: [{
					xtype: 'container',
					html: 'Please Confirm that:'
				},
				{
					xtype: 'container',
					itemId:'confirmText',
					cls:'margin-top-10'
				}]
			},{		
				xtype: 'container',
				cls: 'fa-vl pad-10',
				items: [{
					xtype: 'button',
					itemId: 'daysUnavailableConfirmBtnPress',
					action:'daysUnavailableConfirmBtnPress',
					cls: 'button_greyblue_vl claimStepButton txt-left font-16 pad-10 margin-top-10 fa fa-chevron-right',
					text: 'Confirm',
				}]
			}]
		}]
	}
});