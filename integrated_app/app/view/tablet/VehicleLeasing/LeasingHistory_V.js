// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.LeasingHistory_V', {
    extend: 'Ext.Container',
	alias: 'widget.leasingHistory_T',
    config: {
		itemId :'leasingHistoryPageId',
		id:'leasingHistoryPageId',
		cls:'container_vl bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Leasing History',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToDashboardPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			itemId:'leasinghistory', 
			cls :'pad-10 font-16 f-bold color-2B2F3B height100 vehicleDetail container_vl_wrap',
		}]
	}
});