// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehiclesExpenditureOverview_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehiclesExpenditureOverview_T',
    config: {
		itemId :'vehiclesExpenditureOverviewPageId',
		id:'vehiclesExpenditureOverviewPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicles Expenditure Overview',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backToDashboardPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10 ',
			items:[{
				xtype: 'container',
				cls:'container_vl_wrap',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl',
					itemId:'selectVContainer',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType_1 vlabel',
						usePicker:false,
						itemId:'selectVehicle', 
						label: 'Vehicle :', 
					}]
				}]
			}]
		},{
			xtype: 'container',
			cls :'pad-10-0 bgwhite fa-vl container_vl_wrap',
			items:[{
				xtype: 'button',
				itemId: 'viewFullBudgetBreakdownButton',
				ui: 'none',
				action: 'viewFullBudgetBreakdownButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				text: 'View full budget breakdown'
			}]
		},{
			xtype: 'container',
			cls :'bgwhite container_vl_wrap',
			layout: 'hbox',
			items:[{
				xtype: 'container',
				flex:1,
				itemId:'currentFilterLabel',
				html: 'Last 12 Months',
				cls:'pad-12-0-5-0 font-14'
			},{
				xtype: 'container',
				flex:1,
				items:[{
					xtype: 'button',
					itemId: 'vEchangeDatesButton',
					ui: 'none',
					action: 'vEchangeDatesButtonPress',
					cls:'claimStepButton changedates_dd_dn txt-left font-16',
					text: 'Change Dates'
				}]
			}]
		},{
			xtype: 'container',
			cls :'bgwhite fa-vl container_vl_wrap',
			hidden:true,
			id:'vehicleExpenditureDateContainer',
			items:[{
				xtype: 'button',
				itemId: 'liveToDateButton',
				ui: 'none',
				action: 'vehicleExpenditureDateChangeButtonPress',
				cls:'button_ltgrey_vl txt-left font-16 pad-10 margin-top-7',
				text: 'Live To Date'
			},{
				xtype: 'button',
				itemId: 'last12monthsButton',
				ui: 'none',
				action: 'vehicleExpenditureDateChangeButtonPress',
				cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
				text: 'Last 12 Months'
			},{
				xtype: 'button',
				itemId: 'fBTYearButton',
				ui: 'none',
				action: 'vehicleExpenditureDateChangeButtonPress',
				cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
				text: 'FBT Year'
			},{
				xtype: 'button',
				itemId: 'vEcustomDateRangeButton',
				id:'vEcustomDateRangeButton',
				ui: 'none',
				action: 'vehicleExpenditureDateChangeButtonPress',
				cls:'button_ltgrey_vl txt-left font-16 pad-10  margin-top-7',
				text: 'Custom Date Range'
			}]
		},{
			xtype: 'container',
			cls :'pad-10-0 bgwhite fa-vl container_vl_wrap',
			id:'vehicleExpenditureDateRange',
			hidden:true,
			items:[{
				xtype: 'container',
				id:'vehiclesExpenditureFromCalendar',
				itemId:'vehiclesExpenditureFromCalendar'
			},{
				xtype: 'container',
				id:'vehiclesExpenditureToCalendar',
				itemId:'vehiclesExpenditureToCalendar'
			},{
				xtype: 'button',
				itemId: 'vehiclesExpenditureDatesButton',
				ui: 'none',
				action: 'vehiclesExpenditureDatesButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				text: 'Submit'
			}]
		},{
			xtype: 'container',
			itemId: 'expenses',
		}]
	}
});