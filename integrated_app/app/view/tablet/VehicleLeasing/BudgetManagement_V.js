// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.BudgetManagement_V', {
    extend: 'Ext.Container',
	alias: 'widget.BudgetManagement_T',
    config: {
		itemId :'BudgetManagementPageId',
		id:'BudgetManagementPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Fuel Budget Management',
					itemId:'budgetManagement',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backBudgetManagementPress",
				flex:1,
                html:'Back',
				tabIndex:1,
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls:'pad-10-0 fa-vl container_vl_wrap',
			items:[{
				xtype: 'label',
				cls:'txt-left font-14',
				html: 'Increase Annual Budget:'
			},{
				xtype: 'container',
				cls :'bgwhite font-16 f-bold ',
				items:[{
					xtype: 'numberfield',
					itemId: 'budgetManagementTextBox',
					name: 'budgetManagementTextBox',
					tabIndex:2,
					placeHolder:'$', 
					cls:'font-12 pad-left-5 input_wrap_vl'
				}]
			},{
				xtype: 'container',
				cls :'pad-10 margin-top-10 bgF9F9F9 font-14',
				layout: 'hbox',
				items:[{
					xtype: 'label',
					cls:'txt-left',
					flex:3,
					tabIndex:3,
    				html: 'Life To Date Actual Spend:'	
				},{
					xtype: 'label',
					cls:'txt-right',
					itemId:'actualSpending',
					flex:1,
    			}]
			},{
				xtype: 'container',
				cls :'pad-10 bgF9F9F9 font-14',
				layout: 'hbox',
				items:[{
					xtype: 'label',
					cls:'txt-left',
					flex:3,
    				html: 'Total Annual Budget:'	
				},{
					xtype: 'label',
					cls:'txt-right',
					flex:1,
					itemId:'totalBudget'
    			}]
			},{
				xtype: 'container',
				cls:'fa-vl pad-top-10',
				layout: 'hbox',
				items:[{
					xtype: 'button',
					itemId:'budgetSubmitButtonPress', 
					ui: 'none',
					flex:1,
					action: 'budgetSubmitButtonPress',
					cls:'button_greyblue_vl claimStepButton fa_1 fa-chevron-right txt-left font-16 pad-10 next_btn_vl',
					tabIndex:4,
					data:{"expense":null},
					text: 'Update Budget'
				}]
			}]
		}]	
	}
});