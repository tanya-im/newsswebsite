// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleLeasingOrderNewCard_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingOrderNewCard_T',
    config: {
		itemId :'vehicleLeasingOrderNewCardPageId',
		id:'vehicleLeasingOrderNewCardPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Order a new card',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'vehicleLeasingOrderNewCardTopBackBtnPress',
				action:'vehicleLeasingOrderNewCardTopBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10 ',
			items:[{
				xtype: 'container',
				cls:'container_vl_wrap',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl',
					itemId:'selectVContainer',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType_1 vlabel',
						usePicker:false,
						itemId:'selectVehicleOrderNewCard', 
						label: 'Vehicle :', 
					}]
				}]
			}]
		},
		{
			xtype: 'container',
			cls: 'pad-10 fa-vl container_vl_wrap',
			items: [
			{				
				xtype: 'container',
				cls :'font-normal pad-10 font-14',
				items:[
				{
					xtype: 'label',
					cls:'pad-left-5',
					html: '<p>Please confirm your personal details:</p>',
				},
				{
					xtype: 'container',
					items:[
					{
							
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Name:',
							flex:1
						},{
							xtype: 'label',
							itemId:'user_name',
							cls:'txt-left',
							flex:6
						}]
					},
					{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Email:',
							flex:1
						},{
							xtype: 'label',
							itemId:'user_email',
							cls:'txt-left',
							flex:6
						}]
					},
					{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Phone:',
							flex:1
						},{
							xtype: 'label',
							itemId:'user_contact',
							cls:'txt-left',
							flex:6
						}]
					}]	
				}]
			
			
			},
			{
				xtype: 'container',
				cls:'fa-vl margin-10',
				items: [{
					xtype: 'button',
					text: 'Submit',
					itemId: 'vehicleLeasingOrderNewCardSubmitBtnPress',
					cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				}]
			}]
		}]
	}
});