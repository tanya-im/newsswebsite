// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleDetails_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleDetails_T',
    config: {
		itemId :'vehicleDetailsPageId',
		id:'vehicleDetailsPageId',
		cls:'container_vl bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Details',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"vehicleDetailBackBtnPress",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'pad-10 pad-top-5 bgE7E7E7 font-16 color-2B2F3B height100 vehicleDetail container_vl_wrap',
			items:[{
				xtype: 'container',
				//layout:'hbox',
				cls:'',
				items:[{xtype: 'fieldset',
					//flex:1,
					cls:'selectbox_vl_2 selectbox_vl',
					items: [{
						xtype: 'selectfield',
						usePicker:false,
						label: 'Vehicle :',
						itemId:'selectVehicle', 
						cls:'expenditureType_1 vlabel',
						options: [
							{text: 'Vehicle One',  value: 'Vehicle One'},
							{text: 'Vehicle Two', value: 'Vehicle Two'},
							{text: 'Vehicle Three',  value: 'Vehicle Three'}
						]
					}]
				}]
			},{
				xtype: 'container',
				cls :'bgwhite pad-10 margin-top-10 font-normal shadow_vl',
				items:[{
					xtype: 'container',
					cls :'vborder',
					items:[{
						xtype: 'container',
						cls :'bgwhite',
						items:[{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Vehicle Status:',
								flex:1
							},{
								xtype: 'label',
								itemId:'vehicleStatus',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Rego Status:',
								flex:1
							},{
								xtype: 'label',
								itemId:'regoState',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Rego Number:',
								flex:1
							},{
								xtype: 'label',
								itemId:'regoNumber',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey pad-bottom-7 greyborder_vl',
							items:[{
								xtype: 'label',
								html: 'Rego Expiry:',
								flex:1
							},{
								xtype: 'label',
								itemId:'regoExpiry',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						}]	
					},{
						
						xtype: 'container',
						cls :'bgwhite',
						items:[{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Vehicle Make:',
								flex:1
							},{
								xtype: 'label',
								itemId:'vehicleMake',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Model:',
								flex:1
							},{
								xtype: 'label',
								itemId:'vModel',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Year:',
								flex:1
							},{
								xtype: 'label',
								itemId:'vYear',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'VIN:',
								flex:1
							},{
								xtype: 'label',
								itemId:'vIN',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey pad-bottom-7 greyborder_vl',
							items:[{
								xtype: 'label',
								html: 'Engine Number:',
								flex:1
							},{
								xtype: 'label',
								itemId:'engineNumber',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						}]	
					},{
						xtype: 'container',
						cls :'bgwhite',
						items:[{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Policy Number:',
								flex:1
							},{
								xtype: 'label',
								itemId:'policyNumber',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Provider:',
								flex:1
							},{
								xtype: 'label',
								itemId:'provider',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Start Date:',
								flex:1
							},{
								xtype: 'label',
								itemId:'startDate',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'End Date:',
								flex:1
							},{
								xtype: 'label',
								itemId:'endDate',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						},{
							xtype: 'container',
							layout:'hbox',
							cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: 'Amount:',
								flex:1
							},{
								xtype: 'label',
								itemId:'amount',
								cls:'pad-right-5 txt-right f-bold',
								flex:1,
								html:"xxxxxx"
							}]
						}]	
					}]
				}]
			}/*,{
				xtype: 'container',
				cls :'margin-top-10 fa-vl',
				items:[{
					xtype: 'button',
					itemId: 'backToManageMyBudgetsButton',
					ui: 'none',
					action: 'backToManageMyBudgetsButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
					text: 'Back to manage my budgets'
				}]
			}*/]
		}]
	}
});