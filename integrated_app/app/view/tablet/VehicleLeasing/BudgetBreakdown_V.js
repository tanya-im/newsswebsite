// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.BudgetBreakdown_V', {
    extend: 'Ext.Container',
	alias: 'widget.budgetBreakdown_T',
	requires: ['Ext.chart.PolarChart', 'Ext.chart.series.Pie3D', 'Ext.chart.interactions.RotatePie3D'],
    config: {
		itemId :'budgetBreakdownPageId',
		id:'budgetBreakdownPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Budget Breakdown',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                //action:"backToDashboardPress",
				flex:1,
                html:'Back',
				id:'budgetBreakdownBackButton',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10',
			items:[{
				xtype: 'container',
				cls:'container_vl_wrap',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl',
					itemId:'selectVContainer',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType_1 vlabel',
						usePicker:false,
						itemId:'selectVehicle', 
						label: 'Vehicle :',  
					}]
				}]
			}]
		},{
			xtype: 'container',
			cls :'bgwhite pad-10-0 container_vl_wrap',
			layout: 'hbox',
			items:[{
				xtype: 'container',
				flex:1,				 
				itemId:'currentFilterLabel',
				html: 'Last 12 Months',
				cls:'pad-10 font-14 margin-top-7'
				 
			},{
				xtype: 'container',
				flex:1,
				items:[{
					xtype: 'button',
					itemId: 'changeDatesBudgetBreakdownButton',
					ui: 'none',
					action: 'changeDatesBudgetBreakdownButtonPress',
					cls:'claimStepButton changedates_dd_dn txt-left font-16 margin-top-7',
					text: 'Change Dates'
				}]
			}]
		},{
			xtype: 'container',
			cls :'bgwhite fa-vl pad-10-0 hidearrow container_vl_wrap',
			hidden:true,
			id:'budgetBreakdownDateContainer',
			itemId:'budgetBreakdownDateContainer',
			items:[{
				xtype: 'button',
				itemId: 'liveToDateButton',
				ui: 'none',
				action: 'budgetBreakdownDateChangeButtonPress',
				cls:'button_ltgrey_vl fa fa-chevron-right txt-left font-16 pad-10 margin-top-7',
				text: 'Live To Date'
			},{
				xtype: 'button',
				itemId: 'last12monthsButton',
				id:'last12monthsButton',
				ui: 'none',
				action: 'budgetBreakdownDateChangeButtonPress',
				cls:'salectedDate fa fa-chevron-right txt-left font-16 pad-10 margin-top-7 ',
				text: 'Last 12 Months'
			},{
				xtype: 'button',
				itemId: 'fBTYearButton',
				ui: 'none',
				action: 'budgetBreakdownDateChangeButtonPress',
				cls:'button_ltgrey_vl fa fa-chevron-right txt-left font-16 pad-10 margin-top-7',
				text: 'FBT Year'
			},{
				xtype: 'button',
				itemId: 'customDateRangeButton',
				ui: 'none',
				action: 'budgetBreakdownDateChangeButtonPress',
				cls:'button_ltgrey_vl fa fa-chevron-right txt-left font-16 pad-10 margin-top-7',
				text: 'Custom Date Range'
			}]
		},{
			xtype: 'container',
			cls :'pad-bottom bgwhite fa-vl container_vl_wrap',
			id:'budgetBreakdowncustomDateRange',
			itemId:'budgetBreakdowncustomDateRange',
			hidden:true,
			items:[{
				xtype: 'container',
				id:'breakdownFromCalendar',
				itemId:'breakdownFromCalendar'
			},{
				xtype: 'container',
				id:'breakdownToCalendar',
				itemId:'breakdownToCalendar'
			},{
				xtype: 'button',
				itemId: 'breakdownDatesButton',
				ui: 'none',
				action: 'breakdownDatesButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
				text: 'Submit'
			}]
		},{
			 
			xtype: 'container', 
			layout:'fit', 
			height:'200px',
			cls:'container_vl_wrap',
			items:[{ 
                id:'pieChartIn3D', 
                itemId:'pieChartInBudget',
                xtype: 'polar',
		        width: '100%',  
                background: 'white',
                interactions: 'rotatePie3d',
                store: {
                    fields: ['name', 'budget' ],
                    data: [
						{'name':'Jan', 'budget':10 },
						{'name':'Feb', 'budget':7},
						{'name':'March','budget':5},
						{'name':'Apr', 'budget':2},
						{'name':'May', 'budget':10 },
						{'name':'June', 'budget':6},
						{'name':'July', 'budget':8 },                               
                    ]
                },
                colors: [
                    '#4F2C7E',
                    '#EB2939',
                    '#0070BB',
                    '#E88200',
                    '#ABDDE4', 
                    '#B0B3B1',
                    '#BCD400'
                ],
                animate: {
                    duration: 1000,
                    easing: 'easeInOut'
                },
                series: [
                    {
                        type: 'pie3d',
                        field: 'budget',
                        donut: 50,
                        distortion: 0.4,
                        style: {
                            stroke: "white",
                            strokeStyle: 'white',
                    		opacity: 0.90
                        }
                    }
                ]
            }]
        	 
		},{
			xtype: 'container',
			cls:'bgLightGreyVehicale height100 fa-vl container_vl_wrap',
			id:'budgetTypeList',
			itemId:'budgetTypeList',
			 
		},{
				xtype: 'container',
				cls:'margin-10-0 fa-vl margin-bottom-20 container_vl_wrap',
				items:[{
					xtype: 'button',
					itemId: 'manageMyBudgetButton',
					ui: 'none',
					flex:1,
					action: 'manageMyBudgetButtonPress',
					cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-10-0',
					text: 'Manage My Budget'
				}]
			}]
	}
});