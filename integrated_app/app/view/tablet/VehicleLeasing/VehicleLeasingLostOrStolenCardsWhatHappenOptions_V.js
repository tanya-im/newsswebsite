// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleLeasingLostOrStolenCardsWhatHappenOptions_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingLostOrStolenCardsWhatHappenOptions_T',
    config: {
		itemId :'vehicleLeasingLostOrStolenCardsWhatHappenOptionsPageId',
		id:'vehicleLeasingLostOrStolenCardsWhatHappenOptionsPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Report a lost or stolen card',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'lostOrStolenCardsBackBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			cls: 'pad-10 container_vl_wrap',
			items: [{
					xtype: 'container',
					cls:'fa-vl margin-10',
					itemId:'pressdeCradNo',
					id:'pressdeCradNo',
					html:''
				},
				{
					xtype: 'container',
					cls:'fa-vl margin-10',
					items: [{
						xtype: 'button',
						text: 'Report the card lost',
						itemId: 'vehicleLeasingReportCardLostBtnPress',
						action: 'ReportCardLostBtnPress',
						cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 margin-bottom-5',
					},
					{
						
						xtype: 'button',
						text: 'Report the card Stolen',
						itemId: 'vehicleLeasingReportCardStolenBtnPress',
						action: 'ReportCardStolenBtnPress',
						cls: 'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 ',
					}]
				}]
		}]
	}
});