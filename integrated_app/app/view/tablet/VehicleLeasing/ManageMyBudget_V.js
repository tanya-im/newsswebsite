// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.ManageMyBudget_V', {
    extend: 'Ext.Container',
	alias: 'widget.manageMyBudget_T',
    config: {
		itemId :'manageMyBudgetPageId',
		id:'manageMyBudgetPageId',
		cls:'container_vl',
		scrollable: {
		   	direction: 'vertical',
		   	directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Manage My Budget',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"backManageMyBudget",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{	
			xtype: 'container',
			cls :'bgwhite border-bottom pad-bottom-10 pad-top-10 ',
			items:[{
				xtype: 'container',
				cls:'container_vl_wrap',
				items:[{
					xtype: 'fieldset',
					cls:'selectbox_vl',
					itemId:'selectVContainer',
					items: [{
						xtype: 'selectfield',
						cls:'expenditureType_1 vlabel',
						usePicker:false,
						itemId:'selectVehicleBudgetBreakdown', 
						label: 'Vehicle :', 
					}]
				}]
			}]
		},
		{
			xtype: 'container',
			cls:'pad-15-10-0 fa-vl container_vl_wrap',
			items:[{
				xtype: 'label',
				cls:'txt-left',
				html: 'Which budget would you like to manage?'
			},{
				xtype: 'container',
				itemId:'expenseList'
			}]
		}]
	}
});