// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleLeasing.VehicleLeasingAvailableProductsThankyou_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleLeasingAvailableProductsThankyou_T',
    config: {
		itemId :'vehicleLeasingAvailableProductsThankyouPageId',
		id:'vehicleLeasingAvailableProductsThankyouPageId',
		cls:'container_vl',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [
		{
			xtype: 'container',
			cls :'pad-5 pad-left-10 green-cyan-bg',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'View Available Products',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                itemId: 'viewAvailableProductsThakyouReturnBtnPress',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			items:[{
				xtype: 'label',
				itemId:'productsNameLabel',
				cls: 'f-bold font-18 pad-10-0 container_vl_wrap'
			},
			{	
				xtype: 'container',
				cls :'font-normal font-14 container_vl_wrap',
				items:[{
					xtype: 'container',
					html: '<p>Thank you for your request.</p><p>We will call you back as ASAP.</p>',
				}]
				
			}]
		}]
	}
});