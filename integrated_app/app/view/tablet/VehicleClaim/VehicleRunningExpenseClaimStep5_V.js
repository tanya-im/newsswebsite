// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleClaim.VehicleRunningExpenseClaimStep5_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleRunningExpenseClaimStepFive_T',
    config: {
		itemId :'vehicleRunningExpenseClaimStepFivePageId',
		id:'vehicleRunningExpenseClaimStepFivePageId',
		cls:'bgdarkblue bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-left-10 green-bg-gredient',
			items:[{
				xtype: 'panel',
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Running Expense Claim',
					cls: 'font-white font-18 pad-5'
				}]
			}]
		},{
			
			xtype: 'container',
			cls: 'bgwhite',
			items: [{
				xtype: 'container',
				cls :'bgE7E7E7 font-16 txt-center  header_wrap_grey',
				items:[{
					xtype: 'image',
					//src:'resources/images/SS04-Mobile-Claims-Vehicle-5.png',
					cls:'Claims-Vehicle-steps-img step5_img'
				}]
			},{
				xtype: 'container',
				cls :'pad-10 bgwhite',
				id:'vRegSelectTxt',
				items:[{
					xtype: 'label',
					html: '<h3 class="bluetxt success">Congratulations, your claim submission is now complete.</h3>',
					cls: 'font-white font-16 pad-left-5'
				}]
			}]
		},{
			xtype: 'container',
			cls :'margin-10-10-0-10 pad-10 bgwhite font-16 color-2B2F3B',
			 
			items:[{
				xtype: 'container',
				cls :'font-14 color-2B2F3B',
				layout:'hbox',
				items:[{
					xtype: 'label',
					flex: 1,
					id:'vehicleClaimRefNo',
					itemId:'vehicleClaimRefNo',
				}]
			},{
				xtype: 'label',
				flex: 1,
				html: ' Your claim form will be reviewed and processed',
				cls:'f-bold'
			}]
		},{		
			xtype: 'container',
			cls :'margin-0-10 bgwhite font-16 color-2B2F3B claimStepOneNextButtonWrap',			
			id:'vehicleRunningExpenseClaimConainerId',
			items:[{
				xtype: 'button',
				ui: 'none',
				action: 'trackTheProgressOfYourClaimPress',
				flex:1,
				cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 color-5A5B77',
				text: 'Track the progress of your claim'
			}]
		},{
			xtype: 'spacer',
			cls :'pad-10 mar-top-15 bgwhite'
		},{
			xtype: 'container',
			layout: 'hbox',
			items:[
			{
				xtype: 'container',
				cls :'pad-10 bgE7E7E7 font-20 color-2B2F3B',
				flex: 1,
				items:[{		
						xtype: 'container',
						cls :'font-16 color-2B2F3B mar-0-10 fontFFamilyHelvetica f-bold pad-left-10',
						items:[{
							xtype: 'label',
							flex: 1,
							html: ' Claim Summary'
						}]
					},{
						xtype: 'container',
						cls :'pad-15-10-0 bgwhite font-16 color-2B2F3B margin-10-0',
						items: [{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
									items:[{
											xtype: 'label',
											html: 'Claim lodgement date:',
											flex:3
										},{
											xtype: 'label',
											html: Ext.Date.format(new Date(),'d M Y'),
											cls:'pad-right-5 txt-right f-bold',
											flex:2
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
									items:[{
											xtype: 'label',
											html: 'Total Reimbursement Amount:',
											flex:3
										},{
											xtype: 'label',
											itemId:'totalAmountThirdPage',
											id:'vehicleClaimTotalAmountSubmittedPage',
											html: '',
											cls:'pad-right-5 txt-right f-bold',
											flex:2
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
									items:[{
											xtype: 'label',
											html: 'Employee Name:',
											flex:3
										},{
											xtype: 'label',
											id: 'vehicleClaimFirstName',
											html: '',
											cls:'pad-right-5 txt-right f-bold',
											flex:2
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
									items:[{
											xtype: 'label',
											html: 'Payroll Number:',
											flex:3
										},{
											xtype: 'label',
											id: 'vehicleClaimPayrollNumber',
											html: '12182',
											cls:'pad-right-5 txt-right f-bold',
											flex:2
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
									items:[{
											xtype: 'label',
											html: 'Employer:',
											flex:3
										},{
											xtype: 'label',
											id:'vehicleClaimEmployerName',
											html: '',
											cls:'pad-right-5 txt-right f-bold',
											flex:2
									}]
								}]
					}]
			},{
				xtype: 'container',
				flex: 1,
				itemId:'leftTotalClaimDetailWrapper',
				cls :'min-height-162 pad-10 bgE7E7E7 font-white font-normal cappedClaimRightSideWrapper',
				items:[
				{
					xtype: 'container',
					cls :'bgwhite font-16 color-2B2F3B',
					id:'vehicleClaimDetailedSummaryWrapper',
					itemId:'vehicleClaimDetailedSummaryWrapper',
				}]
			}]
		},{
			xtype: 'container',
			cls :'pad-10 bgwhite font-16 txt-center claimStepOneNextButtonwrap',
			id:'vehicleRunningExpenseClaimConainerId2',
			items:[{
				xtype: 'button',
				itemId: 'returnToYourClaimsButton',
				ui: 'none',
				action: 'returnToYourClaimsPress',
				flex:1, 
				cls:'claimStepOneNextButton claimStepButton bgpurpule_submit txt-left font-16 pad-10 ',
				text: 'Return to your claims'
			}]
		}]
	}
});