// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleClaim.VehicleRunningExpenseClaimStep2_V', {
    extend: 'Ext.form.Panel',
	alias: 'widget.vehicleRunningExpenseClaimStepTwo_T',
    config: {
		itemId :'vehicleRunningExpenseClaimStepTwoPageId',
		id:'vehicleRunningExpenseClaimStepTwoPageId',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-left-10 green-bg-gredient',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Running Expense Claim',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"vClaimStepTwoBackBtn",
				id:'vClaimStepTwoBackBtn',
				itemId:'vClaimStepTwoBackBtn',
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			items: [{
				cls :'bgE7E7E7 header_wrap_grey',
				items:[{
					xtype: 'image',
					//src:'resources/images/SS04-Mobile-Claims-Vehicle-2.png',
					cls:'Claims-Vehicle-steps-img step2_img'
				}]
			},{
				xtype: 'container',
				cls :'pad-10 bgwhite font-16',
				id:'vRegSelectTxtStepTwo',
				itemId:'vRegSelectTxtStepTwo',
				items:[{
					xtype: 'label',
					id:'vehicleRegLabelStepTwo',
					itemId:'vehicleRegLabelStepTwo',
					cls: 'font-white font-16 pad-left-5 bluetxtwrap'
				}]
			}]
		},{
			xtype: 'container',
			id:'vehicleRunningExpenseClaimFormfieldWrapper',
			itemId: 'vehicleRunningExpenseClaimFormfieldWrapper',
			cls:'bgwhite greywrap'
		},
		{
			xtype: 'container',
			cls :'pad-top-10 pad-bottom-10 bgE7E7E7',
			layout: 'vbox',
			id:'claimStepsTwoButtonWrapper',
			itemId:'claimStepsTwoButtonWrapper',
			items:[{
				xtype: 'container',
				cls: 'font-16',
				layout: 'hbox',
				items:[{
					xtype: 'button',
					itemId: 'claimStepTwoBackButton',
					ui: 'none',
					action: 'claimStepTwoBackButtonPress',
					flex:1,
					cls:'claimStepLeftArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Back'
				},{
					xtype: 'button',
					itemId: 'claimStepTwoNextButton',
					id:'claimStepTwoNextButton',
					ui: 'none',
					action: 'claimStepTwoNextButtonPress',
					flex:1,
					cls:'claimStepRightArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Next'
				}]
			},{
				xtype: 'container',
				cls: 'font-16',
				layout: 'hbox',
				items:[{
					xtype: 'button',
					itemId: 'claimStepTwoCancelButton',
					ui: 'none',
					action: 'claimStepTwoCancelButtonPress',
					flex:1,
					cls:'claimStepLeftArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Cancel'
				},{
					xtype: 'button',
					itemId: 'vClaimStepTwoSaveAsDraftButton',
					id: 'vClaimStepTwoSaveAsDraftButton',
					action: 'vClaimStepTwoSaveAsDraftButtonPress',
					ui: 'none',
					flex:1,
					cls:'claimStepRightArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Save as Draft'
				}]
			}]
		}]
	}
});