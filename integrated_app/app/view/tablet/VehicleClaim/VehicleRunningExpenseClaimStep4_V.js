// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleClaim.VehicleRunningExpenseClaimStep4_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleRunningExpenseClaimStepFour_T',
    config: {
		itemId :'vehicleRunningExpenseClaimStepFourPageId',
		id:'vehicleRunningExpenseClaimStepFourPageId',
		cls:'bgdarkblue bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-left-10 green-bg-gredient',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Running Expense Claim',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"vClaimStepFourBackBtn",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls: 'bgwhite',
			items: [{
				xtype: 'container',
				cls :'bgE7E7E7 font-16 txt-center header_wrap_grey',
				items:[{
					xtype: 'image',
					//src:'resources/images/SS04-Mobile-Claims-Vehicle-4.png',
					cls:'Claims-Vehicle-steps-img step4_img'
				}]
			},{
				xtype: 'container',
				cls :'pad-10 bgwhite',
				items:[{
					xtype: 'label',
					id:'vehicleRegLabelStepFour',
					itemId:'vehicleRegLabelStepFour',
					cls: 'font-purple font-16 pad-left-5 bluetxtwrap'
				}]
			}]
		},{
			xtype: 'container',
			cls :'pad-10 bgE7E7E7 font-20 font-normal color-2B2F3B',
			items:[
			{
				xtype: 'label',
				flex: 1,
				cls:'pad-left-20 font-16 f-bold',
				html: 'Your Details'
			},{
				xtype: 'container',
				cls :'bgwhite margin-10-5 font-normal',
				items:[{
					xtype: 'container',
					cls :'pad-5-10 bgwhite',
					items:[{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Name on account:',
							flex:3
						},{
							xtype: 'label',
							itemId:'nameOnAc',
							id:'nameOnAc',
							cls:'pad-right-5 txt-right f-bold',
							flex:1
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'BSB:',
							flex:3
						},{
							xtype: 'label',
							itemId:'BSB',
							id:'BSB',
							cls:'pad-right-5 txt-right f-bold',
							flex:1
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-16 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Account number:',
							flex:3
						},{
							xtype: 'label',
							itemId:'acNo',
							id:'acNo',
							cls:'pad-right-5 txt-right f-bold',
							flex:1
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-16 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Email:',
							flex:1
						},{
							xtype: 'label',
							itemId:'user_email',
							id:'user_email',
							cls:'pad-right-5 txt-right f-bold',
							flex:3
						}]
					},{
						xtype: 'container',
						layout:'hbox',
						cls:'pad-left-5 m-5-0 font-16 f-darkgrey',
						items:[{
							xtype: 'label',
							html: 'Contact number:',
							flex:2
						},{
							xtype: 'label',
							itemId:'user_contact',
							id:'user_contact',
							cls:'pad-right-5 txt-right f-bold',
							flex:3
						}]
					}]	
				},{
					xtype: 'container',
					cls :'bgwhite font-14 color-2B2F3B clearfix',
					items:[{
						xtype: 'button',
						ui: 'none',
						cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 color-5A5B77',
						text: 'Change account'
					}]
				}]
			}]
		},{
			xtype: 'container',
			cls :'pad-10 bgwhite',
			items:[
			{
				xtype: 'label',
				html: 'Declaration',
				cls: 'font-16 pad-15 f-bold'
			},
			{
				xtype: 'container',
				cls:'pad-left-5 pad-bottom-10 m-5-0 font-14 f-darkgrey declarationWp',
				items:[{
					xtype: 'checkboxfield',
					name : 'declarationbox',
					id:'vehicleDeclarationCheckbox',
					itemId:'declarationbox',
					value: 1,
					checked:false,
					cls:'dec-check',
					docked:'left',
				},{
					xtype: 'label',
					itemId:'declarationtxt',
					id:'declarationtxt',
					cls:'pad-left-15'
				}]
			}]
		},{
			xtype: 'container',
			cls :'pad-top-10 pad-bottom-10 bgE7E7E7',
			layout: 'vbox',
			id:'claimStepsButtonWrapper',
			items:[
			{
				xtype: 'container',
				cls: 'font-16',
				layout: 'hbox',
				items:[{
					xtype: 'button',
					itemId: 'claimStepFourBackButton',
					ui: 'none',
					action: 'claimStepFourBackButtonPress',
					flex:1,
					cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Back'
				},
				{
					xtype: 'button',
					itemId: 'claimStepFourSubmitButton',
					ui: 'none',
					action: 'claimStepFourSubmitButtonPress',
					flex:1,
					cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Submit Claim'
				}]
			},
			{
				xtype: 'container',
				cls: 'font-16',
				layout: 'hbox',
				items:[{
					xtype: 'button',
					itemId: 'claimStepFourCancelButton',
					ui: 'none',
					flex:1,
					action: 'claimStepFourCancelButtonPress',
					cls:'claimStepOneBackButton claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Cancel'
				},{
					xtype: 'button',
					ui: 'none',
					flex:1,
					cls:'claimStepOneCancelButton claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Save as draft'
				}]
			}]
		
		}]
	}
});