// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleClaim.VehicleRunningExpenseClaimStep3_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleRunningExpenseClaimStepThree_T',
    config: {
		itemId :'vehicleRunningExpenseClaimStepThreePageId',
		id:'vehicleRunningExpenseClaimStepThreePageId',
		cls:'bgdarkblue bgE7E7E7',
		scrollable: {
		    direction: 'vertical',
		    directionLock: true
		},
		items: [{
			xtype: 'container',
			cls :'pad-left-10 green-bg-gredient',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Running Expense Claim',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"vClaimStepThreeBackBtn",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},{
			xtype: 'container',
			cls: 'bgwhite',
			items: [{
				xtype: 'container',
				cls :'bgE7E7E7 font-16 txt-center header_wrap_grey',
				items:[{
					xtype: 'image',
					//src:'resources/images/SS04-Mobile-Claims-Vehicle-3.png',
					cls:'Claims-Vehicle-steps-img step3_img'
				}]
			},{
				xtype: 'container',
				flex: 1,
				cls :'pad-10 bgwhite',
				items:[{
					xtype: 'label',
					id:'vehicleRegLabelStepThree',
					itemId:'vehicleRegLabelStepThree',
					cls: 'font-purple font-16 pad-left-5 bluetxtwrap'
				}]
			}]
		},{
			xtype: 'container',
			layout: 'hbox',
			items:[{
				xtype: 'container',
				flex: 1,
				cls :'bgE7E7E7 font-16 color-2B2F3B',
				items:[{
					xtype: 'container',
					id:'claimDetailWrapper',
					itemId:'claimDetailWrapper',
				},{
					xtype: 'container',
					cls :'clearfix pad-bottom-10 bgE7E7E7 margin-top-10',
					items:[{
						xtype: 'container',
						cls: 'font-16',
						items:[{
							xtype: 'button',
							itemId: 'SaveAddAnotherExpense',
							ui: 'none',
							action: 'SaveAddAnotherExpensePress',
							cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 margin-10 bgpurple1',
							text: 'Save & Add Another Expense'
						}]
					}]
				}]
			},{
				xtype: 'container',
				flex: 1,
				id:'totalClaimDetailWrapper',
				itemId:'totalClaimDetailWrapper',
				cls :'pad-10 bgE7E7E7 font-white vclaimtotalwap font-normal pad-top-42',
				items:[
				{
					xtype: 'container',
					cls :'pad-10 bgpurpule',
					items:[{
						xtype: 'label',
						id:'vTotalClaimAmountStepThree',
						itemId:'vTotalClaimAmountStepThree',
						cls:'pad-left-5 font-16 pad-bottom-10'
					},{
						xtype: 'label',
						id:'vTotalClaimAmountValueStepThree',
						itemId:'vTotalClaimAmountValueStepThree',
						cls:'pad-left-5 font-26'
					}]
				}]
			}
		]},
		{
			xtype: 'container',
			cls :'pad-top-15 bgwhite',
			layout: 'vbox'
		},{
			xtype: 'container',
			cls :'pad-top-10 pad-bottom-10 bgE7E7E7',
			layout: 'vbox',
			id:'claimStepsOneButtonWrapper',
			itemId:'claimStepsOneButtonWrapper',
			items:[{
				xtype: 'container',
				cls: 'font-16',
				layout: 'hbox',
				items:[{
					xtype: 'button',
					itemId: 'claimStepThreeBackButton',
					ui: 'none',
					action: 'claimStepThreeBackButtonPress',
					flex:1,
					cls:'claimStepLeftArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Back'
				},{
					xtype: 'button',
					itemId: 'claimStepThreeNextButton',
					id:'claimStepThreeNextButton',
					action: 'claimStepThreeNextButtonPress',
					flex:1,
					cls:'claimStepRightArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Next'
				}]
			},{
				xtype: 'container',
				cls: 'font-16',
				layout: 'hbox',
				items:[{
					xtype: 'button',
					itemId: 'claimStepThreeCancelButton',
					ui: 'none',
					action: 'claimStepThreeCancelButtonPress',
					flex:1,
					cls:'claimStepLeftArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Cancel'
				},
				{
					xtype: 'button',
					itemId: 'vClaimStepThreeSaveAsDraftButton',
					id: 'vClaimStepThreeSaveAsDraftButton',
					action: 'vClaimStepThreeSaveAsDraftButtonPress',
					flex:1,
					cls:'claimStepRightArrow claimStepButton txt-left font-16 pad-10 margin-10',
					text: 'Save as Draft'
				}]
			}]
		
		}]
	}
});