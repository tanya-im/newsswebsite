// View file for home page. This file create only html for the app no logic exist here. 
Ext.define('SmartSalary.view.tablet.VehicleClaim.VehicleRegistrationList_V', {
    extend: 'Ext.Container',
	alias: 'widget.vehicleRegistrationListView_T',
    config: {
		itemId :'vehicleRegistrationListPageId',
		id:'vehicleRegistrationListPageId',
		layout: 'vbox',
		cls:'bgdarkblue',
		layout:'vbox',
		items: [
		{
			xtype: 'container',
			docked: 'top',
			cls :'pad-left-10 green-bg-gredient',
			layout:'hbox',
			items:[{
				xtype: 'panel',
				flex:4,
				items:[{
					xtype: 'toolbar',
					title: 'Vehicle Running Expense Claim',
					cls: 'font-white font-18 pad-5'
				}]
			},{
				xtype:"button",
                action:"vClaimRegListBackBtn",
				flex:1,
                html:'Back',
				cls: 'font-white searchlistbackbtn'
			}]
		},
		{
			xtype: 'container',
			docked: 'top',
			cls :'pad-10 bgwhite',
			id:'vRegSelectTxt',
			items:[
			{
				xtype: 'label',
				html: '<h3>Select Vehicle</h3>',
				cls: 'font-white font-16 pad-left-5'
			},
			{
				xtype: 'label',
				html: '<p>Select the registration number of the vehicle that relates to this claim.</p>',
				cls: 'font-white font-14 pad-left-5'
			}]
		},
		{
			xtype: 'container',
			flex:6,
			id:'vehicleRegistrationListWrapper',
			items:[
			{
				xtype:'list',
				height:'100%',
				store:'VehicleRegistrationList',
				itemId: 'vehicleRegistrationList',
				id:'vehicleRegistrationList',
				cls :'vehicleRegistrationList',
				itemTpl:'<div class="vehicleRegistrationList"> {RegoNo} </div>',
				onItemDisclosure: true
			}]
		}]
	}
});