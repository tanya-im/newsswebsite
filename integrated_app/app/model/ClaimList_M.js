Ext.define('SmartSalary.model.ClaimList_M', {
    extend: 'Ext.data.Model',
    config:{
        fields: [
            { name: 'ClaimOnlineId', type: 'string' },
            { name: 'ClaimTypeId', type: 'string' },
            { name: 'ClaimType', type: 'string' },
		    { name: 'ExpenseStatusId', type: 'string' },
		    { name: 'ExpenseStatus', type: 'string' },
		    { name: 'DateSubmitted', type: 'string' },
		    { name: 'DaysRemaining', type: 'string' },
		    { name: 'ClaimReferenceNumber', type: 'string' },
            { name :'VehicleExpenses',type:'auto'},
            { name :'Expenses',type:'auto'}
		]
    }
});

