Ext.define('SmartSalary.model.EmployerList_M', {
    extend: 'Ext.data.Model',
    config:{
        fields: [
            { name: 'PackageID', type: 'string' },
            { name: 'EmployerCode', type: 'string' },
            { name: 'EmployerName', type: 'string' },
		    { name: 'CeasedDate', type: 'string' }
		]
    }
});

