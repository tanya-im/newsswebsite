Ext.define('SmartSalary.model.SlideList_M', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'text', type: 'string'},
            {name: 'menuid', type: 'string'},
			{name :'leaf', type:'bool'},
			{name:'submenu',type:'bool'},
			{name:'uniqueId',type:'int'}
        ]
    }
});
