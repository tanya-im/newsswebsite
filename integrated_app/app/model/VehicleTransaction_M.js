Ext.define('SmartSalary.model.VehicleTransaction_M', {
    extend: 'Ext.data.Model',
    config:{
        fields: [
            { name: 'amount', type: 'string' },
			{ name: 'gst', type: 'string' },
			{ name: 'description', type: 'string' },
			{ name: 'transactionDate', type: 'string' },
		]
    }
});

