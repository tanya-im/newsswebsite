Ext.define('SmartSalary.model.VehicleRegistrationList_M', {
    extend: 'Ext.data.Model',
    config:{
        fields: [
            { name: 'VehicleID', type: 'string' },
            { name: 'RegoNo', type: 'string' }
		]
    }
});

