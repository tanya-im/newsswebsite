Ext.define('SmartSalary.model.Localdata_M', {
    extend: 'Ext.data.Model',
    config:{
        fields: [
            
            { name: 'formData', type: 'auto'},
            { name: 'fileData',type: 'auto'},
            { name: 'claimindex',type: 'auto'},
            { name: 'row_id',type: 'auto'}
		] 
    }
});

