Ext.define('SmartSalary.model.VehicleClaimFormData_M', {
    extend: 'Ext.data.Model',
    config:{
        fields: [
            { name: 'PackageID', type: 'string' },
            { name: 'EmployerCode', type: 'string' },
            { name: 'EmployerName', type: 'string' },
		    { name: 'CeasedDate', type: 'string' }
		]
    }
});

