Ext.define('SmartSalary.model.SearchList_M', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            { name : 'title',type: 'string'},
			{ name : 'shortdescription',type: 'string'},
			{ name : 'nid',type: 'string'}
        ]
    }
});
