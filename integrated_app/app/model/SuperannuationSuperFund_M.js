Ext.define('SmartSalary.model.SuperannuationSuperFund_M', {
    extend: 'Ext.data.Model',
    config:{
        fields: [
            { name: 'name', type: 'string' },
			{ name: 'address', type: 'string' },
			{ name: 'contactNumber', type: 'string' },
			{ name: 'usiEsa', type: 'string' },
			{ name: 'abn', type: 'string' },
			{ name: 'bsb', type: 'string' }
		]
    }
});

