Ext.define('SmartSalary.config.MenuConfig', {
    singleton: true,
    alternateClassName: ['MenuConfig'],    
	home: 1, 
	myBenefits:2,
	vleasing:3,		
	overview:301,
    manageBudgets:302,
	leasingDetails:303,
	manageFuelCards:304,
	leasingHistory:305,
	myProducts:306,
	availableProducts:307,
	onlineClaims:4,
	myPersonalDetails:5,
	fAQ:6
});