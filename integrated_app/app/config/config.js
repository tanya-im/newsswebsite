Ext.define('SmartSalary.config.config', {
    singleton: true,
    alternateClassName: ['Config'],    
	logging: true, 
	log_level:4,		// 1 = debug and 4 =  
	alert_msg:true,
    ApiBaseUrl:'../',
    RestApiBaseUrl:'http://devws.smartsalary.com.au/v3/api/',
	serverRequsetTimeoutTime:3000000
});