Ext.define('SmartSalary.lib.Shared', {
    singleton: true,
    alternateClassName: ['Shared'],
    GSTRate:null,
    userName:null,
	password:null,
	packageId:null,
	employerCode:null,
	objMainContainer:null,     
	vehicleID:null,
	vehicleReg:null,
	base64variable:null,
	EmailRegex:/^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/,
	loadingMask : { xtype: 'loadmask', message: 'Please wait...' },
	fileData:new Array(),
	GSTAndSubTotalCalculationByAmount : function(claimamount,GSTTextBox,SubTotalTextBox){
		if(claimamount==null) {
			GSTTextBox.setValue();
			SubTotalTextBox.setValue();
			Logger.alert(Messages.AlertMsg,Messages.AlertMsgPlsEnetrNumeric);
			return false;
		}
		if(claimamount > 0){			
			GSTAmount = (claimamount / (1 + parseFloat(this.GSTRate))) * this.GSTRate;
			GSTAmount = GSTAmount.toFixed(2);
			SubTotal = claimamount - GSTAmount;
			SubTotal = SubTotal.toFixed(2);
			GSTTextBox.setValue(GSTAmount);
			Logger.debug("SubTotal:"+SubTotal);
			SubTotalTextBox.setValue(SubTotal);
		}
	},
	amountCalculationByGST : function(updatedGST,updatedamount,AmountTextBox,SubTotalTextBox){
		var amount = AmountTextBox.getValue();
		if(amount===null)
			return false;
		if(updatedGST===null) {
			Logger.alert(Messages.AlertMsg,Messages.AlertMsgPlsEnetrNumeric);
			return false;
		}
		if(parseFloat(updatedamount) <= parseFloat(updatedGST)){
			AmountTextBox.setValue(updatedGST);
			updatedamount = updatedGST; 
		}
		var updatedSubTotal =  parseFloat(updatedamount) - parseFloat(updatedGST);
		Logger.debug("SubTotal:"+parseFloat(updatedSubTotal).toFixed(2));
		SubTotalTextBox.setValue(parseFloat(updatedSubTotal).toFixed(2)); 
	},
	checkGSTNotExceedTenPercent  : function(claimamount,getGST){
		claimamount = this.removeDelFromValue(claimamount);		 
		GSTAmount = (claimamount / (1 + parseFloat(this.GSTRate))) * this.GSTRate;
		GSTAmount = GSTAmount.toFixed(2);
		getGST = this.removeDelFromValue(getGST);
		if(parseFloat(getGST) > parseFloat(GSTAmount)){
			Logger.alert(Messages.AlertMsgErrors,Messages.AlertMsgGstExceedMsg);
			return false;
		}else {return true;}
	},
	getCalendarComponent: function(name,fieldKey,i,setdate){
		var newdate;
		Logger.debug(name+","+fieldKey+","+i+","+setdate);
		var existingObj=Ext.getCmp("calendarCompId"+i);
		if(Ext.isDefined(existingObj)){
			Logger.debug("calendar already defined");
			existingObj.destroy(); // remove already existing same component
		}
		var fieldType = Ext.create('Ext.Container',{
			items: [{
				xtype:'label',
				html:fieldKey,
				cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-top-10'
			},{
				xtype:"customCalendar",
				id:"calendarCompId"+i 
			}]
		});
		if(setdate)
			newdate = new Date(setdate);
		else
			newdate = new Date();

		//fieldType.getAt(1).setMaxDate(new Date());// set max date to be able to select by user

		var calendar=fieldType.getAt(1);
		 
		var dayLabel=calendar.getAt(0).getAt(1).getAt(0);
		dayLabel.setItemId(('dayLabel'+i));

		var dateLabel=calendar.getAt(0).getAt(1).getAt(1);
		dateLabel.setItemId(('dateLabel'+i));

		dateLabel.setHtml(Ext.Date.format(newdate, 'd M Y'));
		dayLabel.setHtml(Ext.Date.format(newdate, 'l'));

		calendar.getAt(0).getAt(2).setItemId(('calarrow'+i));
		calendar.getAt(1).setItemId(('calendarView'+i));
		
		/*Get Object of hidden date field*/
		var hiddenDate = Ext.getCmp(name);
		var obj = Ext.ComponentQuery.query('touchcalenderview[itemId='+calendar.getAt(1).getItemId()+']')[0];
		
		/*set Default Today Date in hidden date field*/
		hiddenDate.setValue(Ext.Date.format(newdate, 'd M Y'));

		calendar.getAt(1).setListeners({
			
			selectionchange:function(calendarview, newDate, prevDate){
	    		Ext.ComponentQuery.query('label[itemId='+calendar.getAt(0).getAt(1).getAt(0).getItemId()+']')[0].setHtml(Ext.Date.format(newDate, 'l'))
				Ext.ComponentQuery.query('label[itemId='+calendar.getAt(0).getAt(1).getAt(1).getItemId()+']')[0].setHtml(Ext.Date.format(newDate, 'd M Y'))

				/*set Date in hidden date field*/
				hiddenDate.setValue(Ext.Date.format(newDate, 'd M Y'));

				obj.setHidden(true);// hide the calendar
	    	}
		});
		
		calendar.getAt(0).setListeners({
			element : 'element',
				tap:function (){

					 var obj=Ext.ComponentQuery.query('touchcalenderview[itemId='+calendar.getAt(1).getItemId()+']')[0];
					 var clArr=Ext.ComponentQuery.query('label[itemId='+calendar.getAt(0).getAt(2).getAt(0).getItemId()+']')[0];
					 Logger.debug("Current"+obj.isHidden()); 
					 if(obj.isHidden()){
					 	 
					 	obj.setHidden(false)						 	
						clArr.removeCls("cal-arrow-right");
						clArr.addCls("cal-arrow-down");
					 }
					 else{
					 	 
					 	obj.setHidden(true)
					 	clArr.removeCls("cal-arrow-down")
						clArr.addCls("cal-arrow-right")
					 } 
				}
		});
		Logger.debug(hiddenDate.getValue());
		return fieldType;
	},
	getViewObject:function(strViewXtype){
    	if(Ext.os.is.Desktop){
	    	strViewXtype=strViewXtype+'_D';	
    	}else if(Ext.os.is.Phone){
	    	strViewXtype=strViewXtype+'_P';	    	
    	}else if(Ext.os.is.Tablet){
	    	strViewXtype=strViewXtype+'_T';
    	}
    	var viewObject={xtype:strViewXtype};     
	    return viewObject;
    },
	/* Add new view on viewport */
	pushNewView : function(id,view,direction){
		var me = this;
		
		var _index = -1;
		if(Shared.objMainContainer.getActiveItem()._id==id)
			return false;
		
		Ext.Array.each(Shared.objMainContainer.getItems().items, function(name, index, arr) {
			if(name._id == id)
				_index = index;
		});
		
		if(Ext.os.is.Desktop)
			Shared.objMainContainer.getLayout().setAnimation('fade');
		else
			Shared.objMainContainer.getLayout().setAnimation('slide',direction);
		
		if(_index != -1) {
			Shared.objMainContainer.pop(Shared.objMainContainer.getInnerItems().length - (_index));
		}else{ 
			var viewObj=me.getViewObject(view);
			Shared.objMainContainer.push(viewObj);
		}
		
		try {
			Ext.getCmp(id).getScrollable().getScroller().scrollTo(0,0);
		}
		catch(error) {
			Logger.debug("Error : "+error.message);
		}
	},
	/*CleanArray Return empty array.
	 * @param: array
	*/ 
	cleanArray: function(actual){
		var newArray = new Array();
		for(var j = 0; j<actual.length; j++){
			if (actual[j]){
			  newArray.push(actual[j]);
		  	}
		}
		return newArray;
	},
	 
	/*formDataAsPerClaimIndexes: reorder form  data according to index.
	 * @param: element id. 
	*/ 
	formDataAsPerClaimIndexes: function (){
		var sortedClaimsArr=Array();
		var claimData = Ext.getStore('storeLocalData');	
		claimData.data.each(function(item, index, totalItems ) {
	        if(!Ext.isDefined(sortedClaimsArr[item.get('claimindex')])){
	       		sortedClaimsArr[item.get('claimindex')]=Array();
	       	}
	       	sortedClaimsArr[item.get('claimindex')].push(item)
	    });
	    Logger.debug(sortedClaimsArr)
		return sortedClaimsArr;
	},
	removeDelFromValue : function(val){
		var avoiddotval = val[val.length-1];
		if(avoiddotval=='.'){ val = val.substring(0, val.length - 1);}
		return val;
	},
	checkConnectionIsOnline:function(){
		var connectionStatus = false;
		if(Ext.device.Connection.isOnline()){
			connectionStatus = true;
		}else{
			Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgUserOffLine);
			Ext.Viewport.setMasked(false);
		}
		return connectionStatus;
	},
	addCalendar:function(calendarContainer,calendarLabel,calendarName,no,value){
		var calendarCmp = Ext.getCmp(calendarContainer);
		calendarCmp.removeAll(true,true);
		var fieldHidden = Ext.create('Ext.field.Hidden',{
					name: calendarName,
					id:calendarName,
					itemId:calendarName,
					value:value
				}); // store the date in hidden field
		calendarCmp.add(fieldHidden);
		var calendarObj=Shared.getCalendarComponent(calendarName,calendarLabel,no,new Date());
		calendarCmp.add(calendarObj);
	},
	getAccessToken:function (){
		
		if(!localStorage.getItem("tokenStartTime"))
		{
			var created_on=new Date().getTime();
			localStorage.setItem("tokenStartTime",created_on);
		}
		var current_time=new Date().getTime();
		
Logger.info("current_time::"+current_time);
		
Logger.info("saved_time::"+localStorage.getItem("tokenStartTime"));
		var allowedTime=parseInt(localStorage.getItem("tokenStartTime"))+parseInt(access_token.expires_in*1000)
		
Logger.info(allowedTime);
		if(current_time>allowedTime){
			//Remove data from store when user logoff.
			var userStoreObj = Ext.getStore('User_S'); 
			if(userStoreObj.getCount() > 0) userStoreObj.removeAt(0);
			
			//Remove data from store when user logoff.
			var employerListObj = Ext.getStore('EmployerList');  
	        if(employerListObj.getCount() > 0) employerListObj.removeAll();

			Shared.base64variable=''; // clearing the credentials
	        window.onbeforeunload = null; // unbinding the onbeforeunload event 
	        window.onpagehide = null; // unbinding the onpagehide event 
			localStorage.setItem("tokenStartTime",'');
			alert("Session gets expired,Please login again.");			
			window.location.href="../user/logout";
		}
		if(Ext.isEmpty(access_token)){
			window.location.href="../";
		} 

		return  access_token.access_token;
	},
	getMenuItemId : function(contentUniqueId,menuText,tagcloseOpenFlag){
		var itemId = '';
		if(contentUniqueId == MenuConfig.home)
			itemId=	'<li class="menuItem" id="home">&nbsp;';
		else if(contentUniqueId == MenuConfig.myBenefits)
			itemId=	'<li class="menuItem" id="manageMyBenefits">'+menuText;
		else if(contentUniqueId == MenuConfig.vleasing)
			itemId=	'<li class="vleasing" id="vehicleLeasing">'+menuText
		else if(contentUniqueId == MenuConfig.overview)
			itemId=	'<li class="menuItem" id="overview">'+menuText;
		else if(contentUniqueId == MenuConfig.manageBudgets)
			itemId=	'<li class="menuItem" id="manageBudgets">'+menuText;
		else if(contentUniqueId == MenuConfig.leasingDetails)
			itemId=	'<li class="menuItem" id="leasingDetails">'+menuText;
		else if(contentUniqueId == MenuConfig.manageFuelCards)
			itemId=	'<li class="menuItem" id="manageFuelCards">'+menuText;
		else if(contentUniqueId == MenuConfig.leasingHistory)
			itemId=	'<li class="menuItem" id="leasingHistory">'+menuText;
		else if(contentUniqueId == MenuConfig.myProducts)
			itemId=	'<li class="menuItem" id="myProducts">'+menuText;
		else if(contentUniqueId == MenuConfig.availableProducts)
			tagcloseOpenFlag = false;
		else if(contentUniqueId == MenuConfig.onlineClaims)
			itemId=	'<li class="menuItem" id="expressOnlineClaims">'+menuText;
		else if(contentUniqueId == MenuConfig.myPersonalDetails)
			itemId=	'<li class="menuItem" id="personalDetails">'+menuText;
		else if(contentUniqueId == MenuConfig.fAQ)
			itemId=	'<li class="menuItem" id="whatCanIpack">'+menuText;											
		
		if(tagcloseOpenFlag==true)
			itemId+='</li>'		
		
		return itemId;
	}	
});
