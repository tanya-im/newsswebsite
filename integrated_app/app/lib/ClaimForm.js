Ext.define('SmartSalary.lib.ClaimForm', {
    singleton: true,
    alternateClassName: ['ClaimForm'],
    fieldsWrapper:null,
    filesAndButtonsWrapperObj:null,
    innerFormFieldWrapper:null,
    columns:1,
    uploadControlDescriptionText:Messages.uploadControlDescriptionTextMsg,
    initialize:function(outerWrapper){
    	  
		if(this.columns=='2'){
			this.innerFormFieldWrapper = Ext.create('Ext.Container',{
					id:'innerFormFieldWrapper',
					cls:'greywrap bgE7E7E7',					 
					layout:'hbox',
					itemId: 'innerFormFieldWrapper'
			}); 
		}else{
			this.innerFormFieldWrapper = Ext.create('Ext.Container',{
					id:'innerFormFieldWrapper',
					cls:'greywrap bgE7E7E7',					 
					itemId: 'innerFormFieldWrapper'
			});
		}

		outerWrapper.add(this.innerFormFieldWrapper); // add the inner form fields to the upper wrapper	

		// if device is tablet and attachment  has to be submitted then two column layout will be display
		Logger.debug(this.columns);
		
		if(this.columns=='2')
		{ 
			this.fieldsWrapperObj = Ext.create('Ext.Container',{
				flex:5,
				cls:'pad-10'
			}); // creating container to hold fields
			this.filesAndButtonsWrapperObj = Ext.create('Ext.Container',{
				flex:5,
				cls:'pad-top-36 pad-10'
			}); // creating container to hold file upload section and action button

		}else{
			this.fieldsWrapperObj = this.innerFormFieldWrapper;
			this.filesAndButtonsWrapperObj=this.innerFormFieldWrapper;
		}
    },
    addFields:function(claimTypefields,formData){
    	formData = formData || {};
    	var GSTFlag = false;
		var GSTId = '';
		var SubTotalId ='';
		var SubTotalFlag = false;
		var cssclassname = '';
		if(this.fieldsWrapperObj.down('#GST'))
		{
			this.fieldsWrapperObj.down('#GST').destroy();
		}
		if(this.fieldsWrapperObj.down('#Subtotal'))
		{
			this.fieldsWrapperObj.down('#Subtotal').destroy();
		}
		// For statr that check GST and subtotal
		for(var i=0; i<claimTypefields.length;i++){ 
			if(claimTypefields[i]['Key']=='GST'){
					GSTFlag = true;
					GSTId = i;
					if(this.fieldsWrapperObj.down('#'+claimTypefields[i]['Key'])){
						this.fieldsWrapperObj.down('#'+claimTypefields[i]['Key']).destroy();
					}
			}
			if(claimTypefields[i]['Name']=='Subtotal'){
				SubTotalId = i;
			}
			if(claimTypefields[i]['Key'] && claimTypefields[i]['Name']!='Subtotal'){
				if(this.fieldsWrapperObj.down('#'+claimTypefields[i]['Key'])){
					this.fieldsWrapperObj.down('#'+claimTypefields[i]['Key']).destroy();
				}
			}
			if(claimTypefields[i]['Name']!='Subtotal'){
				if(this.fieldsWrapperObj.down('#'+claimTypefields[i]['Subtotal'])){
					this.fieldsWrapperObj.down('#'+claimTypefields[i]['Subtotal']).destroy();
				}
			}
		}
		// End for for GST.
		
		// For loop that add fields for claim type. 
		for(var i=0; i<claimTypefields.length;i++){

			if(claimTypefields[i]['DataType']=='Money'){ // set Dollar icon for money type field
				var cssclassname = ' icon-sign ';
			}

			if(GSTFlag == true && claimTypefields[i]['Key']=='Amount') // if amount field is there and GST is also there to be calculated
			{ 
				var AmountfieldType = Ext.create('Ext.Container',{
					items: [{
						xtype:'label',
						html:claimTypefields[i]['Name'],
						cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-top-10'
					}]
				});

				var AmountTextBox = Ext.create('Ext.field.Number',{
							/*placeHolder:claimTypefields[i]['Placeholder'],*/
							itemId: claimTypefields[i]['Key'],
							id:claimTypefields[i]['Key'],
							name: claimTypefields[i]['Key'],
							required: claimTypefields[i]['IsRequired'],
							value:formData[claimTypefields[i]['Key']],
							tabIndex: parseInt(claimTypefields[i]['Order']),
							cls:'font-12 pad-left-5 bgdarkgrey txtboxbg'+cssclassname
				});

				AmountfieldType.add(AmountTextBox);
				
				var GSTfieldType = Ext.create('Ext.Container',{
					items: [{
						xtype:'label',
						html:claimTypefields[GSTId]['Name'],
						cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-top-10'
					}]
				});
				var GSTTextBox = Ext.create('Ext.field.Number',{
							/*placeHolder:claimTypefields[GSTId]['Placeholder'],*/
							itemId: claimTypefields[GSTId]['Key'],
							id:claimTypefields[GSTId]['Key'],
							name: claimTypefields[GSTId]['Key'],
							required: claimTypefields[GSTId]['IsRequired'],
							value:formData[claimTypefields[GSTId]['Key']],
							tabIndex: parseInt(claimTypefields[GSTId]['Order']),
							cls:'font-12 pad-left-5 bgdarkgrey txtboxbg'+cssclassname
				});
				GSTfieldType.add(GSTTextBox);
				
				var SubtotalfieldType = Ext.create('Ext.Container',{
					items: [{
						xtype:'label',
						html:claimTypefields[SubTotalId]['Name'],
						cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-top-10'
					}]
				});

				var AmountValue=formData[claimTypefields[SubTotalId]['Name']]?parseFloat(formData[claimTypefields[SubTotalId]['Name']]).toFixed(2):'';

				var SubTotalTextBox = Ext.create('Ext.field.Text',{
							/*placeHolder:claimTypefields[SubTotalId]['Placeholder'],*/
							itemId: claimTypefields[SubTotalId]['Name'],
							id:claimTypefields[SubTotalId]['Name'],
							name: claimTypefields[SubTotalId]['Name'],
							readOnly:true,
							value:AmountValue,
							required: claimTypefields[SubTotalId]['IsRequired'],
							cls:'font-12 pad-left-5 bgdarkgrey txtboxbg'+cssclassname
				});
				SubtotalfieldType.add(SubTotalTextBox);
				
				AmountTextBox.on({
					keyup:function(){
						Shared.GSTAndSubTotalCalculationByAmount(AmountTextBox.getValue(),GSTTextBox,SubTotalTextBox);
					}
				});
				
				GSTTextBox.on({
					keyup:function(){
						Shared.amountCalculationByGST(GSTTextBox.getValue(),AmountTextBox.getValue(),AmountTextBox,SubTotalTextBox);
					}
				});
				
				this.fieldsWrapperObj.add(AmountfieldType);
				this.fieldsWrapperObj.add(GSTfieldType);
				this.fieldsWrapperObj.add(SubtotalfieldType);
			}
			
			if(GSTFlag == false && claimTypefields[i]['Key']=='Amount'){ 
				var AmountValue=formData[claimTypefields[i]['Key']]?parseFloat(formData[claimTypefields[i]['Key']]).toFixed(2):'';
				 
				var fieldType = Ext.create('Ext.Container',{
					items: [{
						xtype:'label',
						html:claimTypefields[i]['Name'],
						cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-top-10'
					},{
						xtype: 'numberfield',
						/*placeHolder:claimTypefields[i]['Placeholder'],*/
						itemId: claimTypefields[i]['Key'],
						id:claimTypefields[i]['Key'],
						name: claimTypefields[i]['Key'],
						required: claimTypefields[i]['IsRequired'],
						value:AmountValue,
						tabIndex: parseInt(claimTypefields[i]['Order']),
						cls:'font-12 pad-left-5 bgdarkgrey txtboxbg'+cssclassname
					}]
				});
				this.fieldsWrapperObj.add(fieldType);
			}
			
			if(claimTypefields[i]['Key']=='GST'){	continue; }
			if(claimTypefields[i]['Name']=='Subtotal'){	continue; }
			// check other fields which are not amount date
			if((claimTypefields[i]['Key']!='Amount') && (claimTypefields[i]['DataType']=='Text' || claimTypefields[i]['DataType']=='Money' || claimTypefields[i]['DataType']=='Number')){ 
				
				// check the type of data so that we could use field accordign to that and on mobile it should bring up the keypad accordingly
				if(claimTypefields[i]['DataType']=='Text')
					var xtypeField='textfield';
				else
					var xtypeField='numberfield';
				 
				var fieldType = Ext.create('Ext.Container',{
					items: [{
						xtype:'label',
						html:claimTypefields[i]['Name'],
						cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-top-10'
					},{
						xtype:xtypeField,
						/*placeHolder:claimTypefields[i]['Placeholder'],*/
						itemId: claimTypefields[i]['Key'],
						id:claimTypefields[i]['Key'],
						name: claimTypefields[i]['Key'],
						required: claimTypefields[i]['IsRequired'],
						value:formData[claimTypefields[i]['Key']],
						tabIndex: parseInt(claimTypefields[i]['Order']),
						cls:'font-12 pad-left-5 bgdarkgrey txtboxbg'+cssclassname
					}]
				});
				this.fieldsWrapperObj.add(fieldType);
			}
			// call the external custom calendar 
			if(claimTypefields[i]['DataType']=='Date'){
				var fieldHidden = Ext.create('Ext.field.Hidden',{
					name: claimTypefields[i]['Key'],
					id: claimTypefields[i]['Key'],
					itemId: claimTypefields[i]['Key'],
					value: formData[claimTypefields[i]['Key']]
				}); // store the date in hidden field

				this.fieldsWrapperObj.add(fieldHidden);

				var setdate = formData[claimTypefields[i]['Key']];

				var calendarObj=Shared.getCalendarComponent(claimTypefields[i]['Key'],claimTypefields[i]['Name'],i,setdate);
				if(setdate)
					calendarObj.getAt(1).setValues(new Date(setdate)); // set the date entered

				this.fieldsWrapperObj.add(calendarObj);
			}
		} 
    },
    addHiddenField:function (fieldArray){
    	var me=this
    	Ext.Object.each(fieldArray,function(key,value){    		 
    		// add a hidden field to track the claim type on submission of expense
    		if(me.fieldsWrapperObj.down('#'+key))
    			me.fieldsWrapperObj.down('#'+key).destroy()

    		var hiddenField= Ext.create('Ext.field.Hidden',{
				name: key,
				id: key,
				itemId: key,
				value:value
			});
			me.fieldsWrapperObj.add(hiddenField);
    	});
    			 
    },
    addAttachmentField:function(fileData){
    	// display the fields for uploading the image
    	var me=this;
    	fileData = fileData || {};	
    	if(this.filesAndButtonsWrapperObj.down('#snap_image'))
		{
			this.filesAndButtonsWrapperObj.down('#snap_image').destroy();
		}	 
		var fieldType = Ext.create('Ext.Container',{
				cls:'bgd7 phone_doc_pad-top-20 phone-pad-bottom-1 noactive',
				items: [{	xtype: 'button',
							itemId: 'cappedClaimStepDocumentRequiredButton',
							ui: 'none',
							cls:'claimStepOneDocumentsRequiredButton claimStepButton txt-left font-16',
							text: 'Document(s) required'
						},{
							xtype:'label',
							html:this.uploadControlDescriptionText,
							cls:'margin-5-10 font-12 pad-left-5 f-darkgrey pad-10'
						},
						{							
							xtype:'container',
							cls:'document_container margin-10',
							items:[{									
									xtype:'container',
									id:'snap_image',
									itemId:'snap_image',
									cls:'snap_images',
								},
								/* external library used for uploading and displaying the image */
								{
									xtype:'capturepicture',
									name: 'capturefiles',
									cls:'capture_images',
									height:'100%',
									width:'100%',
									 
								}
							]
						}]
				});
		this.filesAndButtonsWrapperObj.add(fieldType);
		if(fileData.length>0){
			Ext.each(fileData, function(fileValue,key){
					var reader = new FileReader();

					Shared.fileData[key] = fileValue;
					reader.onload = function (e) {
						 
						var img = Ext.create('Ext.Container', {
							itemId: 'snapcontainer',
							cls: 'snap_shot',
							items:[{
								xtype: 'container',
								html:'<img height="109px" width="109px" src="'+e.target.result+'" />',
								cls:"floatLeft"
							},
							{
								xtype: 'image', 
								data: {"snap_id":key}, 
								cls: 'remove_img floatLeft',
								listeners: {
									tap : function(b, e) {
										this.up('#snapcontainer').destroy();
										delete Shared.fileData[this.getData().snap_id]
										Shared.fileData = Shared.cleanArray(Shared.fileData);
									}
								}
							}]
						}); 
						var objSnapImageContainer=me.filesAndButtonsWrapperObj.down('#snap_image');
						
						objSnapImageContainer.add(img);
					}
					reader.readAsDataURL(fileValue);
			});
		}
    },

    load:function(){
    	if(this.columns=='2')
		{
	    	this.innerFormFieldWrapper.add(this.fieldsWrapperObj);
			this.innerFormFieldWrapper.add(this.filesAndButtonsWrapperObj);
		}
    },
    validateClaimFields:function (claimFields,allowAttachment,allowAttachmentForVehicle){
		if(!Ext.isDefined(allowAttachmentForVehicle))
			allowAttachmentForVehicle = false;
		var errors=Array();
		if(claimFields.length){
			Ext.each(claimFields, function(field,key){
				var component=Ext.getCmp(field.Key); 
				Logger.debug(field); 
				
				if(field.Name=='Subtotal'){	return; }

				if(field.DataType=='Text' && field.IsRequired )
				{ 	
					 
					if(component.getValue().trim()=='')
					{
						errors.push("Please enter "+field.Name);
					}
				}else if((field.DataType=='Money' || field.DataType=='Number' ) && field.IsRequired )
				{ 	 
					if(component.getValue()==null)
					{
						errors.push("Please enter valid "+field.Name);
					} 
				} 
				 
			});
			if(allowAttachment && !allowAttachmentForVehicle){
				if(Shared.fileData.length < 1){ 
					errors.push("Please upload document");
				}
			}
			/*	It's for vehicle claim edit section.*/
			if(allowAttachmentForVehicle){
				var editedDocNo = Ext.getCmp('editedDocNo').getValue();
				editedDocNo = parseInt(editedDocNo);
				if(editedDocNo < 1){
					if(Shared.fileData.length < 1){ 
						errors.push("Please upload document");
					}
				}
			}
		}
		if(errors.length) {
			var msg = "";
	    	for(var i = 0; i < errors.length; i++)
        		msg += (i+1)+'. '+errors[i]+'<br />';
			Logger.alert(Messages.AlertMsg,"<div style='text-align:left;'>"+msg+"</div>");
			return false;
		}
		return true;
	},
	itemListing : function (_this,mainWrapper,claimTypefields,claimType){

		Logger.info("ClaimForm class itemListing function");
		var me=_this;
		var sortedClaimsArr=Shared.formDataAsPerClaimIndexes();
		Logger.debug(sortedClaimsArr);
		var grandTotal=0;			

		var cappedClaimItemsWrapperObj=Ext.getCmp(mainWrapper);
		cappedClaimItemsWrapperObj.removeAll(true,true); 
		Ext.each(sortedClaimsArr,function (claim,indexClaim){
			 
			var totalamt=0;
			if(Ext.isDefined(claim)){ 
				if(claimType=='vehicle'){ 
					var claimName=claimTypefields[indexClaim].Name;
					var fieldObj=claimTypefields[indexClaim].VehicleTransactionTypeFields.VehicleTransactionTypeField;
				}else{
					if(Ext.isDefined(claimTypefields.ExpenseType[indexClaim])){
						var claimName=claimTypefields.ExpenseType[indexClaim].Name;
						var fieldObj=claimTypefields.ExpenseType[indexClaim].ExpenseTypeFields.ExpenseTypeField;
					}
					else{
						var claimName=claimTypefields.ExpenseType.Name;
						var fieldObj=claimTypefields.ExpenseType.ExpenseTypeFields.ExpenseTypeField;
					}
				}
				 
				var claimTypeWrapperObj = Ext.create('Ext.Container',{
					cls :'pad-10 bgE7E7E7 font-16 color-2B2F3B',						 
					items:[
					{
						xtype: 'label',
						id:'cappedClaimStepTwoCount'+indexClaim,
						itemId:'cappedClaimStepTwoCount'+indexClaim,
						flex: 1,
						cls:'heading_left_15 pad-bottom-5 font-16 f-bold',
						html: claimName+' claim(s)'
					},{
						xtype: 'container',
						id:'claimDetailWrapper'+indexClaim,
						itemId:'claimDetailWrapper'+indexClaim,
						cls:'heading_top_10'
					}]
				});
				 
				var claimDetailWrapperObj = Ext.getCmp('claimDetailWrapper'+indexClaim);
				claimDetailWrapperObj.removeAll(true,true);

				for(var i=0;i<claim.length;i++){

					var record = claim[i]; //Get the record        
					var data = record.getData(); //Get the data from the record 

					Logger.debug(data); 
					 
					totalamt=parseFloat(totalamt)+parseFloat(data.formData.Amount);
					 
					var ClaimDetailCnt = Ext.create('Ext.Container',{
						cls :'bgwhite',
						id:'claimlistindex_'+indexClaim+"_"+i,
						itemId:'claimlistindex_'+indexClaim+"_"+i,	
						items: [
							{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-bottom-5 pad-10',
								items:[{
									xtype: 'label',
									html: 'Date Paid: '+data.formData.ExpenseDate,
									cls: 'font-12 font-vlightgrey',
									flex:1
								},{
									xtype: 'label',
									html: '(Ready for submission)',
									cls:'font-13 txt-right text_green',
									flex:1
								}]
							}]
					}); 

					Ext.Array.each(fieldObj,function(field,i){
						if(field.Key=='ExpenseDate')
							return;
						else if(field.Name=="Subtotal")
							itemKey=field.Name;
						else
							itemKey=field.Key;
						if(field.DataType=='Money')
							itemValue='$'+parseFloat(data.formData[itemKey]).toFixed(2);
						else
							itemValue=data.formData[itemKey];

						 
						var totalAmntClaimedContainer = Ext.create('Ext.Container', {
							layout:'hbox',
							cls:'pad-bottom-5 pad-5-10 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: field.Name+':',
								flex:1
							},{
								xtype: 'label',
								html: itemValue,
								cls:'pad-right-5 txt-right f-bold',
								flex:1
							}]
						});
						ClaimDetailCnt.add(totalAmntClaimedContainer);
					});

					

					if(data.fileData.length)
					{
						var documentUploaded = Ext.create('Ext.Container', {
												layout:'hbox',
												cls:'pad-bottom-5 pad-5-10 font-14 f-darkgrey',
												items:[{
													xtype: 'label',
													html: 'No. of documents included:',
													flex:1
												},{
													xtype: 'label',
													html: data.fileData.length,
													cls:'pad-right-5 txt-right f-bold',
													flex:1
												}]
											});
						claimDetail = Ext.getCmp('claimlistindex_'+indexClaim+"_"+i);
						claimDetail.add(documentUploaded);
					}
					var editDeleteButtonCnt = Ext.create('Ext.Container',{
											layout:'hbox',
											cls:'font-14 font-white pad-top-10'
										});
					var editButton =  	Ext.create('Ext.Button', {
											itemId: "editBtn_"+indexClaim+"_"+i,
											ui: 'none',												 
											pressedCls:'editDelBtnPress',
											flex:1,
											cls:'editClaimStepTwoButton claimStepButton txt-left font-16 pad-10',									 											text: 'Edit item'
										});
					var deleteButton =  Ext.create('Ext.Button', {
											itemId: 'deleteBtn_'+data.row_id,
											ui: 'none',												 
											pressedCls:'editDelBtnPress',
											flex:1,
											name:i,
											cls:'deleteClaimStepTwoButton claimStepButton txt-left font-16 pad-10',								 			text: 'Delete item'
										});
					editButton.on({
						tap:function(){
							me.editClaimForm(this.getItemId());
						}
					});	
					deleteButton.on({
						tap:function(){  
							me.deleteClaimForm(this.getItemId());
						}
					});									
					editDeleteButtonCnt.add(editButton);
					editDeleteButtonCnt.add(deleteButton);
					ClaimDetailCnt.add(editDeleteButtonCnt);															
					claimDetailWrapperObj.add(ClaimDetailCnt);
					totalamt=parseFloat(totalamt).toFixed(2);
				}


				cappedClaimItemsWrapperObj.add(claimTypeWrapperObj);					 
				grandTotal=parseFloat(grandTotal)+parseFloat(totalamt);
			}
		})
		grandTotal=grandTotal.toFixed(2);
		return grandTotal;
	},
	// Final submission of the claims
	submitClaim:function(me,submitUrl,claimType,paymentPeriod){
	 
		Logger.info("submitClaim function in ClaimForm");
		paymentPeriod = paymentPeriod || 0;
		if(!Ext.getCmp(claimType+"DeclarationCheckbox").isChecked())
		{
			Logger.alert(Messages.AlertMsg, Messages.AlertMsgDeclarationAcceptMsg);
			return false;
		}
 
		var claimFinalData= Array();

		var objStoreLocalData=Ext.getStore("storeLocalData");  // get locally stored data of all the claims

		var i=0;

		var formData = new FormData();

		objStoreLocalData.data.each(function(record, index, totalItems ){
		     
		    Ext.each(record.get('fileData'), function(value,key){
		    	 
				formData.append(i++,value); // appending all the files to the formData
			});	

		    claimFinalData.push(record.get('formData'));
		}); 

		formData.append(i,Ext.encode(claimFinalData)); 	// Appending claim data to the formData 
 		 
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;
		
		var progressIndicator = Ext.create("Ext.ProgressIndicator", {
			loadingText: Messages.LoadingTextMsg,
			showAnimation : 'fadeIn',
			cls:'width-90'
		});
		
		Ext.Ajax.request({ 									  
			url: submitUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable},
			data: formData,
			params:{'paymentPeriod':paymentPeriod},
			timeout:Config.serverRequsetTimeoutTime,
			progress: progressIndicator,
			type:'POST',			
			success: function(result){

				data = result.responseText;
				try {
					var Response = Ext.JSON.decode(data);
				} catch (e) {
					if(data.length > 80)
					{
						data = data.slice(0,80);
						data = data+"...";
					}
					Ext.Msg.show({ 
						 title   : 'Message', 
						 message     : data, 
						 buttons : [{ 
								  itemId : 'Retry',
								  ui     : 'action',  
								  text   : 'Retry',   
							 },{   
								  itemId : 'cancel',   
								  text   : 'Cancel'    
						 }], 
						 confirm  : {},   
							fn: function(btn) {
								if (btn == 'Retry')
									me.submitClaim();
							}  
					});
					return;
				}				
				if ( typeof(Response.ClaimId) == "undefined" || Response.ClaimId == '')
				{
					if(Response.Description.length > 80)
					{
						Response.Description = Response.Description.slice(0,80);
						Response.Description = Response.Description+"...";
					}
					Ext.Msg.show({ 
						 title   : 'Message', 
						 message : Response.Description, 
						 buttons : [{ 
								  itemId : 'Retry',
								  ui     : 'action',  
								  text   : 'Retry'   
							 },{   
								  itemId : 'cancel',   
								  text   : 'Cancel'    
						 }], 
						 confirm  : {},   
							fn: function(btn) {
								if (btn == 'Retry')
									me.submitClaim();
							}  
					});
				}
				else{  

					Ext.getCmp(claimType+"DeclarationCheckbox").uncheck();

					if(claimType=='meal')
						Shared.pushNewView('mealClaimSubmittedPageId','MealClaimSubmittedWidget');
					else if(claimType=='capped')
						Shared.pushNewView('cappedClaimStepThreePageId','CappedClaimStep3');
					else if(claimType=='vehicle')
						Shared.pushNewView('vehicleRunningExpenseClaimStepFivePageId','vehicleRunningExpenseClaimStepFive');

					ClaimForm.ClaimSubmittedPageLoad(claimType,Response.ClaimId); 

					var claimData = Ext.getStore('storeLocalData');

					claimData.removeAll();
 				}
				return;
			},
			failure: function (response, opts) {
                 Ext.Msg.show({ 
					 title   : 'Message', 
					 message     : 'Server side failure with status code ' + response.status, 
					 buttons : [{ 
							  itemId : 'Retry',
							  ui     : 'action',  
							  text   : 'Retry'   
						 },{   
							  itemId : 'cancel',   
							  text   : 'Cancel'    
					 }], 
					 confirm  : {},   
						fn: function(btn) {
							if (btn == 'Retry')
								me.submitClaim();
					}  
				});
			}
		});
	},
	// page after submission show event
	ClaimSubmittedPageLoad:function(claimType,ClaimId){

		Logger.info("ClaimForm class ClaimSubmittedPageLoad function"); 
		
		ClaimRefNo = Ext.getCmp(claimType+'ClaimRefNo');
		ClaimRefNo.setHtml(Messages.yourclaimRefNoMsg+ClaimId);


		var usersStore = Ext.getStore('User_S').getAt(0);
        
		var FirstName=usersStore.get('FirstName'); 
        var firstNameLabel = Ext.getCmp(claimType+'ClaimFirstName');
		firstNameLabel.setHtml(FirstName);
		
		var EmployerName=usersStore.get('EmployerName');
		var employerNameLabel = Ext.getCmp(claimType+'ClaimEmployerName');
		employerNameLabel.setHtml(EmployerName);
		
		var sortedClaimsArr=Shared.formDataAsPerClaimIndexes();
		 
		var grandTotal=0;

		var claimDetailWrapperObj = Ext.getCmp(claimType+'ClaimDetailedSummaryWrapper');

		claimDetailWrapperObj.removeAll(true,true);

		var totalamt=0;

		var claimStoreObj=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');	
		Ext.each(sortedClaimsArr,function (claim,indexClaim){
			
			if(Ext.isDefined(claim)){ 

				if(claimType=='vehicle'){  
					var fieldObj=claimStoreObj.VehicleClaim.VehicleTransactionTypes.VehicleTransactionType[indexClaim].VehicleTransactionTypeFields.VehicleTransactionTypeField;
				}else if(claimType=='capped'){  
					var fieldObj=claimStoreObj.ThresholdClaim.ExpenseTypes.ExpenseType[indexClaim].ExpenseTypeFields.ExpenseTypeField;
				}
				else{
					if(Ext.isDefined(claimStoreObj.MealClaim.ExpenseTypes.ExpenseType[indexClaim])){
						 var fieldObj=claimStoreObj.MealClaim.ExpenseTypes.ExpenseType[indexClaim].ExpenseTypeFields.ExpenseTypeField;
					}
					else{
						 var fieldObj=claimStoreObj.MealClaim.ExpenseTypes.ExpenseType.ExpenseTypeFields.ExpenseTypeField;
					}
				}
				 
				for(var i=0;i<claim.length;i++){

					var record = claim[i]; //Get the record        
					var data = record.getData(); //Get the data from the record 
					Logger.debug(data);

					totalamt+=parseFloat(data.formData.Amount);

					var ClaimDetailCnt = Ext.create('Ext.Container',{
						cls :'pad-5-10 bgwhite', 
						items: [{
							xtype: 'label',
							html: 'Date Paid: '+data.formData.ExpenseDate,
							cls: 'font-12 pad-left-5 font-vlightgrey margin-10-0'
						}]});
					Ext.Array.each(fieldObj,function(field,i){
						if(field.Key=='ExpenseDate')
							return;
						else if(field.Name=="Subtotal")
							itemKey=field.Name;
						else
							itemKey=field.Key;
						if(field.DataType=='Money')
							itemValue='$'+parseFloat(data.formData[itemKey]).toFixed(2);
						else
							itemValue=data.formData[itemKey]; 

						var totalAmntClaimedContainer = Ext.create('Ext.Container', {
							layout:'hbox',
							cls:'pad-bottom-5 pad-5-10 font-14 f-darkgrey',
							items:[{
								xtype: 'label',
								html: field.Name+':',
								flex:1
							},{
								xtype: 'label',
								html: itemValue,
								cls:'pad-right-5 txt-right f-bold',
								flex:1
							}]
						});
						ClaimDetailCnt.add(totalAmntClaimedContainer);
					});
					 
					if(data.fileData.length)
					{
						var documentUploaded = Ext.create('Ext.Container', {
												layout:'hbox',
												cls:'pad-bottom-5 pad-5-10 font-14 f-darkgrey',
												items:[{
													xtype: 'label',
													html: 'No. of documents included:',
													flex:1
												},{
													xtype: 'label',
													html: data.fileData.length,
													cls:'pad-right-5 txt-right f-bold',
													flex:1
												}]
											});
						 
						ClaimDetailCnt.add(documentUploaded);
					}
					
					claimDetailWrapperObj.add(ClaimDetailCnt);
				}
			}
		});
		var changeAccountBtn = Ext.create('Ext.Container',{
			cls :'bgwhite font-14 color-2B2F3B clearfix',
			items:[{
				xtype: 'button',
				ui: 'none',
				cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 color-5A5B77',
				text: 'Change account'
			}]
		});
		claimDetailWrapperObj.add(changeAccountBtn);				
		totalamt=parseFloat(totalamt).toFixed(2);
		Ext.getCmp(claimType+'ClaimTotalAmountSubmittedPage').setHtml('$'+totalamt);
	}

})