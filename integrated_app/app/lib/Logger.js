Ext.define('SmartSalary.lib.Logger', {
    singleton: true,
    alternateClassName: ['Logger'],
    debug : function(msg){ 
		if (Config.logging && Config.log_level == 1) window.console.log(msg);   
	},
	info : function(msg){ 
		if (Config.logging && Config.log_level <= 2) window.console.info(msg);  
	},
	warn : function(msg){ 
		if (Config.logging && Config.log_level <= 3) window.console.warn(msg);  
	},
	error : function(msg){ 
		if (Config.logging && Config.log_level <= 4) window.console.error(msg); 
	},
	alert : function(headingMsg,msg){
		Ext.Msg.defaultAllowedConfig.showAnimation = false; 
		if(Config.alert_msg) Ext.Msg.alert(headingMsg,msg); 
	}
});