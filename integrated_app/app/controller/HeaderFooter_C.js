Ext.define('SmartSalary.controller.HeaderFooter_C', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
		// Define view element for refrence.
			termConditionButton : 'container button[itemId = termCondition]',
            privacyPolicyButton :'container button[action = privacyPolicy]',
			smartSalaryLogoAction : 'image[itemId = smartSalaryLogo]',
			NewLogoutBtn:'button[itemId=NewLogoutBtn]',	
        },
        control: {
		// On Tap of termConditionButton button tap activeTermConditionView function execute.
            termConditionButton:{tap: 'activeTermConditionView'},
		// On Tap of privacyPolicyButton button  activePrivacyPolicyView function execute.
			privacyPolicyButton:{tap: 'activePrivacyPolicyView'},
		//On Tap of Logo redirect to Home page
			smartSalaryLogoAction: {
				tap:'smartSalaryLogoClickMethod'	
			},
			NewLogoutBtn:{
				tap:'NewLogoutCommand'
			}
			
			
        }
    },
	NewLogoutCommand:function(){
		//Remove data from store when user logoff.
		var userStoreObj = Ext.getStore('User_S'); 
		if(userStoreObj.getCount() > 0) userStoreObj.removeAt(0);
		
		//Remove data from store when user logoff.
		var employerListObj = Ext.getStore('EmployerList');  
        if(employerListObj.getCount() > 0) employerListObj.removeAll();
  
        Shared.base64variable=''; // clearing the credentials
        window.onbeforeunload = null; // unbinding the onbeforeunload event 
        window.onpagehide = null; // unbinding the onpagehide event 

        window.location.href="../user/logout";
	},
    smartSalaryLogoClickMethod:function (){
		var userStoreObj = Ext.getStore('User_S'); 
		if(userStoreObj.getCount() > 0){
			var objStoreLocalData=Ext.getStore('storeLocalData');
			if(objStoreLocalData.getCount() > 0){
				Ext.Msg.confirm(Messages.AlertMsgConfirmation, Messages.AlertMsgLostDataMsg,
					function (buttonId){
						if(buttonId=='yes')
							Shared.pushNewView('dashboardscreen','dashboardView','right');
				}); 
			}
			else{
				Shared.pushNewView('dashboardscreen','dashboardView');
			}
		}
	}
});