Ext.define('SmartSalary.controller.MealClaim_C', {
	extend : 'Ext.app.Controller',
	PaymentPeriods: '', 
	config: {
		refs:{

			/*** Select claim type page refs */
			mealClaimExpenseSelectionView:'container[itemId=MealClaimExpenseSelectionId]',
			mealClaimSelectionPageCancelBtn:'button[action=mealClaimSelectionPageCancelBtnPress]',
			mealClaimSelectionToListingPageSkipButton:'button[action=mealClaimSelectionToListingPageSkipButton]',

			/*** Claim form add/update page refs *****/
			mealClaimFormPageNextButton:'button[action=mealClaimFormPageNextButtonPress]',			
			mealClaimFormPageBackButton:'button[action=mealClaimFormPageBackButtonAction]',
			mealClaimFormCancelButton:'button[action=mealClaimFormCancelButtonPress]',
			mealClaimFormPageSkipButton:'button[action=mealClaimFormPageSkipButtonPress]',
			mealClaimFormPageView:'container[itemId=MealClaimFormPageId]',

			/*** Claim summary before submission page refs ****/
			mealClaimStepTwoSaveAndAnotherButton:'button[action=MealClaimStepTwoSaveAndAnotherButton]',			
			mealClaimListingPageId : 'container[itemId=MealClaimListingPageId]',			
			mealClaimListingPageBackBtnPress: 'button[action=mealClaimListingPageBackBtn]',
			mealClaimListingPageNextButtonPress:'button[action=mealClaimListingPageNextButton]',
			mealClaimListingPageBackBtnUpper:'button[action=mealClaimListingPageBackBtnUpper]',

			/********* Confirmation page before submission page refs **********/
			
			mealClaimConfirmPageView : 'container[itemId=MealClaimConfirmPageId]',			
			mealClaimConfirmPageBackButtonPress:'button[action=mealClaimConfirmPageBackButton]',
			mealClaimConfirmPageSubmitButtonPress: 'button[action=mealClaimConfirmPageSubmitButton]',
			mealClaimConfirmPageBackBtn:'button[action=mealClaimConfirmPageBackBtn]',
			
			/******  Form edit page dynamically added button refs ********/
			saveContinueButtonMealPress:'button[action=saveContinueButtonMealPress]',
			
			/******* Claim summary after submission submission  page refs **************/
			mealTrackTheProgressOfYourClaimPress:'button[action=mealTrackTheProgressOfYourClaimPress]',
			mealReturnToYourClaimsPress: 'button[action=returnToYourClaimsPressMeals]',

			/**** cancel button which is used in confirmation page and summary page ********/
			mealClaimCancelButton:'button[action=mealClaimCancelButton]',

			/*** commonly used refs in controller *************/
			claimAmount:'#Amount',
			claimGST:'#GST',
			claimSubTotal:'#Subtotal'			
			
		},
		control:{

			/*** Select claim type page control */
			mealClaimExpenseSelectionView:{
				show:'MealClaimExpenseSelectionViewShow'
			},
			mealClaimSelectionPageCancelBtn:{
				tap : 'gotToDashboard'	
			},
			mealClaimSelectionToListingPageSkipButton:{
				tap:'moveToClaimListingPage'
			},
			/*** Claim form add/update page control *****/

			mealClaimFormPageNextButton:{
				tap:'SaveClaimExpense'
			},
			mealClaimFormPageBackButton:{
				tap:'cancelClaimForm'	
			},
			mealClaimFormCancelButton:{
				tap : 'gotToDashboard'
			},
			mealClaimFormPageSkipButton:{
				tap:'moveToClaimListingPage'
			},

			/*** Claim summary before submission page control ****/
			mealClaimStepTwoSaveAndAnotherButton:{
				tap:'saveAndAddAnother'
			},
			mealClaimListingPageId:{
				show:'mealClaimListingPageLoad'
			},
			mealClaimListingPageBackBtnPress:{
				tap:'cancelClaimForm'
			},
			mealClaimListingPageNextButtonPress:{
				tap:'moveToConfirmationPage'
			},
			mealClaimListingPageBackBtnUpper:{
				tap:'cancelClaimForm'
			},
			
			/********* Confirmation page before submission page controls **********/

			mealClaimConfirmPageBackButtonPress:{
				tap:'moveToClaimListingPage'
			},
			mealClaimConfirmPageSubmitButtonPress:{
				tap:'submitClaim'
			},
			mealClaimConfirmPageBackBtn:{
				tap:'moveToClaimListingPage'
			},

			/******  Form edit page dynamically added button control ********/
			saveContinueButtonMealPress:{
				tap:'updateClaim'
			},

			/******* Claim summary after submission submission  page control **************/
			mealTrackTheProgressOfYourClaimPress:{
				tap:'moveToTrackMyClaims'	
			},
			mealReturnToYourClaimsPress:{
				tap : 'returnToDashboard'
			}, 

			/**** cancel button which is used in confirmation page and summary page control ********/
			mealClaimCancelButton:{
				tap : 'gotToDashboard'
			}
			 
		} 	  
	},
	saveAndAddAnother:function ()
	{
		var mealClaimListingPageIdObj=this.getMealClaimListingPageId(); // get the view object

		if(mealClaimListingPageIdObj.down("#PaymentPeriods"))
		{
			if(!(/^[0-9]*$/.test(mealClaimListingPageIdObj.down("#PaymentPeriods").getValue())))
			{
				Logger.alert(Messages.AlertMsg,Messages.AlertMsgInvalidPaymentPeriod);
				return false;
			}
			this.PaymentPeriods=mealClaimListingPageIdObj.down("#PaymentPeriods").getValue();	
		}
		this.cancelClaimForm();
	},
	moveToTrackMyClaims:function (){
		Logger.info("MealTrackTheProgressOfYourClaimPress tap");
		Shared.pushNewView('claimlistscreen','claimList','right');
	},
	moveToClaimListingPage:function(){
		Logger.info("moveToClaimListingPage tap");
		Shared.pushNewView('MealClaimListingPageId','MealClaimListingPageWidget','right');
	}, 
	// return to dashboard without confirm
	returnToDashboard:function(){
		 
		Logger.info("returnToDashboard function");
		Shared.pushNewView('dashboardscreen','dashboardView','right');
		
	},

	// re-direct to dashboard with confirmation of user
	gotToDashboard:function (){
		Logger.info("gotToDashboard function");
		Ext.Msg.confirm(Messages.AlertMsgConfirmation, Messages.AlertMsgLostDataMsg, function (buttonId){
			if(buttonId=='yes'){
				Logger.info("yes option choosed");
				Shared.pushNewView('dashboardscreen','dashboardView','right');
			}
		}); 
	},
	
	cancelClaimForm:function (){
		Logger.info("cancelClaimForm function");		
		var claimsResultData=Ext.getStore("ClaimsStore").getData().getAt(0).get('result');  //get meal claim fields records from the claim store
		
		Shared.GSTRate=claimsResultData.GSTRate; // GST rates from store;
		Logger.debug(claimsResultData.MealClaim.ExpenseTypes);
		if(Ext.isDefined(claimsResultData.MealClaim.ExpenseTypes.ExpenseType[0]))
		{  
			Shared.pushNewView('MealClaimExpenseSelectionId','MealClaimExpenseSelectionWidget');	
			 
		}else{
			this.getApplication().getController('MealClaim_C').MealClaimTypeSelected(claimsResultData.MealClaim.ExpenseTypes.ExpenseType.BenefitId+'_0');
		} 
	}, 
	MealClaimExpenseSelectionViewShow:function(){
		Logger.info("MealClaimExpenseSelectionViewShow function");
		var me=this;	 
		var mealClaimExpenseSelectionViewObj = this.getMealClaimExpenseSelectionView();	// get the view object
		
		var claimsResultData=Ext.getStore("ClaimsStore").getData().getAt(0).get('result');  //get meal claim fields records from the claim store
		

		Shared.GSTRate=claimsResultData.GSTRate; // GST rates from store;
		Logger.debug(claimsResultData.MealClaim.ExpenseTypes);
		if(!Ext.isDefined(claimsResultData.MealClaim.ExpenseTypes.ExpenseType[0]))
		{
			me.MealClaimTypeSelected(claimsResultData.MealClaim.ExpenseTypes.ExpenseType.BenefitId+'_0');
			return;
		}

		if(claimsResultData.MealClaim.ExpenseTypes.hasOwnProperty('ExpenseType')) // check if thresholdClaim has Expense types to work on
		{	

 			var mealClaimType=claimsResultData.MealClaim.ExpenseTypes.ExpenseType 

			var mealClaimExpTypeWrapperObj = mealClaimExpenseSelectionViewObj.down('#mealClaimExpTypeWrapper');
			
			mealClaimExpTypeWrapperObj.removeAll(); // clear the previously added form field wrapper
			 
			Ext.each(mealClaimType, function(mealClaim,i) {
				// create the expense type selection field 	 
				var mealClaimTypeFieldBtn = Ext.create('Ext.Container',{
					cls: 'pad-top-15 font-14 borderbtm',
					items:[{
						xtype: 'button',
						cls: 'typeofclaimbox',
						itemId:mealClaim.BenefitId+'_'+i, 
						text:mealClaim.Name,
						listeners:{
							tap:function(){
								me.MealClaimTypeSelected(this.getItemId());
							}
						} 
					}]
				});
				
				mealClaimExpTypeWrapperObj.add(mealClaimTypeFieldBtn); // add radio button to wrapper
			});

			var objStoreLocalData=Ext.getStore('storeLocalData');
			if(objStoreLocalData.getCount()>0){
				mealClaimExpenseSelectionViewObj.down("#mealClaimSelectionPageSkipButton").show();
			}else{
				mealClaimExpenseSelectionViewObj.down("#mealClaimSelectionPageSkipButton").hide();
			}
		}
	},	 

	// called on selection of any expense type

	MealClaimTypeSelected:function(val){
 
		Logger.info("MealClaimTypeSelected function");

		var me = this;
		
		var selectedClaimValue = val.split("_"); // get the selected value
 
		var BenefitId = selectedClaimValue[0]; 

		var claimindex =  selectedClaimValue[1];
  
		Shared.pushNewView('MealClaimFormPageId','MealClaimFormWidget');

		var mealClaimFormPageViewObj=this.getMealClaimFormPageView(); // get view object
		
		mealClaimFormPageViewObj.down("#mealClaimFormButtonWrapper").show(); // show the navigation buttons here
		
 		var claimFormFields =  Ext.getStore('ClaimsStore');

        var claimFormFieldsData =claimFormFields.getAt(0).data.result; // get claim data

        if(Ext.isDefined(claimFormFieldsData.MealClaim.ExpenseTypes.ExpenseType[claimindex]))
        {
        	var claimTypeObj=claimFormFieldsData.MealClaim.ExpenseTypes.ExpenseType[claimindex];
        }
        else
        {
        	var claimTypeObj=claimFormFieldsData.MealClaim.ExpenseTypes.ExpenseType;
        	mealClaimFormPageViewObj.down("#mealClaimFormPageBackButton").setHidden(true);
        	var objStoreLocalData=Ext.getStore('storeLocalData');
			if(objStoreLocalData.getCount()>0){
				this.getMealClaimFormPageSkipButton().show();
			}else{
				this.getMealClaimFormPageSkipButton().hide();
			}
        }


		
		var claimTypefields = claimTypeObj.ExpenseTypeFields.ExpenseTypeField;
		 
		var allowAttachment=claimTypeObj.AllowAttachment
		
		var expenseTypeId=claimTypeObj.Id;
		 
		ClaimForm.columns=(Ext.os.is.Tablet && allowAttachment==='True')?2:1; // setting the column layout of the form

		var mealClaimFormFieldsWrapper = mealClaimFormPageViewObj.down('#mealClaimFormFieldsWrapper');
			mealClaimFormFieldsWrapper.removeAll(true,true);
		ClaimForm.initialize(mealClaimFormFieldsWrapper);

		if(allowAttachment==='True')
			var attachmentFlag = 2;
		else 
			var attachmentFlag = 4;
		var hiddenFields={
							'SubstantiationMethodID':attachmentFlag,
							'ExpenseTypeId':expenseTypeId,
							'BenefitId':BenefitId,
							'claimindex':claimindex
						};		
			  
		ClaimForm.addHiddenField(hiddenFields);	 // adding the hidden fields to form
		ClaimForm.addFields(claimTypefields); // adding fields to the form
		 
		// display the fields for uploading the image

		if(allowAttachment==='True') // check attachment is there
		{
 			ClaimForm.addAttachmentField(); // add attachment field here
		} 

		ClaimForm.load(); //finally load the form
 	},
	// save the current expense
	SaveClaimExpense:function(){

		Logger.info("SaveClaimExpense function");

		var mealClaimFormPageViewObj=this.getMealClaimFormPageView(); // get view object

		var me=this;

		// Show loader......
		Ext.Viewport.setMasked(Shared.loadingMask);//Check GST not exeed 10% of claim value. 

		// getting  submitted values
		var frmPostedData =mealClaimFormPageViewObj.getValues();

		var objStoreLocalData=Ext.getStore('storeLocalData');

		var claimFormFields =  Ext.getStore('ClaimsStore'); 

        var claimFormFieldsData =claimFormFields.getAt(0);
		if(Ext.isDefined(claimFormFieldsData.data.result.MealClaim.ExpenseTypes.ExpenseType[frmPostedData.claimindex]))
			var claimExpenseDetailObj=claimFormFieldsData.data.result.MealClaim.ExpenseTypes.ExpenseType[frmPostedData.claimindex];
		else
			var claimExpenseDetailObj=claimFormFieldsData.data.result.MealClaim.ExpenseTypes.ExpenseType;

		var claimFields=claimExpenseDetailObj.ExpenseTypeFields.ExpenseTypeField;
		var allowAttachment=claimExpenseDetailObj.AllowAttachment;
		if(!ClaimForm.validateClaimFields(claimFields,allowAttachment==='True')) // validate the form fields
		{
			Ext.Viewport.setMasked(false); //  close the loader
			return false;
		}

		if(mealClaimFormPageViewObj.down('#GST')){
			var GSTcal = Shared.checkGSTNotExceedTenPercent(this.getClaimAmount().getValue(),this.getClaimGST().getValue());
			if(!GSTcal){ 
				Ext.Viewport.setMasked(false); //  close the loader
				return false;
			}
		}
		 
		var row_id=objStoreLocalData.getAllCount()+1;
		
		// create model to save submitted data to store
		var objModelLocalData=Ext.create("SmartSalary.model.Localdata_M",{			 
            'formData':frmPostedData,
            'fileData':Shared.fileData,
            'claimindex':frmPostedData.claimindex,
            'row_id':row_id
		});
 
		objStoreLocalData.add(objModelLocalData);

		Ext.Msg.alert(Messages.AlertMsgMessage,Messages.AlertMsgClaimAdded,function(){
			
			Ext.Viewport.setMasked(false); //  close the loader
	 
			Shared.fileData = new Array(); // reset the data variable  
			 
			Shared.pushNewView('MealClaimListingPageId','MealClaimListingPageWidget');
			 
		});
	},
	// edit form show of current expense
	editClaimForm:function(val){ 

 		Logger.info("editClaimForm function");

		// Show loader......
		Ext.Viewport.setMasked(Shared.loadingMask);

		Shared.pushNewView('MealClaimFormPageId','MealClaimFormWidget','right');

		var mealClaimFormPageViewObj=this.getMealClaimFormPageView(); // get view object

		var explArr=val.split("_");
 
		var sortedClaimsArr=Shared.formDataAsPerClaimIndexes('storeLocalData');
 	
 		var dataArr=sortedClaimsArr[explArr[1]][explArr[2]].getData();
		var formData=Array(); 
		var fileData=dataArr.fileData; 
		var _Object=dataArr.formData
		for(var name in _Object){
           formData[name] = _Object[name];
        } 

		var indexClaim=explArr[1];	

		var claimNumber=explArr[2]; 

		var claimStoreObj=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');	

		if(Ext.isDefined(claimStoreObj.MealClaim.ExpenseTypes.ExpenseType[indexClaim]))
        {
        	var claimType=claimStoreObj.MealClaim.ExpenseTypes.ExpenseType[indexClaim];
        }
        else
        {
        	var claimType=claimStoreObj.MealClaim.ExpenseTypes.ExpenseType;        	 
        }
 
		var expenseTypeId=claimType.Id; 

		var BenefitId=claimType.BenefitId; // this will be sent to server again
 
		mealClaimFormPageViewObj.down("#mealClaimFormButtonWrapper").hide(); // hide the navigation buttons here

		var mealClaimFormFieldsWrapper = mealClaimFormPageViewObj.down('#mealClaimFormFieldsWrapper');
			
			mealClaimFormFieldsWrapper.removeAll(true,true);

		var claimTypeHeader = 	Ext.create('Ext.Container',{
				cls: 'claimEditHeading font-16',
				items:[{
					xtype:'label',
					html: '<h3>'+claimType['Name']+'</h3>'
				}]
		});

		// Add claim type radio button 
		mealClaimFormFieldsWrapper.add(claimTypeHeader); 

		ClaimForm.columns=(Ext.os.is.Tablet && claimType.AllowAttachment==='True')?2:1; // setting the column layout of the form

		ClaimForm.initialize(mealClaimFormFieldsWrapper);

		if(claimType.AllowAttachment==='True')
			var attachmentFlag = 2;
		else 
			var attachmentFlag = 4; 

		var hiddenFields={
							'SubstantiationMethodID':attachmentFlag,
							'ExpenseTypeId':expenseTypeId,
							'BenefitId':BenefitId,
							'claimindex':indexClaim,
							'row_id':dataArr.row_id	
						};		
			  
		ClaimForm.addHiddenField(hiddenFields);	 // adding the hidden fields to form	

		var claimTypefields = claimType.ExpenseTypeFields.ExpenseTypeField;
			 
		ClaimForm.addFields(claimTypefields,formData); // adding fields to the form 
		  
		if(claimType.AllowAttachment==='True')// check attachment is there
		{
 			ClaimForm.addAttachmentField(fileData); // add attachment field here
		} 
		//Add button for the claim type.
		var fieldTypeButton = Ext.create('Ext.Container',{
			items: [{
				xtype: 'button',
				itemId: 'saveContinueButtonMealPress',
				ui: 'none',
				action: 'saveContinueButtonMealPress',
				flex:1,
				cls:'claimStepOneSaveAsDraftButton claimSaveButton txt-left font-16 pad-10 margin-10-0',			
				text: 'Save & Continue'
			}]
		});
		
		ClaimForm.filesAndButtonsWrapperObj.add(fieldTypeButton);
		
		ClaimForm.load(); //finally load the form 
		
		// Hide loader......
		Ext.Viewport.setMasked(false);
	},
	//update the claim expense 
	updateClaim:function(){

		Logger.info("updateClaim function");

		var me= this;

		var mealClaimFormPageViewObj=this.getMealClaimFormPageView(); // get view object

		Ext.Viewport.setMasked(Shared.loadingMask);

		var frmPostedData =mealClaimFormPageViewObj.getValues(); // get the values of the form
		
		Logger.debug(frmPostedData);
		 
		var claimFormFields =  Ext.getStore('ClaimsStore');

        var claimFormFieldsData =claimFormFields.getAt(0);

        if(Ext.isDefined(claimFormFieldsData.data.result.MealClaim.ExpenseTypes.ExpenseType[frmPostedData.claimindex]))
        {
        	var claimExpenseDetailObj=claimFormFieldsData.data.result.MealClaim.ExpenseTypes.ExpenseType[frmPostedData.claimindex];
        }
        else
        {
        	var claimExpenseDetailObj=claimFormFieldsData.data.result.MealClaim.ExpenseTypes.ExpenseType;        	 
        }
 
		var claimFields=claimExpenseDetailObj.ExpenseTypeFields.ExpenseTypeField;
		
		var allowAttachment=claimExpenseDetailObj.AllowAttachment;
		if(!ClaimForm.validateClaimFields(claimFields,allowAttachment==='True')) // validate the form fields
		{
			Ext.Viewport.setMasked(false); //  close the loader
			return false;
		}
		if(mealClaimFormPageViewObj.down('#GST'))//Check GST not exeed 10% of claim value. 
		{
			var GSTcal = Shared.checkGSTNotExceedTenPercent(this.getClaimAmount().getValue(),this.getClaimGST().getValue());
			if(!GSTcal){
			 	Ext.Viewport.setMasked(false); //  close the loader
			 	return false;
			}
		}
		// Show loader......
		
		var row_id=mealClaimFormPageViewObj.down('#row_id').getValue();
 	
		var claimData = Ext.getStore('storeLocalData');
		claimData.filter("row_id",row_id);
		
		claimData.getAt(0).set({'formData':frmPostedData,
	            'fileData':Shared.fileData}); // update the selected row in store
		
 		claimData.clearFilter(); // clear the filters
		
		Shared.fileData=Array(); // Reset the file data array;

		Shared.pushNewView('MealClaimListingPageId','MealClaimListingPageWidget');
		
	},
	//delete the claim expense 
	deleteClaimForm:function(val){
		Logger.info("deleteClaimForm function"); 
		var me=this; 
		Ext.Msg.confirm(Messages.AlertMsgConfirmation, Messages.AlertMsgDeleteThisMsg, function (buttonId){
			if(buttonId=='yes'){
				var explArr=val.split("_");
				var row_id=explArr[1];
				Logger.debug(row_id);
				var claimData = Ext.getStore('storeLocalData');
				claimData.filter("row_id",row_id);
				claimData.removeAt(0);
 				claimData.clearFilter();
				if(claimData.getCount()>0){
					me.mealClaimListingPageLoad();
				}else{
					me.cancelClaimForm();
				}	
			}
		});
	},
	// Called on next button of summary page and to move on confirmation page
	moveToConfirmationPage:function (){

		Logger.info("moveToConfirmationPage function");

		var mealClaimListingPageIdObj=this.getMealClaimListingPageId(); // get the view object

		if(mealClaimListingPageIdObj.down("#PaymentPeriods"))
		{ 
			if(!(/^[0-9]*$/.test(mealClaimListingPageIdObj.down("#PaymentPeriods").getValue())))
			{
				Logger.alert(Messages.AlertMsg,Messages.AlertMsgInvalidPaymentPeriod);
				return false;
			}
			this.PaymentPeriods=mealClaimListingPageIdObj.down("#PaymentPeriods").getValue();	
		}

		Shared.pushNewView('MealClaimConfirmPageId','MealClaimConfirm');
		
		var mealClaimConfirmPageViewObj=this.getMealClaimConfirmPageView();


		var ClaimsResult =Ext.getStore("ClaimsStore").getData().getAt(0).get('result');
		
		var nameOnAc = mealClaimConfirmPageViewObj.down('#mealClaimNameOnAc'); 
		nameOnAc.setHtml(ClaimsResult.BankAccount.AccountName);
		
		var BSB = mealClaimConfirmPageViewObj.down('#mealClaimBSB'); 
		BSB.setHtml(ClaimsResult.BankAccount.BSB);
		
		var acNo = mealClaimConfirmPageViewObj.down('#mealClaimAcNo'); 
		acNo.setHtml(ClaimsResult.BankAccount.AccountNumber);
		
		var declarationtxt = mealClaimConfirmPageViewObj.down('#mealClaimdeclarationtxt'); 
		declarationtxt.setHtml(ClaimsResult.DeclarationPage);

		// get user personal detail from user store

		var userStoreObj = Ext.getStore('User_S').getAt(0).data; //  

		var user_email = mealClaimConfirmPageViewObj.down('#user_email'); 
		user_email.setHtml(userStoreObj.Email);

		var user_contact = mealClaimConfirmPageViewObj.down('#user_contact'); 
		user_contact.setHtml(userStoreObj.WorkPhone+" (W)");
	},
	// on show of Meal claim summary page 
	mealClaimListingPageLoad : function(){
		Logger.info("mealClaimListingPageLoad function");
		/*Show Loader*/
 		Ext.Viewport.setMasked(Shared.loadingMask);

		var me=this;

		var mealClaimListingPageIdObj=this.getMealClaimListingPageId(); // get the view object

		var claimData = Ext.getStore('storeLocalData');	

		var mealClaimDetail =  Ext.getStore('ClaimsStore').getAt(0).data.result.MealClaim;

		var grandTotal=ClaimForm.itemListing(me,"mealClaimItemsWrapper",mealClaimDetail.ExpenseTypes,"meal");

		if(mealClaimDetail.ShowPayPeriod==='True')
		{
			var payPeriodField = Ext.create('Ext.Container',{
				cls:'pad-10 noactive margin-10 bgd7',
				items: [{
							xtype:'label',
							html:mealClaimDetail.PayPeriodLabel,
							cls:'margin-10-0 font-12 font-bold f-darkgrey'
						},
						{
							xtype:'numberfield', 
							itemId: 'PaymentPeriods',
							id:'PaymentPeriods',
							name: 'PaymentPeriods',
							required: true,
							value:this.PaymentPeriods,
							cls:'font-12 pad-left-5 bgdarkgrey txtboxbg'
						},{
							xtype:'label',
							html:mealClaimDetail.PayPeriodDescription,
							cls:'margin-10-0 font-12  f-darkgrey '
						}]
			});
			mealClaimListingPageIdObj.down("#mealClaimItemsWrapper").add(payPeriodField);
		}

		var totalClaimAmountListingPage = mealClaimListingPageIdObj.down("#totalClaimAmountListingPage");
		totalClaimAmountListingPage.setHtml('Total value of this claim ('+claimData.getData().length+' item)');
		 
		var totalClaimAmountValueFormListing = mealClaimListingPageIdObj.down("#totalClaimAmountValueFormListing");
		totalClaimAmountValueFormListing.setHtml("$"+grandTotal); 

		Ext.Viewport.setMasked(false);
	},
	// Final submission of the claims
	submitClaim:function(){
		Logger.info("submitClaim of controller");
		var usersStore = Ext.getStore('User_S');
        var aRecord =usersStore.getAt(0);
        var PackageId=aRecord.get('PackageId');
        var submitUrl=Config.ApiBaseUrl+'api/submit_cappedclaim/'+PackageId+"/1";
        ClaimForm.submitClaim(this,submitUrl,'meal',this.PaymentPeriods);
        return false;
	}
	
});