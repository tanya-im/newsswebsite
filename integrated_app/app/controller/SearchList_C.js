Ext.define('SmartSalary.controller.SearchList_C', {
    extend: 'Ext.app.Controller',
    config: {
        refs:{SearchList:'list[itemId=listSearch]'},
        control:{SearchList:{itemtap:'searchListItemTap'}}
	},
    searchListItemTap:function(list, index, target, record, e, eOpts){
    	this.getApplication().getController('Dashboard_C').contentDetailPage(record.get('nid'),true);  
    }
});