Ext.define('SmartSalary.controller.CappedClaim_C', {
	extend : 'Ext.app.Controller', 
	config: {
		refs:{
		/*** Select claim type page refs */
			cappedClaimStepOneView:'panel[itemId=CappedClaimStepOnePageId]',
			cappedClaimStepOneCancelButtonPress:'button[action=cappedClaimStepOneCancelBtnPress]',
			cappedClaimStepOneToStepThirdButtonPress:'button[action=cappedClaimStepOneToStepThirdButtonPress]',
		/*** Claim form add/update page refs *****/
			cappedClaimFormStepPageView:'panel[itemId=CappedClaimFormStepPageId]',
			claimStepOneNextButton:'button[action=cappedClaimStepOneNextButtonPress]',			
			cappedClaimStepFormBackButtonPress:'button[action=cappedClaimStepFormBackButtonAction]',
			cappedClaimStepFormCancelButtonPress:'button[action=cappedClaimStepFormCancelButtonPress]',
			cappedClaimFormPageBackBtn:'button[itemId=cappedClaimFormPageBackBtn]',
		/*** Claim summary before submission page refs ****/
			claimStepTwoSaveAndAnotherButton:'button[action=claimStepTwoSaveAndAnotherButton]',			
			taxFreeCappedClaimStepTwoPageId : 'container[itemId=CappedClaimStepTwoPageId]',			
			cappedClaimStepTwoBackButtonPress: 'button[action=cappedClaimStepTwoBackButtonPress]',
			cappedClaimStepTwoNextButtonPress:'button[action=cappedClaimStepTwoNextButtonPress]',
			cappedClaimListingPageBackBtn:'button[action=cappedClaimListingPageBackBtn]',
		/********* Confirmation page before submission page refs **********/
			cappedClaimConfirmPageView:'panel[itemId=CappedClaimConfirmPageId]',
			cappedClaimStepConfirmBackButton:'button[action=cappedClaimStepConfirmBackButton]',
			cappedClaimStepTwoSubmitButtonPress: 'button[action=cappedClaimStepTwoSubmitButtonPress]',
			cappedClaimConfirmationPageBackBtn:'button[action=cappedClaimConfirmationPageBackBtn]', 
		/******  Form edit page dynamically added button refs ********/
			saveContinueButtonPressCapped:'button[action=saveContinueButtonPressCapped]',
		/******* Claim summary after submission submission  page refs **************/
			cappedClaimStepThreePageView:'panel[itemId=cappedClaimStepThreePageId]',
			cappedTrackTheProgressOfYourClaimPress:'button[action=cappedTrackTheProgressOfYourClaimPress]',
			returnToYourClaimsPress: 'button[action=returnToYourClaimsPress]',
		/**** cancel button which is used in confirmation page and summary page ********/
			cappedClaimStepTwoCancelButtonPress:'button[action=cappedClaimStepTwoCancelButtonPress]',
		/*** commonly used refs in controller *************/
			claimAmount:'#Amount',
			claimGST:'#GST',
			claimSubTotal:'#Subtotal'			
		},
		control:{
		/*** Select claim type page control */
			cappedClaimStepOneView:{show:'CappedClaimStepOneViewShow'},
			cappedClaimStepOneCancelButtonPress:{tap : 'gotToDashboard'},
			cappedClaimStepOneToStepThirdButtonPress:{
				tap:'moveToClaimListingPage'
			},
		/*** Claim form add/update page control *****/			
			claimStepOneNextButton:{tap:'SaveClaimExpense'},
			cappedClaimStepFormBackButtonPress:{tap:'cancelClaimStep2'},
			cappedClaimStepFormCancelButtonPress:{tap : 'gotToDashboard'},
			cappedClaimFormPageBackBtn:{tap:'cancelClaimStep2'},
		/*** Claim summary before submission page control ****/
			claimStepTwoSaveAndAnotherButton:{tap:'cancelClaimStep2'},
			taxFreeCappedClaimStepTwoPageId:{show:'taxFreeCappedClaimStepTwoViewLoad'},
			cappedClaimStepTwoBackButtonPress:{tap:'cancelClaimStep2'},
			cappedClaimStepTwoNextButtonPress:{	tap:'moveToConfirmationPage'},
			cappedClaimListingPageBackBtn:{tap:'cancelClaimStep2'},
		/********* Confirmation page before submission page controls **********/
			cappedClaimStepConfirmBackButton:{
				tap:'moveToClaimListingPage'
			},
			cappedClaimStepTwoSubmitButtonPress:{tap:'submitClaim'},
			cappedClaimConfirmationPageBackBtn:{tap:'moveToClaimListingPage'},	
		/******  Form edit page dynamically added button control ********/
			saveContinueButtonPressCapped:{tap:'updateClaim'},
		/******* Claim summary after submission submission  page control **************/
			cappedTrackTheProgressOfYourClaimPress:{
				tap:'moveToTrackMyClaims'	
			},
			returnToYourClaimsPress:{tap : 'returnToDashboard'}, 
		/**** cancel button which is used in confirmation page and summary page control ********/
			cappedClaimStepTwoCancelButtonPress:{tap : 'gotToDashboard'}
		} 	  
	},
	moveToClaimListingPage:function(){
		Shared.pushNewView('CappedClaimStepTwoPageId','CappedClaimStep2','right');
	},
	moveToTrackMyClaims:function (){
		Shared.pushNewView('claimlistscreen','claimList','right');
	}, 
// return to dashboard without confirm
	returnToDashboard:function(){
		Shared.pushNewView('dashboardscreen','dashboardView','right');
	},
// re-direct to dashboard with confirmation of user
	gotToDashboard:function (){
		Ext.Msg.confirm(Messages.AlertMsgConfirmation, Messages.AlertMsgLostDataMsg, function (buttonId){
			if(buttonId=='yes'){
				Shared.pushNewView('dashboardscreen','dashboardView','right');
			}
		});
	},

	//Show Step two view	
	cancelClaimStep2:function (){		
		Shared.pushNewView('CappedClaimStepOnePageId','CappedClaimStep1','right');		
	},
	// called on show event of step 1
	CappedClaimStepOneViewShow: function(){
			 
		var cappedClaimStep1ViewObj = this.getCappedClaimStepOneView();	// get the view object
		
		var claimsResultData=Ext.getStore("ClaimsStore").getData().getAt(0).get('result');  //get capped claim fields records from the claim store
		
		Shared.GSTRate=claimsResultData.GSTRate; // GST rates from store;

		if(claimsResultData.ThresholdClaim.ExpenseTypes.hasOwnProperty('ExpenseType')) // check if thresholdClaim has Expense types to work on
		{
			var RemainingClaimAmount=claimsResultData.ThresholdClaim.RemainingClaimAmount; // assign the remaining amount
			
			cappedClaimStep1ViewObj.down('#remainingClaimAmountLabel').setHtml("$ "+RemainingClaimAmount);
			 
			var cappedClaimType=claimsResultData.ThresholdClaim.ExpenseTypes.ExpenseType

			var cappedClaimExpTypeWrapperObj = cappedClaimStep1ViewObj.down('#cappedClaimExpTypeWrapper');
			
			cappedClaimExpTypeWrapperObj.removeAll(); // clear the previously added form field wrapper
			
			var me=this;
			 
			Ext.each(cappedClaimType, function(cappedClaim,i) {	 
				// create the expense type selection field 	 
				var cappedClaimTypeFieldRadio = Ext.create('Ext.Container',{
					cls: 'pad-top-15 font-14 borderbtm',
					items:[{
						xtype: 'button',
						cls: 'typeofclaimbox',
						itemId:cappedClaim.BenefitId+'_'+i, 
						text:cappedClaim.Name,
						listeners:{
							tap:function(){
								me.CappedClaimTypeSelected(this.getItemId());
							}
						} 
					}]
				});
				
				cappedClaimExpTypeWrapperObj.add(cappedClaimTypeFieldRadio); // add radio button to wrapper
			})	
		}
		var objStoreLocalData=Ext.getStore('storeLocalData');
		if(objStoreLocalData.getCount()>0){
			cappedClaimStep1ViewObj.down("#cappedClaimStepOneNextButton").show();
		}else{
			cappedClaimStep1ViewObj.down("#cappedClaimStepOneNextButton").hide();
		}
	},

	// called on selection of any expense type

	CappedClaimTypeSelected:function(val){

		Logger.debug("logg-CappedClaimTypeSelected");

		var me = this;

		Shared.pushNewView('CappedClaimFormStepPageId','CappedClaimFormStep');

		cappedClaimFormStepPageObj=this.getCappedClaimFormStepPageView();

		var selectedClaimValue = val.split("_"); // get the selected value
 
		var BenefitId = selectedClaimValue[0]; 

		var claimindex =  selectedClaimValue[1];  
		
		cappedClaimFormStepPageObj.down("#cappedClaimStepFormButtonWrapper").show(); // show the navigation buttons here
		cappedClaimFormStepPageObj.down("#cappedClaimFormPageBackBtn").show();
		
 		var claimFormFields =  Ext.getStore('ClaimsStore');

        var claimFormFieldsData =claimFormFields.getAt(0).data.result; // get claim data
		
		var RemainingClaimAmount=claimFormFieldsData.ThresholdClaim.RemainingClaimAmount; // assign the remaining amount

		cappedClaimFormStepPageObj.down("#remainingClaimAmountLabelOnFormPage").setHtml("$ "+RemainingClaimAmount);
        
        if(Ext.isDefined(claimFormFieldsData.ThresholdClaim.ExpenseTypes.ExpenseType[claimindex]))
        {
        	var claimTypeObj=claimFormFieldsData.ThresholdClaim.ExpenseTypes.ExpenseType[claimindex];
        }
        else
        {
        	var claimTypeObj=claimFormFieldsData.ThresholdClaim.ExpenseTypes.ExpenseType;
        }
		
		var claimTypefields = claimTypeObj.ExpenseTypeFields.ExpenseTypeField;
		 
		var allowAttachment=claimTypeObj.AllowAttachment
		
		var expenseTypeId=claimTypeObj.Id;
		 
		ClaimForm.columns=(Ext.os.is.Tablet && allowAttachment==='True')?2:1; // setting the column layout of the form

		var cappedClaimFormFieldsWrapper = cappedClaimFormStepPageObj.down("#cappedClaimFormFieldsWrapper");
			cappedClaimFormFieldsWrapper.removeAll(true,true);
		ClaimForm.initialize(cappedClaimFormFieldsWrapper);

		if(allowAttachment==='True')
			var attachmentFlag = 2;
		else 
			var attachmentFlag = 4;
		var hiddenFields={
							'SubstantiationMethodID':attachmentFlag,
							'ExpenseTypeId':expenseTypeId,
							'BenefitId':BenefitId,
							'claimindex':claimindex
						};		
			  
		ClaimForm.addHiddenField(hiddenFields);	 // adding the hidden fields to form
		ClaimForm.addFields(claimTypefields); // adding fields to the form
		 
		// display the fields for uploading the image

		if(allowAttachment==='True') // check attachment is there
		{
 			ClaimForm.addAttachmentField(); // add attachment field here
		} 

		ClaimForm.load(); //finally load the form
	},
	// save the current expense
	SaveClaimExpense:function(){

		var me=this;
		// Show loader......
		Ext.Viewport.setMasked(Shared.loadingMask);//Check GST not exeed 10% of claim value. 

		cappedClaimFormStepPageObj=this.getCappedClaimFormStepPageView();
		// getting  submitted values

		var frmPostedData =cappedClaimFormStepPageObj.getValues();

		var objStoreLocalData=Ext.getStore('storeLocalData');

		var claimFormFields =  Ext.getStore('ClaimsStore'); 

        var claimFormFieldsData =claimFormFields.getAt(0);
		
		var claimExpenseDetailObj=claimFormFieldsData.data.result.ThresholdClaim.ExpenseTypes.ExpenseType[frmPostedData.claimindex]
		var claimFields=claimExpenseDetailObj.ExpenseTypeFields.ExpenseTypeField;
		var allowAttachment=claimExpenseDetailObj.AllowAttachment;
		if(!ClaimForm.validateClaimFields(claimFields,allowAttachment==='True')) // validate the form fields
		{
			Ext.Viewport.setMasked(false); //  close the loader
			return false;
		}

		if(this.getClaimGST()){
			var GSTcal = Shared.checkGSTNotExceedTenPercent(this.getClaimAmount().getValue(),this.getClaimGST().getValue());
			if(!GSTcal){ 
				Ext.Viewport.setMasked(false); //  close the loader
				return false;
			}
		}
		 
		var row_id=objStoreLocalData.getAllCount()+1;
		
		// create model to save submitted data to store
		var objModelLocalData=Ext.create("SmartSalary.model.Localdata_M",{			 
            'formData':frmPostedData,
            'fileData':Shared.fileData,
            'claimindex':frmPostedData.claimindex,
            'row_id':row_id
		});
 
		objStoreLocalData.add(objModelLocalData);

		Ext.Msg.alert(Messages.AlertMsgMessage,Messages.AlertMsgClaimAdded,function(){
			
			Ext.Viewport.setMasked(false); //  close the loader
	 
			Shared.fileData = new Array(); // reset the data variable  
			 
			Shared.pushNewView('CappedClaimStepTwoPageId','CappedClaimStep2');
			 
		});
	},
	// edit form show of current expense
	editClaimForm:function(val){ 
 
		// Show loader......
		Ext.Viewport.setMasked(Shared.loadingMask);
		Shared.pushNewView('CappedClaimFormStepPageId','CappedClaimFormStep','right');
		cappedClaimFormStepPageObj=this.getCappedClaimFormStepPageView();
		cappedClaimFormStepPageObj.down("#cappedClaimFormPageBackBtn").hide();
		var explArr=val.split("_");
 
		var sortedClaimsArr=Shared.formDataAsPerClaimIndexes('storeLocalData');
 	
 		var dataArr=sortedClaimsArr[explArr[1]][explArr[2]].getData();
		var formData=Array(); 
		var fileData=dataArr.fileData; 
		var _Object=dataArr.formData
		for(var name in _Object){
           formData[name] = _Object[name];
        } 

		var indexClaim=explArr[1];	

		var claimNumber=explArr[2]; 

		var claimStoreObj=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');	

		var claimType = claimStoreObj.ThresholdClaim.ExpenseTypes.ExpenseType[indexClaim];

		var expenseTypeId=claimType.Id; 

		var BenefitId=claimType.BenefitId; // this will be sent to server again
 
		cappedClaimFormStepPageObj.down("#cappedClaimStepFormButtonWrapper").hide(); // hide the navigation buttons here

		var cappedClaimFormFieldsWrapper = cappedClaimFormStepPageObj.down("#cappedClaimFormFieldsWrapper");
			cappedClaimFormFieldsWrapper.removeAll(true,true);

		var claimTypeHeader = 	Ext.create('Ext.Container',{
				cls: 'claimEditHeading font-16',
				items:[{
					xtype:'label',
					html: '<h3>'+claimType['Name']+'</h3>'
				}]
		});

		// Add claim type radio button 
		cappedClaimFormFieldsWrapper.add(claimTypeHeader); 

		ClaimForm.columns=(Ext.os.is.Tablet && claimType.AllowAttachment==='True')?2:1; // setting the column layout of the form

		ClaimForm.initialize(cappedClaimFormFieldsWrapper);

		if(claimType.AllowAttachment==='True')
			var attachmentFlag = 2;
		else 
			var attachmentFlag = 4; 

		var hiddenFields={
							'SubstantiationMethodID':attachmentFlag,
							'ExpenseTypeId':expenseTypeId,
							'BenefitId':BenefitId,
							'claimindex':indexClaim,
							'row_id':dataArr.row_id	
						};		
			  
		ClaimForm.addHiddenField(hiddenFields);	 // adding the hidden fields to form	

		var claimTypefields = claimType.ExpenseTypeFields.ExpenseTypeField;
			 
		ClaimForm.addFields(claimTypefields,formData); // adding fields to the form 
		  
		if(claimType.AllowAttachment==='True')// check attachment is there
		{
 			ClaimForm.addAttachmentField(fileData); // add attachment field here
		} 
		//Add button for the claim type.
		var fieldTypeButton = Ext.create('Ext.Container',{
			items: [{
				xtype: 'button',
				itemId: 'saveContinueButtonPressCapped',
				ui: 'none',
				action: 'saveContinueButtonPressCapped',
				flex:1,
				cls:'claimStepOneSaveAsDraftButton claimSaveButton txt-left font-16 pad-10 margin-10-0',			
				text: 'Save & Continue'
			}]
		});
		//saveContinueButtonPressCapped
		ClaimForm.filesAndButtonsWrapperObj.add(fieldTypeButton);
		
		ClaimForm.load(); //finally load the form 
		
		// Hide loader......
		Ext.Viewport.setMasked(false);
	},
	//update the claim expense 
	updateClaim:function(){
		 
		var me= this;

		Ext.Viewport.setMasked(Shared.loadingMask);

		cappedClaimFormStepPageObj=this.getCappedClaimFormStepPageView();

		var frmPostedData =cappedClaimFormStepPageObj.getValues(); // get the values of the form
		
		Logger.debug(frmPostedData);
		 
		var claimFormFields =  Ext.getStore('ClaimsStore');

        var claimFormFieldsData =claimFormFields.getAt(0);

		var claimExpenseDetailObj=claimFormFieldsData.data.result.ThresholdClaim.ExpenseTypes.ExpenseType[frmPostedData.claimindex]
		
		var claimFields=claimExpenseDetailObj.ExpenseTypeFields.ExpenseTypeField;
		
		var allowAttachment=claimExpenseDetailObj.AllowAttachment;

		if(!ClaimForm.validateClaimFields(claimFields,allowAttachment==='True')) // validate the form fields
		{
			Ext.Viewport.setMasked(false); //  close the loader
			return false;
		}
		if(this.getClaimGST())//Check GST not exeed 10% of claim value. 
		{
			var GSTcal = Shared.checkGSTNotExceedTenPercent(this.getClaimAmount().getValue(),this.getClaimGST().getValue());
			if(!GSTcal){
			 	Ext.Viewport.setMasked(false); //  close the loader
			 	return false;
			}
		}
		// Show loader......
		
		var row_id=cappedClaimFormStepPageObj.down('#row_id').getValue();
 	
		var claimData = Ext.getStore('storeLocalData');
		claimData.filter("row_id",row_id);
		
		claimData.getAt(0).set({'formData':frmPostedData,
	            'fileData':Shared.fileData}); // update the selected row in store
		
 		claimData.clearFilter(); // clear the filters
		
		Shared.fileData=Array(); // Reset the file data array;

		Shared.pushNewView('CappedClaimStepTwoPageId','CappedClaimStep2');
	},
	//delete the claim expense 
	deleteClaimForm:function(val){
		var me=this; 
		Ext.Msg.confirm(Messages.AlertMsgConfirmation, Messages.AlertMsgDeleteThisMsg, function (buttonId){
			if(buttonId=='yes'){
				var explArr=val.split("_");
				var row_id=explArr[1];
				Logger.debug(row_id);
				var claimData = Ext.getStore('storeLocalData');
				claimData.filter("row_id",row_id);
				claimData.removeAt(0);
 				claimData.clearFilter();
				if(claimData.getCount()>0){
					me.taxFreeCappedClaimStepTwoViewLoad();
				}else{
					Shared.pushNewView('CappedClaimStepOnePageId','CappedClaimStep1','right');			
				}	
			}
		});
	},
	// Called on next button of summary page and to move on confirmation page
	moveToConfirmationPage:function (){

		Shared.pushNewView('CappedClaimConfirmPageId','CappedClaimConfirm');
		
		cappedClaimConfirmPageViewObj=this.getCappedClaimConfirmPageView();

		var ClaimsResult =Ext.getStore("ClaimsStore").getData().getAt(0).get('result');
		
		var nameOnAc = cappedClaimConfirmPageViewObj.down('#cappedClaimNameOnAc');

		nameOnAc.setHtml(ClaimsResult.BankAccount.AccountName);
		
		var BSB = cappedClaimConfirmPageViewObj.down('#cappedClaimBSB'); 
		BSB.setHtml(ClaimsResult.BankAccount.BSB);
		
		var acNo = cappedClaimConfirmPageViewObj.down('#cappedClaimAcNo'); 
		acNo.setHtml(ClaimsResult.BankAccount.AccountNumber);
		
		var declarationtxt = cappedClaimConfirmPageViewObj.down('#cappedClaimdeclarationtxt'); 
		declarationtxt.setHtml(ClaimsResult.DeclarationPage);

		// get user personal detail from user store

		var userStoreObj = Ext.getStore('User_S').getAt(0).data; //  

		var user_email = cappedClaimConfirmPageViewObj.down('#user_email'); 
		user_email.setHtml(userStoreObj.Email);

		var user_contact = cappedClaimConfirmPageViewObj.down('#user_contact'); 
		user_contact.setHtml(userStoreObj.WorkPhone+" (W)");
	},
	// on show of capped claim summary page 
	taxFreeCappedClaimStepTwoViewLoad : function(){
		/*Show Loader*/
 		
 		Ext.Viewport.setMasked(Shared.loadingMask);

		var me=this;
		taxFreeCappedClaimStepTwoPageObj=this.getTaxFreeCappedClaimStepTwoPageId();

		var claimData = Ext.getStore('storeLocalData');		
		Logger.debug(claimData);	
		 
		var claimFormFields =  Ext.getStore('ClaimsStore');
        var claimFormFieldsData = claimFormFields.getAt(0);
		var ClaimsResult =claimFormFieldsData.data.result; 

		var totalClaimAmountStepTwo = taxFreeCappedClaimStepTwoPageObj.down('#totalClaimAmountStepTwo'); 

		totalClaimAmountStepTwo.setHtml('Total value of this claim ('+claimData.getData().length+' item)');
		
		var claimTypefields = claimFormFieldsData.data.result.ThresholdClaim.ExpenseTypes;

		var grandTotal=ClaimForm.itemListing(me,"cappedClaimItemsWrapper",claimTypefields,"capped");
		
		var totalClaimAmountValueStepTwo = taxFreeCappedClaimStepTwoPageObj.down('#totalClaimAmountValueStepTwo');
		totalClaimAmountValueStepTwo.setHtml("$"+grandTotal);
		
		var amountLeftCappedClaimValue = taxFreeCappedClaimStepTwoPageObj.down('#amountLeftCappedClaimValue');
		amountLeftCappedClaimValue.setHtml("$"+ClaimsResult.ThresholdClaim.RemainingClaimAmount);
		
		Ext.Viewport.setMasked(false);
	},
	// Final submission of the claims
	submitClaim:function(){
		Logger.info("submitClaim of controller");
		var usersStore = Ext.getStore('User_S');
        var aRecord =usersStore.getAt(0);
        var PackageId=aRecord.get('PackageId');
        var submitUrl=Config.ApiBaseUrl+'api/submit_cappedclaim/'+PackageId+"/2";
        ClaimForm.submitClaim(this,submitUrl,'capped');
         
	},
	// Final submission of the claims
	submitClaimOld:function(){ 

		if(!this.getCappedClaimConfirmPageView().down("#declarationCheckbox").isChecked())
		{
			Logger.alert(Messages.AlertMsg, Messages.AlertMsgDeclarationAcceptMsg);
			return false;
		}

		var claimFinalData= Array();
// get locally stored data of all the claims
		var objStoreLocalData=Ext.getStore("storeLocalData");  

		var i=0;

		var formData = new FormData();

		objStoreLocalData.data.each(function(record, index, totalItems ){
		     
		    Ext.each(record.get('fileData'), function(value,key){
		    	 
				formData.append(i++,value); // appending all the files to the formData
			});	

		    claimFinalData.push(record.get('formData'));
		}); 

		formData.append(i,Ext.encode(claimFinalData)); 	// Appending claim data to the formData
 
		var usersStore = Ext.getStore('User_S');
        var aRecord =usersStore.getAt(0);
        var PackageId=aRecord.get('PackageId');
		var me=this;
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;
		
		var progressIndicator = Ext.create("Ext.ProgressIndicator",{
			loadingText: Messages.LoadingTextMsg,
			showAnimation : 'fadeIn',
			cls:'width-90'
		});
		
		Ext.Ajax.request({ 									  
			url: Config.ApiBaseUrl+'api/submit_cappedclaim/'+PackageId+"/2",
			headers:{"Authorization" :"Basic " + Shared.base64variable},
			data: formData,
			timeout:Config.serverRequsetTimeoutTime,
			progress: progressIndicator,
			type:'POST',			
			success: function(result){
				data = result.responseText;
				try {
					var Response = Ext.JSON.decode(data);
				} catch (e) {
					if(data.length > 80){
						data = data.slice(0,80);
						data = data+"...";
					}
					Ext.Msg.show({ 
						 title   : Messages.AlertMsgMessage, 
						 message : data, 
						 buttons : [{ 
						 	itemId : 'Retry',
							ui     : 'action',  
							text   : 'Retry',   
						},{   
							itemId : 'cancel',   
							text   : 'Cancel'    
					}], 
					confirm  : {},   
					fn: function(btn) {
						if (btn == 'Retry')
						me.submitClaim();
					}  
				});
				return;
			}				
			if ( typeof(Response.ClaimId) == "undefined" || Response.ClaimId == '')
			{
				if(Response.Description.length > 80){
					Response.Description = Response.Description.slice(0,80);
					Response.Description = Response.Description+"...";
				}
				Ext.Msg.show({ 
					 title   : Messages.AlertMsgMessage, 
					 message     : Response.Description, 
					 buttons : [{ 
					 	itemId : 'Retry',
						ui     : 'action',  
						text   : 'Retry'   
					},{   
						itemId : 'cancel',   
						text   : 'Cancel'    
					}], 
					confirm  : {},   
					fn: function(btn) {
						if (btn == 'Retry')
						me.submitClaim();
					}  
				});
			}
			else{
				me.getCappedClaimConfirmPageView().down("#declarationCheckbox").uncheck();
				Shared.pushNewView('cappedClaimStepThreePageId','CappedClaimStep3');
				me.taxFreeCappedClaimStepThreeViewLoad();
				claimrefno = me.getCappedClaimStepThreePageView().down('#cappedClaimRefNo');
				claimrefno.setHtml(Messages.yourclaimRefNoMsg+Response.ClaimId);
				var claimData = Ext.getStore('storeLocalData');
				claimData.removeAll();
 			}
			return;
		},
		failure: function (response, opts) {
        	Ext.Msg.show({ 
				title   : Messages.AlertMsgMessage, 
				message     : Messages.AlertMsgServerSideFailureStatusCode + response.status, 
				buttons : [{ 
				  itemId : 'Retry',
				  ui     : 'action',  
				  text   : 'Retry'   
				},{   
				  itemId : 'cancel',   
				  text   : 'Cancel'    
				}], 
				confirm  : {},   
				fn: function(btn) {
					if (btn == 'Retry')
						me.submitClaim();
				}  
			});
		}
		});
	},
	// page after submission show event
	taxFreeCappedClaimStepThreeViewLoad:function(){
		
		var usersStore = Ext.getStore('User_S').getAt(0);
        var getCappedClaimStepThreePageObj=me.getCappedClaimStepThreePageView();
		var FirstName=usersStore.get('FirstName'); 
        var firstNameLabel = getCappedClaimStepThreePageObj.down('#cappedClaimFirstName');
		firstNameLabel.setHtml(FirstName);
				
		var EmployerName=usersStore.get('EmployerName');
		var employerNameLabel = getCappedClaimStepThreePageObj.down('#cappedClaimEmployerName');
		employerNameLabel.setHtml(EmployerName);
		
		var sortedClaimsArr=Shared.formDataAsPerClaimIndexes();
		 
		var grandTotal=0;
		
		if(sortedClaimsArr.length){	

			var claimDetailWrapperObj = getCappedClaimStepThreePageObj.down('#cappedClaimDetailedSummeryWrapper');
			claimDetailWrapperObj.removeAll(true,true);
			var totalamt=0;
			Ext.each(sortedClaimsArr,function (claim,indexClaim){
				
				if(Ext.isDefined(claim)){ 
					 
					for(var i=0;i<claim.length;i++){

						var record = claim[i]; //Get the record        
						var data = record.getData(); //Get the data from the record 
						Logger.debug(data); 
						if(!data.formData.hasOwnProperty('ExpenseProvider')) 
						{
							return false;
						}
						totalamt+=parseFloat(data.formData.Amount);

						var ClaimDetailCnt = Ext.create('Ext.Container',{
							cls :'pad-5-10 bgwhite', 
							items: [{
								xtype: 'label',
								html: 'Date Paid: '+data.formData.ExpenseDate,
								cls: 'font-12 pad-left-5 font-vlightgrey margin-10-0'
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Card Provider',
									flex:3
								},{
									xtype: 'label',
									html: data.formData.ExpenseProvider,
									cls:'pad-right-5 txt-right f-bold',
									flex:3
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Total Amount Claimed:',
									flex:3
								},{
									xtype: 'label',
									html: '$'+parseFloat(data.formData.Amount).toFixed(2),
									cls:'pad-right-5 txt-right f-bold',
									flex:1
								}]
							}]
						});
						if(data.fileData.length)
						{
							var documentUploaded = Ext.create('Ext.Container', {
													layout:'hbox',
													cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
													items:[{
														xtype: 'label',
														html: 'Documentation:',
														flex:3
													},{
														xtype: 'label',
														html: 'Uploaded',
														cls:'pad-right-5 txt-right f-bold',
														flex:1
													}]
												});
							 
							ClaimDetailCnt.add(documentUploaded);
						}
						
						claimDetailWrapperObj.add(ClaimDetailCnt);
					}
				}
			});
			var changeAccountBtn = Ext.create('Ext.Container',{
								cls :'bgwhite font-14 color-2B2F3B clearfix',
								items:[{
									xtype: 'button',
									ui: 'none',
									cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 color-5A5B77',
									text: 'Change account'
								}]
			});
			claimDetailWrapperObj.add(changeAccountBtn);				
			totalamt=parseFloat(totalamt).toFixed(2);
			getCappedClaimStepThreePageObj.down('#cappedClaimTotalAmountThirdPage').setHtml('$'+totalamt);
		}
	}

});