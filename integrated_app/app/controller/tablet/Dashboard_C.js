Ext.define('SmartSalary.controller.tablet.Dashboard_C', {
    extend: 'Ext.app.Controller',
	config: {
		isVehicleListStoreLoaded:true,
		isVehicleLeaseStoreLoaded:null,
		isMenuOptionStoreLoaded:null,
		isClaimsStoreLoaded:true,
		availableDashboardExpenses:null,
		lastSelectedDashboardTransaction: null,
		currentVehicleIdSelectedDashboard: null,  
		refs: {
		// Define view element refrence.
            DashboardPageId : 'container[itemId = dashboardView]',
            ContentDetailPageView :'container[itemId = contentDetailPageId]',
			ExpressOnlineClaims : 'button[action = showVehicalClaimList]',
			VehicleExpenseClaim : 'button[action = vExpenseClaimPress]',
			MealClaimButton : 'button[action = MealClaimPress]',
			TrackMyClaim : 'button[action = trackMyClaimPress]',
			CappedClaimBtn : 'button[action = cappedClaimPress]',
			VehicleLeasingListing : 'button[action = vehicleLeasingButtonPress]',
		/* Activity summery button */	
			viewFullStatementActivity : 'button[itemId = viewFullStatementActivity]',
		/* Novated lease view button */	
			viewNovatedLeasingViewButton : 'button[itemId = viewNovatedLeasing]',
		/* Manage My Threshold Cap button */	
			manageMyThresholdCapButton : 'button[itemId = manageMyThresholdCapButton]',
		/* Superannuation button */	
			superannuationButton : 'button[itemId = superannuationButton]',
		/* living Expense Card button */	
			livingExpenseCardButton : 'button[itemId = livingExpenseCardButton]',
		/* Meals and Entertainment Card button */	
			mealsAndEntertainmentCardButton : 'button[itemId = mealsAndEntertainmentCardButton]',
		/* Dashboard Transactions view */	
			dashboardTransactionsShow:'container[itemId=dashboardTransactionsPageId]',
		/* Dashboard Transactions view back button */		
			backDasboardTransactionPress : 'button[action = backDasboardTransactionPress]',	
		},
        control: {
        // Funtion call on button tap.    
			DashboardPageId: {
				show : 'dashboardShowMethod'
			},
			ExpressOnlineClaims:{
				tap:'showVehicleClaimList'
			},
			VehicleExpenseClaim:{
				tap:'showVehicleRegistrationListView',
			},
			TrackMyClaim:{
				tap:'showTrackMyClaimView'
			},
			MealClaimButton:{
				tap:'showMealClaimView'
			},
			CappedClaimBtn:{
				tap:'showCappedClaimView'	
			},
			VehicleLeasingListing:{
				tap:'showVehicleLisingOptions'
			},
		/* Activity summery button */	
			viewFullStatementActivity : {
				tap:'pushDashboardTransactionsScreenWithActivitySummery'	
			},
		/* Novated lease view button */	
			viewNovatedLeasingViewButton : {
				tap:'pushVehicleOverViewScreen'
			},
		/* Manage My Threshold Cap button */	
			manageMyThresholdCapButton : {
				tap:'pushDashboardTransactionsScreenWithMyThresholdCap'	
			},
		/* Superannuation button */	
			superannuationButton : {
				tap:'pushSuperannuationHomeScreen'
			},
		/* living Expense Card button */	
			livingExpenseCardButton : {
				tap:'pushDashboardTransactionsScreenWithLivingExpenseCard'	
			},
		/* Meals and Entertainment Card button */	
			mealsAndEntertainmentCardButton : {
				tap:'pushDashboardTransactionsScreenWithMealsAndEntertainmentCard'
			},
		/* Dashboard Transactions view */	
			dashboardTransactionsShow:{
				show:'dashboardTransactionsShow'
			},
		/* Dashboard Transactions view back button */		
			backDasboardTransactionPress:{
				tap : 'backDasboardTransactionPress'
			}
		}
	},
	pushSuperannuationHomeScreen:function(){
		Shared.pushNewView('homeSuperannuation','homeSuperannuation','left');
	},
	pushVehicleOverViewScreen:function(){
		Shared.pushNewView('vehiclesExpenditureOverviewPageId','vehiclesExpenditureOverview','left');
	},
	pushDashboardTransactionsScreenWithActivitySummery:function(_this){
		this.pushDashboardTransactionsScreenWithSelectedWidgets(_this.getData().id);
	},
	pushDashboardTransactionsScreenWithMyThresholdCap:function(_this){
		this.pushDashboardTransactionsScreenWithSelectedWidgets(_this.getData().id);
	},
	pushDashboardTransactionsScreenWithSuperannuation:function(_this){
		this.pushDashboardTransactionsScreenWithSelectedWidgets(_this.getData().id);
	},
	pushDashboardTransactionsScreenWithLivingExpenseCard:function(_this){
		this.pushDashboardTransactionsScreenWithSelectedWidgets(_this.getData().id);
	},
	pushDashboardTransactionsScreenWithMealsAndEntertainmentCard:function(_this){
		this.pushDashboardTransactionsScreenWithSelectedWidgets(_this.getData().id);
	},
	pushDashboardTransactionsScreenWithSelectedWidgets:function(id){
		this.lastSelectedDashboardTransaction = id; 
		Shared.pushNewView('dashboardTransactionsPageId','dashboardTransactions','left');
	},
	dashboardTransactionsShow:function(){
		var dashboardExpenditureList = this.getDashboardTransactionsShow().down('#dashboardExpenditureTypes');
		dashboardExpenditureList.setOptions(null);
		dashboardExpenditureList.setOptions(this.availableDashboardExpenses); 
		if(this.lastSelectedDashboardTransaction != null)
			dashboardExpenditureList.setValue(this.lastSelectedDashboardTransaction);
		Shared.addCalendar('dashboardTransactionsFromCalendar','From','innerDashboardTransactionsFromCalendar',"d_0",null);
		Shared.addCalendar('dashBoardTransactionsToCalendar','To','innerDashboardTransactionsToCalendar',"d_1",null);
	},
	backDasboardTransactionPress:function(){
		Shared.pushNewView('dashboardscreen','dashboardView','left');
	},
	initializeAppSetup:function(){
		if(Ext.isEmpty(authheader)){
			window.location.href="../";
		} 
		Ext.Viewport.setMasked(Shared.loadingMask);
		
		var me =this; 

        var employeeInfo = userData;

		//Get store obj in usersStoreObj variable. 
		var userStoreObj = Ext.getStore('User_S'); //          
		var anotherUser =  {		
			AccountID: employeeInfo.account.id,
			FirstName: employeeInfo.account.firstName,
			EmployerName: employeeInfo.selectedPackage.employerName,
			EmployerCode: employeeInfo.selectedPackage.employerCode,
			PackageId: employeeInfo.selectedPackage.packageId,
			SelectedPackage:employeeInfo.selectedPackage,
			WorkPhone: employeeInfo.account.workPhone,
			Email: employeeInfo.account.email
 	    };
 
		Shared.packageId=employeeInfo.selectedPackage.packageId;

		// Use the add function to add records in store.
		Logger.debug(anotherUser);
		userStoreObj.add(anotherUser);

 		// COMMENTED DUE TO UNAVAILABLITY OF API ON DEVWS SERVER 27/11/2014
		/*Logger.debug("VehicleRegistrationList loading started");
		var VRegistrationListStoreURL = Config.ApiBaseUrl+'api/vehicle_list/'+Shared.packageId;
		//Set Authorization heade for api request.  
		Ext.getStore('VehicleRegistrationList').setProxy({headers:{
			"Authorization":"Basic " + Shared.base64variable
		}});
		Ext.getStore('VehicleRegistrationList').load({url:VRegistrationListStoreURL,callback:function(records, operation, success){ 
			if(success){
				Logger.debug("VehicleRegistrationList loaded");
				me.setIsVehicleListStoreLoaded(true);
			}else{
				Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
			}
			if(me.getIsVehicleLeaseStoreLoaded() && me.getIsMenuOptionStoreLoaded() && me.getIsClaimsStoreLoaded())
			{
				Ext.Viewport.setMasked(false);
			} 
			 
		}});
		
		Logger.debug("ClaimsStore loading started");
		Ext.getStore('ClaimsStore').setProxy({headers:{
			"Authorization":"Basic " + Shared.base64variable
		}});			 	  
		Ext.getStore('ClaimsStore').load({url:Config.ApiBaseUrl+'api/claims_synopsys/'+Shared.packageId,callback: function(records, operation, success) 	{	
			if(success){
				Logger.debug("ClaimsStore loaded");
				me.setIsClaimsStoreLoaded(true);  
			}else{
				Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
			}
			if(me.getIsVehicleLeaseStoreLoaded() && me.getIsMenuOptionStoreLoaded() && me.getIsVehicleListStoreLoaded())
			{
				Ext.Viewport.setMasked(false);
			} 
			 
		}});
		 ************/


		/*********** not needed in desktop view ********************/
		var storeMenu = Ext.getStore('storeSlideList');
		//Load dynamic menu for user.
		var storeMenuURL = Config.ApiBaseUrl+'api/getmenus/'+Shared.packageId;
		storeMenu.setProxy({'url':storeMenuURL,'type':'ajax'});
		storeMenu.load({ 
			url:storeMenuURL,
			callback: function(store, options, success) {
				if(!store.length){
					window.onbeforeunload = null; // unbinding the onbeforeunload event 
        			window.onpagehide = null; // unbinding the onpagehide event 
					window.location.href="../";
				}
				if(success){
					Ext.getCmp('basic').load(); // menu item listing plugin
					var logoutMenu={text:'Logout',menuid:'',leaf:1,submenu:0,uniqueId:-1};
					storeMenu.insert(storeMenu.getCount()+1,logoutMenu);
					Logger.debug("menu store loaded");
					Ext.getCmp('basic').updateStore(storeMenu); 
					me.setIsMenuOptionStoreLoaded(true);
				}else{
					Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
				}

				if(me.getIsVehicleLeaseStoreLoaded() && me.getIsClaimsStoreLoaded() && me.getIsVehicleListStoreLoaded())
				{
					Ext.Viewport.setMasked(false);
				} 
 			}
		});
		/***************************/
 		
		// leasing vehicle Detail store start. 
		Logger.debug("VehiclesStore loading started");
		var VehiclesStoreStoreURL = Config.ApiBaseUrl+'api/get_leased_vehicle_detail/'+Shared.packageId;
		//Set Authorization heade for api request.  
		Ext.getStore('VehiclesStore').setProxy({headers:{
			"Authorization":"Bearer " + Shared.getAccessToken(),
			"accept": "application/json; charset=utf-8"
		}});
		Ext.getStore('VehiclesStore').load({url:VehiclesStoreStoreURL,callback:function(records, operation, success){ 
			if(success){
				Logger.debug("VehiclesStore loaded");
				me.setIsVehicleLeaseStoreLoaded(true)
				me.populateVehicleDropDown();
			}else{
				Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
			}
			if(me.getIsMenuOptionStoreLoaded() && me.getIsClaimsStoreLoaded() && me.getIsVehicleListStoreLoaded())
			{
				Ext.Viewport.setMasked(false);
			}
			 
		}}); 
 	
	},
	onSignOffCommand:function(){
		  
		//Remove data from store when user logoff.
		var userStoreObj = Ext.getStore('User_S'); 
		if(userStoreObj.getCount() > 0) userStoreObj.removeAt(0);
		
		//Remove data from store when user logoff.
		var employerListObj = Ext.getStore('EmployerList');  
        if(employerListObj.getCount() > 0) employerListObj.removeAll();
  
        Shared.base64variable=''; // clearing the credentials
        window.onbeforeunload = null; // unbinding the onbeforeunload event 
        window.onpagehide = null; // unbinding the onpagehide event 

        window.location.href="../user/logout";

	},
	//showEmployeeDetail function start without parameter.
	dashboardShowMethod: function() { 

		Ext.Viewport.setMasked(Shared.loadingMask);
		var me=this;
		var dashboardViewObj=this.getDashboardPageId();// get view object
		dashboardViewObj.down("#novatedCarLeaseGraph").setHtml('');
		dashboardViewObj.down("#thresholdGraph").setHtml('');

		//****************** dashboard store load and display *************//
		// DashboardStore store start. 
		
		Logger.debug("DashboardStore loading started");
		var DashboardStoreURL = Config.RestApiBaseUrl+'packages/'+Shared.packageId+'/dashboard';
		//Set Authorization heade for api request.  
		Ext.getStore('DashboardStore').setProxy({headers:{
			"Authorization":"Bearer " + Shared.getAccessToken(),
			"accept": "application/json; charset=utf-8"
		}});
		Ext.getStore('DashboardStore').load({url:DashboardStoreURL,callback:function(records, operation, success){ 
			if(success){
				Logger.debug("DashboardStore loaded");
				me.manageWidgets(); 
			}else{
				Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
			} 
			Ext.Viewport.setMasked(false);
		}});

		//Ext.getCmp("vehicleLeasing").hide();
		
Logger.info("dashboardShowMethod function");

		// Laod employee detail from store.
		var usersStore = Ext.getStore('User_S').getAt(0);
        var AccountID=usersStore.get('AccountID');
        var FirstName=usersStore.get('FirstName'); 
        var EmployerName=usersStore.get('EmployerName');
		var EmployerCode=usersStore.get('EmployerCode'); 

		Shared.employerCode = EmployerCode;
		//Load the Express online claims options as per the users permissions
		/* ------------- Commented old desktop references ----------------*/
		/*	me.getVehicleExpenseClaim().hide();
			me.getCappedClaimBtn().hide();
			me.getMealClaimButton().hide();
			if(usersStore.get('SelectedPackage').EmployerAllowsTaxFreeCap=='True') // check if capped claim is allowed to user
			{

				
Logger.info("Capped-Claim::"+usersStore.get('SelectedPackage').EmployerAllowsTaxFreeCap);	
				me.getCappedClaimBtn().show();
			}
			// check if Vehicle claim is allowed to user
			if(usersStore.get('SelectedPackage').EmployerAllowsVehicleLeasing=='True'){			 
				 me.getVehicleExpenseClaim().show();
				 
Logger.info("Vehicle-Claim::"+usersStore.get('SelectedPackage').EmployerAllowsVehicleLeasing);
			}

			if(usersStore.get('SelectedPackage').EmployerAllowsMECard=='True') // check if Meals claim is allowed to user
			{			 
				me.getMealClaimButton().show();
				
Logger.info("Meal-Claim::"+usersStore.get('SelectedPackage').EmployerAllowsMECard);

			}
		*/
		//end
		// Get component obj to add employee information.
		var userDetailPanel = dashboardViewObj.down('#userdetail');
		// Add employee information to dashboard view.
		userDetailPanel.down('#wcuser').setHtml('Welcome, ' + FirstName);
		userDetailPanel.down('#acno').setHtml('Account, ' + AccountID);
		userDetailPanel.down('#employername').setHtml('Employer : ' + EmployerName);
		// clearing the store data if it is has existing data
		var claimData = Ext.getStore('storeLocalData');
 		claimData.removeAll(); 
 		var vehicleClaimList = dashboardViewObj.down('#vehicleClaim');
		/*	if(!(vehicleClaimList.isHidden()))
				vehicleClaimList.hide();
			this.getApplication().getController('MealClaim_C').PaymentPeriods='';
		*/
		
Logger.info("dashboardShowMethod function ended");

	},
	manageWidgets:function(){

		var me=this;
		var dashboardViewObj=this.getDashboardPageId();// get view object

		var widgets=Ext.getStore("DashboardStore").getAt(0).get("widgets");
		var dashboardExpensesType = new Array(); 

		if(dashboardViewObj.down("#selectVehicle")!=null){
			dashboardViewObj.down("#selectVehicle").setValue(0);
		} 

		Ext.each(widgets,function(widgetObj){
			if(widgetObj.id==1)// Activity Summary
			{
				dashboardExpensesType.push({"text" : 'All',"value" : widgetObj.id});
				dashboardViewObj.down("#viewFullStatementActivity").setData({id:widgetObj.id});

				dashboardViewObj.down("#activitySummaryWidget").show();
				dashboardViewObj.down("#activitySummary").setHtml(widgetObj.title);

				dashboardViewObj.down("#activitySummaryFields").removeAll(true,true); 
				Ext.each(widgetObj.fields,function(widgetFields){	
					var lblField=Ext.create('Ext.Label',{
						html:widgetFields.name,
						cls:'pad-top-10'	
					});
					var valField=Ext.create('Ext.Label',{
						html:widgetFields.value,
						cls:'f-bold font-22 pad-bottom-10'
					});
					dashboardViewObj.down("#activitySummaryFields").add(lblField);
					dashboardViewObj.down("#activitySummaryFields").add(valField);
				});
			}
			else if(widgetObj.id==2)//Novated Leasing Overview 
			{
				dashboardViewObj.down("#novatedleasingWidget").show();
				me.showNovatedLeaseWidget(widgetObj); 
				
			}else if(widgetObj.id==3)//Threshold Cap 
			{
				dashboardExpensesType.push({"text" : widgetObj.title,"value" : widgetObj.id});
				dashboardViewObj.down("#manageMyThresholdCapButton").setData({id:widgetObj.id});
				
				dashboardViewObj.down("#thresholdCapWidget").show();
				dashboardViewObj.down("#titleTH").setHtml(widgetObj.title);
				var nameTHE=widgetObj.fields[0].name;
				var valueTHE=widgetObj.fields[0].value;

				dashboardViewObj.down("#entitlementId").setHtml('<span class="f-bold">'+nameTHE+' :</span> '+valueTHE);				
				
				var nameSP=widgetObj.fields[1].name;
				var valueSP=widgetObj.fields[1].value;
				dashboardViewObj.down("#thresholdCapSpent").setHtml('<span class="f-bold">'+nameSP+' :</span><br/>('+valueSP+')');				
				
				var nameCR=widgetObj.fields[2].name;
				var valueCR=widgetObj.fields[2].value;
				dashboardViewObj.down("#thresholdCapRemaining").setHtml('<span class="f-bold">'+nameCR+' :</span><br/>('+valueCR+')');												

				var valueLCSpendable  = valueSP.replace(/\$/g , "");	
            	var valueBBudget  = valueTHE.replace(/\$/g , "");
            	var valueBBudget  = valueBBudget.replace(/\(per FBT\)/g , "");
            		
		 		
				Logger.info(valueLCSpendable+"::"+valueBBudget);
		 		if(parseInt(valueBBudget)>=parseInt(valueLCSpendable)){
    				var percentTotal='100';
    				var percentRun=parseInt((valueLCSpendable/valueBBudget)*100)
    			}else{
    				var percentRun='100';
    				var percentTotal=parseInt((valueBBudget/valueLCSpendable)*100)
    			}

            	var htmlStr='<div class="carCompoWrapper"><div class="meter-vehicle" id="animateId_TH">'
								+'<span class="outer" style="width:100%"></span>'
								+'<span class="lightblue" style="width: '+percentTotal+'%;display:none;"></span>'
								+'<span class="inner-bar" id="meter-vehicle-span-inner" style="width: '+percentRun+'%;display:none;"></span>'
							+'</div></div>';
				dashboardViewObj.down("#thresholdGraph").setHtml(htmlStr);
				AnimateBarGraph("animateId_TH","lightblue");
			}
			else if(widgetObj.id==4)//Superannuation 
			{
				dashboardExpensesType.push({"text" : widgetObj.title,"value" : widgetObj.id});
				dashboardViewObj.down("#superannuationButton").setData({id:widgetObj.id});

				dashboardViewObj.down("#superannuationWidget").show();
				dashboardViewObj.down("#SuperannuationTitle").setHtml(widgetObj.title);

				var nameSYTD=widgetObj.fields[0].name;
				var valueSYTD=widgetObj.fields[0].value;

				dashboardViewObj.down("#FinancialYearToDate").setHtml('<span class="f-bold">'+nameSYTD+" : "+'</span>' +valueSYTD);
							
				
				var nameSNC=widgetObj.fields[1].name;
				var valueSNC=widgetObj.fields[1].value;
				dashboardViewObj.down("#nextContribution").setHtml('<span class="f-bold">'+nameSNC+" : "+'</span>' + valueSNC);				
				
			}
			else if(widgetObj.id==5)//Living Expense Card
			{
				dashboardExpensesType.push({"text" : widgetObj.title,"value" : widgetObj.id});
				dashboardViewObj.down("#livingExpenseCardButton").setData({id:widgetObj.id});

				dashboardViewObj.down("#livingExpenseWidget").show();
				dashboardViewObj.down("#titleLE").setHtml(widgetObj.title);
				
				var nameLElbl=widgetObj.fields[0].name;
				var valueLEvl=widgetObj.fields[0].value;

				dashboardViewObj.down("#lblLE").setHtml('<span class="f-bold">'+nameLElbl+' : '+'</span>'+valueLEvl);				
			}
			else if(widgetObj.id==6)//Meals and Entertainment Card
			{
				dashboardExpensesType.push({"text" : widgetObj.title,"value" : widgetObj.id});
				dashboardViewObj.down("#mealsAndEntertainmentCardButton").setData({id:widgetObj.id});
				
				dashboardViewObj.down("#MEwidget").show();
				dashboardViewObj.down("#titleME").setHtml(widgetObj.title);
				
				var nameMElbl=widgetObj.fields[0].name;
				var valueMEvl=widgetObj.fields[0].value;

				dashboardViewObj.down("#lblme").setHtml('<span class="f-bold">'+nameMElbl+' : '+'</span>'+valueMEvl);				
				
			}
			me.availableDashboardExpenses = null;
			me.availableDashboardExpenses = dashboardExpensesType;
		})
	},
	populateVehicleDropDown: function (){
		var me=this;
		var dashboardViewObj=this.getDashboardPageId();// get view object	

		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		var ArrOption= Array(); 
		Ext.each(VehiclesStore, function(vehicleItem,i){ 
			ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':i}); 
		});
		dashboardViewObj.down("#selectVContainer").removeAll(true,true);
		var selectfield = Ext.create('Ext.field.Select',{  
			cls:'expenditureType_1 vlabel',
			usePicker:false,
			itemId:'selectVehicle', 
			label: 'Vehicle :',
			options:ArrOption,
			listeners:{
				change:function( this_, newValue, oldValue, eOpts ){
					me.getNovatedLeaseWidgetByVehicleId(VehiclesStore[newValue].vehicle.id);
					me.currentVehicleIdSelectedDashboard = newValue;
				}
			}  
		});
		dashboardViewObj.down("#selectVContainer").add(selectfield);
	},
	getNovatedLeaseWidgetByVehicleId:function (vehicleId){
		var me=this;
		Ext.Viewport.setMasked(Shared.loadingMask);
		var novatedLeaseWidgetURL = Config.RestApiBaseUrl+'packages/'+Shared.packageId+'/vehicles/'+vehicleId+'/widget';
		 
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
            url: novatedLeaseWidgetURL,
            method: 'GET',
            headers :{
				"Authorization":"Bearer " + Shared.getAccessToken(),
				"accept": "application/json; charset=utf-8"
			},
            success: function (response) {
            	Ext.Viewport.setMasked(false); 
            	 
				var responseData = Ext.JSON.decode(response.responseText);
				me.showNovatedLeaseWidget(responseData); 
				// Hide loader......
				Ext.Viewport.setMasked(false); 
			},
				// If Api return failed show error message.
            failure: function (response) {
				// Hide loader......
				Ext.Viewport.setMasked(false);
            	Logger.alert(Messages.AlertMsg,Messages.AlertMsgServerNotRersponding);
            }
        });
	},
	showNovatedLeaseWidget:function (widgetObj){
		var me=this;
		var dashboardViewObj=this.getDashboardPageId();// get view object		
		dashboardViewObj.down("#NLTitle").setHtml(widgetObj.title);
		dashboardViewObj.down("#novatedCarLeaseGraph").setHtml('');
		var nameL=widgetObj.fields[0].name;
		var valueL=widgetObj.fields[0].value;

		dashboardViewObj.down("#ntCurrentSpent").setHtml('<span class="f-bold">'+nameL+' :</span><br/>('+valueL+')');				
		
		var nameB=widgetObj.fields[1].name;
		var valueB=widgetObj.fields[1].value;

		dashboardViewObj.down("#ntBudget").setHtml('<span class="f-bold">'+nameB+' :</span><br/>('+valueB+')');				
		
		var nameO=widgetObj.fields[2].name;
		var valueO=widgetObj.fields[2].value;

		dashboardViewObj.down("#OdometerVL").setHtml('<span class="f-bold">'+nameO+' :</span>  '+valueO);												
		
		var valueLCSpendable  = valueL.replace(/\$/g , "");	
    	var valueBBudget  = valueB.replace(/\$/g , "");	
 		
		Logger.info(valueLCSpendable+"::"+valueBBudget);
 		if(parseInt(valueBBudget)>=parseInt(valueLCSpendable))
		{	var percentTotal='100';
			var percentRun=parseInt((valueLCSpendable/valueBBudget)*100)
		}else{
			var percentRun='100';
			var percentTotal=parseInt((valueBBudget/valueLCSpendable)*100)
		}

    	var htmlStr='<div class="carCompoWrapper"><div class="meter-vehicle" id="animateId_NVL">'
						+'<span class="outer" style="width:100%"></span>'
						+'<span class="darkgreen" style="width: '+percentTotal+'%;display:none;"></span>'
						+'<span class="inner-bar" id="meter-vehicle-span-inner" style="width: '+percentRun+'%;display:none;"></span>'
					+'</div></div>';
		dashboardViewObj.down("#novatedCarLeaseGraph").setHtml(htmlStr);
		AnimateBarGraph("animateId_NVL","darkgreen");
	},
	// contentDetailPage start here. with node id and search flag. 
	contentDetailPage: function (id,isFromSearchList) {

		
Logger.info("contentDetailPage function");
	//Show loading mask.
		Ext.Viewport.setMasked(Shared.loadingMask);
		var dataFlag= false;
	//Get this ref in me variable.
		var me = this;
	//Load user detail from store. 
		var usersStore = Ext.getStore('User_S');
	//Api endpoint url. 
		var contentDetailApiUrl = Config.ApiBaseUrl+'api/getcontent/'+ id; 
		if(usersStore.getCount()>0){
	// Get employer code from store and send it with api call to get content for this employer.
			var employercode = usersStore.getAt(0);
			var employerCode = employercode.get('EmployerCode');
			var contentDetailApiUrl = Config.ApiBaseUrl+'api/getcontent/'+ id + '/' +employerCode; 
		}
	// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
	// Api call for salary packaging data.
		Ext.Ajax.request({
            url: contentDetailApiUrl,
            method: 'GET',
            success: function (response) {
				var responseData = Ext.JSON.decode(response.responseText);
				if(responseData['success']==1 && responseData['content']){
	//Active content view.
					Shared.pushNewView('contentDetailPageId','contentDetailView');
					var contentDetailPageViewObj=me.getContentDetailPageView();
					//Remove all elements from MyPackagingItem component.       
					contentDetailPageViewObj.down('#MyPackagingItem').removeAll();
					var labelClass='font-white pad-5 font-18';
					if(isFromSearchList)
						labelClass='font-white font-16 pad-5 floatLeft';
	//If title exists add it to page title.
					if(responseData['content']['title']){
						var responseDataTitle = Ext.create('Ext.Label',{
							html:responseData['content']['title'],
							cls: labelClass,
							width:'90%'
						});
						var dataTitleComponent = contentDetailPageViewObj.down('#MyPackagingItem');
						dataTitleComponent.add(responseDataTitle);
						
						if(isFromSearchList){
							var backButton = Ext.create('Ext.Button',{
							itemId:'btnBack',
							html:'Back',
							cls: 'font-white searchlistbackbtn'
						});
						dataTitleComponent.add(backButton);
						}
					}
	//Get container object in variable  
					var dataWrapperComponent = contentDetailPageViewObj.down('#dataWrapper');
	//Remove content from container to add new content. 
					dataWrapperComponent.removeAll();
	//If upper body exists add it to container.
					if(responseData['content']['upper_body']){
						var responseDataUpperbody = Ext.create('Ext.Label',{
							html:responseData['content']['upper_body'],
							cls: 'font-16 pad-5 color-purple'				
						});
						dataWrapperComponent.add(responseDataUpperbody),
						dataFlag = true
					}
	// Add block one by one to the container.
					if(responseData['content']['blocks']){
						for(var i=0;i<responseData['content']['blocks'].length;i++){
							var contentBlockHeading = Ext.create('Ext.Label',{
								html:responseData['content']['blocks'][i]['title'],
								width:'100%',
								cls:'salayfeeheading font-18'			
							});
							dataWrapperComponent.add(contentBlockHeading);
							var contentBlockDetail = Ext.create('Ext.Label',{
								html:responseData['content']['blocks'][i]['content'],
								width:'100%',
								cls:'salaryfeedetail font-14'			
							});
							dataWrapperComponent.add(contentBlockDetail);
						}
						dataFlag = true;
					}
	//If lower body exists add it to container.
					if(responseData['lower_body']){
						var responseDataLowerBody = Ext.create('Ext.Label',{
							html:salaryfeeresponse['lower_body'],
							cls: 'font-16 pad-5 color-purple'				
						});
						dataWrapperComponent.add(responseDataLowerBody);
						dataFlag = true;
					}
					if(!dataFlag){
						var responseDataUpperbody = Ext.create('Ext.Label',{
							html:"Please insert content for this page.",
							cls: 'font-16 pad-5 color-purple'				
						});
						dataWrapperComponent.add(responseDataUpperbody);	
					}
					Ext.Array.each(Ext.select("a").elements, function(name, index, countriesItSelf) {
						if(Ext.select("a").elements[index].getAttribute("rel")) {
							Logger.debug("inside if");
							Ext.select("a").elements[index].setAttribute("href" ,"javascript:void(0);");
						}
					});				
				}else{
	// If record not found show alert message to user.
					Logger.alert(Messages.AlertMsg,Messages.AlertMsgNoRecordFound);
				}
	// Hide loader......
				Ext.Viewport.setMasked(false); 
			},
	// If Api return failed show error message.
            failure: function (response) {
	// Hide loader......
				Ext.Viewport.setMasked(false);
            	Logger.alert(Messages.AlertMsg,Messages.AlertMsgServerNotRersponding);
            }
        });
	},
	// LogOff command execute.
	callSignOff : function(){
		this.getApplication().getController('Login_C').onSignOffCommand();
	},
	//Show emplyer list
	showEmployerList : function(){
		Shared.pushNewView('myEmployerListPageId','employerListView');
	},
	showDashboard : function(){
		
Logger.info("showDashboard function");
		this.dashboardShowMethod();
		Shared.pushNewView('dashboardscreen','dashboardView');
	},
	showVehicleClaimList : function(){ 
		var dashboardViewObj=this.getDashboardPageId();// get view object
		var vehicleClaimList = dashboardViewObj.down('#vehicleClaim');
		if(vehicleClaimList.isHidden())
			vehicleClaimList.show();
		else
			vehicleClaimList.hide();
	},
	showVehicleLisingOptions : function(){
		var dashboardViewObj=this.getDashboardPageId();// get view object
		var vehicleClaimList = dashboardViewObj.down('#vehicleLeasing');
		if(vehicleClaimList.isHidden())
			vehicleClaimList.show();
		else
			vehicleClaimList.hide();
		
		var dashboardscreenHeight = Ext.getCmp("dashboardscreen");
		dashboardscreenHeight.getScrollable().getScroller().scrollTo(0,1200,true);
	},
	showVehicleRegistrationListView:function(){
		this.LoadNextOrVehicleView('VehicleRegistrationList');
	},
	showTrackMyClaimView:function (){
		
Logger.info("showTrackMyClaimView function called")
		//var obj=Ext.create('SmartSalary.view.phone.TrackMyClaims.ClaimList_V');
		//Shared.objMainContainer.push(obj);
		Shared.pushNewView('claimlistscreen','claimList');	  
	},
	showCappedClaimView: function (){
		Shared.pushNewView('CappedClaimStepOnePageId','CappedClaimStep1');
	},
	showMealClaimView:function(){

		var claimsResultData=Ext.getStore("ClaimsStore").getData().getAt(0).get('result');  //get meal claim fields records from the claim store
		
		Shared.GSTRate=claimsResultData.GSTRate; // GST rates from store;
		Logger.debug(claimsResultData.MealClaim.ExpenseTypes);
		if(Ext.isDefined(claimsResultData.MealClaim.ExpenseTypes.ExpenseType[0]))
		{
			
Logger.info("showMealClaimView function");
			Shared.pushNewView('MealClaimExpenseSelectionId','MealClaimExpenseSelectionWidget');	
			 
		}else{
			this.getApplication().getController('MealClaim_C').MealClaimTypeSelected(claimsResultData.MealClaim.ExpenseTypes.ExpenseType.BenefitId+'_0');
		}
	},
	LoadNextOrVehicleView : function(storeId){
		var me =this;
		var VehicleRegistrationListStore = Ext.getStore(storeId);
		if(VehicleRegistrationListStore.getCount()==1){
			var VehicleRegistrationListStoreData = VehicleRegistrationListStore.getAt(0);
			Shared.VehicleID = VehicleRegistrationListStoreData.get('VehicleID');
			Shared.vehicleReg = VehicleRegistrationListStoreData.get('RegoNo');
			me.getApplication().getController('VehicleRegistrationList_C').loadClaimStepOneView();
		}else{
			Shared.pushNewView('vehicleRegistrationListPageId','vehicleRegistrationListView');					
		}
	}
});