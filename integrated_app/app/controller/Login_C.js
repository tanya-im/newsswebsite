Ext.define('SmartSalary.controller.Login_C', {
    extend: 'Ext.app.Controller',
	VehicleRegistrationListStore:false,
	ClaimsStore:false,
    config: {
        refs: {
			// Define view name and fields detail for reference.
			userName: '#userNameTextField',
			password: '#passwordTextField',
            loginView: 'container[itemId=loginScreen]',
            loginButton: 'button[action=loginpress]'
        },
		control: {
            loginButton: {
                // On Tap of 'login' button  onSignIn function execute.
				tap: 'signInUser'
            } 
		}
    },
	// onSignIn method start here.
	signInUser: function () {
		//Assign this to me variable. 
		var me = this;
        var loginView = me.getLoginView();
		
		// Get user name and password from view fields.
		var strUserName = this.getUserName().getValue();
        var strPassword = this.getPassword().getValue();
        
		// Remove space from username and password
		strUserName=strUserName.trim();
		strPassword=strPassword.trim();
		
		// Email pattern match

		// Validate username or password not empty.
		if (strUserName.length === 0 && strPassword.length === 0) {
			me.signInFailure(Messages.AlertMsgUnamePwd);
            return;
        }
		if (strUserName.length === 0) {
			me.signInFailure(Messages.AlertMsgUname);
            return;
        }
		if(!Shared.EmailRegex.test(strUserName)){
			me.signInFailure(Messages.AlertMsgUnameEmail);
            return;
		}
		if (strPassword.length === 0) {
			me.signInFailure(Messages.AlertMsgPwd);
            return;
        }
		
		// Set username and password in shared variable.  
		Shared.userName = strUserName;
		Shared.password = strPassword;
        
		// show loader.
		Ext.Viewport.setMasked(Shared.loadingMask);
		
		// Check dataconnection is online or not if not then return false.
		Logger.info(Ext.device.Connection);
		if(!Shared.checkConnectionIsOnline())
			return false;
		
		Shared.base64variable =  btoa(strUserName+':'+strPassword);	
		// Make Api Call for authentication with username or password.  
		//If login API return success 1. data store in sencha store and show the main view.
		Ext.Ajax.request({
			url: Config.ApiBaseUrl+'api/login',
			 headers:{
				"Authorization" :"Basic " + Shared.base64variable
			},
			success: function (response) {
				var loginResponse = Ext.JSON.decode(response.responseText);
				// If login success execute onSuccessDataSet and signInSuccess  function. Else execute signInFailure function. 	
				if (loginResponse.success ===1) {
					if(loginResponse.result==false){
						me.signInFailure(Messages.AlertMsgSignInFailure);
						Ext.Viewport.setMasked(false);
					}
					me.onSuccessDataSet(loginResponse.result);
				}else {
					me.signInFailure(loginResponse.error.Description);
					Ext.Viewport.setMasked(false);
				}
			},
			// Authentication failed show error message.
			failure: function (response) {
				me.signInFailure(Messages.AlertMsgSignInFailure);
				Ext.Viewport.setMasked(false);
			}
        });
	},
	// On success save user detail in store.
    onSuccessDataSet: function(data){
		var me =this; 
        var employeeInfo = data;
		if(employeeInfo.AccountID){
			//Get store obj in usersStoreObj variable. 
			var userStoreObj = Ext.getStore('User_S'); //          
			var anotherUser = {		AccountID: employeeInfo.AccountID,
									FirstName: employeeInfo.FirstName,
									EmployerName: employeeInfo.SelectedPackage.EmployerName,
									EmployerCode: employeeInfo.SelectedPackage.EmployerCode,
									PackageId: employeeInfo.SelectedPackage.PackageId,
									SelectedPackage:employeeInfo.SelectedPackage,
									WorkPhone: employeeInfo.WorkPhone,
									Email: employeeInfo.Email
						 	  };
			// VehicleRegistrationList store start. 
			Logger.debug("VehicleRegistrationList loading started");
			var VRegistrationListStoreURL = Config.ApiBaseUrl+'api/vehicle_list/'+employeeInfo.SelectedPackage.PackageId;
			//Set Authorization heade for api request.  
			Ext.getStore('VehicleRegistrationList').setProxy({headers:{
				"Authorization":"Basic " + Shared.base64variable
			}});
			Ext.getStore('VehicleRegistrationList').load({url:VRegistrationListStoreURL,callback:function(records, operation, success){ 
				if(success){
					Logger.debug("VehicleRegistrationList loaded");
					me.VehicleRegistrationListStore = true;
				}else{
					Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
					Ext.Viewport.setMasked(false);
				}
				if(me.VehicleRegistrationListStore && me.ClaimsStore)
					Ext.Viewport.setMasked(false);
			}});
			
			Logger.debug("ClaimsStore loading started");
			Ext.getStore('ClaimsStore').setProxy({headers:{
				"Authorization":"Basic " + Shared.base64variable
			}});			 	  
			Ext.getStore('ClaimsStore').load({url:Config.ApiBaseUrl+'api/claims_synopsys/'+employeeInfo.SelectedPackage.PackageId,callback: function(records, operation, success) 	{	
				if(success){
					Logger.debug("ClaimsStore loaded");
					Logger.debug(Ext.getStore('ClaimsStore'));
					me.ClaimsStore = true;
				}else{
					Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
					Ext.Viewport.setMasked(false);
				}
				if(me.VehicleRegistrationListStore && me.ClaimsStore)
					Ext.Viewport.setMasked(false);
			}});
			var storeMenu = Ext.getStore('storeSlideList');
			//Load dynamic menu for user.
			var storeMenuURL = Config.ApiBaseUrl+'api/getmenus/'+employeeInfo.SelectedPackage.EmployerCode;
			storeMenu.load({
				url:storeMenuURL,
				callback: function(store, options, success) {
					if(success){
						Logger.debug("menu store loaded");
						storeMenu.each(function(record, index, animated) {
				 			if(record.get('text')=="Log In")
								record.set('text','Log Out');
						});
					}else{
						Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
						Ext.Viewport.setMasked(false);
					}
     			}
			});
			// Use the add function to add records in store.
			Logger.debug(anotherUser);
			userStoreObj.add(anotherUser);

			var label = this.getLoginView().down('#signInFailedLabel')
    		label.hide();

			Shared.pushNewView('dashboardscreen','dashboardView');
		}else{
			 this.signInFailure(Messages.AlertMsgSignInFailure);
		}
	},
	 
	// Sign in failure show error message.
    signInFailure: function (message) {
		var label = this.getLoginView().down('#signInFailedLabel');
        label.setHtml(message);
		label.show();
    },
	// If user Tap sign off button,show the login screen.
    onSignOffCommand: function () {
		
		Shared.pushNewView('loginScreen','loginView');
        
	//Remove data from store when user logoff.
		var userStoreObj = Ext.getStore('User_S'); 
		if(userStoreObj.getCount() > 0) userStoreObj.removeAt(0);
		
	//Remove data from store when user logoff.
		var employerListObj = Ext.getStore('EmployerList');  
        if(employerListObj.getCount() > 0) employerListObj.removeAll();
	
	// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;
	
	//update menu store.
		var storeMenu = Ext.getStore('storeSlideList');
		var storeMenuURL = Config.ApiBaseUrl+'api/getgenericmenus';
		storeMenu.load({url:storeMenuURL,callback: function(store, options, success) {
			if(success){
				Logger.debug('storeSlideList'+" "+Messages.AlertMsgStoreLoaded);
			}else{
				Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
				Ext.Viewport.setMasked(false);
			}
		}});
		this.getUserName().setValue();
        this.getPassword().setValue();
	}
});