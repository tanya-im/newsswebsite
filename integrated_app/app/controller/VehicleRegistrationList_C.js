Ext.define('SmartSalary.controller.VehicleRegistrationList_C', {
    extend: 'Ext.app.Controller',
    config: {
        refs:{

        	vehicleRunningExpenseClaimStepOnePageView:'container [itemId=vehicleRunningExpenseClaimStepOnePageId]',
        	onVehicleRegistrationList:'container[itemId=vehicleRegistrationList]'
        },
		control:{onVehicleRegistrationList:{itemtap: 'loadVehicleList'}}
	},
	loadVehicleList: function(dataview, index, target, record, e, options){  
		Shared.VehicleID = record.get('VehicleID');
		Shared.vehicleReg = record.get('RegoNo');
		var me = this;
		me.loadClaimStepOneView();
	},
	loadClaimStepOneView : function(){
		var me = this;
	// Show loader......
		Ext.Viewport.setMasked(Shared.loadingMask);
	// Push Vehicle Claim view	
    	Shared.pushNewView('vehicleRunningExpenseClaimStepOnePageId','vehicleRunningExpenseClaimStepOne');
	// Add 	vehicle registration No.
		var vehicleRunningExpenseClaimStepOnePageViewObj=this.getVehicleRunningExpenseClaimStepOnePageView();
		var vreglbl= vehicleRunningExpenseClaimStepOnePageViewObj.down('#vehicleRegLabel');
		vreglbl.setHtml('<h3>'+Messages.claimForVehicle+Shared.vehicleReg+'</h3>');
		
		var vehicleRunningExpenseClaimFormfieldWrapperObj = vehicleRunningExpenseClaimStepOnePageViewObj.down('#vehicleRunningExpenseClaimTypeWrapper');
		vehicleRunningExpenseClaimFormfieldWrapperObj.removeAll(true,true);
		
		
		var claimFormFieldsData=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');
		var claimType = claimFormFieldsData.VehicleClaim.VehicleTransactionTypes.VehicleTransactionType;

		Shared.GSTRate=claimFormFieldsData.GSTRate; // GST rates from store;
		
		if(Ext.os.is.Tablet){
				var layoutType = 'hbox';
				var flexno = 18;
		}
		else{ 
			var layoutType = false; 
			var flexno = 7;
		}
	//Create radio button for each claim type.	
		for(var i=0; i < claimType.length ;i++){ 
			var claimTypeFieldRadio = Ext.create('Ext.Container',{
					cls: 'font-14 bgwhite borderbtm vehicle_claim_list class_active',
					items:[{	
						xtype: 'button',
						cls: 'claimTypeButton_n claimTypeButton',
						itemId:claimType[i]['Id']+'_'+i, 
						text:claimType[i]['Name'],
						listeners:{
							tap:function(){
								me.getApplication().getController('VehicleRunningExpenseClaim_C').claimTypeSelected(this.getItemId());
							}
						} 
					}]
			});
			// Add claim type radio button 
			vehicleRunningExpenseClaimFormfieldWrapperObj.add(claimTypeFieldRadio);
		}		
		Ext.Viewport.setMasked(false);
	}
});