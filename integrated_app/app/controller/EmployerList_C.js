Ext.define('SmartSalary.controller.EmployerList_C', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
		// Define view name for refrence.
            myEmployerListPageId : 'container[itemId = myEmployerListPageId]',
			onEmployerList:'container [itemId=employerList]'
        },
        control: {
            onEmployerList:{
				disclose : 'onEmployerListPressCommand',
				itemTap:'onEmployerListPressCommand'
			},
			myEmployerListPageId:{
				show : 'loadEmployerList'
			}
		}
    },
	loadEmployerList:function(){
	//Show mask 
		Ext.Viewport.setMasked(Shared.loadingMask);
	// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;
		var employerStoredata = Ext.getStore('EmployerList');
		employerStoredata.setProxy({headers:{"Authorization" :"Basic " + Shared.base64variable}});
		var employerStoreURL = Config.ApiBaseUrl+'api/getemployerslistofuser';
		employerStoredata.load({
			url:employerStoreURL,
			callback: function(records, operation, success){
				if(success){
					Logger.debug("EmployerList "+Messages.AlertMsgStoreLoaded );
				}else{
					Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
					Ext.Viewport.setMasked(false);
				}
			}
		});
	// Hide loader......
		Ext.Viewport.setMasked(false);
	},
	// If user push sign off button show the login screen.
    onEmployerListPressCommand: function (view,record, target, index, e, eOpts) {
	// Update store reocrd.
		var usersStoredata = Ext.getStore('User_S');
		var employercode = usersStoredata.getAt(0);
		employercode.set("EmployerName", record.get('EmployerName')); 
		employercode.set("EmployerCode", record.get('EmployerCode'));
	//Load menu store.
		var storeMenu = Ext.getStore('storeSlideList');
	// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;
	//Load dynamic menu for selected employer.
		var storeMenuURL = Config.ApiBaseUrl+'api/getmenus/'+record.get('EmployerCode');
		storeMenu.load({
			url:storeMenuURL,
			callback: function(store, options, success) {
				if(success){
					 
					storeMenu.each(function(record, index, animated) {

						if(record.get('text')=="Log In"){
							Logger.debug(record.get('text'));
							record.set('text','Log Out');
						}
					});

				}else{
					Logger.alert(Messages.AlertMsg,Messages.AlertMsgStoreDataNotLoaded);
					Ext.Viewport.setMasked(false);
				}
			}
		});
	//Get this ref in me var.
		var me =this;
		Shared.pushNewView('dashboardscreen','dashboardView');	
	}
});