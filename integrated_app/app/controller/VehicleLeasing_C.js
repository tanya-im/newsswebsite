Ext.define('SmartSalary.controller.VehicleLeasing_C', {
	extend: 'Ext.app.Controller',
	config: {
		currentVehicleFuelCards:null,
		currentFuelCard:null,
		cProductSelected:null,
		unAvailableReasons:null,
		leaseStartDate:null,
		lastSelectedFilter:null,
		currentVehicleIdSelected:null,
		lastSelectedFilterOverView:null,
		currentVehicleIdSelectedOverView:null,
		lastSelectedVehicleTransaction:null,
		availableVehicleExpenses : null,
		vehicleTransactionButtonClick : null,
		colorsArray: [
		{'name':'fa-circle-o-purple','code':'#4F2C7E'},
		{'name':'fa-circle-o-red','code':'#EB2939'},
		{'name':'fa-circle-o-darkblue','code':'#0070BB'},
		{'name':'fa-circle-o-orange','code':'#E88200'},
		{'name':'fa-circle-o-ltblue','code':'#ABDDE4'},
		{'name':'fa-circle-o-grey','code':'#B0B3B1'},
		{'name':'fa-circle-o-green','code':'#BCD400'}
		],
		refs:{
			/*** vehicleDetailsPage  **/
			vehicleDetailsPage:'container[itemId=vehicleDetailsPageId]',
			/***** vehicleLeaseDetailPage **/
			leaseDetailsPage:'container[itemId=leaseDetailsPageId]',
			/*** fBTDetailsPage **/
			fBTDetailsPage:'container[itemId=fBTDetailsPageId]',
			/***** myProductsPage **/
			myProductsPage:'container[itemId=myProductsPageId]',
			/***** leasing history page ***/
			leasingHistoryPage:'container[itemId=leasingHistoryPageId]',
			/****** manage Fuel card  page *****/
			manageFuelCardPage:'container[itemId=manageFuelCardPageId]',
			/****** odoReadingPage*************/
			oDOReadingPage:'container[itemId=oDOReadingPageId]',
			oDOReadingSubmitButton:'button[action=oDOReadingSubmitButtonPress]',
			/*******Lost or Stolen Cards page ************/
			lostOrStolenCardsPage:'container[itemId=vehicleLeasingLostOrStolenCardsPageId]',
			vehicleCardsListingLSPage:'list[itemId=vehicleCardsListingLSPage]',
			/********* vehicle expenditure ************/
			vehicleExpenditureDateChangeButtonPress:'button[action=vehicleExpenditureDateChangeButtonPress]',
			vehiclesExpenditureDatesButtonPress:'button[action=vehiclesExpenditureDatesButtonPress]',
			/********** budget expenses breakdown **********/
			breakdownDatesButtonPress:'button[action=breakdownDatesButtonPress]',
			/********* budget update *****************/
			budgetManagementPage:'container[itemId=BudgetManagementPageId]',
			updateNewBudgetButton:'button[action=budgetSubmitButtonPress]',
			/****** manage budget *********/
			manageMyBudgetPage:'container[itemId=manageMyBudgetPageId]',
			/****** Whathappend card page *********/
			whatHappenOptionsPageId:'container[itemId=vehicleLeasingLostOrStolenCardsWhatHappenOptionsPageId]',
			/****** daysUnavailable confirm page *********/
			vehicleUnAvailableAddConfirmPage:'container[itemId=vehicleLeasingDaysUnavailableConfirmScreenPageId]',			
			
			budgetBreakdownPageId:'container[itemId=budgetBreakdownPageId]',
			vlOverviewPage:'container[itemId=vehiclesExpenditureOverviewPageId]',
			fullTransactionsButtonPress : 'button[itemId=fullTransactionsButton]',
			vehiclesExpenditureTransactionsPageId:'container[itemId=vehiclesExpenditureTransactionsPageId]',
			vehicleExpenditureTransactionList: 'list[id=vehicleExpenditureTransactionList]',
			leasingDetailsButton:'button[action=leasingDetailsButtonPress]',
			vehicleDetailsPress:'button[action=vehicleDetailsPress]',
			leaseDetailsPress:'button[action=leaseDetailsPress]',
			availableProductsButtonPress:'button[action=availableProductsButtonPress]',
			FBTDetailsPress:'button[action=FBTDetailsPress]',
			vehicleDetailBackBtnPress:'button[action=vehicleDetailBackBtnPress]',
			myODOReadingButtonPress:'button[action=myODOReadingButtonPress]',
			daysUnavailableButtonPress:'button[action=daysUnavailableButtonPress]',
			backToFBTDetails:'button[action=backToFBTDetails]',
			backToDashboard :'button[action=backToDashboardPress]',
			manageFuelCardsPress:'button[action=manageFuelCardsPress]',
			MyProductsButtonPress:'button[action=MyProductsButtonPress]',
			leasingHistoryPress:'button[action=leasingHistoryPress]',
			leasingOverviewButtonPress:'button[action=leasingOverviewButtonPress]',
			viewFullBudgetBreakdownButtonPress:'button[action=viewFullBudgetBreakdownButtonPress]',
			manageBudgetsButtonPress:'button[action=manageBudgetsButtonPress]',
			maintenanceTransactionsButtonPress:'button[action=maintenanceTransactionsButtonPress]',
			otherTransactionsButtonPress:'button[action=otherTransactionsButtonPress]',
			backExpenditureTransactionPress:'button[action=backExpenditureTransactionPress]',
			transactionList:'list[itemId=expenseList]',
			backTransactionsDetailPress:'button[action=backTransactionsDetailPress]',
			manageMyBudgetButtonPress:'button[action=manageMyBudgetButtonPress]',
			backManageMyBudget:'button[action=backManageMyBudget]',
			manageBudgetFuelButtonPress:'button[action=manageBudgetFuelButtonPress]',
			backBudgetManagementPress:'button[action=backBudgetManagementPress]',
			
			nextTransactionButton: 'button[itemId = nextTransactionButton]',
			previousTransactionButton: 'button[itemId = previousTransactionButton]',			
			changeDatesBudgetBreakdownButtonPress:'button[action=changeDatesBudgetBreakdownButtonPress]',
			vehicleTransactionsDatesButtonPress: 'button[action=vehicleTransactionsDatesButtonPress]',

			customDateRangeButton:'button[itemId=customDateRangeButton]',
			vEchangeDatesButtonPress:'button[action=vEchangeDatesButtonPress]',
			vEcustomDateRangeButtonPress:'button[action=vEcustomDateRangeButtonPress]',
			manageBudgetAccordionButtonPress:'button[action=manageBudgetAccordionButtonPress]',
			budgetBreakdownDateChangeButtonPress:'button[action=budgetBreakdownDateChangeButtonPress]',
			vEcustomDateRangeButton:'button[itemId=vEcustomDateRangeButton]',
			goBackToVehExpOve:'button[itemId=goBackToVehExpOve]',
			goBackToDashboardFromBreakdownScreen:'button[itemId=goBackToDashboardFromBreakdownScreen]',
			goToVehicleLeasingAvailableProducts:'button[action=goToVehicleLeasingAvailableProducts]',
			
			/*****Available Products*******/
			goToMyProducts : 'button[itemId=availableProductsListingBackBtn]',
			availableProductsView: 'container[itemId=vehicleLeasingAvailableProductsPageId]',
			//availableProductsReadMoreBtn : 'button[itemId=availableProductsReadMoreBtnPress]',
			availableProductsDetailPageBackBtn : 'button[itemId=availableProductsDetailPageBackBtnPress]',
			availableProductsUserDetailPageBackBtn : 'button[itemId=availableProductsUserDetailPageBackBtnPress]',
			availableProductsDetailPageSendEnquiry : 'button[itemId=availableProductsDetailPageSendEnquiryBtnPress]',
			viewAvailableProductsThakyouReturnBtn : 'button[itemId=viewAvailableProductsThakyouReturnBtnPress]',
			viewAvailableProductsThakyouReturnBackBtn : 'button[action=viewAvailableProductsThakyouReturnBackBtn]',
			availableProductsUserDetailPageSubmitBtn : 'button[itemId=availableProductsUserDetailPageSubmitBtnPress]',
			viewAvailableProductsThakyouBackBtn : 'button[itemId=viewAvailableProductsThakyouBackBtnPress]',
			availableProductsListingBackToDashboardBtn:'button[itemId=availableProductsListingBackToDashboardBtn]',
			productsEnquiryUserDetailView: 'container[itemId=vehicleLeasingAvailableProductsUserLogedInDetailPageId]',
			productsDetailView: 'container[itemId=vehicleLeasingAvailableProductsDetailPageId]',
			productsEnquirySubmittedView: 'container[itemId=vehicleLeasingAvailableProductsThankyouPageId]',
			vehiclesTransactionsDetailPageId: 'container[itemId=vehiclesTransactionsDetailPageId]',
			
			/*****Days Unavailable*******/
			daysUnavailablePage:'container[itemId=vehicleLeasingDaysUnavailablePageId]',
			vehicleLeasingUnavailableBackBtn : 'button[itemId=vehicleLeasingDaysUnavailableBackBtnPress]',
			vehicleLeasingDaysUnavailableAddPageId: 'container[itemId=vehicleLeasingDaysUnavailableAddPageId]',
			daysUnavailableConfirmBackBtn : 'button[itemId=daysUnavailableConfirmBackBtnPress]',
			vehicleLeasingDaysUnavailableNextBtn : 'button[itemId=vehicleLeasingDaysUnavailableAddNextBtnPress]',
			vehicleLeasingDaysUnavailableAddBackBtn : 'button[itemId=vehicleLeasingDaysUnavailableAddBackBtnPress]',
			vehicleLeasingDaysUnavailableAddBtn : 'button[itemId=vehicleLeasingDaysUnavailableAddBtnPress]',
			vehicleCardsListing : 'list[itemId=vehicleCardsListing]',
			daysUnavailableConfirmBtnPress:'button[itemId=daysUnavailableConfirmBtnPress]',
			
			/******Lost Or Stolen card*******/
			lostOrStolenCardsBackBtn : 'button[itemId=lostOrStolenCardsBackBtnPress]',
			vehicleLeasingReportCardStolenAcceptRequestBackBtn : 'button[itemId=vehicleLeasingReportCardStolenAcceptRequestBackBtnPress]',
			reportCardLostBtn : 'button[action=ReportCardLostBtnPress]',
			reportCardStolenBtn : 'button[action=ReportCardStolenBtnPress]',
			vehicleLeasingLostOrStolenCardsAcceptedRequestBackBtn : 'button[itemId=vehicleLeasingLostOrStolenCardsAcceptedRequestBackBtnPress]',
			vlostOrStolenCardsBackBtnPress:'button[itemId=vlostOrStolenCardsBackBtnPress]',
			
			/******Order new card*******/
			orderNewFuelCardPage:'container[itemId=vehicleLeasingOrderNewCardPageId]',
			vehicleLeasingOrderNewCardSubmitBtn : 'button[itemId=vehicleLeasingOrderNewCardSubmitBtnPress]',
			vehicleLeasingOrderNewCardThankYouBackBtn : 'button[itemId=vehicleLeasingOrderNewCardThankYouBackBtnPress]',
			vehicleLeasingOrderNewCardThankYouReturnBtn : 'button[itemId=vehicleLeasingOrderNewCardThankYouReturnBtn]',
			vehicleLeasingOrderNewCardTopBackBtnPress:'button[itemId=vehicleLeasingOrderNewCardTopBackBtnPress]',
			manageFuelCardsButtonPress:'button[action=manageFuelCardsButtonPress]',

			/* -------- Manage Fuel card----------- */
			reportLostStolenCardButtonPress:'button[action=reportLostStolenCardButtonPress]',
			orderNewCardButtonPress:'button[action=orderNewCardButtonPress]',
			/* -------- End Manage Fuel card----------- */
		},
		control:{
			fBTDetailsPage:{show:'onShowFBTDetailPage'},
			budgetBreakdownPageId:{show:'budgetBreakDownPageShow'},
			
			vlOverviewPage:{show: 'overviewPageShown'},
			vehiclesExpenditureTransactionsPageId:{show: 'showVehicleExpenditureTransaction'},
			leasingDetailsButton:{tap:'showLeasingDetailPage'},
			vehicleDetailsPress:{tap:'showVehicleDetail'},
			leaseDetailsPress:{tap: 'showLeaseDetailPage'},
			FBTDetailsPress:{tap:'moveToFBTDetailPage'},
			vehicleCardsListingLSPage:{itemtap:'vehicleCardsListingLSItemTap'},
			availableProductsButtonPress:{tap:'availableproductlistingFrmDashboard'}, 
			vehicleDetailBackBtnPress:{tap: 'goToLeasingDetails'},
			myODOReadingButtonPress:{tap: 'showOdoReadingPage'},
			oDOReadingSubmitButton:{tap:'updateOdoReading'},
			daysUnavailableButtonPress:{tap:'daysUnavailablePage'},
			fullTransactionsButtonPress: {tap:'goToTransactiopnsScreen'},
			nextTransactionButton: {tap:'onTransactionDetailPrevNext'},
			previousTransactionButton: {tap:'onTransactionDetailPrevNext'},
			backToFBTDetails:{
				tap:function(){
					Shared.pushNewView('fBTDetailsPageId','fBTDetails','left');	
				}
			},
			backToDashboard:{
				tap:function(){
					Shared.pushNewView('dashboardscreen','dashboardView','left');		
				}},
				manageFuelCardsPress:{
					tap:'showFuelCards'
				},
				MyProductsButtonPress:{
					tap:'showMyProductsListing'
				},
				leasingHistoryPress:{
					tap:'showLeasingHistory'
				},
				leasingOverviewButtonPress:{
					tap:'showOverviewPage'
				},
				viewFullBudgetBreakdownButtonPress:{
					tap:function(){
						Shared.pushNewView('budgetBreakdownPageId','budgetBreakdown','right');	
						if(!Ext.os.is.Desktop)
							Ext.getCmp('budgetBreakdownBackButton').setItemId("goBackToVehExpOve");
					}
				},
				manageBudgetsButtonPress:{
					tap:'showManageBudgetsShow'
				},
				goBackToDashboardFromBreakdownScreen:{
					tap:function(){
						Shared.pushNewView('dashboardscreen','dashboardView','left');	
					}
				},
				maintenanceTransactionsButtonPress:{ tap:'goToTransactiopnsScreen'},
				otherTransactionsButtonPress:{ tap:'goToTransactiopnsScreen'},
				backExpenditureTransactionPress:{ 
					tap:function(){ 
						Shared.pushNewView('vehiclesExpenditureOverviewPageId','vehiclesExpenditureOverview','left');	
					}
				},
				transactionList:{
					itemtap:'showTransactionDetail'
				},
				backTransactionsDetailPress:{
					tap:function(){ 
						Shared.pushNewView('vehiclesExpenditureTransactionsPageId','vehiclesExpenditureTransactions','left');	
					}
				},
				manageMyBudgetButtonPress:{
					tap:'manageMyBudgetList'
				},
				backManageMyBudget:{
					tap:'moveToBudgetChartPage'
				},
				backBudgetManagementPress:{
					tap:'moveToBudgetChartPage'
				},
				changeDatesBudgetBreakdownButtonPress:{ 
					tap:'showHideDateContainer'
				},
				customDateRangeButton:{
					tap:'showHideCustomDateRangeContainer'
				},
				vEchangeDatesButtonPress:{
					tap:'showHidevehicleExpenditureDateContainer'
				},
				vEcustomDateRangeButtonPress:{
					tap:'showHidevehicleExpenditureCustomDateRangeContainer'
				},
				manageBudgetAccordionButtonPress:{
					tap:'budgetBreakDownList'
				},
				budgetBreakdownDateChangeButtonPress:{
					tap:'bBreakdownDateChangeButtonPress'
				},
				vehicleExpenditureDateChangeButtonPress:{
					tap:'vExpenditureDateChangeButtonPress',
				},
				breakdownDatesButtonPress:{
					tap:'customDatesButtonPressBudget',
				},
				vehiclesExpenditureDatesButtonPress:{
					tap:'customDatesButtonPress',
				},
				goBackToVehExpOve:{ 
					tap:function(){
						Shared.pushNewView('vehiclesExpenditureOverviewPageId','vehiclesExpenditureOverview','right');	
					}
				},
				goToVehicleLeasingAvailableProducts:{tap:'availableProductsList'},
				goToMyProducts : {
					tap : function(){
						Shared.pushNewView('myProductsPageId','myProducts');
					}
				},
				/*****Available Products*******/
				
				availableProductsDetailPageBackBtn : {tap:'backToAvailableProductListing'},
				availableProductsUserDetailPageBackBtn : {tap : 'backToVehicleLeasingAvailableProductDetail'},
				availableProductsDetailPageSendEnquiry : {tap : 'goToVehicleLeasingAvailableProductUserDetail'},
				viewAvailableProductsThakyouReturnBtn : {tap : 'backToAvailableProductListing'},
				viewAvailableProductsThakyouReturnBackBtn : {tap : 'backToAvailableProductListing'},
				availableProductsUserDetailPageSubmitBtn : {tap : 'goToVehicleLeasingAvailableProductThankyou'},
				viewAvailableProductsThakyouBackBtn : {tap : 'backToAvailableProductUserDetail'},
				vehicleExpenditureTransactionList : {itemtap : 'onTransactionListItemTap'},
				availableProductsListingBackToDashboardBtn:{tap:function(){
					Shared.pushNewView('dashboardscreen','dashboardView','left');
				}},
				
				/*****Days Unavailable*******/
				vehicleLeasingUnavailableBackBtn: {tap: function(){
					Shared.pushNewView('fBTDetailsPageId','fBTDetails');
				}},
				vehicleLeasingDaysUnavailableAddPageId: {
					show:  function(){ 
						Shared.addCalendar('daysUnavailableDropOffCalendar','Drop off','daysUnavailableDropOffCalendarId',"DA_0",null);
						Shared.addCalendar('daysUnavailablePickUpCalendar','Pick up','daysUnavailablePickUpCalendarId',"DA_1",null); 
					}
				},
				daysUnavailableConfirmBackBtn : {tap : 'backToVehicleLeasingDaysUnavailableAdd'},
				vehicleLeasingDaysUnavailableNextBtn : {tap : 'goToVehicleLeasingDaysUnavailableConfirm'},
				vehicleLeasingDaysUnavailableAddBackBtn: {tap : 'backToVehicleLeasingDaysUnavailable'},
				vehicleLeasingDaysUnavailableAddBtn : {tap : 'goToVehicleLeasingDaysUnavailableAdd'},
				daysUnavailableConfirmBtnPress:{tap:'addDaysUnAvailable'},
				
				/******Lost Or Stolen card*******/
				
				lostOrStolenCardsBackBtn : {tap: "backToVehicleLeasingCardLostOrStolen"},
				vehicleLeasingReportCardStolenAcceptRequestBackBtn : {tap : "backToVehicleLeasingCardStolenOption"},
				reportCardLostBtn : {tap : "reportCardLostSubmit"},
				reportCardStolenBtn : {tap : "reportCardStolenSubmit"},
				vehicleLeasingLostOrStolenCardsAcceptedRequestBackBtn : {tap : "backToVehicleLeasingCardStolenOption"},
				vlostOrStolenCardsBackBtnPress:{tap:function(){
					Shared.pushNewView('manageFuelCardPageId','manageFuelCard');
				}},
				/******Order new card*******/
				vehicleLeasingOrderNewCardSubmitBtn : {tap : "requestForNewCardSubmit"},
				vehicleLeasingOrderNewCardThankYouBackBtn : {tap : "backToVehicleLeasingOrderNewCard"},
				vehicleLeasingOrderNewCardThankYouReturnBtn : {tap : "backToVehicleLeasingOrderNewCard"},
				manageFuelCardsButtonPress : {tap : "pushManageFuelCardsView"},
				vehicleLeasingOrderNewCardTopBackBtnPress:{tap:function(){
					Shared.pushNewView('manageFuelCardPageId','manageFuelCard');
				}},

				/* -------- Manage Fuel card----------- */
				reportLostStolenCardButtonPress:{tap:'reportLostStolenCard'},
				orderNewCardButtonPress:{tap:'requestForNewCard'},
				/* -------- End Manage Fuel card----------- */

				/* ----------- Update budget page -------------*/
				updateNewBudgetButton:{tap:'updateNewBudget'},
				/*-------Manage my budget-------------*/
				manageMyBudgetPage:{show : 'loadBudgetList'},
				/*--------Vehicle transactions----------*/
				vehicleTransactionsDatesButtonPress: {tap : 'vehicleTransactionsCustomDatesButton'}

			}
		},
		showOverviewPage:function(){
			Shared.pushNewView('vehiclesExpenditureOverviewPageId','vehiclesExpenditureOverview','left');	
		},
		showManageBudgetsShow:function(){
			Shared.pushNewView('budgetBreakdownPageId','budgetBreakdown','right');
			if(!Ext.os.is.Desktop)
				Ext.getCmp('budgetBreakdownBackButton').setItemId("goBackToDashboardFromBreakdownScreen");
		},
		showLeasingDetailPage: function(){
			Shared.pushNewView('leasingDetailsPageId','leasingDetails','right');	
		},
		moveToBudgetChartPage:function(){
			
			Logger.info("moveToBudgetChartPage");
			Shared.pushNewView('budgetBreakdownPageId','budgetBreakdown','left');
		},
		budgetBreakDownPageShow: function(){
			
			Logger.info("budgetBreakDownPageShow");
			var me=this; 
			if(this.getBudgetBreakdownPageId().down("#budgetBreakdownFromCalendar")==null){
				Shared.addCalendar('breakdownFromCalendar','From','budgetBreakdownFromCalendar',"BB_0",null);
				Shared.addCalendar('breakdownToCalendar','To','budgetBreakdownToCalendar',"BB_1",null); 
			}

			var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');		 
			if(VehiclesStore.length>0)
			{
				var ArrOption= Array();
				var indexArr=Array();
				Ext.each(VehiclesStore, function(vehicleItem,i){ 
					ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':i});
					indexArr.push(vehicleItem.vehicle.id);
				}); 
				if(!(me.currentVehicleIdSelected != null))
					me.currentVehicleIdSelected = VehiclesStore[0].vehicle.id; 
				
				this.getBudgetBreakdownPageId().down("#selectVContainer").removeAll(true,true);
				var selectfield = Ext.create('Ext.field.Select',{  
					cls:'expenditureType_1 vlabel',
					usePicker:false,
					itemId:'selectVehicle', 
					label: 'Vehicle :',
					options:ArrOption,
					listeners:{
						change:function( this_, newValue, oldValue, eOpts ){
							me.setValuesInBudgetBreakdownPage(VehiclesStore[newValue].vehicle.id,me.getLastSelectedFilter());
							me.currentVehicleIdSelected =  VehiclesStore[newValue].vehicle.id;
						}
					}  
				});

				selectfield.suspendEvents();
				
				for (var num in VehiclesStore) {
					if (VehiclesStore[num].vehicle.id == me.currentVehicleIdSelected)
					{
						selectfield.setValue(num);
						me.currentVehicleIdSelected =  VehiclesStore[num].vehicle.id;
					}   
				}

				selectfield.resumeEvents(true);

				this.getBudgetBreakdownPageId().down("#selectVContainer").add(selectfield);
			}
			if(me.getLastSelectedFilter()!=null || me.getCurrentVehicleIdSelected()!=null){
				var cVal=Ext.Array.indexOf(indexArr,me.currentVehicleIdSelected); 
				var control =me.getBudgetBreakdownPageId().down("#selectVehicle");
				control.suspendEvents();
				control.setValue(cVal);
				control.resumeEvents(true);
				me.setValuesInBudgetBreakdownPage(me.getCurrentVehicleIdSelected(),me.getLastSelectedFilter());
			}else{
				
				if(me.currentVehicleIdSelected != null)
					me.setValuesInBudgetBreakdownPage(me.currentVehicleIdSelected,2);
				else
					me.setValuesInBudgetBreakdownPage(VehiclesStore[0].vehicle.id,2);
			}
		},
		overviewPageShown: function(){
			
			Logger.info("overviewPageShown");		 
			var me=this;
			me.getVlOverviewPage().down('#vehicleExpenditureDateContainer').getItems('button').each(function(key, value, myself){
				key.setStyle("background-color: #e7e7e7;color: #595b77;");
			});
			if(this.getVlOverviewPage().down("#vExpenditureFromCalendar")==null){
				Shared.addCalendar('vehiclesExpenditureFromCalendar','From','vExpenditureFromCalendar',"VE_0",null);
				Shared.addCalendar('vehiclesExpenditureToCalendar','To','vExpenditureToCalendar',"VE_1",null); 
			} 
			var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');		 
			if(VehiclesStore.length>0)
			{
				var ArrOption= Array();
				var indexArr=Array();
				Ext.each(VehiclesStore, function(vehicleItem,i){ 
					ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':i});
					indexArr.push(vehicleItem.vehicle.id);
				});
				
				this.getVlOverviewPage().down("#selectVContainer").removeAll(true,true);
				
				var selectfield = Ext.create('Ext.field.Select',{  
					cls:'expenditureType_1 vlabel',
					usePicker:false,
					itemId:'selectVehicle', 
					label: 'Vehicle :',
					options:ArrOption,
					listeners:{
						change:function( this_, newValue, oldValue, eOpts ){
							
							Shared.addCalendar('vehiclesExpenditureFromCalendar','From','vExpenditureFromCalendar',"VE_0",null);
							Shared.addCalendar('vehiclesExpenditureToCalendar','To','vExpenditureToCalendar',"VE_1",null);
							
							me.setValuesInOverviewPage(VehiclesStore[newValue].vehicle.id);	
							me.currentVehicleIdSelected = VehiclesStore[newValue].vehicle.id;				
						}
					}  
				});
				this.getVlOverviewPage().down("#selectVContainer").removeAll(true,true);
				if(Ext.os.is.Tablet)
					var vehicleSequence = SmartSalary.app.getController('SmartSalary.controller.tablet.Dashboard_C').currentVehicleIdSelectedDashboard;
				else if(Ext.os.is.Desktop)
					var vehicleSequence = SmartSalary.app.getController('SmartSalary.controller.desktop.Dashboard_C').currentVehicleIdSelectedDashboard;
				else 
					var vehicleSequence = SmartSalary.app.getController('SmartSalary.controller.phone.Dashboard_C').currentVehicleIdSelectedDashboard; 
				
				selectfield.suspendEvents();

				if(vehicleSequence != null)
				{	
					selectfield.setValue(vehicleSequence);
					me.currentVehicleIdSelected = VehiclesStore[vehicleSequence].vehicle.id
				}
				else
				{	
					me.currentVehicleIdSelected = VehiclesStore[0].vehicle.id;
				}

				selectfield.resumeEvents(true);

				this.getVlOverviewPage().down("#selectVContainer").add(selectfield);
			}
		/*
		-----Commented to reset vehicle dropdown(currently verifying)------- 
		if(me.getLastSelectedFilterOverView()!=null || me.getCurrentVehicleIdSelectedOverView()!=null)
		{
			var cVal=Ext.Array.indexOf(indexArr,me.getCurrentVehicleIdSelectedOverView()); 
			me.currentVehicleIdSelected = me.getCurrentVehicleIdSelectedOverView();
			var control =me.getVlOverviewPage().down("#selectVehicle");
			control.suspendEvents();
			control.setValue(cVal);
			control.resumeEvents(true);
			me.setValuesInOverviewPage(me.getCurrentVehicleIdSelectedOverView(),me.getLastSelectedFilterOverView());
 
		}else{
			*/	
			me.setValuesInOverviewPage(me.currentVehicleIdSelected,2);
		/*
		}
		*/
		
	},
	setValuesInOverviewPage: function (vehicleId,filterType){

		
		Logger.info("setValuesInOverviewPage");

		Ext.Viewport.setMasked(Shared.loadingMask);	
		
		Logger.info(vehicleId+","+filterType);
		var me=this;
		if(filterType==null){
			if(me.getLastSelectedFilterOverView()==null)
			{
				filterType=2	
			}else{
				filterType=me.getLastSelectedFilterOverView();	
			}
		}
		var startDate= '';
		var endDate='';
		if(filterType==2) // last 12 months
		{
			me.getVlOverviewPage().down("#currentFilterLabel").setHtml("Last 12 Months");
			var date = new Date();
			startDate=date.getTime();
			endDate=new Date(date.getFullYear()-1, date.getMonth(),  date.getDate()-1).getTime();
		}
		else if(filterType==1) // leave to date
		{
			me.getVlOverviewPage().down("#currentFilterLabel").setHtml("Live To Date");
			var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
			var LeaseStartDate=VehiclesStore[this.getVlOverviewPage().down("#selectVehicle").getValue()].vehicleLease.leaseStartDate;
			startDate=new Date(LeaseStartDate).getTime();
			endDate=new Date().getTime();
			me.setLastSelectedFilterOverView(1);
		}
		else if(filterType==3) // FBT Year
		{
			me.getVlOverviewPage().down("#currentFilterLabel").setHtml("FBT Year");
			startDate= '';
			endDate='';
			me.setLastSelectedFilterOverView(3);
		}
		else if(filterType==4) // custom date range
		{
			me.getVlOverviewPage().down("#currentFilterLabel").setHtml("Custom Date Range");
			startDate=new Date(me.getVlOverviewPage().down("#vExpenditureFromCalendar").getValue()).getTime();
			endDate=new Date(me.getVlOverviewPage().down("#vExpenditureToCalendar").getValue()).getTime();
			me.setLastSelectedFilterOverView(4);

		}

		
		Logger.info(vehicleId+","+filterType);
		me.setCurrentVehicleIdSelectedOverView(vehicleId);

		var overviewApiUrl = Config.ApiBaseUrl+'api/getExpenditureOverview/'+ Shared.packageId + '/' +vehicleId+'/'+startDate+'/'+endDate; 
		
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		me.getVlOverviewPage().down("#expenses").removeAll();

		Ext.Ajax.request({
			url: overviewApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {

				var responseData = Ext.JSON.decode(response.responseText);
				var vehicleExpenses = new Array();
				if(responseData.success && responseData.result!=null){
					Ext.each(responseData.result.vehicleExpenseList,function (expenses,i){ 
						
						vehicleExpenses.push({"text" : expenses.name,"value" : expenses.id});
						
						var onBudgetZeroStyle='';
						if(expenses.annualBudget>=expenses.actualSpending )
							{	var percentTotal='100';
						var percentRun=parseInt((expenses.actualSpending/expenses.annualBudget)*100)
					}else{
						var percentRun='100';
						var percentTotal=parseInt((expenses.annualBudget/expenses.actualSpending)*100)
						if(expenses.annualBudget==0){
							onBudgetZeroStyle='display:none;';
						}
					}
					

					var htmlStr='<div class="carCompoWrapper"><div class="meter-vehicle" id="animateId_'+expenses.id+'">' 	
					+'<div class="textDiv" style="width: '+percentTotal+'%;min-width:20%;"  >'
					+'<span class="meterStartText">0</span>'
					+'<span class="meterEndText" style="'+onBudgetZeroStyle+'">Budget</span>'
					+'<div class="clearBoth"></div>'
					+'</div>'
					+'<span class="outer" style="width:100%"><div class="strips"></div></span>'
					+'<span class="greenColor" style="width: '+percentTotal+'%;display:none;"></span>'
					+'<span class="inner" id="meter-vehicle-span-inner" style="width: '+percentRun+'%;display:none;"></span>'
					+'<span class="TravelledLabel">Current</span>'
					+'</div></div>';	
					

					if(expenses.annualBudget>=expenses.actualSpending)
					{
						var labelStr="Within Budget";
						var clsStr='fa fa-check f-bold f-0E9F80';
					}
					else{
						var labelStr="Over Budget";
						var clsStr='fa fa-close f-bold f-e98300';
					}
					
					if(Ext.os.is.Tablet || Ext.os.is.Desktop)
						var LayoutCls = 'container_vl_wrap_grey pad-t10-b10';
					else 
						var LayoutCls ='bgE7E7E7'; 
					
					var ExpenseDetailCnt = Ext.create('Ext.Container',{ 
						cls :'pad-10 margin-10-0 '+LayoutCls,
						items:[{
							xtype: 'container',
							layout:'hbox',
							items:[{
								xtype: 'label',
								flex:2,
								cls:'txt-left',
								html: expenses.name
							},{
								xtype: 'label',
								flex:1,
								cls:'txt-right',
								html: '$'+expenses.actualSpending+' of $'+expenses.annualBudget
							}]	
						},{
							xtype: 'container', 
							height:'75px',
							html:htmlStr 
						},{
							xtype: 'container',
							cls:'bgwhite pad-6-10-7 margin-top-10 fa-vl',
							items:[{
								xtype: 'label',
								cls:clsStr,
								html:"<div class='fontFFamilyHelvetica pad-left-36'>"+labelStr+"</div>"		
							}]
						},{
							xtype: 'button',
							itemId: 'vehicleExpenses'+expenses.id,
							data: {'transactionId':expenses.id},
							ui: 'none',
							cls:'button_grey_vl claimStepButton txt-left font-16 margin-top-7',
							text: 'View Transactions',
							listeners:{
								tap:function (_this){
									
									Ext.Viewport.setMasked(Shared.loadingMask);
									
									me.setLastSelectedVehicleTransaction(_this.getData().transactionId);
									Shared.pushNewView('vehiclesExpenditureTransactionsPageId','vehiclesExpenditureTransactions','right');
									
									Logger.info(me.getLastSelectedVehicleTransaction());
								}
							}
							
						}]
					});
me.getVlOverviewPage().down("#expenses").add(ExpenseDetailCnt);
AnimateVehicle("animateId_"+expenses.id);

					});// foreach ended
me.availableVehicleExpenses = null;
me.availableVehicleExpenses = vehicleExpenses;
}

Ext.Viewport.setMasked(false);  
}
}); 
},
setValuesInBudgetBreakdownPage:function(vehicleId,filterType){
	Logger.info("setValuesInBudgetBreakdownPage");
	Ext.Viewport.setMasked(Shared.loadingMask);
	var me=this;
	Logger.info("vehicleId:"+vehicleId+",filterType:"+filterType)
	if(!Ext.isDefined(filterType)){
		if(me.getLastSelectedFilter()==null)
		{
			filterType=2	
		}else{
			filterType=me.getLastSelectedFilter();	
		}
	}
	else if(filterType == null)
	{
		filterType = 2;
	}
	Logger.info("vehicleId:"+vehicleId+",filterType:"+filterType);

	var startDate= '';
	var endDate='';

		if(filterType==2) // last 12 months
		{
			me.getBudgetBreakdownPageId().down("#currentFilterLabel").setHtml("Last 12 Months");
			var date = new Date();
			startDate=date.getTime();
			endDate=new Date(date.getFullYear()-1, date.getMonth(),  date.getDate()-1).getTime();
			me.setLastSelectedFilter(2);
		}
		else if(filterType==1) // leave to date
		{
			me.getBudgetBreakdownPageId().down("#currentFilterLabel").setHtml("Live To Date");
			
			var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
			var LeaseStartDate=VehiclesStore[this.getBudgetBreakdownPageId().down("#selectVehicle").getValue()].vehicleLease.leaseStartDate;
			
			startDate=new Date(LeaseStartDate).getTime();
			endDate=new Date().getTime();
			me.setLastSelectedFilter(1);
		}
		else if(filterType==3) // FBT Year
		{
			me.getBudgetBreakdownPageId().down("#currentFilterLabel").setHtml("FBT Year");
			startDate= '';
			endDate='';
			me.setLastSelectedFilter(3);
		}
		else if(filterType==4) // custom date range
		{
			me.getBudgetBreakdownPageId().down("#currentFilterLabel").setHtml("Custom Dates");
			startDate=new Date(me.getBudgetBreakdownPageId().down("#budgetBreakdownFromCalendar").getValue()).getTime();
			endDate=new Date(me.getBudgetBreakdownPageId().down("#budgetBreakdownToCalendar").getValue()).getTime();
			me.setLastSelectedFilter(4);
		}
		
		me.currentVehicleIdSelected = vehicleId;
		
		var overviewApiUrl = Config.ApiBaseUrl+'api/getBudgetBreakDown/'+ Shared.packageId + '/' +vehicleId+'/'+startDate+'/'+endDate; 
		me.setCurrentVehicleIdSelected(vehicleId);
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		me.getBudgetBreakdownPageId().down("#budgetTypeList").removeAll(true,true);

		Ext.Ajax.request({
			url: overviewApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {
				var responseData = Ext.JSON.decode(response.responseText);
				if(responseData.success && responseData.result!=null){
					me.getBudgetBreakdownPageId().down("#manageMyBudgetButton").show();
					var chartData=[];
					var colors=[]; 
					var availableColors=me.getColorsArray();
					var vehicleExpenses = new Array();
					
					Ext.each(responseData.result.vehicleExpenseList,function (expenses,i){ 
						var j=i+1;
						vehicleExpenses.push({"text" : expenses.name,"value" : expenses.id});
						var ExpenseDetailCnt = Ext.create('Ext.Container',{ 
							xtype: 'container',
							items:[{
								xtype: 'button',
								itemId: 'manageBudgetLeaseButton_'+j,
								id:'manageBudgetLeaseButton_'+j,
								action: 'manageBudgetAccordionButtonPress',
								ui: 'none',
								flex:1,
								cls:'button_ltgrey_vl fa fa-chevron-right fa-circle-o '+availableColors[i].name+' txt-left font-16 pad-10 margin-top-10',
								text: '<label class="budgetName pad-left-20">'+expenses.name+'</label><label class="budgetAmount">$'+expenses.annualBudget+'</label>'
							},{
								xtype: 'container',
								cls :'pad-10 bgF9F9F9 font-14',
								id:'accordionContainer_'+j,
								hidden:true,
								items:[{
									xtype: 'container',
									layout:'hbox',
									cls:'margin-top-7',
									items:[{
										xtype: 'label',
										html: 'Total Actual Spend:',
										flex:1
									},{
										xtype: 'label',
										itemId:'lifeToDateBudget',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:expenses.actualSpending
									}]
								},{
									xtype: 'container',
									layout:'hbox',
									cls:'margin-top-7',
									items:[{
										xtype: 'label',
										
										html: 'Total Annual Budget:',
										flex:1
									},{
										xtype: 'label',
										itemId:'yearToDateActualSpend',
										cls:'pad-right-5 txt-right f-bold',
										flex:1,
										html:expenses.annualBudget
									}]
								},{
									xtype: 'container',
									items:[{
										xtype: 'button',
										itemId: expenses,
										ui: 'none',
										flex:1,
										action: 'manageBudgetRegoandCPTButtonPress',
										cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-10-0',
										text: 'Edit Budget',
										listeners:{
											tap:function (_this){
												me.moveToBudgetUpdatePage(_this.getItemId());
											}
										}
									}]
								}]	
							}]
						});

colors.push(availableColors[i].code);
chartData.push({'name':expenses.name,'budget':expenses.annualBudget});

me.getBudgetBreakdownPageId().down("#budgetTypeList").add(ExpenseDetailCnt);						
					});// foreach ended
me.availableVehicleExpenses = null;
me.availableVehicleExpenses = vehicleExpenses;
me.animatePieChart(chartData,colors); 
}else{
	me.getBudgetBreakdownPageId().down("#manageMyBudgetButton").hide();
} 
Ext.Viewport.setMasked(false);  
}
}); 
},
loadBudgetList: function(){
	
	
	Logger.info("loadBudgetList");
	var me=this;
	var manageBudgetList = me.getManageMyBudgetPage().down("#expenseList");
	var manageMyBudgetPage = me.getManageMyBudgetPage();
	
	var VehiclesStore = Ext.getStore('VehiclesStore').getAt('0').get('result');
	
	if(VehiclesStore.length>0)
	{
		var ArrOption= Array();
		Ext.each(VehiclesStore, function(vehicleItem,i){ 
			ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':VehiclesStore[i].vehicle.id});
		}); 
		manageMyBudgetPage.down("#selectVehicleBudgetBreakdown").setOptions(ArrOption);
		
		if(me.currentVehicleIdSelected != null)
			manageMyBudgetPage.down("#selectVehicleBudgetBreakdown").setValue(me.currentVehicleIdSelected);	
		
		manageMyBudgetPage.down("#selectVehicleBudgetBreakdown").on({
			change:function( this_, newValue, oldValue, eOpts ){ 
				me.currentVehicleIdSelected = newValue;
				me.loadBudgetListRequest();
			}
		});

	}

	me.loadBudgetListRequest();
	
},
loadBudgetListRequest: function(){
	
	Logger.info("loadBudgetListRequest");
	Ext.Viewport.setMasked(Shared.loadingMask);

	var me=this;
	var manageBudgetList = me.getManageMyBudgetPage().down("#expenseList");
	manageBudgetList.removeAll(true,true);
	var manageMyBudgetPage = me.getManageMyBudgetPage();
	
	var VehiclesStore = Ext.getStore('VehiclesStore').getAt('0').get('result');

	
	Logger.info("vehicleId:"+me.currentVehicleIdSelected);
	var startDate = '';
	var endDate = '';
	
	var overviewApiUrl = Config.ApiBaseUrl+'api/getBudgetBreakDown/'+ Shared.packageId + '/' +me.currentVehicleIdSelected+'/'+startDate+'/'+endDate; 
	
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: overviewApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {

				var responseData = Ext.JSON.decode(response.responseText);
				if(responseData.success && responseData.result!=null){
					var vehicleExpenseList = responseData.result.vehicleExpenseList;	
					if(vehicleExpenseList.length>0){
						Ext.each(vehicleExpenseList,function (expenses,i){ 
							var manageBudgetBtn = Ext.create('Ext.Button',{
								xtype: 'button',
								itemId: "expenses"+expenses.id,
								ui: 'none',
								data: {"expenses":expenses},
								flex:1,					
								cls:'claimStepButton button_grey_vl_2 fa fa-chevron-right margin-top-10',
								text: '<span class="font-helveticalight">'+expenses.name+'</span>',
								listeners:{
									tap:function (_this){
										me.moveToBudgetUpdatePage(_this.getData().expenses);
									}
								}
							});
							
							Logger.info(i);
							manageBudgetList.add(manageBudgetBtn);
							
						});
						
					}
				}else{
					//do nothing
				} 
				Ext.Viewport.setMasked(false);
			}
		});
},
manageMyBudgetList:function(){
	
	Logger.info("manageMyBudgetList");
	var me=this;
	Shared.pushNewView('manageMyBudgetPageId','manageMyBudget');
		//me.getManageMyBudgetPage().down("#expenseList").removeAll(true,true); 
	},
	moveToBudgetUpdatePage:function (itemId){
		
		Logger.info("moveToBudgetUpdatePage");
		
		Logger.info(itemId);
		var me=this;
		Shared.pushNewView('BudgetManagementPageId','BudgetManagement','right');
		
		me.getBudgetManagementPage().down("#budgetManagement").setTitle(itemId.name+" Budget Management");
		me.getBudgetManagementPage().down("#totalBudget").setHtml(itemId.annualBudget);
		me.getBudgetManagementPage().down("#actualSpending").setHtml(itemId.actualSpending);
		me.getBudgetManagementPage().down("#budgetSubmitButtonPress").setData({'expense':itemId});
	},
	updateNewBudget:function (_this){
		
		Logger.info("updateNewBudget");

		var expense = _this.getData().expense;
		var me=this;
		Ext.Viewport.setMasked(Shared.loadingMask);
		var newBudget=me.getBudgetManagementPage().down("#budgetManagementTextBox").getValue();
		
		if( newBudget==null)
		{
			Logger.alert(Messages.AlertMsg,Messages.AlertMsgInvalidAnnualBudget); 
			Ext.Viewport.setMasked(false);
			return false;
		}		
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		
		var vehicleId= me.currentVehicleIdSelected;

		var overviewApiUrl = Config.ApiBaseUrl+'api/updateBudget/'+ Shared.packageId + '/' +vehicleId+'/'+expense.expenseTypeId+'/'+newBudget; 
		
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		

		Ext.Ajax.request({
			url: overviewApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {
				var responseData = Ext.JSON.decode(response.responseText);
				if(responseData.success && responseData.result!=null){
					me.getBudgetManagementPage().down("#budgetManagementTextBox").setValue();
					Ext.Msg.alert(Messages.AlertMsg,responseData.result.message,function(){
						Ext.Viewport.setMasked(false); /*  close the loader */
						Shared.pushNewView('budgetBreakdownPageId','budgetBreakdown','left');
					});
				}				
			}
		});
	},
	animatePieChart:function (chartData,colors){
		
		Logger.info("animatePieChart");
		var me=this;
		me.getBudgetBreakdownPageId().down("#pieChartInBudget").setColors(colors);
		me.getBudgetBreakdownPageId().down("#pieChartInBudget").setStore({
			fields: ['name', 'budget'],
			data:chartData
		})
	},
	

	availableproductlistingFrmDashboard:function(){
		
		Logger.info("availableproductlistingFrmDashboard");
		Shared.pushNewView('vehicleLeasingAvailableProductsPageId','vehicleLeasingAvailableProducts');
		this.availableProductListingGenFunction();
		if(!Ext.os.is.Desktop)
			Ext.getCmp('availableProductsListingBackBtn').setItemId('availableProductsListingBackToDashboardBtn');
	},
	availableProductsList:function(){
		
		Logger.info("availableProductsList");
		Shared.pushNewView('vehicleLeasingAvailableProductsPageId','vehicleLeasingAvailableProducts');
		this.availableProductListingGenFunction();
		if(!Ext.os.is.Desktop)
			Ext.getCmp('availableProductsListingBackBtn').setItemId('availableProductsListingBackBtn');
	},	
	daysUnavailablePage: function(){
		
		Logger.info("daysUnavailablePage");
		
		var me=this;
		Shared.pushNewView('vehicleLeasingDaysUnavailablePageId','vehicleLeasingDaysUnavailable','left');	
		var vehicleLeasingDaysUnavailablePage = me.getDaysUnavailablePage();
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		
		if(VehiclesStore.length>0)
		{
			var ArrOption= Array();
			Ext.each(VehiclesStore, function(vehicleItem,i){ 
				ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':VehiclesStore[i].vehicle.id});
			}); 
			vehicleLeasingDaysUnavailablePage.down("#selectVehicleDaysUnavailable").setOptions(ArrOption);
			
			Logger.info(me.currentVehicleIdSelected);
			if(me.currentVehicleIdSelected != null)
				vehicleLeasingDaysUnavailablePage.down("#selectVehicleDaysUnavailable").setValue(me.currentVehicleIdSelected);	
			
			vehicleLeasingDaysUnavailablePage.down("#selectVehicleDaysUnavailable").on({
				change:function( this_, newValue, oldValue, eOpts ){ 
					me.currentVehicleIdSelected = newValue;
					var daysUnavailableApiUrl = Config.ApiBaseUrl+'api/daysunavailable/'+ Shared.packageId + '/' +me.currentVehicleIdSelected;
					me.getDaysUnavailablePage().down("#dataGridItemId").getStore().setData(null);
					me.getDaysUnavailablePage().down("#vehicleLeasingDaysUnavailableAddBtnPress").setData(null);
					
					Logger.info("change");

					me.updateDaysUnavailable(daysUnavailableApiUrl);
				}
			});

		}
		
		var vehicleId=VehiclesStore[this.getFBTDetailsPage().down("#selectVehicle").getValue()].vehicle.id;
		var vehicle=VehiclesStore[this.getFBTDetailsPage().down("#selectVehicle").getValue()].vehicle;
		var daysUnavailableApiUrl = Config.ApiBaseUrl+'api/daysunavailable/'+ Shared.packageId + '/' +vehicleId; 
		
		me.updateDaysUnavailable(daysUnavailableApiUrl); 
	},
	updateDaysUnavailable: function(daysUnavailableApiUrl){
		Ext.Viewport.setMasked(Shared.loadingMask);
		var me = this;

		if(!(me.getDaysUnavailablePage().down("#dataGridItemId").getStore() == null))
			me.getDaysUnavailablePage().down("#dataGridItemId").getStore().setData(null);

		var vehicleLeasingDaysUnavailablePage = me.getDaysUnavailablePage();
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		
		for (var obj in VehiclesStore) {
			if (VehiclesStore[obj].vehicle.id == me.currentVehicleIdSelected)
			{
				var vehicle = VehiclesStore[obj].vehicle;
			}   
		}
		
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: daysUnavailableApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {

				var responseData = Ext.JSON.decode(response.responseText);
				if(responseData.success && responseData.result.daysUnavailableList.length>0){
					var unavailableDays = responseData.result.daysUnavailableList; 
					unavailableDays.reverse();

            		me.getDaysUnavailablePage().down("#dataGridItemId").setData(unavailableDays);					// Hide loader......
            		
            		Logger.info(responseData.result.daysUnavailableList);
            		me.getDaysUnavailablePage().down("#dataGridItemId").refresh();
            	}
            	
            	me.getDaysUnavailablePage().down("#vehicleLeasingDaysUnavailableAddBtnPress").setData(vehicle);
            	me.setUnAvailableReasons(responseData.result.daysUnavailableTypes); 
            	Ext.Viewport.setMasked(false);  
            }
        });
},
requestForNewCard:function(){
	
	Logger.info("requestForNewCard");
	Shared.pushNewView('vehicleLeasingOrderNewCardPageId','vehicleLeasingOrderNewCard');
	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
	var me = this;

	if(VehiclesStore.length>0)
	{
		var ArrOption= Array();
		Ext.each(VehiclesStore, function(vehicleItem,i){ 
			ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':VehiclesStore[i].vehicle.id});
		});

		this.getOrderNewFuelCardPage().down("#selectVehicleOrderNewCard").setOptions(ArrOption);
		this.getOrderNewFuelCardPage().down("#selectVehicleOrderNewCard").setValue(me.currentVehicleIdSelected);
		this.getOrderNewFuelCardPage().down("#selectVehicleOrderNewCard").show();
		this.getOrderNewFuelCardPage().down("#selectVehicleOrderNewCard").on({
			change:function( this_, newValue, oldValue, eOpts ){ 
				me.currentVehicleIdSelected = newValue;
			}
		});

	}

	var userObj=Ext.getStore('User_S').getAt(0);

	this.getOrderNewFuelCardPage().down("#user_name").setHtml(userObj.get('FirstName'));
	this.getOrderNewFuelCardPage().down("#user_email").setHtml(userObj.get('Email'));
	this.getOrderNewFuelCardPage().down("#user_contact").setHtml(userObj.get('WorkPhone')); 
	

},
requestForNewCardSubmit : function(){
	
	Logger.info("requestForNewCardSubmit");
	Ext.Viewport.setMasked(Shared.loadingMask);
	var me=this; 
	
	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result'); 

	var vehicleId = me.currentVehicleIdSelected;
	var requestNewCardApiUrl = Config.ApiBaseUrl+'api/orderNewFuelCard/'+ Shared.packageId+"/"+vehicleId; 
	
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: requestNewCardApiUrl,
			method: 'GET',
			success: function (response) {
				rs=Ext.JSON.decode(response.responseText)
				Ext.Viewport.setMasked(false);
				Logger.alert(Messages.AlertMsg,rs.result.message);
				Shared.pushNewView('vehicleLeasingOrderNewCardThankYouPageId','vehicleLeasingOrderNewCardThankYou');
			},
			// If Api return failed show error message.
			failure: function (response) {
				// Hide loader......
				Ext.Viewport.setMasked(false);
				Logger.alert(Messages.AlertMsg,Messages.AlertMsgServerNotRersponding);
			}
		});
	},
	reportLostStolenCard:function(){
		
		Logger.info("reportLostStolenCard");
		var me=this;
		Shared.pushNewView('vehicleLeasingLostOrStolenCardsPageId','vehicleLeasingLostOrStolenCards');		 		
		me.getLostOrStolenCardsPage().down("#vehicleNameHeader").setHtml("Select a card for vehicle "+this.getManageFuelCardPage().down("#selectVehicle").getRecord().get('text'));
		me.getVehicleCardsListingLSPage().setData(me.getCurrentVehicleFuelCards());
	},
	vehicleCardsListingLSItemTap:function( _this, index, target, record, e, eOpts ){
		
		Logger.info("vehicleCardsListingLSItemTap");
		var me = this; 
		this.setCurrentFuelCard(record);
		Shared.pushNewView('vehicleLeasingLostOrStolenCardsWhatHappenOptionsPageId','vehicleLeasingLostOrStolenCardsWhatHappenOptions');
		Ext.getCmp('pressdeCradNo').setHtml("Please let us know what happened to card "+record.get('cardNumber'));
	},
	updateOdoReading:function (){
		
		Logger.info("updateOdoReading");
		Ext.Viewport.setMasked(Shared.loadingMask);
		var me=this;
		this.getODOReadingPage().down("#lastReadingId")
		var oDOReadingValue=this.getODOReadingPage().down("#oDOReading").getValue();
		if(oDOReadingValue==null){
			
			Ext.Viewport.setMasked(false);
			Logger.alert(Messages.AlertMsg,Messages.AlertMsgEmptyOdoReading);
			return false;
		}
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		
		var vehicleId = me.currentVehicleIdSelected;

		var updateOdoReadingApiUrl = Config.ApiBaseUrl+'api/updateOdo/'+ Shared.packageId+"/"+vehicleId+"/"+oDOReadingValue; 
		
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: updateOdoReadingApiUrl,
			method: 'GET',
			success: function (response) {
				rs=Ext.JSON.decode(response.responseText)
				me.getODOReadingPage().down("#oDOReading").setValue();
				Ext.Viewport.setMasked(false);            	
				Ext.Msg.alert(Messages.AlertMsg,rs.result.message,function(){
					Ext.Viewport.setMasked(false); /*  close the loader */
					Shared.pushNewView('fBTDetailsPageId','fBTDetails','left');	
				});
			},
			// If Api return failed show error message.
			failure: function (response) {
				// Hide loader......
				Ext.Viewport.setMasked(false);
				Logger.alert(Messages.AlertMsg,Messages.AlertMsgServerNotRersponding);
			}
		});		
	},
	showOdoReadingPage:function(){
		
		Logger.info("showOdoReadingPage");
		Shared.pushNewView('oDOReadingPageId','oDOReading','left');
		var me=this;
		var vehicleFBT=me.getFBTDetailsPage().down("#myODOReadingButton").getData();
		
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		if(VehiclesStore.length>0)
		{
			var ArrOption= Array();
			Ext.each(VehiclesStore, function(vehicleItem,i){ 
				ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':VehiclesStore[i].vehicle.id});
			}); 
			this.getODOReadingPage().down("#selectVehicleODOReading").setOptions(ArrOption);
			
			if(me.currentVehicleIdSelected != null)
				this.getODOReadingPage().down("#selectVehicleODOReading").setValue(me.currentVehicleIdSelected);
			
			this.getODOReadingPage().down("#selectVehicleODOReading").show();
			this.getODOReadingPage().down("#selectVehicleODOReading").on({
				change:function( this_, newValue, oldValue, eOpts ){
					me.currentVehicleIdSelected = newValue;
					var fbtDetailApiUrl = Config.ApiBaseUrl+'api/fbtpage/'+ Shared.packageId + '/' +newValue;
					me.getFBTDetails(fbtDetailApiUrl);
				}
			});

		}
		
		lastOdoReading=vehicleFBT.lastOdoReading;
		lastOdoReadingDate=vehicleFBT.lastOdoReadingDate;
		this.getODOReadingPage().down("#lastReadingId").setHtml(lastOdoReading+' Km');
		this.getODOReadingPage().down("#lastReadingDate").setHtml('Last reading (on '+lastOdoReadingDate+')');
	},
	getFBTDetails:function(fbtDetailApiUrl) {
		
		Logger.info("getFBTDetails");
		var me = this;

		Ext.Viewport.setMasked(Shared.loadingMask);
		Ext.Ajax.request({
			url: fbtDetailApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {

				var responseData = Ext.JSON.decode(response.responseText);
				
				if(responseData.success){
					var vehicleFBT=responseData.result.vehicleFBT;
					lastOdoReading=vehicleFBT.lastOdoReading;
					lastOdoReadingDate = vehicleFBT.lastOdoReadingDate;
					me.getODOReadingPage().down("#lastReadingId").setHtml(lastOdoReading+' Km');
					me.getODOReadingPage().down("#lastReadingDate").setHtml('Last reading (on '+lastOdoReadingDate+')');
					Ext.Viewport.setMasked(false);
				}
			}
		});    	
	},
	showFuelCards:function(){
		
		Logger.info("showFuelCards");
		Ext.Viewport.setMasked(Shared.loadingMask);
		
		Shared.pushNewView('manageFuelCardPageId','manageFuelCard','left');
		
		var me=this;
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');		 
		if(VehiclesStore.length>0)
		{
			var ArrOption= Array();
			Ext.each(VehiclesStore, function(vehicleItem,i){ 
				ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':i});
			});
			me.currentVehicleIdSelected = VehiclesStore[0].vehicle.id;

			this.getManageFuelCardPage().down("#selectVContainer").removeAll(true,true);
			var selectfield = Ext.create('Ext.field.Select',{  
				cls:'expenditureType_1 vlabel',
				usePicker:false,
				itemId:'selectVehicle', 
				label: 'Vehicle :',
				options:ArrOption,
				listeners:{
					change:function( this_, newValue, oldValue, eOpts ){
						Ext.Viewport.setMasked(Shared.loadingMask);  
						me.setValuesInFuelCardManagePage(VehiclesStore[newValue].vehicle.id);
						me.currentVehicleIdSelected = VehiclesStore[newValue].vehicle.id;
					}
				}  
			});
			this.getManageFuelCardPage().down("#selectVContainer").add(selectfield);
			

		}
		me.setValuesInFuelCardManagePage(VehiclesStore[0].vehicle.id);
	},
	setValuesInFuelCardManagePage:function(vehicleId){
		
		Logger.info("setValuesInFuelCardManagePage");
		var me=this;

		me.getManageFuelCardPage().down("#reportLostStolenCardButton").hide();
		
		var myFuelCardApiUrl = Config.ApiBaseUrl+'api/get_fuel_cards/'+ Shared.packageId + '/' +vehicleId; 
		me.getManageFuelCardPage().down("#manageFuelCardList").removeAll();	 

		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: myFuelCardApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {
				var responseData = Ext.JSON.decode(response.responseText);
				
				if(responseData.success){
					if(responseData.result.length>0){ 
						me.setCurrentVehicleFuelCards(responseData.result);
						me.getManageFuelCardPage().down("#reportLostStolenCardButton").show();
						me.getManageFuelCardPage().down("#manageFuelCardList").setHtml('');
						Ext.each(responseData.result,function (fuelCardItem,i){ 
							var FCDetailCnt = Ext.create('Ext.Container',{
								cls :'bgwhite margin-top-10 margin-bottom-15',
								items:[{
									xtype: 'container',
									cls :'vborder pad-10',
									items:[{
										xtype: 'container',
										items:[{
											xtype: 'container',
											layout:'hbox',
											cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
											items:[{
												xtype: 'label',
												html: 'Card Number:',
												flex:1
											},{
												xtype: 'label',
												itemId:'cardNumber',
												cls:'pad-right-5 txt-right f-bold',
												flex:1,
												html:fuelCardItem.cardNumber
											}]
										},{
											xtype: 'container',
											layout:'hbox',
											cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
											items:[{
												xtype: 'label',
												html: 'Rego:',
												flex:1
											},{
												xtype: 'label',
												itemId:'rego',
												cls:'pad-right-5 txt-right f-bold',
												flex:1,
												html:fuelCardItem.regoNo
											}]
										},{
											xtype: 'container',
											layout:'hbox',
											cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
											items:[{
												xtype: 'label',
												html: 'Provider:',
												flex:1
											},{
												xtype: 'label',
												itemId:'provider',
												cls:'pad-right-5 txt-right f-bold',
												flex:1,
												html:fuelCardItem.provider
											}]
										},{
											xtype: 'container',
											layout:'hbox',
											cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
											items:[{
												xtype: 'label',
												html: 'Expiry Date:',
												flex:1
											},{
												xtype: 'label',
												itemId:'expiryDate',
												cls:'pad-right-5 txt-right f-bold',
												flex:1,
												html:fuelCardItem.expiryDate
											}]
										},{
											xtype: 'container',
											layout:'hbox',
											cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
											items:[{
												xtype: 'label',
												html: 'Status:',
												flex:1
											},{
												xtype: 'label',
												itemId:'expiryDate',
												cls:'pad-right-5 txt-right f-bold',
												flex:1,
												html:fuelCardItem.status
											}]
										}]	  
									}]
								}]

							});	 
me.getManageFuelCardPage().down("#manageFuelCardList").add(FCDetailCnt);
if(Ext.os.is.Desktop)
	me.getManageFuelCardPage().down("#manageFuelCardList").removeCls('not_av_msg');
else
	me.getManageFuelCardPage().down("#manageFuelCardList").removeCls('margin-0-10');
})
}else{
	if(Ext.os.is.Desktop)
		me.getManageFuelCardPage().down("#manageFuelCardList").addCls('not_av_msg');
	else
		me.getManageFuelCardPage().down("#manageFuelCardList").addCls('margin-0-10');
	me.getManageFuelCardPage().down("#manageFuelCardList").setHtml("No Cards");
} 
					// Hide loader......
					Ext.Viewport.setMasked(false); 
				}
			}
		});
},
showLeasingHistory:function(){
	
	Logger.info("showLeasingHistory");
	Ext.Viewport.setMasked(Shared.loadingMask);
	var me=this;
	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
	
	var myLHApiUrl = Config.ApiBaseUrl+'api/get_leasing_history/'+ Shared.packageId + '/' +VehiclesStore[0].vehicle.id; 
	Shared.pushNewView('leasingHistoryPageId','leasingHistory','left');	 
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;	

		me.getLeasingHistoryPage().down("#leasinghistory").removeAll(true,true);		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: myLHApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {
				var responseData = Ext.JSON.decode(response.responseText);
				
				if(responseData.success){
					
					if(Ext.os.is.Desktop)
						var cls = 'bgwhite pad-10 font-normal margin-bottom-15 shadow_vl';
					else
						var cls = 'bgwhite pad-10 font-normal margin-top-10 margin-bottom-15 shadow_vl';
					
					Ext.each(responseData.result,function (historyItem,i){ 
						var leaseDetailCnt = Ext.create('Ext.Container',{
							cls : cls,
							items:[{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'FBT Year:',
									flex:1
								},{
									xtype: 'label',
									itemId:'provider',
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html:historyItem.fbtYear
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Rego:',
									flex:1
								},{
									xtype: 'label',
									
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html:historyItem.regoNo
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Start Date:',
									flex:1
								},{
									xtype: 'label',
									itemId:'startDate',
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html:historyItem.startDate
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'End Date:',
									flex:1
								},{
									xtype: 'label',
									itemId:'endDate',
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html:historyItem.endDate
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 m-5-0 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Status:',
									flex:1
								},{
									xtype: 'label',
									itemId:'vehicleStatus',
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html:historyItem.status
								}]
							}]	 });	  

me.getLeasingHistoryPage().down("#leasinghistory").add(leaseDetailCnt);
})
				// Hide loader......
				Ext.Viewport.setMasked(false); 
				
			}
		}
	});
},
showMyProductsListing:function(){
	
	Logger.info("showMyProductsListing");
	var me=this;
	Ext.Viewport.setMasked(Shared.loadingMask);
	Shared.pushNewView('myProductsPageId','myProducts','left');

	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
	
	if(VehiclesStore.length>0)
	{
		var ArrOption= Array();
		Ext.each(VehiclesStore, function(vehicleItem,i){ 
			ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':i});
		}); 
		
		this.getMyProductsPage().down("#selectVContainer").removeAll(true,true);
		var selectfield = Ext.create('Ext.field.Select',{  
			cls:'expenditureType_1 vlabel',
			usePicker:false,
			itemId:'selectVehicle', 
			label: 'Vehicle :',
			options:ArrOption,
			listeners:{
				change:function( this_, newValue, oldValue, eOpts ){
					
					Logger.info("change");
					var myProductApiUrl = Config.ApiBaseUrl+'api/get_users_products/'+ Shared.packageId + '/' +VehiclesStore[newValue].vehicle.id; 
					me.getMyProductsPage().down("#myProductList").removeAll(true,true);
					Ext.Viewport.setMasked(Shared.loadingMask);
					me.getProductsDetailListing(myProductApiUrl);	
				}
			}  
		});
		this.getMyProductsPage().down("#selectVContainer").add(selectfield);			
	}

	var myProductApiUrl = Config.ApiBaseUrl+'api/get_users_products/'+ Shared.packageId + '/' +VehiclesStore[0].vehicle.id; 
	me.getMyProductsPage().down("#myProductList").removeAll(true,true);
	me.getProductsDetailListing(myProductApiUrl);

},
getProductsDetailListing: function(myProductApiUrl){

	var me = this;
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: myProductApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {
				var responseData = Ext.JSON.decode(response.responseText);
				
				if(responseData.result!=null){
					
					if(Ext.os.is.Desktop)
						var cls = 'bgwhite shadow_vl pad-10 font-normal margin-bottom-15';					
					else
						var cls = 'bgwhite shadow_vl pad-10 font-normal margin-top-10 margin-bottom-15';	
					
					Ext.each(responseData.result,function (productItem,i){ 
						var productDetailCnt = Ext.create('Ext.Container',{
							cls : cls,
							items:[{
								xtype: 'container',
								cls:'pad-left-5 margin-top-7 margin-bottom-10 font-14 f-darkgrey f-bold',
								items:[
								{
									xtype: 'label',
									html: 'Product Name : '+productItem.name,
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Provider:',
									flex:1
								},{
									xtype: 'label',
									itemId:'provider',
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html: productItem.provider
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Policy Number:',
									flex:1
								},{
									xtype: 'label',
									itemId:'policyNumber',
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html:productItem.policyNumber
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Start Date:',
									flex:1
								},{
									xtype: 'label',
									itemId:'startDate',
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html:productItem.startDate
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'End Date:',
									flex:1
								},{
									xtype: 'label',
									itemId:'endDate',
									cls:'pad-right-5 txt-right f-bold',
									flex:1,
									html:productItem.endDate
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'pad-left-5 margin-top-7 font-14 f-darkgrey',
								items:[{
									xtype: 'label',
									html: 'Status:',
									flex:0.8
								},{
									xtype: 'label',
									itemId:'status',
									cls:'pad-right-5 txt-right f-bold',
									flex:1.1,
									html:productItem.status
								}]
							}]});	  

me.getMyProductsPage().down("#myProductList").add(productDetailCnt);
me.getMyProductsPage().down("#myProductList").removeCls('not_pro_av_msg');
if(!Ext.os.is.Desktop)
	me.getMyProductsPage().down("#myProductList").removeCls('margin-10');
})
}else{
	me.getMyProductsPage().down("#myProductList").setHtml("No products");
	me.getMyProductsPage().down("#myProductList").addCls('not_pro_av_msg');
	if(!Ext.os.is.Desktop)
		me.getMyProductsPage().down("#myProductList").addCls('margin-10');
}
				// Hide loader......
				Ext.Viewport.setMasked(false); 
			}
		});
},
showVehicleDetail:function(){
	
	Logger.info("showVehicleDetail");
	Shared.pushNewView('vehicleDetailsPageId','vehicleDetails','right'); 
	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
	var me=this;
	if(VehiclesStore.length>0)
	{
		var ArrOption= Array();
		Ext.each(VehiclesStore, function(vehicleItem,i){ 
			ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':i});
		}); 
		this.getVehicleDetailsPage().down("#selectVehicle").setOptions(ArrOption);
		this.getVehicleDetailsPage().down("#selectVehicle").show();
		this.getVehicleDetailsPage().down("#selectVehicle").on({
			change:function( this_, newValue, oldValue, eOpts ){ 
				me.setValuesInVehicleDetailsPage(VehiclesStore[newValue].vehicle);
			}
		});

	}		
	this.setValuesInVehicleDetailsPage(VehiclesStore[0].vehicle); 
},
showLeaseDetailPage:function(){
	
	Logger.info("showLeaseDetailPage");
	Shared.pushNewView('leaseDetailsPageId','leaseDetails','right');
	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
	var me=this;
	if(VehiclesStore.length>0)
	{
		var ArrOption= Array();
		Ext.each(VehiclesStore, function(vehicleItem,i){ 
			ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':i});
		}); 
		this.getLeaseDetailsPage().down("#selectVehicle").setOptions(ArrOption)
		this.getLeaseDetailsPage().down("#selectVehicle").show();
		this.getLeaseDetailsPage().down("#selectVehicle").on({
			change:function( this_, newValue, oldValue, eOpts ){
				
				Logger.info(VehiclesStore[newValue]);
				me.setValuesInLeaseDetailsPage(VehiclesStore[newValue].vehicle.regoNo,VehiclesStore[newValue].vehicleLease); 	
			}
		});

	}	
	this.setValuesInLeaseDetailsPage(VehiclesStore[0].vehicle.regoNo,VehiclesStore[0].vehicleLease); 	
},
onShowFBTDetailPage:function(){
	Logger.info("onShowFBTDetailPage");
	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
	var me=this;
	if(VehiclesStore.length>0)
	{
		var ArrOption= Array();
		Ext.each(VehiclesStore, function(vehicleItem,i){ 
			ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':i});
		});
		me.currentVehicleIdSelected = VehiclesStore[0].vehicle.id;
		me.setCurrentVehicleIdSelected();
		this.getFBTDetailsPage().down("#selectVContainer").removeAll(true,true);
		var selectfield = Ext.create('Ext.field.Select',{  
			cls:'expenditureType_1 vlabel',
			usePicker:false,
			itemId:'selectVehicle', 
			label: 'Vehicle :',
			options:ArrOption,
			listeners:{
				change:function( this_, newValue, oldValue, eOpts ){
					me.setValuesInFBTDetailsPage(VehiclesStore[newValue].vehicle.id,VehiclesStore[newValue].vehicleFBT);
					me.currentVehicleIdSelected = VehiclesStore[newValue].vehicle.id;	
				}
			}  
		});
		this.getFBTDetailsPage().down("#selectVContainer").add(selectfield);

			/*this.getFBTDetailsPage().down("#selectVehicle").setOptions(ArrOption)
			this.getFBTDetailsPage().down("#selectVehicle").show();
			this.getFBTDetailsPage().down("#selectVehicle").on({
				change:function( this_, newValue, oldValue, eOpts ){ 
					me.setValuesInFBTDetailsPage(VehiclesStore[newValue].vehicle.id,VehiclesStore[newValue].vehicleFBT); 	
				}
			});*/
}	 
this.setValuesInFBTDetailsPage(VehiclesStore[0].vehicle.id,VehiclesStore[0].vehicleFBT); 	
},
moveToFBTDetailPage:function(){
	
	Logger.info("moveToFBTDetailPage");
	Shared.pushNewView('fBTDetailsPageId','fBTDetails','right');
},

setValuesInVehicleDetailsPage: function (currentVehicleObject){
	
	Logger.info("setValuesInVehicleDetailsPage");
	if(!Ext.isDefined(currentVehicleObject))
		return;

		//this.getVehicleDetailsPage().down("#vehicleNo").setHtml('Vehicle : '+currentVehicleObject.regoNo);

		this.getVehicleDetailsPage().down("#vehicleStatus").setHtml(currentVehicleObject.vehicleStatus);
		this.getVehicleDetailsPage().down("#regoState").setHtml(currentVehicleObject.regoState);
		this.getVehicleDetailsPage().down("#regoNumber").setHtml(currentVehicleObject.regoNo);
		this.getVehicleDetailsPage().down("#regoExpiry").setHtml(currentVehicleObject.regoExpiryDate);

		this.getVehicleDetailsPage().down("#vehicleMake").setHtml(currentVehicleObject.make);
		this.getVehicleDetailsPage().down("#vModel").setHtml(currentVehicleObject.model);
		this.getVehicleDetailsPage().down("#vYear").setHtml(currentVehicleObject.year);
		this.getVehicleDetailsPage().down("#vIN").setHtml(currentVehicleObject.vin);
		this.getVehicleDetailsPage().down("#engineNumber").setHtml(currentVehicleObject.engineNo);

		this.getVehicleDetailsPage().down("#policyNumber").setHtml(currentVehicleObject.policyNo);
		this.getVehicleDetailsPage().down("#provider").setHtml(currentVehicleObject.policyProvider);
		this.getVehicleDetailsPage().down("#startDate").setHtml(currentVehicleObject.policyStartDate);
		this.getVehicleDetailsPage().down("#endDate").setHtml(currentVehicleObject.policyEndDate);
		this.getVehicleDetailsPage().down("#amount").setHtml(currentVehicleObject.policyPremium);
	},
	setValuesInLeaseDetailsPage: function (vehicleNo,currentVehicleObject){
		
		Logger.info("setValuesInLeaseDetailsPage");
		//this.getLeaseDetailsPage().down("#vehicleNo").setHtml('Vehicle : '+vehicleNo);

		this.getLeaseDetailsPage().down("#leaseReferenceNumber").setHtml(currentVehicleObject.leaseRefNo);
		this.getLeaseDetailsPage().down("#financier").setHtml(currentVehicleObject.financer);
		this.getLeaseDetailsPage().down("#leaseStart").setHtml(currentVehicleObject.leaseStartDate);
		this.getLeaseDetailsPage().down("#leaseEnd").setHtml(currentVehicleObject.leaseEndDate);

		this.getLeaseDetailsPage().down("#refinanceDate").setHtml(currentVehicleObject.refinanceDate);
		this.getLeaseDetailsPage().down("#residualValue").setHtml(currentVehicleObject.residualValue);
		this.getLeaseDetailsPage().down("#netAmount").setHtml(currentVehicleObject.paymentNetAmount);
		this.getLeaseDetailsPage().down("#total").setHtml(currentVehicleObject.paymentTotalAmount);
		this.getLeaseDetailsPage().down("#frequency").setHtml(currentVehicleObject.frequency);
		this.getLeaseDetailsPage().down("#gstAmount").setHtml(currentVehicleObject.paymentGSTAmount);
	},
	setValuesInFBTDetailsPage: function (vehicleId,currentVehicleObject){ 
		
		Logger.info("setValuesInFBTDetailsPage");
		var me=this;

		Ext.Viewport.setMasked(Shared.loadingMask); 
		
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result'); 
		
		var myProductApiUrl = Config.ApiBaseUrl+'api/fbtpage/'+ Shared.packageId + '/' +vehicleId; 
		
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;
		me.getFBTDetailsPage().down("#FBTCarAni").removeAll(true,true); 			
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: myProductApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {

				var responseData = Ext.JSON.decode(response.responseText);
				
				if(responseData.success){

					var vehicleFBT=responseData.result.vehicleFBT
					var vehicleFBTtravelledKms = vehicleFBT.travelledKms;
					vehicleFBTtravelledKms  = vehicleFBTtravelledKms.replace(/,/g , "");	
					vehicleFBTtravelledKms  = vehicleFBTtravelledKms.replace(/km/g , "");		

					var vehicleFBTtargetKms = vehicleFBT.targetKms;
					vehicleFBTtargetKms  = vehicleFBTtargetKms.replace(/,/g , "");	
					vehicleFBTtargetKms  = vehicleFBTtargetKms.replace(/km/g , "");		
					
					Logger.info("t:"+vehicleFBTtravelledKms+"tar:"+vehicleFBTtargetKms)
					var onBudgetZeroStyle='';
					if(parseInt(vehicleFBTtargetKms) >= parseInt(vehicleFBTtravelledKms))
						{	var percentTotal='100';
					var percentRun=parseInt((vehicleFBTtravelledKms/vehicleFBTtargetKms)*100);
					console.log("t"+percentRun);
				}else{
					var percentRun='100';
					var percentTotal=parseInt((vehicleFBTtargetKms/vehicleFBTtravelledKms)*100)
					if(vehicleFBT.targetKms==0){
						onBudgetZeroStyle='display:none;';
					}
				}
				console.log("t"+percentTotal);

				var htmlStr='<div class="carCompoWrapper"> <div class="meter-vehicle" id="testingId" >' 	
				+'<div class="textDiv" style="width: '+percentTotal+'%;min-width:25%;" >'
				+'<span class="meterStartText">0</span>'
				+'<span class="meterEndText" style="'+onBudgetZeroStyle+'">Required</span>'
				+'<div class="clearBoth"></div>'
				+'</div>'
				
				+'<span class="outer" style="width:100%"><div class="strips"></div></span>'
				+'<span class="greenColor" style="width: '+percentTotal+'%;display:none;"></span>'
				+'<span class="inner" id="meter-vehicle-span-inner" style="width: '+percentRun+'%;display:none;"></span>'
				+'<span class="TravelledLabel">Travelled</span>'
				+'</div></div>';	

				var carAniObj = Ext.create('Ext.Container',{
					height:'100px',
					html:htmlStr});
				me.getFBTDetailsPage().down("#FBTCarAni").add(carAniObj);
				me.getFBTDetailsPage().down("#travelledKms").setHtml(vehicleFBT.travelledKms);
				
				me.getFBTDetailsPage().down("#remainingKms").setHtml(vehicleFBT.remainingKms);
				me.getFBTDetailsPage().down("#averageKmsRequired").setHtml(vehicleFBT.averageKmsRequired);
				me.getFBTDetailsPage().down("#remainingDays").setHtml(vehicleFBT.remainingDays);
				me.getFBTDetailsPage().down("#myODOReadingButton").setHtml('My ODO Reading- '+vehicleFBT.lastOdoReading);
				me.getFBTDetailsPage().down("#myODOReadingButton").setData(vehicleFBT);
				if(!Ext.os.is.Phone)
					me.getFBTDetailsPage().down("#kmStatusHeader").setHtml(vehicleFBT.travelledKms+" of "+vehicleFBT.targetKms);
				else
					me.getFBTDetailsPage().down("#kmStatusHeader").setHtml("Kms Travelled (FBT Year) "+vehicleFBT.travelledKms+" of "+vehicleFBT.targetKms);
				
				if(vehicleFBT.hasFBTLiability){
					me.getFBTDetailsPage().down("#residualValue").hide(); 
				}
				else{ 
					me.getFBTDetailsPage().down("#residualValue").show(); 
				}

				AnimateVehicle("testingId");   
					// Hide loader......
					Ext.Viewport.setMasked(false); 
				}
			}
		}); 
},
goToLeasingDetails:function(){
	
	Logger.info("goToLeasingDetails");
	Shared.pushNewView('leasingDetailsPageId','leasingDetails','left');	
},
goToTransactiopnsScreen:function(){
	
	Logger.info("goToTransactiopnsScreen") 
	Shared.pushNewView('vehiclesExpenditureTransactionsPageId','vehiclesExpenditureTransactions','right');
},
showTransactionDetail:function(dataview, index, target, record, e, options){
	
	Logger.info("showTransactionDetail") 
	Shared.pushNewView('vehiclesTransactionsDetailPageId','vehiclesTransactionsDetail');
},
showHideDateContainer:function(){
	
	Logger.info("showHideDateContainer") 
	var me =this;
	var datecontainer = me.getBudgetBreakdownPageId().down('#budgetBreakdownDateContainer');
	if(datecontainer.isHidden()){
		datecontainer.show();
		if(me.getBudgetBreakdownPageId().down('#customDateRangeButton').element.hasCls('dateselected')){
			me.getBudgetBreakdownPageId().down('#budgetBreakdowncustomDateRange').show();	
		}
	}
	else{
		me.getBudgetBreakdownPageId().down('#budgetBreakdowncustomDateRange').hide();
		datecontainer.hide();
	}
},
showHideCustomDateRangeContainer:function(){
	
	Logger.info("showHideCustomDateRangeContainer") 
	var me =this;
	var customDateRange = me.getBudgetBreakdownPageId().down('#budgetBreakdowncustomDateRange');
	if(customDateRange.isHidden())
		customDateRange.show();
	else
		customDateRange.hide();
},
budgetBreakDownList:function(_this){ 
	
	Logger.info("budgetBreakDownList") 
	var accordionButton = 6;
	var buttonItemId = _this._itemId;
	var buttonNo = buttonItemId.split("_");
	Ext.getCmp('budgetTypeList').getItems('button').each(function(key, value, myself){
		if(key.action=="manageBudgetAccordionButtonPress"){
			key.removeCls("fa-chevron-down"); 
		}
	});	
	for(var i=1;i<=accordionButton;i++){
		if(buttonNo[1]==i){
			if(Ext.getCmp('accordionContainer_'+i).isHidden()){
				_this.addCls("fa-chevron-down");
				Ext.getCmp('accordionContainer_'+i).show();
			}else{
				_this.removeCls("fa-chevron-down");
				Ext.getCmp('accordionContainer_'+i).hide();
			}
		}else{
			Ext.getCmp('accordionContainer_'+i).hide();
		}
	}
},
showHidevehicleExpenditureCustomDateRangeContainer:function(){
	
	Logger.info("showHidevehicleExpenditureCustomDateRangeContainer") 
	this.showHideDateRengeCalendar('vehicleExpenditureDateRange');
},
bBreakdownDateChangeButtonPress:function(_this){
	
	Logger.info("bBreakdownDateChangeButtonPress") 
	var me =this;
	me.getBudgetBreakdownPageId().down('#budgetBreakdownDateContainer').getItems('button').each(function(key, value, myself){
		if(key._itemId!=_this._itemId){
			key.removeCls('dateselected'); 
			key.setStyle("background-color:#e7e7e7;color:#595b77;");
			me.getBudgetBreakdownPageId().down('#budgetBreakdowncustomDateRange').hide();
		}else{
			key.addCls('dateselected');
			key.setStyle("background-color:#3cbe9f;color:#fff;");
			if(me.getBudgetBreakdownPageId().down('#customDateRangeButton').element.hasCls('dateselected'))	me.getBudgetBreakdownPageId().down('#budgetBreakdowncustomDateRange').show();
				//Add delay task to hide the date container.
				var task = Ext.create('Ext.util.DelayedTask',function(){
					if(!me.getBudgetBreakdownPageId().down('#customDateRangeButton').element.hasCls('dateselected'))		me.getBudgetBreakdownPageId().down('#budgetBreakdownDateContainer').hide();
				});
				task.delay(1500); 
			}
		});

	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
	var vehicleId=VehiclesStore[this.getBudgetBreakdownPageId().down("#selectVehicle").getValue()].vehicle.id;
	
	if(_this._itemId=='liveToDateButton')
	{
		me.getBudgetBreakdownPageId().down("#currentFilterLabel").setHtml("Live To Date");
		me.setValuesInBudgetBreakdownPage(vehicleId,1);
	}else if(_this._itemId=='last12monthsButton')
	{
		me.getBudgetBreakdownPageId().down("#currentFilterLabel").setHtml("Last 12 Months");
		me.setValuesInBudgetBreakdownPage(vehicleId,2);
	}else if(_this._itemId=='fBTYearButton')
	{
		me.getBudgetBreakdownPageId().down("#currentFilterLabel").setHtml("FBT Year");
		me.setValuesInBudgetBreakdownPage(vehicleId,3);
	}
},
customDatesButtonPressBudget:function(){
	
	Logger.info("customDatesButtonPressBudget") 
	var me=this; 
	var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
	var vehicleId=VehiclesStore[this.getBudgetBreakdownPageId().down("#selectVehicle").getValue()].vehicle.id;
	me.setValuesInBudgetBreakdownPage(vehicleId,4);
	me.getBudgetBreakdownPageId().down('#budgetBreakdownDateContainer').hide();
	me.getBudgetBreakdownPageId().down('#budgetBreakdowncustomDateRange').hide();
		//me.getBudgetBreakdownPageId().down('#customDateRangeButton').removeCls('dateselected');  
	},
	customDatesButtonPress:function(){
		
		Logger.info("customDatesButtonPress") 
		var me=this; 
		
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		var vehicleId=VehiclesStore[this.getVlOverviewPage().down("#selectVehicle").getValue()].vehicle.id;
		me.setValuesInOverviewPage(vehicleId,4);
		me.getVlOverviewPage().down('#vehicleExpenditureDateContainer').hide();
		me.getVlOverviewPage().down('#vehicleExpenditureDateRange').hide();
	},
	showHidevehicleExpenditureDateContainer:function(){
		
		Logger.info("showHidevehicleExpenditureDateContainer") 
		var me =this;
		var datecontainer = me.getVlOverviewPage().down("#vehicleExpenditureDateContainer");
		if(datecontainer.isHidden()){
			datecontainer.show();
			if(me.getVlOverviewPage().down('#vEcustomDateRangeButton').element.hasCls('dateselected'))
				me.getVlOverviewPage().down('#vehicleExpenditureDateRange').show();	
			else
				me.getVlOverviewPage().down('#vehicleExpenditureDateRange').hide();
		}
		else{
			me.getVlOverviewPage().down('#vehicleExpenditureDateRange').hide();
			datecontainer.hide();
		}
	},
	vExpenditureDateChangeButtonPress:function(_this){
		
		Logger.info("vExpenditureDateChangeButtonPress") 
		var me=this;
		me.getVlOverviewPage().down('#vehicleExpenditureDateContainer').getItems('button').each(function(key, value, myself){
			if(key._itemId!=_this._itemId){
				key.removeCls('dateselected'); 
				me.getVlOverviewPage().down('#vehicleExpenditureDateRange').hide();
				key.setStyle("background-color:#e7e7e7;color:#595b77;");
			}else{
				key.addCls('dateselected');
				key.setStyle("background-color:#3cbe9f;color:#fff;");
				if(me.getVlOverviewPage().down('#vEcustomDateRangeButton').element.hasCls('dateselected'))					me.getVlOverviewPage().down('#vehicleExpenditureDateRange').show();	
				//create the delayed task to hide the date container.
				var task = Ext.create('Ext.util.DelayedTask',function(){
					if(!me.getVlOverviewPage().down('#vEcustomDateRangeButton').element.hasCls('dateselected'))		me.getVlOverviewPage().down('#vehicleExpenditureDateContainer').hide();
				});
				task.delay(1500); 
			}
		});
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		var vehicleId=VehiclesStore[this.getVlOverviewPage().down("#selectVehicle").getValue()].vehicle.id;
		
		if(_this._itemId=='liveToDateButton')
		{
			me.getVlOverviewPage().down("#currentFilterLabel").setHtml("Live To Date");
			me.setValuesInOverviewPage(vehicleId,1);
		}else if(_this._itemId=='last12monthsButton')
		{
			me.getVlOverviewPage().down("#currentFilterLabel").setHtml("Last 12 Months");
			me.setValuesInOverviewPage(vehicleId,2);
		}else if(_this._itemId=='fBTYearButton')
		{
			me.getVlOverviewPage().down("#currentFilterLabel").setHtml("FBT Year");
			me.setValuesInOverviewPage(vehicleId,3);
		}
	},
	vEcustomDateRangeButtonAction:function(){
		
		Logger.info("vEcustomDateRangeButtonAction") 
		//this.showHideDateRengeCalendar('vehicleExpenditureDateRange');	
	},
	showHideDateRengeCalendar:function(DateRange){ 
		
		Logger.info("showHideDateRengeCalendar") 
		var customDateRange=Ext.getCmp(DateRange);
		if(customDateRange.isHidden())
			customDateRange.show();
		else
			customDateRange.hide();
	}, 
	toogleAccordion : function(optionNumber,totalOptions){
		    	
		Logger.info("toogleAccordion") 
		var claimListViewObj=this.getAvailableProductsView(); // get the page object 
		
		var currentOptionRefParent = claimListViewObj.down("#availableProductsListing"+optionNumber);
		var currentOptionRef = currentOptionRefParent.getParent() 
		    
		var currentOptionContainer = currentOptionRef.down('#availableProductsListing'+optionNumber);
		var currentOptionBtn = currentOptionRef.down('#availableProductsListingButton'+optionNumber);
		
		
		if(currentOptionContainer.getHidden()){     
			    	
			for(var optionsCount=1;optionsCount<=totalOptions;optionsCount++)
			{
				
				var currentOptionRefParentTemp = claimListViewObj.down("#availableProductsListing"+optionsCount);
				var currentOptionRefTemp = currentOptionRefParentTemp.getParent() 
				    
				var currentOptionContainerTemp = currentOptionRefTemp.down('#availableProductsListing'+optionsCount);
				var currentOptionBtnTemp = currentOptionRefTemp.down('#availableProductsListingButton'+optionsCount);
				
				if(!(currentOptionContainerTemp.getHidden()))
				{
					currentOptionContainerTemp.hide();
					currentOptionBtnTemp.removeCls("fa-chevron-down");
				}
			}
			
			currentOptionContainer.show();
			currentOptionBtn.removeCls("claimStepRightArrow");
			currentOptionBtn.addCls("fa-chevron-down");
		}
		    else {
			
			currentOptionContainer.hide();
			currentOptionBtn.removeCls("fa-chevron-down");
		}
	},
	readMoreDetail : function(productItem){
		
		Logger.info("readMoreDetail") 
		Shared.pushNewView('vehicleLeasingAvailableProductsDetailPageId','vehicleLeasingAvailableProductsDetail');
		this.getProductsDetailView().down("#productNameLabel").setHtml(productItem.name);
		this.setCProductSelected(productItem); 
	},
	backToAvailableProductListing:function (){	
		
		Logger.info("backToAvailableProductListing") 	
		Shared.pushNewView('vehicleLeasingAvailableProductsPageId','vehicleLeasingAvailableProducts');		
	},
	backToFbtDetails: function(){
		
		Logger.info("backToFbtDetails") 	
		Shared.pushNewView('vehicleLeasingAvailableProductsPageId','vehicleLeasingAvailableProducts');
	},
	backToVehicleLeasingDaysUnavailableAdd: function(){
		
		Logger.info("backToVehicleLeasingDaysUnavailableAdd") 	
		Shared.pushNewView('vehicleLeasingDaysUnavailableAddPageId','vehicleLeasingDaysUnavailableAdd');
	},
	backToVehicleLeasingDaysUnavailable: function(){
		
		Logger.info("backToVehicleLeasingDaysUnavailable") 	
		Shared.pushNewView('vehicleLeasingDaysUnavailablePageId','vehicleLeasingDaysUnavailable');
	},
	goToVehicleLeasingDaysUnavailableConfirm: function(){
		
		Logger.info("goToVehicleLeasingDaysUnavailableConfirm") 	
		
		var vehicle =this.getVehicleLeasingDaysUnavailableAddPageId().down('#vehicleLeasingDaysUnavailableAddNextBtnPress').getData(vehicle);		
		var ReasonId=this.getVehicleLeasingDaysUnavailableAddPageId().down('#reasonsDropDown').getValue();
		
		var dropOff=this.getVehicleLeasingDaysUnavailableAddPageId().down('#daysUnavailableDropOffCalendarId').getValue();
		var pickup=this.getVehicleLeasingDaysUnavailableAddPageId().down('#daysUnavailablePickUpCalendarId').getValue();
		var startDate=new Date(dropOff).getTime();
		var endDate=new Date(pickup).getTime();
		var ONE_DAY = 1000 * 60 * 60 * 24
		
	    // Calculate the difference in milliseconds
	    var difference_ms = Math.abs(startDate - endDate)

	    var numberDays= Math.round(difference_ms/ONE_DAY);
	    
	    if(startDate>endDate)
	    {	
	    	Logger.alert(Messages.AlertMsg,Messages.AlertMsgInvalidDateRange);
	    	return;
	    }
	    else
	    	Shared.pushNewView('vehicleLeasingDaysUnavailableConfirmScreenPageId','vehicleLeasingDaysUnavailableConfirmScreen');
	    
	    var reason="";
	    Ext.each(this.getUnAvailableReasons(), function(reasons,i){ 
	    	if(ReasonId==reasons.id)
	    	{
	    		reason=reasons.name
	    	}
	    });
	    
	    var htmlStr ="Vehicle "+vehicle.regoNo+" will be unavailable from "+dropOff+" to "+pickup+" for a total "+numberDays+" day(s) due to "+reason

	    this.getVehicleUnAvailableAddConfirmPage().down('#confirmText').setHtml(htmlStr);
	},
	addDaysUnAvailable:function(){
		Ext.Viewport.setMasked(Shared.loadingMask);
		var me=this;  
		var vehicle =this.getVehicleLeasingDaysUnavailableAddPageId().down('#vehicleLeasingDaysUnavailableAddNextBtnPress').getData(vehicle);		
		var vehicleId=vehicle.id;
		var ReasonId=this.getVehicleLeasingDaysUnavailableAddPageId().down('#reasonsDropDown').getValue(); 
		var dropOff=this.getVehicleLeasingDaysUnavailableAddPageId().down('#daysUnavailableDropOffCalendarId').getValue();
		var pickup=this.getVehicleLeasingDaysUnavailableAddPageId().down('#daysUnavailablePickUpCalendarId').getValue();
		var startDate=new Date(dropOff).getTime();
		var endDate=new Date(pickup).getTime();

		var fileData = Shared.fileData || {};
		var addDaysUnAvApiUrl = Config.ApiBaseUrl+'api/addDaysUnAvailable/'+ Shared.packageId+"/"+vehicleId+"/"+ReasonId+"/"+startDate+"/"+endDate; 
		
		var formData = new FormData();
		var i=0;
		
		Logger.info(fileData)
		Ext.each(fileData, function(value,key){	
			
			Logger.info(value);    	 
			formData.append(i++,value); // appending all the files to the formData
		});	
		
		Logger.info(formData)
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: addDaysUnAvApiUrl,             
			data:formData,
			type:'POST',
			success: function (response) {
				Shared.fileData=null;
				rs=Ext.JSON.decode(response.responseText)
				Logger.alert(Messages.AlertMsg,rs.result.message);
				Ext.Viewport.setMasked(false);
				me.daysUnavailablePage();
			},
			// If Api return failed show error message.
			failure: function (response) {
				// Hide loader......
				Ext.Viewport.setMasked(false);
				Logger.alert(Messages.AlertMsg,Messages.AlertMsgServerNotRersponding);
			}
		}); 
		//Shared.pushNewView('vehicleLeasingDaysUnavailablePageId','vehicleLeasingDaysUnavailable');
	},
	goToVehicleLeasingDaysUnavailableAdd: function(){
		var me=this;
		
		Logger.info("goToVehicleLeasingDaysUnavailableAdd") 	
		Shared.pushNewView('vehicleLeasingDaysUnavailableAddPageId','vehicleLeasingDaysUnavailableAdd');
		var ArrOption= Array();
		Ext.each(this.getUnAvailableReasons(), function(reasons,i){ 
			ArrOption.push({'text':reasons.name,'value':reasons.id});
		}); 
		var vehicle = me.getDaysUnavailablePage().down("#vehicleLeasingDaysUnavailableAddBtnPress").getData();
		
		this.getVehicleLeasingDaysUnavailableAddPageId().down('#vehicleDetailText').setHtml("Vehicle "+vehicle.regoNo+" will be unavailable");	
		this.getVehicleLeasingDaysUnavailableAddPageId().down('#reasonsDropDown').setOptions(ArrOption);
		this.getVehicleLeasingDaysUnavailableAddPageId().down('#vehicleLeasingDaysUnavailableAddNextBtnPress').setData(vehicle);
		
	},
	goToVehicleLeasingAvailableProductUserDetail : function(){
		
		Logger.info("goToVehicleLeasingAvailableProductUserDetail") 
		Shared.pushNewView('vehicleLeasingAvailableProductsUserLogedInDetailPageId','vehicleLeasingAvailableProductsUserLogedInDetail');
		this.getProductsEnquiryUserDetailView().down("#cProductLable").setHtml(this.getCProductSelected().name);
		
		var userObj=Ext.getStore('User_S').getAt(0);
		this.getProductsEnquiryUserDetailView().down("#user_name").setHtml(userObj.get('FirstName'));
		this.getProductsEnquiryUserDetailView().down("#user_email").setHtml(userObj.get('Email'));
		this.getProductsEnquiryUserDetailView().down("#user_contact").setHtml(userObj.get('WorkPhone')); 
		
	},
	goToVehicleLeasingAvailableProductThankyou : function(){
		
		Logger.info("goToVehicleLeasingAvailableProductThankyou") 
		Ext.Viewport.setMasked(Shared.loadingMask);
		var me=this;  
		var cProduct=this.getCProductSelected();
		var productId=cProduct.id 
		var updateProductEnquiryApiUrl = Config.ApiBaseUrl+'api/productEnquiry/'+ Shared.packageId+"/"+productId; 
		
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: updateProductEnquiryApiUrl,
			method: 'GET',
			success: function (response) {
				rs=Ext.JSON.decode(response.responseText)
				Logger.alert(Messages.AlertMsg,rs.result.message);
				Shared.pushNewView('vehicleLeasingAvailableProductsThankyouPageId','vehicleLeasingAvailableProductsThankyou');
				me.getProductsEnquirySubmittedView().down("#productsNameLabel").setHtml(me.getCProductSelected().name);
				Ext.Viewport.setMasked(false);
			},
			// If Api return failed show error message.
			failure: function (response) {
				// Hide loader......
				Ext.Viewport.setMasked(false);
				Logger.alert(Messages.AlertMsg,Messages.AlertMsgServerNotRersponding);
			}
		});
		
	},
	backToVehicleLeasingAvailableProductDetail : function(){
		
		Logger.info("backToVehicleLeasingAvailableProductDetail") 
		Shared.pushNewView('vehicleLeasingAvailableProductsDetailPageId','vehicleLeasingAvailableProductsDetail');
	},
	backToAvailableProductUserDetail : function(){
		
		Logger.info("backToAvailableProductUserDetail") 
		Shared.pushNewView('vehicleLeasingAvailableProductsUserLogedInDetailPageId','vehicleLeasingAvailableProductsUserLogedInDetail');
	},
	backToVehicleLeasingCardLostOrStolen : function(){
		
		Logger.info("backToVehicleLeasingCardLostOrStolen") 
		Shared.pushNewView('vehicleLeasingLostOrStolenCardsPageId','vehicleLeasingLostOrStolenCards');
	},
	backToVehicleLeasingCardStolenOption : function(){
		
		Logger.info("backToVehicleLeasingCardStolenOption") 
		Shared.pushNewView('vehicleLeasingLostOrStolenCardsWhatHappenOptionsPageId','vehicleLeasingLostOrStolenCardsWhatHappenOptions');
	},	
	reportCardStolenSubmit : function(){ 
		
		Logger.info("reportCardStolenSubmit") 
		this.reportCardlostStolenRequest('10'); // status id 10 is for stolen of card
		
	},
	reportCardLostSubmit : function(){
		
		Logger.info("reportCardLostSubmit") 
		this.reportCardlostStolenRequest('9'); // status id 9 is for lost of card
		
	}, 
	backToVehicleLeasingOrderNewCard : function(){
		
		Logger.info("backToVehicleLeasingOrderNewCard")
		Shared.pushNewView('manageFuelCardPageId','manageFuelCard');
	},
	showVehicleExpenditureTransaction : function(){ 
		Ext.Viewport.setMasked(Shared.loadingMask);
		var me = this;
		
		Logger.info("showVehicleExpenditureTransaction");
		
		Shared.addCalendar('transactionsFromCalendar','From','vehiclesTransactionsFromCalendar',"tr_0",null);
		Shared.addCalendar('transactionsToCalendar','To','vehiclesTransactionsToCalendar',"tr_1",null);
		var vehicleTransactionPage = this.getVehiclesExpenditureTransactionsPageId();
		
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');
		if(VehiclesStore.length>0)
		{
			var ArrOption= Array();
			Ext.each(VehiclesStore, function(vehicleItem,i){ 
				ArrOption.push({'text':vehicleItem.vehicle.regoNo,'value':VehiclesStore[i].vehicle.id});
			}); 
			vehicleTransactionPage.down("#selectVehicleExpenditureTransaction").setOptions(ArrOption);
			
			if(me.currentVehicleIdSelected != null)
				vehicleTransactionPage.down("#selectVehicleExpenditureTransaction").setValue(me.currentVehicleIdSelected);	
			
			vehicleTransactionPage.down("#selectVehicleExpenditureTransaction").on({
				change:function( this_, newValue, oldValue, eOpts ){ 
					me.currentVehicleIdSelected = newValue;
					me.resetVehicleTransactions();
				}
			});

		}

		var vehicleExpenditureList = vehicleTransactionPage.down('#vehicleExpenditureTypes');
		vehicleExpenditureList.setOptions(null);
		vehicleExpenditureList.setOptions(this.availableVehicleExpenses);
		if(this.getLastSelectedVehicleTransaction()!=null)
			vehicleExpenditureList.setValue(this.getLastSelectedVehicleTransaction());
		
		var startDate = '';
		var endDate = '';
		var expenseId = this.getLastSelectedVehicleTransaction();
		
		if(expenseId == "" || expenseId == null)
			expenseId = vehicleExpenditureList.getValue();
		
		var overviewApiUrl = Config.ApiBaseUrl+'api/vehicleTransaction/'+ Shared.packageId + '/' +this.currentVehicleIdSelected+'/'+expenseId+'/'+startDate+'/'+endDate;
		
		Logger.info(overviewApiUrl);
		this.setVehicleExpenditureTransactions(overviewApiUrl);
	},
	resetVehicleTransactions : function(){
		
		Logger.info("resetVehicleTransactions");
		Ext.Viewport.setMasked(Shared.loadingMask);
		Shared.addCalendar('transactionsFromCalendar','From','vehiclesTransactionsFromCalendar',"tr_0",null);
		Shared.addCalendar('transactionsToCalendar','To','vehiclesTransactionsToCalendar',"tr_1",null);

		var vehicleTransactionPage = this.getVehiclesExpenditureTransactionsPageId();

		var vehicleExpenditureList = vehicleTransactionPage.down('#vehicleExpenditureTypes');
		vehicleExpenditureList.setValue(this.availableVehicleExpenses[0].value);
		
		var startDate = '';
		var endDate = '';
		expenseId = vehicleExpenditureList.getValue();
		
		var overviewApiUrl = Config.ApiBaseUrl+'api/vehicleTransaction/'+ Shared.packageId + '/' +this.currentVehicleIdSelected+'/'+expenseId+'/'+startDate+'/'+endDate;
		
		Logger.info(overviewApiUrl);
		this.setVehicleExpenditureTransactions(overviewApiUrl);
	},
	vehicleTransactionsCustomDatesButton : function(){
		
		Logger.info("vehicleTransactionsCustomDatesButton");
		var me = this;
		var vehicleTransactionPage = this.getVehiclesExpenditureTransactionsPageId();
		Ext.Viewport.setMasked(Shared.loadingMask);
		
		var vehicleTransactionPage = this.getVehiclesExpenditureTransactionsPageId();
		
		var vehicleExpenditureList = vehicleTransactionPage.down('#vehicleExpenditureTypes');
		var startDate = '';
		var endDate = '';
		
		startDate = new Date(vehicleTransactionPage.down("#vehiclesTransactionsFromCalendar").getValue()).getTime();
		endDate = new Date(vehicleTransactionPage.down("#vehiclesTransactionsToCalendar").getValue()).getTime();
		
		var currentDate = new Date();
		currentDate = Ext.Date.format(currentDate, 'd M Y');
		
		Logger.info("formated" + currentDate);
		currentDate = new Date(currentDate).getTime();
		
		if((startDate.toString() == currentDate.toString()) && (endDate.toString() == currentDate.toString()))
		{
			startDate = '';
			endDate = '';
		}
		
		var overviewApiUrl = Config.ApiBaseUrl+'api/vehicleTransaction/'+ Shared.packageId + '/' +this.currentVehicleIdSelected+'/'+vehicleExpenditureList.getValue()+'/'+startDate+'/'+endDate;
		
		Logger.info(overviewApiUrl);
		this.setVehicleExpenditureTransactions(overviewApiUrl);
		
	},
	setVehicleExpenditureTransactions : function(overviewApiUrl){
		
		Logger.info("setVehicleExpenditureTransactions");
		
		var me = this;
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		
		var vehicleTransactionPage = this.getVehiclesExpenditureTransactionsPageId(); // get the page object 
		var objVehicleTrasactionListing=vehicleTransactionPage.down("#vehicleExpenditureTransactionList");
		objVehicleTrasactionListing.setEmptyText('');

		if(Ext.getStore('VehicleTransactions')){
			Ext.getStore('VehicleTransactions').destroy();
			objVehicleTrasactionListing.setStore(null); 
		}
		var pageListSize = null;
		
		if(Ext.os.is.Tablet)
		{
			pageListSize = 10;
		}
		else if (Ext.os.is.Desktop) {
			pageListSize = 20;
		}
		else
		{
			pageListSize = 5;
		}
		var objVehicleTransactionStore=Ext.create("Ext.data.Store",{
			storeId: "VehicleTransactions",
			model: "SmartSalary.model.VehicleTransaction_M",
			autoLoad:true,
			pageSize: pageListSize, 
			proxy:
			{
				type: 'ajax',
				url : overviewApiUrl,
				headers:{"Authorization" :"Basic " + Shared.base64variable},
				reader: {
					type: 'json', 
					rootProperty:'result.transactions',
					totalProperty: 'result.totalRows'
				}				
			},
			listeners: {
				load: function (store,records,success,operation,eOpts) {
					if(success){
						
						Logger.info("loaded sucessfully");
						if(me.vehicleTransactionButtonClick)
						{
							var vehicleTransactionPgaeObj = me.getVehiclesTransactionsDetailPageId();
							me.onTransactionDetailPrevNext(vehicleTransactionPgaeObj.down('#nextTransactionButton'));
							me.vehicleTransactionButtonClick = false;
						}		
						
					}
					else{
						
						Logger.info("error in store");
						Ext.Viewport.setMasked(false);	
					}
					Ext.Viewport.setMasked(false);
					objVehicleTrasactionListing.setEmptyText('Transaction(s) not found');
				}
			} 
		});

me.setVehicleTrasactionListStoreAfterLoad();
},
setVehicleTrasactionListStoreAfterLoad: function(){
	
	Logger.info('setVehicleTrasactionListStoreAfterLoad');
	
		var vehicleTransactionPage = this.getVehiclesExpenditureTransactionsPageId(); // get the page object 

		var objVehicleTrasactionListing=vehicleTransactionPage.down("#vehicleExpenditureTransactionList");
		
		objVehicleTrasactionListing.setStore(Ext.getStore("VehicleTransactions"));
		
		if(!objVehicleTrasactionListing.getPlugins() && Ext.getStore('VehicleTransactions').getCount()==0){
			
			Logger.info("plugin applyed");
			
			objVehicleTrasactionListing.setPlugins({
				xclass : 'Ext.plugin.ListPaging',
				autoPaging : true,
				loadMoreText: '<span class="font-tablet-loadMore">Load More...</span>',
				noMoreRecordsText: 'End of list',
				id:'vehiclePluginId'
			});
		}
		if(Ext.getStore('VehicleTransactions').getCount()==0){
			var myObj=Ext.getCmp('vehiclePluginId');
			myObj.getLoadMoreCmp().hide(); 
		}
	},
	reportCardlostStolenRequest:function(status){
		
		Logger.info("reportCardlostStolenRequest") 
		var me=this; 
		
		var VehiclesStore=Ext.getStore('VehiclesStore').getAt('0').get('result');

		var record=this.getCurrentFuelCard();
		var cardId=record.getId()
		
		var vehicleId=VehiclesStore[this.getManageFuelCardPage().down("#selectVehicle").getValue()].vehicle.id;
		
		var reportCardStatusApiUrl = Config.ApiBaseUrl+'api/updateFuelCardStatus/'+ Shared.packageId+"/"+vehicleId+"/"+cardId+"/"+status; 
		
		Logger.info(reportCardStatusApiUrl);
		
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: reportCardStatusApiUrl,
			method: 'GET',
			success: function (response) {
				rs=Ext.JSON.decode(response.responseText)
				Logger.alert(Messages.AlertMsg,rs.result.message);
				Shared.pushNewView('vehicleLeasingLostOrStolenCardsAcceptedRequestPageId','vehicleLeasingLostOrStolenCardsAcceptedRequest');
			},
			// If Api return failed show error message.
			failure: function (response) {
				// Hide loader......
				Ext.Viewport.setMasked(false);
				Logger.alert(Messages.AlertMsg,Messages.AlertMsgServerNotRersponding);
			}
		});
	},
	availableProductListingGenFunction:function(){
		
		Logger.info("availableProductListingGenFunction")
		Ext.Viewport.setMasked(Shared.loadingMask);
		var me=this; 
		me.getAvailableProductsView().down("#availableProductsListing").removeAll(true,true);
		var myAvailableProductsApiUrl = Config.ApiBaseUrl+'api/getAvailableProducts/'+ Shared.packageId; 
		// Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;		
		// Api call for salary packaging data.
		Ext.Ajax.request({
			url: myAvailableProductsApiUrl,
			headers:{"Authorization" :"Basic " + Shared.base64variable}, 
			timeout:Config.serverRequsetTimeoutTime,
			method: 'GET',
			success: function (response) {
				var responseData = Ext.JSON.decode(response.responseText); 
				if(responseData.success){
					var totalproducts=responseData.result.length;

					Ext.each(responseData.result,function (productItem,i){ 
						var j=i+1;
						var product = Ext.create('Ext.Container',{ 
							cls: 'pad-5',
							items : [
							{
								xtype: 'button',
								text: productItem.name,
								itemId: 'availableProductsListingButton'+j,
								cls: 'button_grey_vl_2 claimStepButton fa fa-chevron-right txt-left font-16 pad-10',
								listeners : {
									element : 'element',
									tap : function(){			                    	
										me.toogleAccordion(j,totalproducts)
									}
								}
							},
							{
								xtype: 'container',
								itemId: 'availableProductsListing'+j,
								hidden: true,
								cls:'font-14 pad-10 fa-vl border3sides',
								items: [
								{
									xtype: 'container',
									html: '<p>Extend your factory warranty for piece of mind.</p>'
								},
								{
									xtype: 'button',
									data: {'product':productItem},
									action: 'availableProductsReadMoreBtnPress',
									itemId: 'availableProductsReadMoreBtnPress',
									cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
									html: 'Read more',
									listeners:{
										tap:function(){
											me.readMoreDetail(this.getData().product);
										}
									}
								}]
							}]
						});

me.getAvailableProductsView().down("#availableProductsListing").add(product);
});
            		// Hide loader......
            		Ext.Viewport.setMasked(false);
            	}
            }
        })
},
onTransactionListItemTap : function( _this, index, target, record, e, eOpts){
	var me=this;
	Shared.pushNewView('vehiclesTransactionsDetailPageId','vehiclesTransactionsDetail');
	
	var vehicleTransactionPgaeObj = this.getVehiclesTransactionsDetailPageId();
	vehicleTransactionPgaeObj.down('#transactionDate').setHtml(record.get('transactionDate'));
	vehicleTransactionPgaeObj.down('#transactionDescription').setHtml(record.get('description'));
	vehicleTransactionPgaeObj.down('#transactionAmount').setHtml(record.get('amount'));
	
	this.adjustTransactionNextPrev(index);
	
},
adjustTransactionNextPrev : function(index){
	var vehicleTransactionPgaeObj = this.getVehiclesTransactionsDetailPageId();
	
	if(index>0)
	{
		vehicleTransactionPgaeObj.down('#previousTransactionButton').setData({'index':(index-1)});
		
		vehicleTransactionPgaeObj.down('#nextTransactionButton').setData({'index':(index+1)});
	}
	else
	{
		vehicleTransactionPgaeObj.down('#previousTransactionButton').setData({'index':0});
		
		vehicleTransactionPgaeObj.down('#nextTransactionButton').setData({'index':1});
	}
},
onTransactionDetailPrevNext : function(_this){
	
	var me=this;
	
	var record = Ext.getStore('VehicleTransactions').getAt(_this.getData().index);
	var vehicleTransactionPgaeObj = this.getVehiclesTransactionsDetailPageId();
	
	if(!(typeof(record) == "undefined"))
	{
		
		vehicleTransactionPgaeObj.down('#transactionDate').setHtml(record.get('transactionDate'));
		vehicleTransactionPgaeObj.down('#transactionDescription').setHtml(record.get('description'));
		vehicleTransactionPgaeObj.down('#transactionAmount').setHtml(record.get('amount'));
		
		this.adjustTransactionNextPrev(_this.getData().index);
	}
	else{
		if(!((Ext.getStore('VehicleTransactions').getTotalCount()) == (Ext.getStore('VehicleTransactions').getAllCount())))
		{	
			Ext.Viewport.setMasked(Shared.loadingMask);
			
			Ext.getCmp('vehiclePluginId').loadNextPage();
			me.vehicleTransactionButtonClick = true;
		}
	}
},
pushManageFuelCardsView : function(){
	Shared.pushNewView('manageFuelCardPageId','manageFuelCard');
},


/**My Chnages*/

showPersonalDetail :function(){
	Shared.pushNewView('personalDetailScreenId','personalDetailView');
}

});








