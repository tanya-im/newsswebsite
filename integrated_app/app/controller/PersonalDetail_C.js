Ext.define('SmartSalary.controller.PersonalDetail_C', {
	extend : 'Ext.app.Controller',
	config: {
		refs:{
			/*** Accordian Button Reference*/
			personalDetailAccordianButton:'button[itemId=btnAccordian1]',
			contactDetailAccordianButton:'button[itemId=btnAccordian2]',
			addressAccordianButton:'button[itemId=btnAccordian3]',
			changeMyPasswordAccordianButton:'button[itemId=btnAccordian4]',
			bankingDetailAccordianButton:'button[itemId=btnAccordian5]',
			payrollDetailAccordianButton:'button[itemId=btnAccordian6]',
			
			/**Save and Cancel button Reference*/
			personalDetailSaveButton:'button[itemId=btnPersonalDetailSave]',
			contactDetailSaveButton:'button[itemId=btnContactDetailSave]',
			addressSaveButton:'button[itemId=btnAddressSave]',
			changeMyPasswordSaveButton:'button[itemId=btnPasswordSave]',
			bankingDetailSaveButton:'button[itemId=btnPersonalBankDetailSave]',
			payrollDetailSaveButton:'button[itemId=btnPayrollDetailSave]',
			
     		personalDetailCancelButton:'button[itemId=btnPersonalDetailCancel]',
			contactDetailCancelButton:'button[itemId=btnContactDetailCancel]',
			addressCancelButton:'button[itemId=btnAddressCancel]',
			changeMyPasswordCancelButton:'button[itemId=btnPasswordCancel]',
			bankingDetailCancelButton:'button[itemId=btnPersonalBankDetailCancel]',
			payrollDetailCancelButton:'button[itemId=btnPayrollDetailCancel]',
			
			newPasswordTextField:'textfield[itemId=txtPasswordNewPassword]',
		},
		control:{
			/*** Accordian Button Contol*/
			personalDetailAccordianButton:  {tap : 'accordianButtonTapped'},
			contactDetailAccordianButton:   {tap : 'accordianButtonTapped'},
			addressAccordianButton:         {tap : 'accordianButtonTapped'},
			changeMyPasswordAccordianButton:{tap : 'accordianButtonTapped'},
			bankingDetailAccordianButton:   {tap : 'accordianButtonTapped'},
			payrollDetailAccordianButton:   {tap : 'accordianButtonTapped'},
			
			/**Save and Cancel button Actions*/
			personalDetailSaveButton:  {tap : 'personalDetailSaveButtonTapped'},
			contactDetailSaveButton:   {tap : 'contactDetailSaveButtonTapped'},
			addressSaveButton:         {tap : 'addressSaveButtonTapped'},
			changeMyPasswordSaveButton:{tap : 'changeMyPasswordSaveButtonTapped'},
			bankingDetailSaveButton:   {tap : 'bankingDetailSaveButtonTapped'},
			payrollDetailSaveButton:   {tap : 'payrollDetailSaveButtonTapped'},
			
			personalDetailCancelButton:	 {tap : 'personalDetailCancelButtonTapped'},
			contactDetailCancelButton:	 {tap : 'contactDetailCancelButtonTapped'},
			addressCancelButton:		 {tap : 'addressCancelButtonTapped'},
			changeMyPasswordCancelButton:{tap : 'changeMyPasswordCancelButtonTapped'},
			bankingDetailCancelButton:	 {tap : 'bankingDetailCancelButtonTapped'},
			payrollDetailCancelButton:	 {tap : 'payrollDetailCancelButtonTapped'},
			
			newPasswordTextField:{keyup : 'newPasswordKeyUp'}
		} 	  
	},
	accordianButtonTapped:function (button,e,eOpts){
		
		var parentContainer=Ext.getCmp('personalDetailScreenId');
		
		var totalOptions=6;
		/**Current button and container which is active/deactive by button*/				
		var currentOptionContainer = parentContainer.down("#hideUnhideAccordianContainer"+button.getData().buttonNumber);
		 
		var currentOptionBtn = parentContainer.down('#btnAccordian'+button.getData().buttonNumber);
		/*************************/
		
		/**Login for hide and unhide the container*/
		if(currentOptionContainer.getHidden()){     
			    	
			for(var optionsCount=1;optionsCount<=totalOptions;optionsCount++)
			{
				var optionContainer = parentContainer.down("#hideUnhideAccordianContainer"+optionsCount);
				    
				var optionButton = parentContainer.down('#btnAccordian'+optionsCount);
				
				if(!(optionContainer.getHidden()))
				{
					optionContainer.hide();
					optionButton.removeCls("fa-chevron-down");
				}
			}
			currentOptionContainer.show();
			//currentOptionBtn.removeCls("claimStepRightArrow");
			currentOptionBtn.addCls("fa-chevron-down");
		}
		    else {
			currentOptionContainer.hide();
			currentOptionBtn.removeCls("fa-chevron-down");
		}
		
		//x-unsized x-button x-button-none button_ltgrey_vl fa fa-chevron-right fa-circle-o fa-circle-o-darkblue txt-left font-16 pad-10 margin-top-10
		
		//x-unsized x-button x-button-none button_ltgrey_vl fa fa-chevron-right fa-circle-o fa-circle-o-darkblue txt-left font-16 pad-10 margin-top-10 fa-chevron-down
		
		
	},
	
	/**Save and Cancel button Actions*/
	personalDetailSaveButtonTapped:function(){
	},
	contactDetailSaveButtonTapped:function(){
	},
	addressSaveButtonTapped:function(){
	},
	changeMyPasswordSaveButtonTapped:function(){
	},
	bankingDetailSaveButtonTapped:function(){
	},
	payrollDetailSaveButtonTapped:function(){
	},
	personalDetailCancelButtonTapped:function(){
	},
	contactDetailCancelButtonTapped:function(){
	},
	addressCancelButtonTapped:function(){
	},
	changeMyPasswordCancelButtonTapped:function(){
	},
	bankingDetailCancelButtonTapped:function(){
	},
	payrollDetailCancelButtonTapped:function(){
	},
	
	newPasswordKeyUp:function(){
		animatePasswordStrength(250);
	}
});