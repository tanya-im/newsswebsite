Ext.define('SmartSalary.controller.Superannuation_C', {
    extend: 'Ext.app.Controller',
    config: {
        refs:{
        	/*-----------------SuperannuationHome refs---------------------*/
        	superannuationHome:'container[itemId=homeSuperannuation]',	
        	GSEBWestStateButton:'button[itemId=GSEBWestStateButton]',
        	addANewContributionButtonPress:'button[itemId=addANewContributionButton]',
        	clickToUpdateFundDetail:'button[itemId=clickToUpdateFundDetail]',
			/*-----------------SuperannuationHome refs---------------------*/
        	
        	/*------SuperannuationAddNewContributionList------*/
			backToSuperannuationHomePress : 'button[action=backToSuperannuationHome]',
			superannuationCommercialSuperFundPress : 'button[action=superannuationCommercialSuperFundPress]',
			superannuationAddNewContributionListPage : 'container[itemId=superannuationAddNewContributionListPageId]',
			superannuationSelfManagedSuperFundPress : 'button[action=superannuationSelfManagedSuperFundPress]',


			/*-----SuperannuationAddComercialSuperfund-------*/
			backToAddSuperannuationContributionListPress : 'button[itemId=backToAddSuperannuationContributionList]',
			addCommercialSuperFundBackBtnPress : 'button[action=addCommercialSuperFundBackBtn]',
        	superannuationAddCommercialSuperFundPage : 'container[itemId=superannuationAddCommercialSuperFundPageId]',
        	/*-----------------GSEBFundDetailSuperannuation refs---------------------*/
        	GEBSuperannuation:'container[itemId=GEBSuperannuation]',
        	backToSuperannuationHome:'button[action=backToSuperannuationHomePress]',
			
			cantFindMySuperFundButtonPress:'button[action=cantFindMySuperFundButtonPress]',
			/*-----------------completeFundDetailSuperannuation refs---------------------*/
        	completeFundDetailSuperannuation:'container[itemId=completeFundDetailSuperannuation]',
        	
        	/*-----------------completeFundDetailSuperannuation refs---------------------*/
        	
        	backToGEBSuperannuation:'button[action=backToGEBSuperannuationPress]',	
        	
        	/*-----------------showContributionSuperannuation refs---------------------*/
        	contributionDetailSuperannuation:'container[itemId=updateContributionDetailSuperannuation]',		
        	updateMyContributionButton:'button[itemId=updateMyContributionButton]',
        	stopMyContributionButton:'button[itemId=stopMyContributionButton]',
        	backToSuperannuationScreen:'button[action=backToSuperannuationScreenPress]',
			/*-----------------UpdateContactDetailSuperannuation refs---------------------*/
        	updateContactDetailSuperannuation:'container[itemId=updateContactDetailSuperannuation]',	
        	backToUpdateContributionScreenPress:'button[action=backToUpdateContributionScreenPress]',	

        	/*-------superannuationAddNewFundDetail----------*/
        	superannuationAddNewFundDetailPageShow: 'container[itemId=superannuationAddNewFundDetailPageId]',
        	superfundDetailBackBtnPress:'button[itemId=superfundDetailBackBtn]',
        	superfundDetailSelfManagedBackBtnPress : 'button[itemId=superfundDetailSelfManagedBackBtn]'
		 },
		control:{
			/*----------------SuperannuationHome controls----------------- */
			superannuationHome:{show:'superannuationHomeLoad'},
			GSEBWestStateButton:{tap:'PushGEBSuperannuationView'},
			addANewContributionButtonPress:{tap:'goToAddNewContribution'},
			clickToUpdateFundDetail:{ tap:'goToGEBSuperannuationPush'},
			/*----------------SuperannuationHome controls----------------- */
			
			/*------SuperannuationAddNewContributionList------*/
			backToSuperannuationHomePress : {tap : 'goToSuperannuationHome'},
			superannuationCommercialSuperFundPress : {tap : 'goToAddCommercialSuperFund'},
			//superannuationAddNewContributionListPage : {show : 'superannuationContributionListPageShow'},
			superannuationSelfManagedSuperFundPress : {tap : 'goToSelfManagedAddSuperFundDetailPage'},

			/*-----SuperannuationAddComercialSuperfund-------*/
			backToAddSuperannuationContributionListPress : {tap : 'goToAddNewContribution'},
			addCommercialSuperFundBackBtnPress : {tap : 'goToAddNewContribution'},
			superannuationAddCommercialSuperFundPage : {show : 'superannuationAddCommercialSuperFundPageShow'},
			cantFindMySuperFundButtonPress : {tap : 'goToCantFindAddSuperFundDetailPage'},


			/*----------------SuperannuationHome controls----------------- */
			GEBSuperannuation:{ show:'GEBSuperannuationLoad'},
			backToSuperannuationHome:{ tap:'backToSuperannuationHome'},
			
			/*----------------completeFundDetailSuperannuation controls----------------- */
			backToGEBSuperannuation:{ tap:'backToGEBSuperannuation'},
			
			/*-----------------showContributionSuperannuation controls---------------------*/

        	//contributionDetailSuperannuation:{ show:'contributionDetailSuperannuationLoad' },
        	updateMyContributionButton:{ tap:'updateContactDetailSuperannuationLoadwithUpdateFund' },
        	backToSuperannuationScreen:{tap:'goToSuperannuationHome'},
        	backToUpdateContributionScreenPress:{ tap:'goBackToUpdateContributionScreen'}, 
        	stopMyContributionButton:{ tap:'updateContactDetailSuperannuationLoadwithStopFund' },

			
			/*-------superannuationAddNewFundDetailPageId----------*/
                        superannuationAddNewFundDetailPageShow:{show:'mySuperFundAddNewDetailsPageShow'},
                        superfundDetailBackBtnPress:{tap:'goToAddCommercialSuperFund'},
                        superfundDetailSelfManagedBackBtnPress:{tap:'goToAddNewContribution'}
		}
	},
	superannuationHomeLoad:function(){
		var me =this;
		Shared.addCalendar('superannuationFromCalendar','From','superannuationFromCalendarId',"DA_0",null);
		Shared.addCalendar('superannuationToCalendar','To','superannuationToCalendarId',"DA_1",null);
		var dataWapperCmp = me.getSuperannuationHome().down("#superannuationFundContributionDetailHome");
		dataWapperCmp.removeAll(true,true);
		for(var i=0;i<4;i++){
			var lblField=Ext.create('Ext.Label',{
				html:'Toatl contribution last 12 month',
				cls:'pad-top-10',
			});
			var valField=Ext.create('Ext.Label',{
				html:'$6201.00',
				cls:'f-bold font-22'
			});
			dataWapperCmp.add(lblField);
			dataWapperCmp.add(valField);	
		}
		var dataButtonWapperCmp = me.getSuperannuationHome().down("#fundContributionButtonsHome");
		dataButtonWapperCmp.removeAll(true,true);
		for(var i=0;i<3;i++){
			var ButtonField=Ext.create('Ext.Button',{
				text: 'GSEB - West State',
				itemId: 'GSEBWestStateButton'+i,
				action: 'GSEBWestStateButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
				listeners:{ tap:function(_this){
					me.contributionDetailSuperannuationLoad(_this);
				}}
			});
			dataButtonWapperCmp.add(ButtonField);
		}
		me.getSuperannuationHome().down("#selectVContainerSuperannuation").removeAll(true,true);
		var ArrOption= Array(); 
		ArrOption.push({text: 'All',  value: 'all'},{text: 'Deductions', value: 'deductions'},{text: 'Payments',  value: 'payments'}); 
		var selectfield = Ext.create('Ext.field.Select',{  
			cls:'expenditureType_1 vlabel',
			usePicker:false,
			itemId:'paymentType', 
			label: '',
			options:ArrOption
		});
		me.getSuperannuationHome().down("#selectVContainerSuperannuation").add(selectfield);
	},
	PushGEBSuperannuationView:function(){
		Shared.pushNewView('GEBSuperannuation','GEBSuperannuation','left');
	},
	goToAddNewContribution: function(){
		Shared.pushNewView('superannuationAddNewContributionListPageId','superannuationaddnewcontributionlist','right');
	},
	goToSuperannuationHome: function(){
		Shared.pushNewView('homeSuperannuation','homeSuperannuation','right');
	},
	goToAddCommercialSuperFund: function(){
		Shared.pushNewView('superannuationAddCommercialSuperFundPageId','superannuationaddcommercialsuperfund','right');
	},
	goToGEBSuperannuationPush: function(){
		Shared.pushNewView('GEBSuperannuation','GEBSuperannuation','left');
	},
	goBackToUpdateContributionScreen:function(){
		Shared.pushNewView('updateContributionDetailSuperannuation','updateContributionDetailSuperannuation','right');  
	},
	superannuationAddCommercialSuperFundPageShow: function(){
		var commercialSuperFundPageObj = this.getSuperannuationAddCommercialSuperFundPage();
		
		var superAnnuationFund = Array();
		var length = Ext.getStore('SuperannuationSuperFunds').getCount();
		
		for (var i = 0; i < length; i++) {
			var recordObj = Ext.getStore('SuperannuationSuperFunds').getAt(i).data;
			superAnnuationFund.push( {'text' : recordObj.name, 'value': recordObj.abn});
		};

		var commercialSuperFundStartDate = [
							{text: '16/12/2014',  value: '16/12/2014'},
							{text: '16/12/2014',  value: '16/12/2014'},
							{text: '16/12/2014',  value: '16/12/2014'}
		];
		var commercialSuperFundEndDate = [
							{text: '17/12/2014',  value: '17/12/2014'},
							{text: '17/12/2014',  value: '17/12/2014'},
							{text: '17/12/2014',  value: '17/12/2014'},
							{text: 'Ongoing',  value: 'ongoing'}
		];
		commercialSuperFundPageObj.down('#commercialSuperFundList').setOptions(superAnnuationFund);
		commercialSuperFundPageObj.down('#commercialSuperFundStartDate').setOptions(commercialSuperFundStartDate);
		commercialSuperFundPageObj.down('#commercialSuperFundEndDate').setOptions(commercialSuperFundEndDate);
	},	
	GEBSuperannuationLoad:function(){
		var me = this;
		var fundIncomleteButtonsCnt = me.getGEBSuperannuation().down("#fundIncomleteButtonsCnt");	
		fundIncomleteButtonsCnt.removeAll(true,true);
		for(var i=0;i<3;i++){
			var ButtonField=Ext.create('Ext.Button',{
				text: 'GSEB - West State'+i,
				itemId: 'GSEBWestStateButton'+i,
				action: 'GSEBWestStateButtonPress',
				cls:'button_greyblue_vl claimStepButton fa fa-chevron-right txt-left font-16 pad-10 margin-top-10',
				listeners:{ tap:function(_this){
					me.completeFundDetailSuperannuationLoad(_this);
				}}
			});
			fundIncomleteButtonsCnt.add(ButtonField);
		}
	},
	backToSuperannuationHome:function(){
		Shared.pushNewView('homeSuperannuation','homeSuperannuation','right');
	},
	backToGEBSuperannuation:function(){
		Shared.pushNewView('GEBSuperannuation','GEBSuperannuation','right');
	},
	completeFundDetailSuperannuationLoad:function(_this){
		Shared.pushNewView('completeFundDetailSuperannuation','completeFundDetailSuperannuation','left');
		this.getCompleteFundDetailSuperannuation().down("#fundNameCompleteScreen").setHtml(_this.getText());
	},
	contributionDetailSuperannuationLoad:function(_this){
		console.log(_this.getText());
		var me = this;
		Shared.pushNewView('updateContributionDetailSuperannuation','updateContributionDetailSuperannuation','left');
		var dataWapperCmp = me.getContributionDetailSuperannuation().down("#superannuationFundContributionDetailBox");
		//dataWapperCmp.removeAll(true,true);
                var contributionDetailSuperannuationObj = me.getContributionDetailSuperannuation();
                console.log(contributionDetailSuperannuationObj);
                console.log(contributionDetailSuperannuationObj.down("#fundContributionName"));
                contributionDetailSuperannuationObj.down("#fundContributionName").setHtml("Catholic super fund 231765");
                contributionDetailSuperannuationObj.down("#fundContributionUSILabel").setHtml("8231765");
                contributionDetailSuperannuationObj.down("#fundContributionABNLabel").setHtml("63 212 212 121");
                contributionDetailSuperannuationObj.down("#fundContibutionPayFrequencyLabel").setHtml("Fortnightly");
                contributionDetailSuperannuationObj.down("#fundContibutionTotalContributionLabel").setHtml("$19,500.00");
		/*for(var i=0;i<5;i++){
			var lblField=Ext.create('Ext.Label',{
				html:'Toatl contribution last 12 month',
				cls:'pad-top-10',
			});
			dataWapperCmp.add(lblField);
		}*/	
	},
	goToCantFindAddSuperFundDetailPage : function(){
		Shared.pushNewView('superannuationAddNewFundDetailPageId','superannuationaddnewfunddetail','right');
		var cantFindMySuperFundAddPageObj = this.getSuperannuationAddNewFundDetailPageShow();
		Ext.getCmp('superfundDetailBackBtn').setItemId('superfundDetailBackBtn');
		cantFindMySuperFundAddPageObj.down('#superfundEsaUsi').setHtml('USI*');
	},
	goToSelfManagedAddSuperFundDetailPage : function(){
		Shared.pushNewView('superannuationAddNewFundDetailPageId','superannuationaddnewfunddetail','right');
		var selfManagedAddSuperFundDetailPageObj = this.getSuperannuationAddNewFundDetailPageShow();
		Ext.getCmp('superfundDetailBackBtn').setItemId('superfundDetailSelfManagedBackBtn');
		selfManagedAddSuperFundDetailPageObj.down('#superfundEsaUsi').setHtml('ESA*');
	},
	mySuperFundAddNewDetailsPageShow :function(){
		var cantFindMySuperFundAddPageObj = this.getSuperannuationAddNewFundDetailPageShow();
		var mySuperFundStartDate = [
							{text: '16/12/2014',  value: '16/12/2014'},
							{text: '16/12/2014',  value: '16/12/2014'},
							{text: '16/12/2014',  value: '16/12/2014'}
		];
		var mySuperFundEndDate = [
							{text: '17/12/2014',  value: '17/12/2014'},
							{text: '17/12/2014',  value: '17/12/2014'},
							{text: '17/12/2014',  value: '17/12/2014'},
							{text: 'Ongoing',  value: 'ongoing'}
		];
		cantFindMySuperFundAddPageObj.down('#superfundDetailStartDate').setOptions(mySuperFundStartDate);
		cantFindMySuperFundAddPageObj.down('#superfundDetailEndDate').setOptions(mySuperFundEndDate);
	},	
	updateContactDetailSuperannuationLoadwithUpdateFund:function(){
		var me = this;
		Ext.Msg.confirm(Messages.AlertMsg,Messages.AlertMsgSuperannuationContribution,function(btn){
			if(btn!='yes'){
				return false;  
			}else{
				var fundEndDate = me.getContributionDetailSuperannuation().down("#fundEndDate").getValue();
				var contributionAmount = me.getContributionDetailSuperannuation().down("#contributionAmount").getValue();
				console.log(fundEndDate+"::"+contributionAmount);
				Shared.pushNewView('updateContactDetailSuperannuation','updateContactDetailSuperannuation','left');
				me.getUpdateContactDetailSuperannuation().down("#USIDetail").setHtml("2314562");
				me.getUpdateContactDetailSuperannuation().down("#ABNDetail").setHtml("63 212 212 212");
				me.getUpdateContactDetailSuperannuation().down("#StartDateContactDetail").setHtml("20/11/2014");
				me.getUpdateContactDetailSuperannuation().down("#contactEmail").setHtml("garyn@smartsalary.com");
				me.getUpdateContactDetailSuperannuation().down("#contactNumber").setValue("0459 989 699");
				me.getUpdateContactDetailSuperannuation().down("#endDateincontactDetail").setHtml(fundEndDate);
				me.getUpdateContactDetailSuperannuation().down("#updateAmountincontactDetail").setHtml(contributionAmount);
			}	
		});	
		
	},
	updateContactDetailSuperannuationLoadwithStopFund:function(){
		var me = this;
		Ext.Msg.confirm(Messages.AlertMsg,Messages.AlertMsgSuperannuationContribution,function(btn){
			if(btn!='yes'){
				return false;
			}else{
			}	
		});	
		
	}

});