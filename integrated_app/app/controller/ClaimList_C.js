Ext.define('SmartSalary.controller.ClaimList_C', {
    extend : 'Ext.app.Controller',
 	itemId:'ClaimList_C', 
 	id:'ClaimList_C',
 	toggleFlag :true,
	setFilter :false,
 	claimReasons:null,
	startDate:'',
	endDate:'',
 	config: {
		refs:{

			/***** claim listing page  refs ****/
			claimListView:'container[itemId=claimlistscreen]',
			searchField: 'textfield[itemId=searchField]',
			searchBtn:'image[itemId=srchBtn]',
			listingControl:'list[itemId=claimlisting_page]',
			filterListingControl:'list[itemId=claimListingFilter]',
			selectClaimType:'selectfield[itemId=selectClaimType]', 
			selectDate:'textfield[itemId=selectDate]', 
			selectStatus:'selectfield[itemId=selectStatus]',
			searchInfoBtn:'image[itemId=searchInfoBtn]',
			dateField: 'textfield[itemId=selectDate]',
			claimListBackBtn:'button[itemId=claimListBackBtn]', 
			//

			/***** claim detail listing page  refs ****/
			claimDetailPageView:'container[itemId=ClaimDetailPageId]',
			detailListing:'list[itemId=claimdetailList_page]',
			backBtn:'button[itemId=backBtn]',
			// 

			/***** claim data page refs ****/
			claimDataPageView:'container[itemId=ClaimDataIdTab]',
			reasonBtn:'button[itemId=reasonBtn]',
			dataBackBtn:'button[itemId=dataBackBtn]', 
			//

			/***** claim reasons page refs ****/
			claimReasonsList:'list[itemId=claimReasonsListId]',			
			reasonsBackBtn:'button[itemId=reasonsBackBtn]', 
			//
			
		},
		control:{

			/***** claim listing page  refs ****/
			searchField:{clearicontap:'searchFieldCancel'},
			searchBtn:{tap:'searchBtnClick'},
			claimListView:{	show:'listingControlInit'},
			listingControl:{itemtap: 'onListItemTap'},
			filterListingControl:{itemtap: 'onListItemTap'},
			selectClaimType:{change:'claimChange'},
			selectDate:{change:'claimChange'},
			selectStatus:{change:'claimChange'},
			claimListBackBtn:{tap:'moveToDashboard'},
			searchInfoBtn:{ tap:'trackMyclaimSearchInfo'},
			dateField:{clearicontap:'dateFieldCancel'},
			//
			
			/***** claim detail listing page  refs ****/
			backBtn:{tap:'backButtonTapped'},
			detailListing:{itemtap:'onDetailListItemTap'},
			//

			/***** claim data page refs ****/
			dataBackBtn:{tap:'backButtonDataTapped'},
			reasonBtn:{tap:'reasonBtnClick'},
			//

			/***** claim reasons page refs ****/
			reasonsBackBtn:{tap:'reasonsBackBtnClick'}, 
			//
			
		}
 	},
 	moveToDashboard: function(){				 
		Shared.pushNewView('dashboardscreen','dashboardView','right');
	},
 	reasonsBackBtnClick: function (){ 
	  	Shared.pushNewView('ClaimDataId','ClaimDataView','right');
 	},
 	reasonBtnClick: function(record){ 	
 		var me=this; 
		Shared.pushNewView('ClaimReasonsPageId','ClaimReasonsView','left'); 
		Logger.debug(this.claimReasons)
		var ClaimReasonsList=me.getClaimReasonsList();
		Logger.debug(ClaimReasonsList)
		if(ClaimReasonsList.getStore()){
			ClaimReasonsList.getStore().removeAll();
		}
		ClaimReasonsList.setData(this.claimReasons);
		ClaimReasonsList.refresh();
		ClaimReasonsList.getStore().load();
		Logger.debug(ClaimReasonsList.getData());

 	},
 	backButtonTapped: function(){
 		Logger.info("backButtonTapped function");	 	 
		Shared.pushNewView('claimlistscreen','claimList','right');
	},
	backButtonDataTapped: function(){

		Logger.info("backButtonDataTapped function");
	 	 
		Shared.pushNewView('ClaimDetailPageId','ClaimDetailView','right');
 	},
 	// on click of the claims detail

 	onDetailListItemTap: function ( _this, index, target, record, e, eOpts){
	 	var me=this; 
	 	Shared.pushNewView('ClaimDataId','ClaimDataView','left');

	 	var claimDataPageViewObj=this.getClaimDataPageView();

		claimDataPageViewObj.down("#reasonBtn").hide();

 		if(record.get('ClaimTypeId')==1)// meal claim
 		{
		 	var Fields=record.get("ExpenseTypeFields").ExpenseTypeField
			//Active content view.  
		}else if(record.get('ClaimTypeId')==2)// capped claim
		{
			var Fields=record.get("ExpenseTypeFields").ExpenseTypeField

		}else if(record.get('ClaimTypeId')==3)// vehicle claim
		{
			var Fields=record.get("VehicleTransactionTypeFields").VehicleTransactionTypeField

			var VehicleNumber=this.getClaimDetailPageView().down("#VehicleNumber").getHtml();

			claimDataPageViewObj.down("#VehicleNumberData").setHtml(VehicleNumber);
		}
 
		claimDataPageViewObj.down("#claimTypeAndRefNum").setHtml(record.get("claimTypeAndRefNum"));

		var claimdataList_page=claimDataPageViewObj.down("#claimDataContainer");
		
		claimdataList_page.removeAll(true,true);

		Ext.each(Fields,function (iObj){
			 var ClaimDataCnt = Ext.create('Ext.Container',{
								layout:'hbox',
								cls:'pad-10-0',
								items:[{
									xtype: 'label',
									html: iObj.Name,
									flex:3
								},{
									xtype: 'label',
									html: iObj.Value,
									cls:'txt-right',
									flex:3
								}]
			});
			claimdataList_page.add(ClaimDataCnt);  
		}); 

		var ExpenseReason=Array()

		if(record.get("ExpenseReasons").length==0){
			ExpenseReason=false;
		}
		else if(record.get("ExpenseReasons").ExpenseReason.length>1){
			ExpenseReason=record.get("ExpenseReasons").ExpenseReason;
		}
		else
			ExpenseReason[0]=record.get("ExpenseReasons").ExpenseReason;
	 	
		if(ExpenseReason)
		{	 		
			this.claimReasons=ExpenseReason; 
			claimDataPageViewObj.down("#reasonBtn").show();
		}

 	},
 	onListItemTap: function ( _this, index, target, record, e, eOpts){

 		var me=this;

		var claimName='';

		Shared.pushNewView('ClaimDetailPageId','ClaimDetailView');

		var ClaimDetailPageObj=this.getClaimDetailPageView();

 		var objClaimsStore=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');
 		
 		var claimExpenses=Array();  

 		if(record.get("ClaimTypeId")==1) // meal claim
 		{
 			var Expenses=record.get("Expenses").Expense;
		 	
			if(record.get("Expenses").Expense.length>1){ 
				claimExpenses = Expenses;
			}else { 
				claimExpenses[0]= Expenses; 
			} 
			var expenseTypes=objClaimsStore.MealClaim.ExpenseTypes.ExpenseType; 
  		
			Ext.each(claimExpenses,function (vObj){
				//claimAndRefType
				vObj.claimTypeAndRefNum=expenseTypes.Name+" - "+record.get("ClaimReferenceNumber");
				vObj.AmountToBePaid=(vObj.hasOwnProperty('AmountToBePaid'))?vObj.AmountToBePaid:0;
				vObj.Amount =  (vObj.hasOwnProperty('Amount'))?parseFloat(vObj.Amount).toFixed(2):0;
				vObj.ClaimTypeId =  record.get("ClaimTypeId");
			});
 		}
 		else if(record.get("ClaimTypeId")==2)// capped  claim
 		{
 			var Expenses=record.get("Expenses").Expense; 

			if(record.get("Expenses").Expense.length>1){ 
				claimExpenses = Expenses;
			}else { 
				claimExpenses[0]= Expenses; 
			}
			var expenseTypes=objClaimsStore.ThresholdClaim.ExpenseTypes.ExpenseType

			 
			Logger.debug(expenseTypes)
			Ext.each(claimExpenses,function (vObj){
				//claimAndRefType
				Logger.debug(vObj.ExpenseTypeId)
				Logger.debug(expenseTypes[vObj.ExpenseTypeId]);
				
				vObj.claimTypeAndRefNum=vObj.BenefitName+" - "+record.get("ClaimReferenceNumber");
				vObj.AmountToBePaid=(vObj.hasOwnProperty('AmountToBePaid'))?vObj.AmountToBePaid:0;
				vObj.Amount = (vObj.hasOwnProperty('Amount'))?parseFloat(vObj.Amount).toFixed(2):0;
				vObj.ClaimTypeId =  record.get("ClaimTypeId");
			});
 			 
 		}else if(record.get("ClaimTypeId")==3) // vehicle claim
 		{
 			var VehicleExpenses=record.get("VehicleExpenses").VehicleExpense;
		 	
			if(record.get("VehicleExpenses").VehicleExpense.length>1){ 
				claimExpenses = VehicleExpenses;
			}else { 
				claimExpenses[0]= VehicleExpenses; 
			}

			var VehicleExpenses=objClaimsStore.VehicleClaim.VehicleTransactionTypes.VehicleTransactionType;
			 
			var Vehicle=objClaimsStore.VehicleClaim.Vehicles.Vehicle
		    if(claimExpenses[0].VehicleId){
		    	var vehicleNumber='';
		    	Logger.debug(Vehicle)
			    Ext.each(Vehicle,function(vehObj){
			    	 Logger.debug(vehObj.Id+"=="+claimExpenses[0].VehicleId)
			    	 if(vehObj.Id==claimExpenses[0].VehicleId){
			    	 	Logger.debug(vehObj.Name)
			    	 	vehicleNumber=vehObj.Name;	
			    	 }
			    })

			    ClaimDetailPageObj.down("#VehicleNumber").setHtml(Messages.claimForVehicle+vehicleNumber);
			}
			Ext.each(claimExpenses,function (vObj){
				
				Ext.Array.each(VehicleExpenses,function (exObj,index){
					if(exObj.Id == vObj.VehicleTransactionTypeId)
					{
						claimName = exObj.Name;
						return false; // break here
					}
				});
				 
				vObj.claimTypeAndRefNum=claimName+" - "+record.get("ClaimReferenceNumber");
				vObj.AmountToBePaid=(vObj.hasOwnProperty('AmountToBePaid'))?vObj.AmountToBePaid:0;
				vObj.Amount = parseFloat(vObj.Amount).toFixed(2);
				vObj.ClaimTypeId =  record.get("ClaimTypeId");
			});
		     
 		}
 		 
 		Logger.debug(claimExpenses);
	 	//Active content view.
	   
		var claimdetailList_page= this.getDetailListing();	
		   
		if(claimdetailList_page.getStore()){
			claimdetailList_page.getStore().removeAll();
		} 
		claimdetailList_page.setData(claimExpenses);
	    claimdetailList_page.refresh(); 
     
    },
	claimFilteredList: function(claimQueryString){

		var claimListStore = Ext.getStore("ClaimListFull");
		claimListStore.setProxy({headers:{"Authorization" :"Basic " + Shared.base64variable}});
		claimListStore.getProxy().setTimeout(Config.serverRequsetTimeoutTime);
		
		var claimListViewObj=this.getClaimListView(); // get the page object
		
		var claimListAll = claimListViewObj.down('#claimlisting_page');
		var claimListFilter = claimListViewObj.down('#claimListingFilter');
		
		claimListFilter.setHidden(false);
		claimListAll.setHidden(true);
			
		claimListStore.getProxy().setUrl(claimQueryString);
		claimListStore.load({
			callback : function(){
				Ext.Viewport.setMasked(false);
			}	
		
		});
			
		claimListFilter.refresh();
	}, 
 	claimChange: function(selectbox,newValue,oldValue){

		var me = this;
		
		var claimListViewObj=this.getClaimListView(); // get the page object

		var claimType=claimListViewObj.down('#selectClaimType').getValue();
		var dateSelected=claimListViewObj.down('#selectDate').getValue();
		var statusSelected=claimListViewObj.down('#selectStatus').getValue();
		
		if(selectbox.getId() == "selectDate")
		{	
			newdate = Ext.Date.parse(newValue,"d/m/Y",true);
			if(newdate!=null && Ext.Date.isValid(newdate.getFullYear(),newdate.getMonth(),newdate.getDate()))
			{
				me.startDate = Ext.Date.format(newdate,'d/m/Y');
				newdate = Ext.Date.add(newdate,Ext.Date.DAY,1);	
				me.endDate = Ext.Date.format(newdate,'d/m/Y');
			}
			else{
				if(newValue.length>0)
				{
					Logger.alert(Messages.AlertMsg,Messages.AlertMsgInvalidDateFormatMsg);
					return;
				}
			}
		}
		
		Logger.info("claimChange called");

	 	var userStoreObj = Ext.getStore('User_S');

		var PackageId = userStoreObj.getAt(0).get('PackageId');
		
		var claimListAll = claimListViewObj.down('#claimlisting_page');
		var claimListFilter = claimListViewObj.down('#claimListingFilter');
		
		if(!(selectbox.getData().initdata))
		{
			Ext.Viewport.setMasked(true); 
			
			var claimQueryString = Config.ApiBaseUrl+'api/claims/'+PackageId+'?filter=1';
			
			if(claimType==0 && statusSelected==0 && !(dateSelected.length>0)){ 
				claimListAll.setHidden(false);
				claimListFilter.setHidden(true);
				if(!me.toggleFlag)
					me.onSlideInSlideOutRequest();
				Ext.Viewport.setMasked(false);
				return;
			}
			 
			if(claimType!=0)
			{
				claimQueryString += '&claimtype='+claimType;
			}
	
			if(statusSelected!=0)
			{
				claimQueryString += '&status='+statusSelected;
			}
			
			if(dateSelected.length)
			{
				claimQueryString += '&dtstart='+me.startDate;
				claimQueryString += '&dtend='+me.endDate;
			}
			
			me.claimFilteredList(claimQueryString);
			
			if(claimListViewObj.down('#searchField').getValue().length)
			{
				claimListViewObj.down('#searchField').reset();
			}
		}
	},
	setClaimListFillterValues: function(claimsResponse)
	{

	 	var dataArrClaimType = Array(),
		 	dataArrDate = Array(),
		 	dataArrStatus = Array();
		
		var claimListViewObj=this.getClaimListView(); // get the page object

		var obj1=claimListViewObj.down('#selectClaimType');
		var obj2=claimListViewObj.down('#selectDate');
		var obj3=claimListViewObj.down('#selectStatus');
		
		obj1.getData().initdata = true;
		obj2.getData().initdata = true;
		obj3.getData().initdata = true;
		
		var claimListAll = claimListViewObj.down('#claimlisting_page');
		var claimListFilter = claimListViewObj.down('#claimListingFilter');
		
		claimListFilter.setHidden(true);
		claimListAll.setHidden(false);
		
		var finalJSONClaimType = Array();
		finalJSONClaimType[0]={text:'Please select',value:0}
		Ext.Array.each(claimsResponse.result.ClaimTypes.ClaimType,function(obj,index){
			finalJSONClaimType[index+1] = {
				text: obj.Description,value: obj.Id
			}
		});
		obj1.setOptions(null);
		obj1.setUsePicker(false);
		obj1.setOptions(finalJSONClaimType);
		obj1.getData().initdata = false;
	
		obj2.setValue("");
		obj2.getData().initdata = false;
		 
		var finalJSONStatus = Array();
		finalJSONStatus[0]={text:'Please select',value:0};
		Ext.Array.each(claimsResponse.result.ExpenseStatuses.ExpenseStatus,function(obj,index){
			finalJSONStatus[index+1] = {
				text: obj.Description,value: obj.Id
			}
		});
		
		obj3.setOptions(null);
		obj3.setUsePicker(false);
		obj3.setOptions(finalJSONStatus);
		obj3.getData().initdata = false;
		
	},
 	listingControlInit: function (_this, eOpts ){
		Logger.info("listingControlInit function ");
		this.setFilter = false;
	 	var claimListViewObj=this.getClaimListView(); // get the page object 
		var objListing=this.getListingControl();
		objListing.setEmptyText('');
		// show loader.
		claimListViewObj.setMasked({xtype: 'loadmask'});
	 	var me=this;
		this.toggleFlag = false;
		this.onSlideInSlideOutRequest();
	 	var userStoreObj = Ext.getStore('User_S');
	 	var PackageId=userStoreObj.getAt(0).get('PackageId');
	 	if(Ext.getStore('ClaimList')){
	 		Ext.getStore('ClaimList').destroy();
	 		objListing.setStore(null); 
	 	}
	 	 
		var objClaimListStore=Ext.create("Ext.data.Store",{
		 	  storeId: "ClaimList",
		      model: "SmartSalary.model.ClaimList_M",
		      autoLoad:true,
		      pageSize: 5, 
		      proxy:
		      {
		        type: 'ajax',
		        url : Config.ApiBaseUrl+'api/claims/'+PackageId,
				headers:{"Authorization" :"Basic " + Shared.base64variable},
		        reader: {
		          type: 'json', 
		          rootProperty:'result.ClaimOnlineList.ClaimOnline',
				  totalProperty: 'result.TotalClaims'
		        }				
		      },
			  listeners: {
			     load: function (store,records,success,operation,eOpts) {
			        if(success){
			        	/*setFilter is a flag used to get status and claim list for filters, first time only
						* besause load method call every time when pager request works
						*/
						if(!(me.setFilter))
						{
							var claimsResponse = JSON.parse(operation.getResponse().responseText);
							me.setClaimListFillterValues(claimsResponse);
							me.setFilter = true;
						}
						Logger.info("ClaimList store loaded"); 
					}else{
						Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
					}
					claimListViewObj.setMasked(false);
			     }
			  } 
		}); 
		me.setListStoreAfterLoad(); 
			
	    // setting the admin message
	 	var TrackMyClaimsDescription=Ext.getStore("ClaimsStore").getData().getAt(0).get('result').TrackMyClaimsDescription
	    claimListViewObj.down("#claimListMsgId").setHtml(TrackMyClaimsDescription);
		
 	},
 	setListStoreAfterLoad:function (){

 		var claimListViewObj=this.getClaimListView(); // get the page object 

		var objListing=claimListViewObj.down("#claimlisting_page");
 		objListing.setStore(Ext.getStore("ClaimList"));

	 	if(!objListing.getPlugins() && Ext.getStore('ClaimList').getCount()==0){
		 	objListing.setPlugins({
                xclass : 'Ext.plugin.ListPaging',
                autoPaging : true,
				loadMoreText: '<span class="font-tablet-loadMore">Load More...</span>',
				noMoreRecordsText: 'End of list',
                id:'pluginId'
            });
		 	if(!this.toggleFlag)
				this.onSlideInSlideOutRequest();
		}
		if(Ext.getStore('ClaimList').getCount()==0){
			var myObj=Ext.getCmp('pluginId');
		    myObj.getLoadMoreCmp().hide(); 
		}
 	},
 	clearFilterForSearchBtn: function(){

 		var claimListViewObj=this.getClaimListView(); // get the page object 
		var obj1=claimListViewObj.down("#selectClaimType");
		var obj2=claimListViewObj.down("#selectDate");
		var obj3=claimListViewObj.down("#selectStatus");
		
		obj1.getData().initdata = true;
		obj2.getData().initdata = true;
		obj3.getData().initdata = true;
		
		obj1.reset();
		obj2.reset();
		obj3.setValue('');
		
		obj1.getData().initdata = false;
		obj2.getData().initdata = false;
		obj3.getData().initdata = false;
	},
	searchBtnClick:function (){

		var claimListViewObj=this.getClaimListView(); // get the page object 
		var userStoreObj = Ext.getStore('User_S');
		var PackageId = userStoreObj.getAt(0).get('PackageId');
		
		var keyword=claimListViewObj.down("#searchField").getValue();
	    if(keyword=="")
			return;
			
		this.clearFilterForSearchBtn();	
		Ext.Viewport.setMasked(true);
	    
		var claimQueryString = Config.ApiBaseUrl+'api/claims/'+PackageId+'?filter=1';
		claimQueryString += '&claimid='+keyword;
		
		this.claimFilteredList(claimQueryString);
	},
 	searchFieldCancel:function (){

 		var claimListViewObj=this.getClaimListView(); // get the page object 
		var claimListFilter = claimListViewObj.down('#claimListingFilter');
		var claimListAll = claimListViewObj.down('#claimlisting_page');
	    claimListAll.setStore('ClaimList').refresh(); 
	    
		var claimType = claimListViewObj.down('#selectClaimType').getValue();
		var dateSelected = claimListViewObj.down('#selectDate').getValue();
		var statusSelected = claimListViewObj.down('#selectStatus').getValue();
		
		if(claimType==0 && !(dateSelected.length>0) &&  statusSelected==0)
		{
			claimListAll.setHidden(false);
			claimListFilter.setHidden(true);
		}
	},
	dateFieldCancel:function (_this){ 
		_this.setValue('');
	},
    onSlideInSlideOutRequest : function(button, pressed) {
    
		var claimListViewObj=this.getClaimListView(); // get the page object 

	    if(this.toggleFlag){     
	     	claimListViewObj.down("#filterSection").show();
			claimListViewObj.down("#toggleBtn").removeCls("arrow-up")
			claimListViewObj.down("#toggleBtn").addCls("arrow-down");
			this.toggleFlag = false;
	    }
	    else {
			claimListViewObj.down("#filterSection").hide();
			claimListViewObj.down("#toggleBtn").removeCls("arrow-down")
			claimListViewObj.down("#toggleBtn").addCls("arrow-up");
			this.toggleFlag = true;
	    }
    },
	trackMyclaimSearchInfo:function(){ 
		var claimListViewObj=this.getClaimListView(); // get the page object 
		var adminMsg= claimListViewObj.down("#claimListMsgId").getHtml(); 
		var overlay = Ext.Viewport.add({
			xtype:'panel',
			cls:'trackMyClaimAdminMsg',
			centered:true,
			showAnimation:{	type:'popIn',duration:250,easing: 'ease-out'},
			hideAnimation : {type: 'popOut', duration: 250, easing: 'ease-out'},
			width:'300px',
			hideOnMaskTap:true,
			styleHtmlContent:true,
			modal:true, 
			height:'200px',
			html:adminMsg,
			items:[{
				docked:'top',
				xtype:'toolbar',
				title:'Info'
			}],
		});
		overlay.show()
	}
});