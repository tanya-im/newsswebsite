Ext.define('SmartSalary.controller.SwipeContainerView_C',{
	extend: 'Ext.app.Controller',    
	config:{
		refs:{
			swipeContainerViewPageId: 'container[itemId="swipeContainerViewPageId"]',
			main : 'container[itemId="viewHome"]',
			navigation : 'container[itemId="basic"]',
			appHeader : 'toolbar[itemId="headerToolBar"]',
			menuToggle: 'image[itemId="menuToggle"]',
			mailButton:'image[itemId="mailButton"]',
			searchTextField:'textfield[itemId="txtSearchField"]',
			searchButton:'button[itemId="btnSearchField"]',
			transparentContainer:'container[itemId="transparentContainer"]'
		},
		control:{
			appHeader :	{initialize : 'swipeToolbar'},
			navigation : {itemtap :'SlideNavigationListTapped'},
			menuToggle:{tap:'menuToggle'},
			mailButton:{tap:'mailButtonTapped'},
			searchButton:{tap:'searchButtonClicked'},
			transparentContainer : {initialize : 'containerTap'}
		}
	},
	SlideNavigationListTapped : function(list1, list, index, target, record, e){
		var contentText = list.getStore().getAt(index).get('text');
		var contentMenuid = list.getStore().getAt(index).get('menuid');
		var contentleaf = list.getStore().getAt(index).get('leaf');
		var contentUniqueId = list.getStore().getAt(index).get('uniqueId');
		
		switch(contentUniqueId){

			case MenuConfig.home:
				//this.getApplication().getController('Dashboard_C').showDashboard();
				Shared.pushNewView('dashboardscreen','dashboardView','left');
				this.toggleNav();
			break;
			case MenuConfig.overview:
				this.getApplication().getController('VehicleLeasing_C').showOverviewPage();
				this.toggleNav();
				break;
			case MenuConfig.manageBudgets:
				this.getApplication().getController('VehicleLeasing_C').showManageBudgetsShow();
				this.toggleNav();
				break;
			case MenuConfig.leasingDetails:
				this.getApplication().getController('VehicleLeasing_C').showLeasingDetailPage();
				this.toggleNav();
				break;
			case MenuConfig.manageFuelCards:
				this.getApplication().getController('VehicleLeasing_C').showFuelCards();
				this.toggleNav();
				break;
			case MenuConfig.leasingHistory:
				this.getApplication().getController('VehicleLeasing_C').showLeasingHistory();
				this.toggleNav();
				break;
			case MenuConfig.myProducts:
				this.getApplication().getController('VehicleLeasing_C').showMyProductsListing();
				this.toggleNav();
				break;
			case MenuConfig.availableProducts:
				this.getApplication().getController('VehicleLeasing_C').availableproductlistingFrmDashboard();
				this.toggleNav();
				break;


    		case MenuConfig.myPersonalDetails:			this.getApplication().getController('VehicleLeasing_C').showPersonalDetail();
				this.toggleNav();
			break;
			case -1:
				if(Ext.os.is.Phone){
					this.getApplication().getController('phone.Dashboard_C').onSignOffCommand();
				}else if(Ext.os.is.Tablet){
					this.getApplication().getController('tablet.Dashboard_C').onSignOffCommand();
				}
				this.toggleNav();
			break;
			 
			case 'My Employer List':
				if(Ext.os.is.Phone){
					this.getApplication().getController('phone.Dashboard_C').showEmployerList();
				}else if(Ext.os.is.Tablet){
					this.getApplication().getController('tablet.Dashboard_C').showEmployerList();
				}
				this.toggleNav();
			break;
			
			default:
				if(contentMenuid > 0){
					if(Ext.os.is.Phone){
						this.getApplication().getController('phone.Dashboard_C').contentDetailPage(contentMenuid);
					}else if(Ext.os.is.Tablet){
						this.getApplication().getController('tablet.Dashboard_C').contentDetailPage(contentMenuid);
					}
				}
				if(contentleaf)
					this.toggleNav();
			break;
		}
	},
	swipeToolbar: function(img){
		var me = this,
		mainEl = me.getMain().element;
		img.element.on({
			swipe: function(e, node, options){
				if(e.direction == "left") {
					if (!mainEl.hasCls('in')) {
		            	mainEl.removeCls('out').addCls('in');  
						me.getTransparentContainer().setHidden(true);			
				    }
				}else if(e.direction == "right"){
					if (!mainEl.hasCls('out')) {
						mainEl.removeCls('in').addCls('out');  
						me.getTransparentContainer().setHidden(false);
					}                    
				}
			},
		    tap:function(e, node, options){
				if (!mainEl.hasCls('in')&&mainEl.hasCls('out')) {
		        	mainEl.removeCls('out').addCls('in');  
					me.getTransparentContainer().setHidden(true);		
				}
			}
		});
	},
	containerTap:function(container){
		var me=this;
		container.element.on({
	    	tap:function(e, node, options){
				me.toggleNav();		
			}
		});			
	},
	/**
		* Toggle the slide navogation view
	*/
	toggleNav : function(){
		var me = this,
		mainEl = me.getMain().element;
		if (mainEl.hasCls('out')) {
			mainEl.removeCls('out').addCls('in'); 
			me.getTransparentContainer().setHidden(true);
		}else{
			mainEl.removeCls('in').addCls('out');  
			me.getTransparentContainer().setHidden(false);
		}
	},
	menuToggle:function(){
		var me = this,
		mainEl = me.getMain().element;
		if (!mainEl.hasCls('out')) {
			mainEl.removeCls('in').addCls('out');  
			me.getTransparentContainer().setHidden(false);
		} 
	},
	mailButtonTapped:function(){},
	searchButtonClicked:function(){
		var me=this;
		if(this.getSearchTextField().getValue().length>0){
		//Show loading mask.
			Ext.Viewport.setMasked(Shared.loadingMask);
			this.toggleNav();
			var searchListStore=Ext.getStore('storeSearchList');
			var usersStore = Ext.getStore('User_S');
			// Check dataconnection is online or not if not then return false.
			if(!Shared.checkConnectionIsOnline())
				return false;
			//Load dynamic menu for user.
			var searchListStoreURL = Config.ApiBaseUrl+'api/search/'+ this.getSearchTextField().getValue();
			//Api endpoint url. 
			
			if(usersStore.getCount()>0){
				// Get employer code from store to get employer content.
				var employerCode = usersStore.getAt(0).get('EmployerCode');
				 searchListStoreURL = Config.ApiBaseUrl+'api/search/'+ this.getSearchTextField().getValue() +'/'+employerCode;
			}
						
			searchListStore.load({
				url:searchListStoreURL,
				callback: function(records, operation, success) {
					if(success){
						if(searchListStore.getCount()<1){
								Logger.alert(Messages.AlertMsg,Messages.AlertMsgNoRecordFound);
						}else{
							//Active the next viwe to user.
							Shared.pushNewView('searchListView','searchListView');
						}
						// Hide loader......
						Ext.Viewport.setMasked(false);
					}else{
						Logger.alert(Messages.AlertMsgMessage,Messages.AlertMsgStoreDataNotLoaded);
						Ext.Viewport.setMasked(false);
					}
				}
			});
			this.getSearchTextField().setValue('');
			}else{
				Logger.alert(Messages.AlertMsg,Messages.AlertMsgEnetrSearchKey);
			}
		}
		
});