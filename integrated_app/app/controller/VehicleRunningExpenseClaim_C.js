Ext.define('SmartSalary.controller.VehicleRunningExpenseClaim_C', {
    extend: 'Ext.app.Controller',
	count:null,
    config: {
        refs: {
			vClaimRegListBackBtn : 'button[action=vClaimRegListBackBtn]',
		/* Step one view element reference. */
			stepOneView: 'container[itemId=vehicleRunningExpenseClaimStepOnePageId]',
		  	claimStepOneCancelButton: 'button[action=claimStepOneCancelButtonPress]',
			claimStepOneNextButton: 'button[action=claimStepOneNextButtonPress]',
			claimAmount:'numberfield[itemId=Amount]',
			claimGST:'numberfield[itemId=GST]',
			claimSubTotal:'textfield[itemId=Subtotal]',
		/* Step two view element reference.	*/
			
			stepTwoView: 'container[itemId=vehicleRunningExpenseClaimStepTwoPageId]',
			saveContinueButton : 'button[action=saveContinueButtonPress]',
			claimStepTwoNextButton: 'button[action=claimStepTwoNextButtonPress]',
			claimStepTwoBackButtonPress: 'button[action=claimStepTwoBackButtonPress]',
			claimStepTwoCancelButtonPress: 'button[action=claimStepTwoCancelButtonPress]',
			vClaimStepTwoBackBtnPress: 'button[action=vClaimStepTwoBackBtn]',
		/*	Step three view element reference*/	
			vehicleRunningExpenseClaimStepThreePageId : 'container[itemId=vehicleRunningExpenseClaimStepThreePageId]',
			claimStepThreeBackButton: 'button[action=claimStepThreeBackButtonPress]',
			claimStepThreeNextButton : 'button[action=claimStepThreeNextButtonPress]',
			claimStepThreeCancelButton: 'button[action=claimStepThreeCancelButtonPress]',
			saveAddAnotherExpenseButton: 'button[action=SaveAddAnotherExpensePress]',
			vClaimStepThreeBackBtnPress: 'button[action=vClaimStepThreeBackBtn]',
		/*	Step four view element reference*/	
		
			stepFourView : 'container[itemId=vehicleRunningExpenseClaimStepFourPageId]',
			claimStepFourBackButtonPress: 'button[action=claimStepFourBackButtonPress]',
			claimStepFourSubmitButtonPress : 'button[action=claimStepFourSubmitButtonPress]',
			claimStepFourCancelButtonPress: 'button[action=claimStepFourCancelButtonPress]',
			vClaimStepFourBackBtnPress: 'button[action=vClaimStepFourBackBtn]',

		/* Step five view element reference. */	
		
			stepFiveView : 'container[itemId=vehicleRunningExpenseClaimStepFivePageId]',
			returnToYourClaimsPress: 'button[action=returnToYourClaimsPress]',
			trackMyClaimsBtn: 'button[action=trackTheProgressOfYourClaimPress]'
		},
        control: {
			vClaimRegListBackBtn:{ tap:'returnToYourClaim'},
		/* Step one view element reference. */ 
			stepOneView:{
				show:'stepOneViewShowEvent'
			},
			
			claimStepOneNextButton:{
				tap:'goToStepThree'
			},
			claimStepOneCancelButton:{tap:'goToDashboard'},
		/* Step two view element reference. */
			saveContinueButton:{tap:'saveEditClaim'},	
			claimStepTwoNextButton:{tap:'SaveClaimExpense'},
			claimStepTwoBackButtonPress:{tap:'goToStepOne'},
			claimStepTwoCancelButtonPress:{tap:'goToDashboard'},
			vClaimStepTwoBackBtnPress:{tap:'goToStepOne'},	
		/*	Step three view element reference*/	
			vehicleRunningExpenseClaimStepThreePageId:{show:'vehicleRunningExpenseClaimStepThreeViewLoad'},
			claimStepThreeBackButton: {tap:'goToStepOne'},
			claimStepThreeNextButton : {tap:'goToStepFour'},
			claimStepThreeCancelButton: {tap:'goToDashboard'},
			saveAddAnotherExpenseButton: {tap:'goToStepOne'},
			vClaimStepThreeBackBtnPress: {tap:'goToStepOne'},	
		/*	Step four view element reference*/	
			claimStepFourBackButtonPress: {tap:'goToStepThree'},
			claimStepFourSubmitButtonPress : {tap:'submitClaim'},
			claimStepFourCancelButtonPress : {tap:'goToDashboard'},
			vClaimStepFourBackBtnPress: {tap:'goToStepThree'},
		/* Step five methods. */	
			returnToYourClaimsPress:{tap:'returnToYourClaim'},
			trackMyClaimsBtn:{tap:'goToTrackMyClaims'}
		}
	},
	stepOneViewShowEvent:function(){
		var objStoreLocalData=Ext.getStore('storeLocalData');
		var stepOneViewObj=this.getStepOneView();
		if(objStoreLocalData.getCount()>0)
			stepOneViewObj.down("#claimStepOneNextButton").show();
		else
			stepOneViewObj.down("#claimStepOneNextButton").hide();
	},
	goToStepTwo : function(){
		Shared.pushNewView('vehicleRunningExpenseClaimStepTwoPageId','vehicleRunningExpenseClaimStepTwo','right');	
	},
	goToStepThree : function(){
		Shared.pushNewView('vehicleRunningExpenseClaimStepThreePageId','vehicleRunningExpenseClaimStepThree','right');	
	},
	goToStepFour : function(){
		this.vehicleRunningExpenseClaimStepFourViewLoad();	
	},
	returnToYourClaim:function(){
		Shared.pushNewView('dashboardscreen','dashboardView','right');
	},
	goToVehicleList:function(){/* Push the Vehicle listing View */
		Shared.pushNewView('vehicleRegistrationListPageId','vehicleRegistrationListView');					
	},
	/* goToDashboard without parameter. Remove all claim and send user to dashboard. */
	goToDashboard:function(){
		Ext.Msg.confirm(Messages.AlertMsgConfirmation, Messages.AlertMsgLostDataMsg, function(btn){
			if (btn == 'yes'){
				Shared.pushNewView('dashboardscreen','dashboardView','right');
			}
		});		
	},
	/* claimTypeSelected fired when the claim type button is click with index id. */
	claimTypeSelected: function(selectedClaimIdWithIndex){  
	/* Show loader...... */
		var claimsResultData=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');
		var claimCount = Ext.getStore('storeLocalData').getCount();
		Logger.debug(claimCount);
		var claimLimit = claimsResultData.VehicleClaim.MaxExpenseItems;
		/*Code to check max claim submission But it will be implemented later*/
		
		/*if(claimLimit <= claimCount){
			Ext.Msg.alert(Messages.AlertMsgMessage,Messages.AlertMsgMaxClaimLimit);
			return false;
		}*/
	
		Ext.Viewport.setMasked(Shared.loadingMask);
		var selectedClaimValue = selectedClaimIdWithIndex.split("_");
		var claimid = selectedClaimValue[0];
		var claimindex =  selectedClaimValue[1];

		Shared.pushNewView('vehicleRunningExpenseClaimStepTwoPageId','vehicleRunningExpenseClaimStepTwo');
		var stepTwoViewObj=this.getStepTwoView();
		stepTwoViewObj.down("#claimStepsTwoButtonWrapper").show(); // show the navigation buttons here
	/* Show	vehicle registration No. */
		var vreglbl= stepTwoViewObj.down('#vehicleRegLabelStepTwo');
		vreglbl.setHtml('<h3>'+Messages.claimForVehicle+Shared.vehicleReg+'</h3>');
		stepTwoViewObj.down('#vClaimStepTwoBackBtn').show();

		ClaimForm.columns=(Ext.os.is.Phone)?1:2; // setting the column layout of the form

		var claimTypeInnerFormFieldWrapperObj =stepTwoViewObj.down('#vehicleRunningExpenseClaimFormfieldWrapper');
			claimTypeInnerFormFieldWrapperObj.removeAll(true,true);

		ClaimForm.initialize(claimTypeInnerFormFieldWrapperObj);
	
	/*	Check device type. If it tablet then add two inner container */
		 var hiddenFields={ 'claimindex':claimindex,
							'VehicleTransactionTypeId':claimid
		};		
		/* Get claim list data form from store.*/
		
		var claimFormFieldsData=claimsResultData.VehicleClaim.VehicleTransactionTypes.VehicleTransactionType[claimindex]
		var claimTypefields = claimFormFieldsData.VehicleTransactionTypeFields.VehicleTransactionTypeField;
		
	  
		ClaimForm.addHiddenField(hiddenFields);	 // adding the hidden fields to form
		ClaimForm.addFields(claimTypefields); // adding fields to the form 
	 
		ClaimForm.uploadControlDescriptionText=claimFormFieldsData.Description // set description text on upload control
		ClaimForm.addAttachmentField(); // add attachment field here		 
		ClaimForm.load(); //finally load the form
		
		Ext.Viewport.setMasked(false);
	},
	/* claimTypechange function end. */
	/* SaveClaimExpense with no parameter. Send claim  data to api.*/
	SaveClaimExpense:function(){
	/* Show loader...... */
		Ext.Viewport.setMasked(Shared.loadingMask);
		var me= this;
		var stepTwoViewObj=this.getStepTwoView(); // get the view object

		var frmPostedData = stepTwoViewObj.getValues();
		var objStoreLocalData=Ext.getStore('storeLocalData');
		var claimindex =  frmPostedData.claimindex;
	
	/* Get claim list form from store for validation. */ 
		var claimFormFieldsData=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');
		var claimTypefields = claimFormFieldsData.VehicleClaim.VehicleTransactionTypes.VehicleTransactionType[claimindex].VehicleTransactionTypeFields.VehicleTransactionTypeField;
	
	/* validate the form fields	*/
		if(!ClaimForm.validateClaimFields(claimTypefields,'True')){
			Ext.Viewport.setMasked(false); /*  close the loader */
			return false;
		}
	
	/* Check GST is not exceed 10% of claim amount. */	
		if(stepTwoViewObj.down('#GST')){
			var GSTcal = Shared.checkGSTNotExceedTenPercent(this.getClaimAmount().getValue(),this.getClaimGST().getValue());
			if(!GSTcal){
				Ext.Viewport.setMasked(false); /*  close the loader */
				return false;
			}
		}
		var row_id=objStoreLocalData.getAllCount()+1;
		/* create model to save submitted data to store */
		var objModelLocalData=Ext.create("SmartSalary.model.Localdata_M",{			 
            'formData':frmPostedData,
            'fileData':Shared.fileData,
            'claimindex':claimindex,
            'row_id':row_id
		});
		
		objStoreLocalData.add(objModelLocalData);
		
		Ext.Msg.alert(Messages.AlertMsgMessage,Messages.AlertMsgClaimAdded,function(){
			Ext.Viewport.setMasked(false); /*  close the loader */
	 		Shared.fileData = new Array(); /* reset the data variable  */
			Shared.pushNewView('vehicleRunningExpenseClaimStepThreePageId','vehicleRunningExpenseClaimStepThree');
		});
	},
	editClaimForm:function(val){
		// Show loader......
		Ext.Viewport.setMasked(Shared.loadingMask);
		Shared.pushNewView('vehicleRunningExpenseClaimStepTwoPageId','vehicleRunningExpenseClaimStepTwo','right');
		var stepTwoViewObj=this.getStepTwoView(); // get the view object

		stepTwoViewObj.down('#vClaimStepTwoBackBtn').hide();
		var vreglbl= stepTwoViewObj.down('#vehicleRegLabelStepTwo');
		vreglbl.setHtml('<h3>'+Messages.claimForVehicle+Shared.vehicleReg+'</h3>');
		
		var explArr=val.split("_");
 		var sortedClaimsArr=Shared.formDataAsPerClaimIndexes('storeLocalData');
		
		var dataArr=sortedClaimsArr[explArr[1]][explArr[2]].getData();
		var formData=Array(); 
		var fileData=dataArr.fileData;
		
		var _Object=dataArr.formData
		for(var name in _Object){
           formData[name] = _Object[name];
        } 
		var indexClaim=explArr[1];	

		var claimNumber=explArr[2];
		
		var claimStoreObj=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');	
		var claimType = claimStoreObj.VehicleClaim.VehicleTransactionTypes.VehicleTransactionType[indexClaim];
		 
		stepTwoViewObj.down('#claimStepsTwoButtonWrapper').hide();
		
		var claimTypeInnerFormFieldWrapperObj = stepTwoViewObj.down('#vehicleRunningExpenseClaimFormfieldWrapper');
			claimTypeInnerFormFieldWrapperObj.removeAll(true,true);
		
		var claimTypeHeader = 	Ext.create('Ext.Container',{
				cls: 'claimEditHeading font-16',
				items:[{
					xtype:'label',
					html: '<h3>'+claimType['Name']+'</h3>'
				}]
			});
		// Update claim name
		claimTypeInnerFormFieldWrapperObj.add(claimTypeHeader);	

		ClaimForm.columns=(Ext.os.is.Phone)?1:2; // setting the column layout of the form
 
		ClaimForm.initialize(claimTypeInnerFormFieldWrapperObj);

		var claimTypefields = claimType.VehicleTransactionTypeFields.VehicleTransactionTypeField;
		
		var hiddenFields={
			'claimindex':indexClaim,
			'VehicleTransactionTypeId':claimType.Id,
			'row_id':dataArr.row_id			 
		};		
			  
		ClaimForm.addHiddenField(hiddenFields);	 // adding the hidden fields to form
		
		ClaimForm.addFields(claimTypefields,formData); // adding fields to the form 
		ClaimForm.uploadControlDescriptionText=claimType.Description
		ClaimForm.addAttachmentField(fileData); // add attachment field here 
	/* Add button for the claim type.*/
		var fieldTypeButton = Ext.create('Ext.Container',{
			items: [{
				xtype: 'button',
				itemId: 'saveContinueButtonPress',
				ui: 'none',
				action: 'saveContinueButtonPress',
				flex:1,
				cls:'claimStepOneSaveAsDraftButton claimSaveButton txt-left font-16 pad-10 margin-10-0',			
				text: 'Save & Continue'
			}]
		});
		ClaimForm.filesAndButtonsWrapperObj.add(fieldTypeButton);
		
		ClaimForm.load(); //finally load the form 
		 
		/* Hide loader......*/	
		Ext.Viewport.setMasked(false);
	},
	saveEditClaim:function(){
		me =this;
		Ext.Viewport.setMasked(Shared.loadingMask);
		var stepTwoViewObj=this.getStepTwoView(); // get the view object
		/* get the values of the form */

		var frmPostedData =stepTwoViewObj.getValues(); 
		
		var claimFormFields =  Ext.getStore('ClaimsStore');

        var claimFormFieldsData =claimFormFields.getAt(0);

		var claimExpenseDetailObj=claimFormFieldsData.data.result.VehicleClaim.VehicleTransactionTypes.VehicleTransactionType[frmPostedData.claimindex];
		
		var claimFields=claimExpenseDetailObj.VehicleTransactionTypeFields.VehicleTransactionTypeField;
		
		if(!ClaimForm.validateClaimFields(claimFields,'True')) // validate the form fields
		{
			Ext.Viewport.setMasked(false); //  close the loader
			return false;
		}
		if(stepTwoViewObj.down('#GST'))//Check GST not exeed 10% of claim value. 
		{
			var GSTcal = Shared.checkGSTNotExceedTenPercent(this.getClaimAmount().getValue(),this.getClaimGST().getValue());
			if(!GSTcal){
			 	Ext.Viewport.setMasked(false); //  close the loader
			 	return false;
			}
		}
		var row_id=stepTwoViewObj.down('#row_id').getValue();
		var claimData = Ext.getStore('storeLocalData');
		claimData.filter("row_id",row_id);
		claimData.getAt(0).set({'formData':frmPostedData,'fileData':Shared.fileData}); // update the selected row in store
		Logger.debug(claimData);
		claimData.clearFilter(); // clear the filters
		Logger.debug(claimData);
		Shared.fileData=Array(); // Reset the file data array;
		Shared.pushNewView('vehicleRunningExpenseClaimStepThreePageId','vehicleRunningExpenseClaimStepThree');
		Ext.Viewport.setMasked(false);
	},
	goToStepOne:function(){
		Shared.pushNewView('vehicleRunningExpenseClaimStepOnePageId','vehicleRunningExpenseClaimStepOne','right');
 	},
	vehicleRunningExpenseClaimStepThreeViewLoad: function(){
	/* Show loader...... */
		Ext.Viewport.setMasked(Shared.loadingMask);	

		var stepThreeViewObj=this.getVehicleRunningExpenseClaimStepThreePageId(); // get the view object
	/* Show	vehicle registration No. */
		var vreglbl= stepThreeViewObj.down('#vehicleRegLabelStepThree');
		vreglbl.setHtml('<h3>'+Messages.claimForVehicle+Shared.vehicleReg+'</h3>');
		var me=this;
		
		var claimData = Ext.getStore('storeLocalData');		
		var claimType = Ext.getStore('ClaimsStore').getData().getAt(0).get('result').VehicleClaim.VehicleTransactionTypes.VehicleTransactionType;
		Logger.debug(claimData);	
		 
		var totalClaimAmountStepThree = stepThreeViewObj.down('#vTotalClaimAmountStepThree'); 
		totalClaimAmountStepThree.setHtml(Messages.AlertMsgTotalValueOfThisClaimMsg+'('+claimData.getData().length+' item)');
		 
		var grandTotal=ClaimForm.itemListing(me,"claimDetailWrapper",claimType,"vehicle"); 
		 
		var totalClaimAmountValueStepTwo = stepThreeViewObj.down('#vTotalClaimAmountValueStepThree');
		totalClaimAmountValueStepTwo.setHtml("$"+grandTotal);
		Ext.Viewport.setMasked(false);
	},
	vehicleRunningExpenseClaimStepFourViewLoad: function(){

		Shared.pushNewView('vehicleRunningExpenseClaimStepFourPageId','vehicleRunningExpenseClaimStepFour');
		
		var stepFourViewObj=this.getStepFourView(); // get the view object
		
		var ClaimsResult=Ext.getStore('ClaimsStore').getData().getAt(0).get('result');
		Logger.debug(ClaimsResult.BankAccount);
		
	/* Show loader...... */
		Ext.Viewport.setMasked(Shared.loadingMask);
		
		var vreglbl= stepFourViewObj.down('#vehicleRegLabelStepFour');
		vreglbl.setHtml('<h3>'+Messages.claimForVehicle+Shared.vehicleReg+'</h3>');
		
		var nameOnAc = stepFourViewObj.down('#nameOnAc'); 
		nameOnAc.setHtml(ClaimsResult.BankAccount.AccountName);
		
		var BSB = stepFourViewObj.down('#BSB'); 
		BSB.setHtml(ClaimsResult.BankAccount.BSB);
		
		var acNo = stepFourViewObj.down('#acNo'); 
		acNo.setHtml(ClaimsResult.BankAccount.AccountNumber);
		
		var declarationtxt = stepFourViewObj.down('#declarationtxt'); 
		declarationtxt.setHtml(ClaimsResult.DeclarationPage);
		
		// get user personal detail from user store

		var userStoreObj = Ext.getStore('User_S').getAt(0).data; //  

		var user_email = stepFourViewObj.down('#user_email'); 
		user_email.setHtml(userStoreObj.Email);

		var user_contact = stepFourViewObj.down('#user_contact'); 
		user_contact.setHtml(userStoreObj.WorkPhone+" (W)");
		
		Ext.Viewport.setMasked(false);
	},
	deleteClaimForm:function(val){
		var me=this;
		Ext.Msg.confirm(Messages.AlertMsgConfirmation, Messages.AlertMsgDeleteThisMsg, function (buttonId){
			if(buttonId=='yes'){
				var explArr=val.split("_");
				var row_id=explArr[1];
				Logger.debug(row_id);
				var claimData = Ext.getStore('storeLocalData');
				claimData.filter("row_id",row_id);
				claimData.removeAt(0);
 				claimData.clearFilter();
				if(claimData.getCount()>0){
					me.vehicleRunningExpenseClaimStepThreeViewLoad();
				}else{
					me.goToStepOne();
				}	
			}
		});
	},
	// Final submission of the claims
	submitClaim:function(){
		Logger.info("submitClaim of controller");
		var usersStore = Ext.getStore('User_S');
        var aRecord =usersStore.getAt(0);
        var PackageId=aRecord.get('PackageId');
        var submitUrl=Config.ApiBaseUrl+'api/submit_vehicleclaim/'+PackageId+'/'+Shared.VehicleID;
        ClaimForm.submitClaim(this,submitUrl,'vehicle');
         
	},
	submitClaimOld:function(){

		var stepFourViewObj=this.getStepFourView(); // get the view object
		var declarationbox = stepFourViewObj.down("#declarationbox").isChecked();
		if(!declarationbox){
			Logger.alert(Messages.AlertMsg,Messages.AlertMsgDeclarationAcceptMsg);
			return false;	
		}
		var me = this;
	/* Show loader...... */
		//Ext.Viewport.setMasked(Shared.loadingMask);
	/* Laod employee detail from store. */
		var claimFinalData= Array();
	/* get locally stored data of all the claims */
		var objStoreLocalData=Ext.getStore("storeLocalData");  
		
		var i=0;
		var formData = new FormData();
		
		objStoreLocalData.data.each(function(record, index, totalItems ){
		     
		    Ext.each(record.get('fileData'), function(value,key){
		    	 
				formData.append(i++,value); /* appending all the files to the formData */
			});	

		    claimFinalData.push(record.get('formData'));
		});
		formData.append(i,Ext.encode(claimFinalData)); 	/* Appending claim data to the formData */
		
		Logger.debug(objStoreLocalData); 
		Logger.debug(formData);
		
		
		var PackageId = Ext.getStore('User_S').getAt(0).get('PackageId');
        // Check dataconnection is online or not if not then return false.
		if(!Shared.checkConnectionIsOnline())
			return false;
		
		var progressIndicator = Ext.create("Ext.ProgressIndicator", {
			loadingText: Messages.LoadingTextMsg,
			showAnimation : 'fadeIn',
			cls:'width-90',
		});
			
		Ext.Ajax.request({ 									  
			url: Config.ApiBaseUrl+'api/submit_vehicleclaim/'+PackageId+'/'+Shared.VehicleID,
			headers:{"Authorization" :"Basic " + Shared.base64variable},
			data: formData,
			timeout:Config.serverRequsetTimeoutTime,
			progress: progressIndicator,
			type:'POST',
			success: function(data){
				data = data.responseText;
				try {
					var Response = Ext.JSON.decode(data);
				} catch (e) {
					if(data.length > 80)
					{
						data = data.slice(0,80);
						data = data+"...";
					}
					Ext.Msg.show({ 
						 title   : Messages.AlertMsgMessage, 
						 message     : data, 
						 buttons : [{ 
								  itemId : 'Retry',
								  ui     : 'action',  
								  text   : 'Retry',   
							 },{   
								  itemId : 'cancel',   
								  text   : 'Cancel'    
						 }], 
						 confirm  : {},   
							fn: function(btn) {
								if (btn == 'Retry')
									me.submitClaim();
							}  
					});
					return;
				}				
				if ( typeof(Response.ClaimId) == "undefined" || Response.ClaimId == '')
				{
					
					if(Response.Description.length > 80)
					{
						Response.Description = Response.Description.slice(0,80);
						Response.Description = Response.Description+"...";
					}
					Ext.Msg.show({ 
						 title   : Messages.AlertMsgMessage, 
						 message     : Response.Description, 
						 buttons : [{ 
								  itemId : 'Retry',
								  ui     : 'action',  
								  text   : 'Retry',   
							 },{   
								  itemId : 'cancel',   
								  text   : 'Cancel'    
						 }], 
						 confirm  : {},   
							fn: function(btn) {
								if (btn == 'Retry')
									me.submitClaim();
							}  
					});
				}
				else{
					stepFourViewObj.down("#declarationbox").uncheck();
					me.loadVehicleClaimListStepFive(Response.ClaimId);
					var claimData = Ext.getStore('storeLocalData');
					claimData.removeAll();
 				}
			},
			failure: function (response, opts) {
            	Ext.Msg.show({ 
					 title   : Messages.AlertMsgMessage, 
					 message     : Messages.AlertMsgServerSideFailureStatusCode + response.status, 
					 buttons : [{ 
							  itemId : 'Retry',
							  ui     : 'action',  
							  text   : 'Retry',   
						 },{   
							  itemId : 'cancel',   
							  text   : 'Cancel'    
					 }], 
					 confirm  : {},   
						fn: function(btn) {
							if (btn == 'Retry')
								me.submitClaim();
					}  
				});
			}
		});
	},
	loadVehicleClaimListStepFive : function(ClaimId){
		var me = this;
		Shared.pushNewView('vehicleRunningExpenseClaimStepFivePageId','vehicleRunningExpenseClaimStepFive');
		
		var stepFiveView=this.getStepFiveView();// get the view object
		
		claimrefno = stepFiveView.down('#claimrefno');
		claimrefno.setHtml(Messages.yourclaimRefNoMsg+ClaimId);
		var claimDetailStepFiveWrapperObj = stepFiveView.down('#claimSummeryWrapperFive');
		claimDetailStepFiveWrapperObj.removeAll(true,true);
		
		var sortedClaimsArr=Shared.formDataAsPerClaimIndexes();
		
		var totalClaimAmountStepFive = stepFiveView.down('#noOfClaimStepFive'); 
		totalClaimAmountStepFive.setHtml('Total value of this claim ('+Ext.getStore('storeLocalData').getData().length+' item)');
		if(sortedClaimsArr.length){	
			var totalamt=0;
			Ext.each(sortedClaimsArr,function (claim,indexClaim){
				if(Ext.isDefined(claim)){ 
					for(var i=0;i<claim.length;i++){
						var record = claim[i]; //Get the record        
						var data = record.getData(); //Get the data from the record 
						Logger.debug(data); 
						totalamt+=parseFloat(data.formData.Amount);
						var ClaimDetailStepThreeCnt = Ext.create('Ext.Container',{
							cls :'bgwhite',
							items: [{
								xtype: 'label',
								html: 'Date Paid: '+data.formData.ExpenseDate,
								cls: 'font-12 pad-right-20 pad-left-20 font-vlightgrey pad-top-10'
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'font-14 f-darkgrey pad-5 pad-right-20 pad-left-20',
								items:[{
									xtype: 'label',
									html: 'Registration cost:',
									flex:3
								},{
								xtype: 'label',
									html: '$'+data.formData.Amount,
									cls:'txt-right f-bold',
									flex:1
								}]
							},{
								xtype: 'container',
								layout:'hbox',
								cls:'font-14 f-darkgrey  pad-5 pad-right-20 pad-left-20',
								items:[{
									xtype: 'label',
									html: 'No. of documents included:',
									flex:3
								},{
									xtype: 'label',
									html: data.fileData.length,
									cls:'txt-right f-bold',
									flex:1
								}]
							}]
						});
						claimDetailStepFiveWrapperObj.add(ClaimDetailStepThreeCnt);
					}
				}
			});
			var changeAccountBtn = Ext.create('Ext.Container',{
								cls :'bgwhite font-14 color-2B2F3B clearfix',
								items:[{
									xtype: 'button',
									ui: 'none',
									cls:'claimStepOneNextButton claimStepButton txt-left font-16 pad-10 color-5A5B77',
									text: 'Change account'
								}]
					});
			claimDetailStepFiveWrapperObj.add(changeAccountBtn);
			totalamt=parseFloat(totalamt).toFixed(2);
			stepFiveView.down('#totalAmountOnStepFive').setHtml('$'+totalamt);
		}
	},
	goToTrackMyClaims: function(){
		Shared.pushNewView('claimlistscreen','claimList'); 
	} 
});