Ext.define('SmartSalary.controller.ContentDetail_C', {
    extend: 'Ext.app.Controller',
    config: {
        refs: { BackButton:'button[itemId=btnBack]'},
        control: {	BackButton:{tap:'backButtonTapped'}} },
        backButtonTapped:function(){
			//Active the next viwe to user.
		  	Shared.pushNewView('searchListView','searchListView');	
		},
        dataWrraperContainerClicked:function(event, node, options, eOpts){
			if(event.getTarget('a').rel){ 	        																								this.getApplication().getController('Dashboard_C').contentDetailPage(event.getTarget('a').rel);
			}
        }    
});