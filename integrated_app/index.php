<?php
$currdir=getcwd();

define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']."/newsswebsite") ; 

$base_url="http://".$_SERVER['HTTP_HOST']."/newsswebsite"; 

chdir(DRUPAL_ROOT); 
 
require_once "./includes/bootstrap.inc";
 
drupal_bootstrap(DRUPAL_BOOTSTRAP_SESSION); 

$sessionVariable=json_encode($_SESSION['user_packman']);
?>
<!DOCTYPE HTML>
<html manifest="" lang="en-US">
<head>
    <meta charset="UTF-8"> 
    <link rel="shortcut icon" href="resources/images/favicon.ico" type="image/vnd.microsoft.icon" />
    <link href="resources/images/favicon.ico" rel="apple-touch-icon" />
    <link href="resources/images/favicon_76.png" rel="apple-touch-icon" sizes="76x76" />
    <link href="resources/images/favicon_120.png" rel="apple-touch-icon" sizes="120x120" />
    <link href="resources/images/favicon_152.png" rel="apple-touch-icon" sizes="152x152" />
    <title>SmartSalary</title>
    <style type="text/css">
         /**
         * Example of an initial loading indicator.
         * It is recommended to keep this as minimal as possible to provide instant feedback
         * while other resources are still being loaded for the first time
         */
        html, body {
            height: 100%;
            background-color: #4D2473
        }

        #appLoadingIndicator {
            position: absolute;
            top: 50%;
            left:50%;
			margin-top: -36px;
            margin-left:-100px;
		}

        .progressbar_wrapper{
			width:200px;
		}
		
		.logo_loader{
			background:url(resources/images/logo_loader@2x.gif) no-repeat;
			background-size:183px auto;
			height:58px;
			margin-bottom:5px;
		}
		/* Defining the animation */
		
		@-webkit-keyframes progress
		{
			to {background-position: 30px 0;}
		}
		
		@-moz-keyframes progress
		{
		  to {background-position: 30px 0;}
		}
		
		@keyframes progress
		{
		  to {background-position: 30px 0;}
		}
		
		/* Set the base of our loader */
		
		.barBg {
			background:#282828;
			width:100%;
			height:9px;
			border:10px solid #282828;
			border-radius: 20px;
			-moz-border-radius: 20px;
			-webkit-border-radius: 20px;
			box-shadow: 0px 5px 17px rgba(40, 40, 40, 0.5);
			margin-bottom:30px;
		}
		
		
		.bar {
			background: #7aff32;
			height:30px;
			height: 9px;
			border-radius: 12px;
			-moz-border-radius: 12px;
			-webkit-border-radius: 10px;
		}
		
		/* Set the linear gradient tile for the animation and the playback */
		
		.barFill {
			width: 100%;
			height: 9px;
			border-radius: 20px;
			-webkit-animation: progress 1s linear infinite;
			-moz-animation: progress 1s linear infinite;
			animation: progress 1s linear infinite;
			background-repeat: repeat-x;
			background-size: 30px 30px;
			background-image: -webkit-linear-gradient(-45deg, rgba(255, 255, 255, 1) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 1) 50%, rgba(255, 255, 255, 1) 75%, transparent 75%, transparent);
			background-image: linear-gradient(-45deg, rgba(255, 255, 255, 1) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 1) 50%, rgba(255, 255, 255, 1) 75%, transparent 75%, transparent);
			border:1px solid #fff;
		}
		
		/* Here's some predefined widths to control the fill. Add these classes to the "bar" div */
		
		
		.hundred {
			width: 100%; /* Sets the progress to 100% */
		}	
		
		/* Some colour classes to get you started. Add the colour class to the "bar" div */
		
		.bluewhite_grad {
			background: #4f2684;
			background: -moz-linear-gradient(-45deg,  #4f2684 0%, #4f2684 100%);
			background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,#4f2684), color-stop(100%,#4f2684));
			background: -webkit-linear-gradient(-45deg,  #4f2684 0%,#4f2684 100%);
			background: -o-linear-gradient(-45deg,  #4f2684 0%,#4f2684 100%);
			background: -ms-linear-gradient(-45deg,  #4f2684 0%,#4f2684 100%);
			background: linear-gradient(135deg,  #4f2684 0%,#4f2684 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4f2684', endColorstr='#4f2684',GradientType=1 );
		
		}
		
		@media(-webkit-min-device-pixel-ratio: 2),(min-resolution: 192dpi) { 
			.logo_loader{
				background:url(resources/images/logo_loader@2x.gif) no-repeat center center;
				background-size:183px auto;
			}
		}
    </style>

    <!-- The line below must be kept intact for Sencha Command to build your application -->
    <script id="microloader" type="text/javascript" src="touch/microloader/development.js"></script>
</head>
<body>
    <div id="appLoadingIndicator">
        <div class="progressbar_wrapper">
            <div class="logo_loader"></div>
            <div class="bar hundred bluewhite_grad">
                <div class="barFill"></div>
            </div>
        </div>
	</div>
</body>
</html>
<script type="text/javascript">
var userData=<?php echo $sessionVariable;?>;
var authheader="<?php echo $_SESSION['authheader'];?>"; 
var access_token=<?php echo json_encode($_SESSION['access_token']);?> 
</script>
<script type="text/javascript" src="resources/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="resources/js/jquery.atooltip.js"></script> 
<script type="text/javascript">

function applyTooltip(){ 
	$('div.normalTip').aToolTip();
};	

var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel";

document.addEventListener(mousewheelevt, function (e) {
	var el = e.target;
	var results = [];
	while (el !== document.body) {
		if (el && el.className && el.className.indexOf('x-container') >= 0) {
			var cmp = Ext.getCmp(el.id);
			if (cmp &&
				typeof cmp.getScrollable == 'function' &&
				cmp.getScrollable()) {
				var scroller = cmp.getScrollable().getScroller();
			if (scroller) {
				var scrollValue = e.detail? e.detail*(-18) : (e.wheelDelta* 0.5);
				var offset = {
					x: 0,
					y: -scrollValue
				};
				scroller.fireEvent('scrollstart',
					scroller,
					scroller.position.x,
					scroller.position.y,
					e);
				scroller.scrollBy(offset.x, offset.y);
				scroller.snapToBoundary();
				scroller.fireEvent('scrollend',
					scroller,
					scroller.position.x,
					scroller.position.y - offset.y);
				break;
			}
		}
	}
	results.push(el = el.parentNode);
}
return results;
}, false);
</script>
<script>

	function AnimateVehicle(id){
		var textWid=$("#"+id+" > span.inner").width();
		$("#"+id+" > span.inner").show().width(0).animate({width: textWid},
			{
			  	step: function( now, fx ) {
				    //console.log(now+"fx:"+fx+"w:"+$("#"+id+" > span.outer").width())
				    if(now>$("#"+id+" > span.outer").width()){
				    	$("#"+id+" .greenColor").show();
				    	$("#"+id+" .inner").addClass('redColor'); 
				    }
				    if($("#"+id+" .greenColor").width()<$("#"+id+" .inner").width()){
				    	$("#"+id+" .greenColor").show();
				    	$("#"+id+" .inner").addClass('redColor');
				    }	
					$("#"+id+" .CarAnimate").width(now);
				    $("#"+id+" .TravelledLabel").width(now);
			  	},
			  	duration :3000
		});
	}
	function AnimateBarGraph(id,cls){
		var textWid=$("#"+id+" > span.inner-bar").width();
		$("#"+id+" > span.inner-bar").show().width(0).animate({width: textWid},{
			step: function( now, fx ) {
			    if(now>$("#"+id+" > span.outer").width()){
			    	$("#"+id+" ."+cls).show();
			    	$("#"+id+" .inner-bar").addClass('redColor'); 
			    } 
			    if($("#"+id+" ."+cls).width()<$("#"+id+" .inner-bar").width()){
				   	$("#"+id+" ."+cls).show();
				   	$("#"+id+" .inner-bar").addClass('redColor');
				}	 
			},duration :3000
		});
	}
	
	function animatePasswordStrength(passwordStrengthWidth){
		$("#passwordStrenghtIndicator").show().width(0).animate({width: passwordStrengthWidth},
			{
			  	step: function( now, fx ) {				   	
					$("#passwordStrenghtIndicator").width(now);
			  	},
			  	duration :3000
		});
	}
</script>